<?php

error_reporting(E_ERROR);
ini_set('display_errors', 1);

#$ROOT_URL = "http://localhost";
$ROOT_URL = "https://heroes3.cz";
$ROOT_DIR = $_SERVER['DOCUMENT_ROOT'];
                  
$INCLUDE_DIR = $ROOT_DIR.'/_include';

// nastaveni session
ini_set('arg_separator.output', '&amp;');
ini_set( 'url_rewriter.tags' , 'a=href,area=href,frame=src,input=src,fieldset=' );
ini_set('session.use_cookies', '1');
ini_set('session.use_only_cookies', '1');
ini_set('session.use_trans_sid', '0');
ini_set('session.cookie_lifetime','0');
ini_set("session.cache_expire", '0');  

ob_start();
session_start();

# include all
# pozor na poradi!
include $INCLUDE_DIR.'/define.php';
include $INCLUDE_DIR.'/sql.php';
include $INCLUDE_DIR.'/network.php';
include $INCLUDE_DIR.'/redirect.php';
include $INCLUDE_DIR.'/ban.php';
include $INCLUDE_DIR.'/html.php';
include $INCLUDE_DIR.'/html_start_end.php';
include $INCLUDE_DIR.'/email.php';
include $INCLUDE_DIR.'/date-time.php';
include $INCLUDE_DIR.'/funkce.php';
include $ROOT_DIR.'/hry/html_hra.php';
require $INCLUDE_DIR.'/pavouk.php';


db_open();

// kontrola, jestli neni ban

ban_test();

// prihlaseni
include $INCLUDE_DIR.'/login_logout.php';

?>
