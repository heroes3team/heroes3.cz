<?php
/**
 * Smazani neaktivnich hracu, 1x denne
 **/

include('../_common_start.php');

$odmitnuti_text = "Hra odmítnuta poraženým hráčem automaticky z důvodu dlouhé neaktivity.";

$deadline = strtotime('-1 MONTH');
$deadline = date('Y-m-d H:i:s', $deadline);
$sql = sql_query(sprintf("SELECT id FROM hry WHERE vytvoreno<'%s' AND souhlas_porazeneho IS NULL", $deadline));

while($row = sql_fetch_array($sql)) {
	sql_begin();
	$id = $row['id'];
	$sql1 = sql_query(sprintf("UPDATE hry SET souhlas_porazeneho=FALSE WHERE id=%d", $id));
	$sql2 = sql_query(sprintf("INSERT INTO hry_komentare (vlozil, hra, text) VALUES (%d, %d, '%s')", $SYSTEM_ID, $id, $odmitnuti_text));
	if($sql1 && $sql2) {
		sql_commit();
	} else {
		sql_rollback();
	}
}
update_pocet_prispevku($SYSTEM_ID);

?>
