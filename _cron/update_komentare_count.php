<?php

require("../_common_start.php");

sql_query(sprintf("UPDATE hraci SET prispevku=0"));

$sum = array();

$sql = sql_query("SELECT vlozil, COUNT(*) as count FROM forum_prispevky GROUP BY vlozil;");

while($row = sql_fetch_array($sql)) {
 $sum[$row['vlozil']] = $row['count'];  
}

$sql = sql_query("SELECT vlozil, COUNT(*) as count FROM hry_komentare GROUP BY vlozil;");

while($row = sql_fetch_array($sql)) {
 $sum[$row['vlozil']] += $row['count'];  
}

foreach($sum as $h => $count) {
  sql_query(sprintf("UPDATE hraci SET prispevku=%d WHERE id=%d", $count, $h));
}

echo "done.";

?>