<?php

require('../_common_start.php');

$m = intval(Date('m'));
switch($m) {
  case 1:
  case 2:
  case 3:
    $deadline = (intval(date("Y"))-1)."-12-31";
    $obdobi = "Q4 ".(intval(date("Y"))-1);
    $next = date("Y-03-31");
  break;
  case 4:
  case 5:
  case 6:
    $deadline = date("Y-03-31");
    $obdobi = "Q1 ".date("Y");
    $next = date("Y-06-30");
  break;
  case 7:
  case 8:
  case 9:
    $deadline = date("Y-06-30");
    $obdobi = "Q2 ".date("Y");
    $next = date("Y-09-30");
  break;
  case 10:
  case 11:
  case 12:
    $deadline = date("Y-09-30");
    $obdobi = "Q3 ".date("Y");
    $next = date("Y-12-31");
  break;
  default:
    die("chyba v urceni mesice");  
}

# zkontroluj jestli uzaverka uz existuje
$row = sql_one_row(sprintf("SELECT * FROM uzaverky WHERE cas >='%s'", $deadline.' 00:00:00'));
if(!empty($row)) {
  echo "Jiz byla provedena uzaverka za obdobi ".$obdobi;
  return;
} else {
  echo "provadime uzaverku za obdobi ".$obdobi;
}


$sql = sql_query("SELECT * FROM skupiny ORDER BY id");
  
$skupiny = array(); 
while($row = sql_fetch_array($sql)) {
 $skupiny[$row['id']] = $row;
}

# nejdrive spocitame prestupy
$prestupy = array();
foreach($skupiny as $id => $skupina) {
 
  if($id==1) continue;

  $postup = array();
  $sestup = array();
  
  if($id == 2) {
    $sql = sql_query(sprintf("SELECT * FROM hraci WHERE skupina=1 OR skupina=2 ORDER BY body DESC, elo DESC, id DESC LIMIT %d", $skupina['postupuje']));
    while($row = sql_fetch_array($sql)) {
      $postup[] = $row;      
    }
    if(isset($skupiny[$id+1])) {
      $sql = sql_query(sprintf("SELECT * FROM hraci WHERE skupina=1 OR skupina=2 ORDER BY body, elo, id LIMIT %d", $skupiny[$id+1]['postupuje']));
      while($row = sql_fetch_array($sql)) {
        $sestup[] = $row;      
      }    
    }
  } else {
    $sql = sql_query(sprintf("SELECT * FROM hraci WHERE skupina=%d ORDER BY body DESC, elo DESC, id DESC LIMIT %d", $id, $skupina['postupuje']));
    while($row = sql_fetch_array($sql)) {
      $postup[] = $row;      
    }
    $sql = sql_query(sprintf("SELECT * FROM hraci WHERE skupina=%d ORDER BY body, elo, id LIMIT %d", $id, $skupiny[$id+1]['postupuje']));
    while($row = sql_fetch_array($sql)) {
      $sestup[] = $row;      
    }
  }

  $prestup = array('postup' => $postup, 'sestup' => $sestup);
  $prestupy[$id] = $prestup;
}

$text = "";
$text .= "======== AUTOMATICKÉ SDĚLENÍ ======== \n";
$text .= "===== UZÁVĚRKA ZA OBDOBÍ ".$obdobi." ======\n\n";

foreach($skupiny as $id => $skupina) {

  if($id == 1) continue;
  
  
  if($id == 2) {
    
    if($prestupy[2]['postup'][0]['skupina']==1) {
      $text .= "Post Šampiona obhajuje: ".$prestupy[2]['postup'][0]['jmeno']." | body: ".$prestupy[2]['postup'][0]['body']." elo: ".$prestupy[2]['postup'][0]['elo']."\n\n";  
    } else {
      $text .= "Novým Šampionem se stává: ".$prestupy[2]['postup'][0]['jmeno']." | body: ".$prestupy[2]['postup'][0]['body']." elo: ".$prestupy[2]['postup'][0]['elo']."\n\n";      
    }
    
    $text .= $skupina['nazev'].":\n\n";
    
    $text .= "Do skupiny ".$skupiny[$id+1]['nazev']." sestupují:\n";
    foreach($prestupy[$id]['sestup'] as $h) {
      $text .= $h['jmeno']." | body: ".$h['body']." elo: ".$h['elo']."\n";
    }
    $text .= "\n";
    
  } else {
      $text .= $skupina['nazev'].":\n";
      
      $text .= "Do skupiny ".$skupiny[$id-1]['nazev']." postupují:\n";
      foreach($prestupy[$id]['postup'] as $h) {
        $text .= $h['jmeno']." | body: ".$h['body']." elo: ".$h['elo']."\n";
      }
      $text .= "\n";
      
      if(isset($skupiny[$id+1])) {
        $text .= "Do skupiny ".$skupiny[$id+1]['nazev']." sestupují:\n";
        foreach($prestupy[$id]['sestup'] as $h) {
          $text .= $h['jmeno']." | body: ".$h['body']." elo: ".$h['elo']."\n";
        }
        $text .= "\n";
      }      
  } 
}

sql_begin();

# presun hracu v DB
foreach($skupiny as $id => $skupina) {

  if($id == 1) continue;
  
  
  if($id == 2) {
    
    # automaticke degradovani predchoziho sampiona	
    $q0 = sql_query("UPDATE hraci SET skupina=2 WHERE skupina=1");
    $q1 = sql_query(sprintf("UPDATE hraci SET skupina=1 WHERE id=%d", $prestupy[2]['postup'][0]['id']));

    foreach($prestupy[$id]['sestup'] as $h) {
      $q2 = sql_query(sprintf("UPDATE hraci SET skupina=3 WHERE id=%d", $h['id']));
    }

    
  } else {
    foreach($prestupy[$id]['postup'] as $h) {
      $q3 = sql_query(sprintf("UPDATE hraci SET skupina=%d WHERE id=%d", $id-1, $h['id']));
    }
    
    if(isset($skupiny[$id+1])) {
      foreach($prestupy[$id]['sestup'] as $h) {
        $q4 = sql_query(sprintf("UPDATE hraci SET skupina=%d WHERE id=%d", $id+1, $h['id']));
      }
    }      
  } 
}

# vynulovani bodu
$q5 = sql_query("UPDATE hraci SET body=0");

# medaile pro sampiona
$q6 = sql_query(sprintf("INSERT INTO hraci_medaile (udelil, hrac, medaile, popis) VALUES (%d, %d, '%s', '%s')", $SYSTEM_ID, $prestupy[2]['postup'][0]['id'], 'sampion', $obdobi));

# info do sdeleni
$q7 = sql_query(sprintf("INSERT into forum_prispevky (vlozil, vlakno, text) VALUES (%d, %d, '%s')", $SYSTEM_ID, 7, sql_string($text)));

#spunt
$q8 = sql_query("INSERT into uzaverky () VALUES();");

if($q0 && $q1 && $q2 && $q3 && $q4 && $q5 && $q6 && $q7 && $q8) {
  sql_commit();
  echo "done.";
} else {
  sql_rollback();
  echo "rollback :-(";
}

?>
