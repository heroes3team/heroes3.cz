BEGIN;

CREATE TABLE skupiny (
  id int(11) NOT NULL auto_increment,
  nazev varchar(20),
  kapacita  int(11) NOT NULL DEFAULT 0,
  postupuje int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY skupiny_pk  (id),
  UNIQUE KEY skupiny_uk1 (nazev)
);

INSERT INTO skupiny (id, nazev, kapacita, postupuje) VALUES(1, 'Šampion', 1, -1);
INSERT INTO skupiny (id, nazev, kapacita, postupuje) VALUES(2, 'Elitní 4', 4, 1);
INSERT INTO skupiny (id, nazev, kapacita, postupuje) VALUES(3, 'Top 10', 5, 1);
INSERT INTO skupiny (id, nazev, kapacita, postupuje) VALUES(4, '1. liga', 20, 2);
INSERT INTO skupiny (id, nazev, kapacita, postupuje) VALUES(5, '2. liga', 50, 3);
INSERT INTO skupiny (id, nazev, kapacita, postupuje) VALUES(6, '3. liga', 100, 4);
INSERT INTO skupiny (id, nazev, kapacita, postupuje) VALUES(7, 'Pralesní liga', -1, 5);

CREATE TABLE hraci (
  id int(11) NOT NULL auto_increment,
  registrace_cas datetime,
  registrace_mail  varchar(50),
  registrace_ip varchar(39),
  jmeno varchar(15) NOT NULL,
  heslo varchar(50) NOT NULL,
  posledni_prihlaseni timestamp DEFAULT CURRENT_TIMESTAMP,
  vyhranych_her int(11) NOT NULL DEFAULT 0,
  celkem_her int(11) NOT NULL DEFAULT 0,
  uspesnost float NOT NULL DEFAULT 0,
  elo int(11) NOT NULL DEFAULT 0,
  skupina int(11) NOT NULL DEFAULT 7,
  body int(11) NOT NULL DEFAULT 0,
  ban bool NOT NULL DEFAULT FALSE,
  aktivni bool NOT NULL DEFAULT FALSE,
  admin bool NOT NULL DEFAULT FALSE,
  moderator bool NOT NULL DEFAULT FALSE,
  rozhodci bool NOT NULL DEFAULT FALSE,
  email varchar(50),
  icq varchar(11),
  hamachi varchar(15),
  gameranger varchar (15),
  popis text,
  prispevku int(11) NOT NULL DEFAULT 0,
  karma int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY  hraci_pk (id),
  UNIQUE KEY hraci_uk1 (registrace_mail),
  UNIQUE KEY hraci_uk2 (jmeno),
  INDEX hraci_idx1 (jmeno),
  INDEX hraci_idx2 (posledni_prihlaseni),
  INDEX hraci_idx3 (skupina),
  FOREIGN KEY hraci_fk1 (skupina) REFERENCES skupiny (id) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE hraci_medaile (
  id int(11) NOT NULL auto_increment,
  udelil int NOT NULL,
  udeleno timestamp DEFAULT CURRENT_TIMESTAMP,
  hrac int NOT NULL,
  medaile varchar(15) NOT NULL,
  popis text,
  PRIMARY KEY  hraci_medaile_pk (id),
  INDEX hraci_medaile_idx1 (hrac),
  FOREIGN KEY hraci_medaile_fk1 (udelil) REFERENCES hraci (id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY hraci_medaile_fk2 (hrac) REFERENCES hraci (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE ip_banned (
  id int(11) NOT NULL auto_increment,
  ip varchar(39) NOT NULL,
  PRIMARY KEY ip_banned_pk (id),
  INDEX ip_banned_idx1 (ip)
);

CREATE TABLE ip_login (
  id int(11) NOT NULL auto_increment,
  hrac int(11) NOT NULL,
  ip varchar(39) NOT NULL,
  pocet int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY ip_login_pk (id),
  INDEX ip_login_idx1 (ip),
  FOREIGN KEY ip_login_fk1 (hrac) REFERENCES hraci (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE ip_denied (
  id int(11) NOT NULL auto_increment,
  cas timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  hrac int(11) NOT NULL,
  heslo varchar(50) NOT NULL, 
  ip varchar(39) NOT NULL,
  PRIMARY KEY ip_denied_pk (id),
  INDEX ip_denied_idx1 (hrac),
  FOREIGN KEY ip_denied_fk1 (hrac) REFERENCES hraci (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE hry
(
  id int(11) NOT NULL auto_increment,
  vytvoreno timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  vitez_cerveny bool NOT NULL DEFAULT TRUE,
  vitez int(11) NOT NULL,
  vitez_start_mesto varchar(15) NOT NULL,
  vitez_hlavni_mesto varchar(15) NOT NULL,
  vitez_start_hrdina varchar(15) NOT NULL,
  vitez_hlavni_hrdina varchar(15) NOT NULL,
  vitez_fairplay bool,
  porazeny int(11) NOT NULL,
  porazeny_start_mesto varchar(15) NOT NULL,
  porazeny_hlavni_mesto varchar(15) NOT NULL,
  porazeny_start_hrdina varchar(15) NOT NULL,
  porazeny_hlavni_hrdina varchar(15) NOT NULL,
  porazeny_fairplay bool,
  typ_mapy varchar(15) NOT NULL,
  velikost_mapy varchar(2) NOT NULL,
  obtiznost varchar(15) NOT NULL,
  delka int(11) NOT NULL,
  souhlas_porazeneho bool,
  souhlas_rozhodciho bool,
  potvrzeno timestamp,
  locked bool NOT NULL DEFAULT FALSE,
  PRIMARY KEY hry_pk (id),
  INDEX hry_idx1 (vitez),
  FOREIGN KEY hry_fk1 (vitez) REFERENCES hraci (id) ON UPDATE CASCADE ON DELETE CASCADE,
  INDEX hry_idx2 (porazeny),
  FOREIGN KEY hry_fk2 (porazeny) REFERENCES hraci (id) ON UPDATE CASCADE ON DELETE CASCADE
  INDEX hry_idx3 (potvrzeno),
);

CREATE TABLE hry_komentare
(
  id int(11) NOT NULL auto_increment,
  vlozeno timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  vlozil int(11) NOT NULL,
  hra int(11) NOT NULL,
  text text,
  PRIMARY KEY hry_komentare_pk (id),
  INDEX hry_komentare_idx1 (vlozil),
  FOREIGN KEY hry_komentare_fk1 (vlozil) REFERENCES hraci (id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY hry_komentare_fk2 (hra) REFERENCES hry (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE hry_screeny
(
  id int(11) NOT NULL auto_increment,
  vlozeno timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  vlozil int(11) NOT NULL,
  hra int(11) NOT NULL,
  PRIMARY KEY hry_komentare_pk (id),
  FOREIGN KEY hry_komentare_fk1 (vlozil) REFERENCES hraci (id) ON UPDATE CASCADE ON DELETE CASCADE,
  INDEX hry_komentare_idx1 (hra),
  FOREIGN KEY hry_komentare_fk2 (hra) REFERENCES hry (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE forum_vlakna
(
  id int(11) NOT NULL auto_increment,
  vytvoreno timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  vytvoril int(11) NOT NULL,
  rodic int(11),
  list bool NOT NULL DEFAULT TRUE,
  nazev varchar(50),
  popis varchar(50),
  PRIMARY KEY forum_vlakna_pk (id),
  FOREIGN KEY forum_vlakna_fk1 (vytvoril) REFERENCES hraci (id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY forum_vlakna_fk2 (rodic) REFERENCES forum_vlakna (id) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO forum_vlakna (id, vytvoril, nazev, popis) VALUES(1, 1, 'Výzvy', 'Shánění hráčů na hru');
INSERT INTO forum_vlakna (id, vytvoril, nazev, popis) VALUES(2, 1, 'Pokec', 'Tlachání o čemkoliv');
INSERT INTO forum_vlakna (id, vytvoril, nazev, popis) VALUES(3, 1, 'Spory', 'Řeąení sporů zejména kolem dodrľování pravidel');
INSERT INTO forum_vlakna (id, vytvoril, nazev, popis) VALUES(4, 1, 'Web', 'Návrhy, komentáře, hláąení bugů');
INSERT INTO forum_vlakna (id, vytvoril, nazev, popis) VALUES(5, 1, 'Srazy', 'Organizace real-life srazů');
INSERT INTO forum_vlakna (id, vytvoril, nazev, popis) VALUES(6, 1, 'Turnaje', 'Diskuze k turnajům');
INSERT INTO forum_vlakna (id, vytvoril, nazev, popis) VALUES(7, 1, 'Oznámení', 'Oficiální sdělení admina');

CREATE TABLE forum_prispevky
(
  id int(11) NOT NULL auto_increment,
  vlozeno timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  vlozil int(11),
  vlakno int(11),
  text text,
  PRIMARY KEY forum_prispevky_pk (id),
  INDEX forum_prispevky_idx1 (vlozil),
  FOREIGN KEY forum_prispevky_fk1 (vlozil) REFERENCES hraci (id) ON UPDATE CASCADE ON DELETE CASCADE,
  INDEX forum_prispevky_idx2 (vlakno),
  FOREIGN KEY forum_prispevky_fk2 (vlakno) REFERENCES forum_vlakna (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE uzaverky
(
  id int(11) NOT NULL auto_increment,
  cas timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY uzaverky_pk (id),
  INDEX uzaverky_idx1 (cas)
);

/* zbyva: kvartalni vyhodnoceni, dobyvatel, galerie-hrdinove, vse ke statistikam */

COMMIT;
