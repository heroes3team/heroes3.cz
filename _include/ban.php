<?php
/*
* Funkce pro praci s hracskymi bany a IP bany + pomocne cookie bany
*/


function ip_ban_test() {	
  if($_SESSION['ip_ok']) return true;
  if(sql_exists("SELECT * FROM ip_banned WHERE ip='".get_cient_ip()."'"))
    return false;
  else {
    $_SESSION['ip_ok'] = true;
    return true;
  }
}

function cookie_ban_test() {
  if($_COOKIE["susenka"] == md5("ban"))
    return false;
  else return true;
}

function is_banned_ip($ip) {
  return sql_exists("SELECT * FROM ip_banned WHERE ip='".addslashes($ip)."'");
}

function ban_ip($ip) {
  sql_begin();
  if(!is_banned_ip($ip))
    sql_query("INSERT INTO ip_banned (ip) VALUES ('".addslashes($ip)."')");
  sql_commit();
}

function unban_ip($ip) {
  sql_query("DELETE FROM ip_banned WHERE ip='".addslashes($ip)."'");
}

function potrestej() {
	global $ROOT_URL;
  ban_ip(get_cient_ip());
  setcookie("susenka", md5("ban"));
  if($_SERVER['REQUEST_URI'] != '/banned.php')
    header_redirect($ROOT_URL.'/banned.php', 0);
}

function ban_test() {
  if(ip_ban_test() && cookie_ban_test()
  ) return true;
  else {
    potrestej();
    return false;
  }
}

?>
