<?php

/**

 * funkce pro převod data/času mezi db a uľivatelskou reprezentací.
 */


function dt_date_db2user($datum, $tvar = 1)
{
  if (empty($datum)) {
    return false;
  }

  $datum_array = split("-", $datum, 3);
  if (count($datum_array) < 3) {
    return false;
  }

  list($rok, $mesic, $den) = $datum_array;
  $den = ltrim($den, "0");
  $mesic = ltrim($mesic, "0");

  if ($den == 0) {
    if ($mesic == 0) {
      return "$rok";
    } else {
      return $tvar = 4 ? "$rok" : "$mesic/$rok";
    }
  } elseif ($rok == 0) {
    return "$den. $mesic.";
  } else {
    switch ($tvar) {
      case 1: return sprintf("%d. %d. %d", $den, $mesic, $rok);
              break;
      case 2: return sprintf("%d. %d.", $den, $mesic);
              break;
      case 3: return sprintf("%d/%d", $mesic, $rok);
              break;
      case 4: return sprintf("%d", $rok);
              break;
    }
    return false;
  }
}

function dt_date_user2db($datum)
{
  $datum = trim($datum);
  if (empty($datum)) {
    return false;
  }

  $casti = preg_split("/\D+/", $datum);
 
  if (isset($casti[2]) && !$casti[2]) {
     unset($casti[2]); //pokud $datum konci napr. '.',
                       //je $casti[2] prazdny, ale nastaveny!
  }
  if (isset($casti[1]) && !$casti[1]) {
     unset($casti[1]);
  }

  $datum=getdate();

  if (isset($casti[0]) && isset($casti[1]) && isset($casti[2])) {
    $den = is_numeric($casti[0]) && $casti[0] <=31 && $casti[0] > 0 ? $casti[0] : 0;
    $mesic = is_numeric($casti[1]) && $casti[1] <=12 && $casti[1] > 0 ? $casti[1] : 0;
    $rok = is_numeric($casti[2]) && $casti[2] <= 3000 && $casti[2] > 0 ? $casti[2] : 0;

  } elseif (isset($casti[0]) && isset($casti[1])) {

    if ($casti[0] > 12) { // dd/mm
      $den = is_numeric($casti[0]) && $casti[0] <= 31 ? $casti[0] : 0;
      $mesic = is_numeric($casti[1]) && $casti[1] > 0 && $casti[1] <= 12 ? $casti[1] : 0;
      $rok = $datum['year'];
    } elseif ($casti[1] > 12) { // mm/rrrr
      $den = $datum['mday'];
      $mesic = is_numeric($casti[0]) && $casti[0] > 0 && $casti[0] <= 12 ? $casti[0] : 0;
      $rok = is_numeric($casti[1]) && $casti[1] <= 3000 ? $casti[1] : 0;
    } else { // predpokladame dd/mm
      $den = is_numeric($casti[0]) && $casti[0] > 0 && $casti[0] <= 31 ? $casti[0] : 0;
      $mesic = is_numeric($casti[1]) && $casti[1] > 0 && $casti[1] <= 12 ? $casti[1] : 0;
      $rok = $datum['year'];
    }

  } elseif (isset($casti[0])) { //pouze rok
    $den = $datum['mday'];
    $mesic = $datum['month'];
    $rok = is_numeric($casti[0]) && $casti[0] > 0 && $casti[0] <= 3000 ? $casti[0] : 0;
  
  } else {
    $den = $mesic = $rok = 0;
  }

  if ($rok > 99 && $rok < 1000) {
    $rok = 0;
  } elseif ($rok > 0 && $rok < 100) {
    if ($rok < 30) {
      $rok += 2000;
    } else {
      $rok += 1900;
    }
  }

  if ($den != 0 && $mesic != 0 && $rok != 0) {
    return sprintf("%d-%02d-%02d", $rok, $mesic, $den);
  } else {
    return false;
  }
}

function dt_time_db2user($cas, $typ=1)
{
  if (empty($cas)) {
    return false;
  }

  $casti = split(":", $cas);

  if (!isset($casti[2])) {
    return false;
  }
 
  if ($typ == 1) {
    return sprintf("%01d:%02d:%02d", $casti[0], $casti[1], $casti[2]);
  } else {
    return sprintf("%01d:%02d", $casti[0], $casti[1]);
  }
}

function dt_time_user2db($cas)
{
  $cas = trim($cas);
  if (empty($cas)) {
    return false;
  }

	// tvar hmm (bez oddelovacich znaku) {{{
	if (ereg('^[0-9]{3}$', $cas)) {
		$h = intval(substr($cas,0,1));
		$m = intval(substr($cas,1,2));
		if ($m > 59) {
			$m = -1;
		};
		$s = 0;
	// }}}

	// tvar hhmm (bez oddelovacich znaku) {{{
	} elseif (ereg('^[0-9]{4}$', $cas)) {
		$h = intval(substr($cas,0,2));
		if ($h > 24) {
			$h = -1;
		}
		$m = intval(substr($cas,2,2));
		if ($m > 59) {
			$m = -1;
		};
		$s = 0;
	//}}}

	// tvar hmmss (bez oddelovacich znaku) {{{
	} elseif (ereg('^[0-9]{5}$', $cas)) {
		$h = intval(substr($cas,0,1));
		$m = intval(substr($cas,1,2));
		if ($m > 59) {
			$m = -1;
		};
		$s = intval(substr($cas,3,2));
		if ($s > 59) {
			$s = -1;
		};
	// }}}

	// tvar hhmmss (bez oddelovacich znaku) {{{
	} elseif (ereg('^[0-9]{6}$', $cas)) {
		$h = intval(substr($cas,0,2));
		if ($h > 24) {
			$h = -1;
		}
		$m = intval(substr($cas,2,2));
		if ($m > 59) {
			$m = -1;
		};
		$s = intval(substr($cas,4,2));
		if ($s > 59) {
			$s = -1;
		};
	// }}}

	// tvar s oddelovacimi znaky {{{
	} else {

	  $casti = preg_split("/\D+/", $cas);

  	if (isset($casti[0]) && isset($casti[1]) && isset($casti[2])) {
    	$h = is_numeric($casti[0]) && $casti[0] <=24 && $casti[0] >= 0 ? intval($casti[0]) : -1;
	    $m = is_numeric($casti[1]) && $casti[1] <=59 && $casti[1] >= 0 ? intval($casti[1]) : -1;
  	  $s = is_numeric($casti[2]) && $casti[2] <=59 && $casti[2] >= 0 ? intval($casti[2]) : -1;

	  } elseif (isset($casti[0]) && isset($casti[1])) {
  	  $h = is_numeric($casti[0]) && $casti[0] <= 24 && $casti[0] >= 0 ? intval($casti[0]) : -1;
    	$m = is_numeric($casti[1]) && $casti[1] <= 59 && $casti[1] >= 0 ? intval($casti[1]) : -1;
	    $s = 0;

  	} elseif (isset($casti[0]) && !empty($casti[0])) {
    	$h = is_numeric($casti[0]) && $casti[0] <= 24 && $casti[0] >= 0 ? intval($casti[0]) : -1;
	    $m = 0;
  	  $s = 0;

	  } else {
  	  $h = $m = $s = -1;
	  }
	}
	//}}}

  if ($h == 24 && $m == 0 && $s == 0) {
    $h = 23; $m = 59; $s = 59; //jen kvuli Postgres
  }

	if ($h == 24 && ($m > 0 || $s > 0)) {
		$h = $m = $s = -1;
	}

  if ($h != -1 && $m != -1 && $s != -1) {
    $ret= sprintf("%02d:%02d:%02d", (int) $h, (int) $m, (int) $s);
    return $ret;
  } else {
    return false;
  }
}

function dt_ts_date_db2user($timestamp, $tvar=1) {
  $parts = explode(' ', $timestamp);
  return(dt_date_db2user($parts[0], $tvar));
}


function dt_ts_time_db2user($timestamp, $typ=1) {
  $parts = explode(' ', $timestamp);
  return(dt_time_db2user(substr($parts[1], 0, 8), $typ));
}


?>
