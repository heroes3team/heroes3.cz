<?php

/*
*  Vsechny globalni definice
*/

$WEB_VERSION = "1.9";

$INCLUDE_URL = $ROOT_URL.'/_include';

$IMG = $INCLUDE_URL.'/img';
$CSS = $INCLUDE_URL.'/css';
$JS = $INCLUDE_URL.'/js';

$admin_mail = 'liga@heroes3.cz';
$SYSTEM_ID = 5032;

function karma($x) {
  if($x <= -50) return 'zlosyn';
  if($x <= -10) return 'darebák';
  if($x <= -1) return 'rošťák';
  if($x <= 9) return 'slušňák';
  if($x <= 49) return 'dobrák';
  if($x <= 249) return 'dobrodinec';
  return 'spasitel';  
}

function zkusenost($x) {
  if($x <= 9) return 'nováček';
  if($x <= 24) return 'pionýr';
  if($x <= 49) return 'bojovník';
  if($x <= 99) return 'ostřílený bojovník';
  if($x <= 249) return 'mazák';
  if($x <= 1000) return 'veterán';
  return 'legenda';  
}

function ukecanost($x) {
  if($x <= 9) return 'mlčoun';
  if($x <= 49) return 'sváteční pisálek';
  if($x <= 99) return 'diskutér';
  if($x <= 499) return 'kecálek';
  if($x <= 999) return 'řečník';
  return 'spisovatel';
}

$medaile = array(
  'zlato' => array('nazev' => 'Zlatá medaile', 'zkratka' => 'zlato', 'popis' => 'za první místo v turnaji'),
  'stribro' => array('nazev' => 'Stříbrná medaile','zkratka' => 'stribro', 'popis' => 'za druhé místo v turnaji'),
  'bronz' => array('nazev' => 'Bronzová medaile', 'zkratka' => 'bronz', 'popis' => 'za třetí místo v turnaji'),
  'sampion' => array('nazev' => 'Pohár šampiona', 'zkratka' => 'sampion', 'popis' => 'za získání nebo obhájení titulu šampiona'),
  'zasluhy' => array('nazev' => 'Medaile za zásluhy', 'zkratka' => 'zasluhy', 'popis' => 'za materiální nebo duševní podporu ligy'),
  'dobyvatel' => array('nazev' => 'Meč dobyvatele', 'zkratka' => 'dobyvatel', 'popis' => 'za rozdrcení nejlepších hráčů ligy'),
  'repre' => array('nazev' => 'Reprezentant', 'zkratka' => 'repre', 'popis' => 'za reprezentaci české ligy na mezinárodním turnaji'),
  'varovani' => array('nazev' => 'Varování', 'zkratka' => 'varovani', 'popis' => 'za porušování pravidel nebo nevhodné chování, v případě opakování bude následovat ban'),
  'lotr' => array('nazev' => 'Prsten moci', 'zkratka' => 'lotr', 'popis' => 'Ocenění v rámci single player challange "Jeden prsten vládne všem".')

);

$mesta = array('castle' => 'hrad',
              'rampart' => 'bašta',
              'tower' => 'věž',
              'inferno' => 'peklo',
              'dungeon' => 'kobka',
              'necropolis' => 'nekropolis',
              'stronghold' => 'tvrz',
              'fortress' => 'pevnost',
              'conflux' => 'soutok',
              'cove' => 'zátoka'
);

?>
