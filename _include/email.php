<?php
/*
* Funkce na posilani mailu
*/

function send_email($to_addr, $subj, $msg, $content_type = 'text/plain') {

	global $INCLUDE_DIR;

		/*use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;*/
	
	require $INCLUDE_DIR.'/PHPMailer-6.0.7/src/Exception.php';
	require $INCLUDE_DIR.'/PHPMailer-6.0.7/src/PHPMailer.php';
	require $INCLUDE_DIR.'/PHPMailer-6.0.7/src/SMTP.php';
		
	$mail = new PHPMailer\PHPMailer\PHPMailer(true);
	
	try {
    //Server settings
    //$mail->SMTPDebug = 2;                                       // Enable verbose debug output
    $mail->isSMTP();                                            // Set mailer to use SMTP
    $mail->Host       = 'smtp.snackhost.eu';  					// Specify main and backup SMTP servers
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'liga@heroes3.cz';                     // SMTP username
    $mail->Password   = 'yrSGPRAf2z';                       	// SMTP password
    $mail->SMTPSecure = 'ssl';                                  // Enable TLS encryption, `ssl` also accepted
    $mail->Port       = 465;                                    // TCP port to connect to
	$mail->Encoding = 'base64';
	$mail->CharSet = 'UTF-8';

    //Recipients
    $mail->setFrom('liga@heroes3.cz', 'Liga Heroes 3');
    $mail->addAddress($to_addr);     // Add a recipient
    $mail->addReplyTo('liga@heroes3.cz', 'Liga Heroes 3');

    // Content
    $mail->Subject = $subj;
    $mail->Body    = $msg;

    $mail->send();
    //echo 'Message has been sent';
	return true;
} catch (Exception $e) {
   //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	return false;
}
	
}

function send_email_old($to_addr, $subj, $msg, $content_type = 'text/plain', $from_addr = 'Liga Heroes 3 <liga@heroes3.cz>') {
	if(!empty($to_addr)) {
		$hlavicky = '';
		$hlavicky .= "MIME-Version: 1.0\r\n";
		$hlavicky .= 'From: '.$from_addr."\r\n";
		$hlavicky .= 'X-Sender: '.$from_addr."\r\n";
		$hlavicky .= "X-Priority: 3\r\n";
		$hlavicky .= 'Reply-To: '.$from_addr."\r\n";
		$hlavicky .= 'Return-Path: '.$from_addr."\r\n";
		$hlavicky .= 'Content-Type: '.$content_type."; charset=utf-8\r\n";

        $encoded = preg_replace("/=$/","", chunk_split(bin2hex($subj), 2, '='));
        $subj_encoded = '=?utf-8?Q?='.$encoded.'?=';

    if(mail($to_addr, $subj_encoded, $msg, $hlavicky)) return (true);
		else return (false);
	}
	else return (false);
}

?>
