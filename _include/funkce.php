<?php
/*
* Ruzne funkce
*/

function javascript_alert($text) {
echo "
<script type=\"text/javascript\">
//<!--
	 alert('".$text."');     
//-->
</script>	
";
}

function dalsi_funkce($hrac) {
  $out = array();
  if($hrac['admin']) $out[] = '<span class="admin">admin</span>';
  if($hrac['moderator']) $out[] = '<span class="moderator">moderátor</span>';
  if($hrac['rozhodci']) $out[] = '<span class="rozhodci">rozhodčí</span>';
  return $out;
}

function html_medaile($med) {
  
  global $IMG, $medaile;

  $html = "";

  $html .= "<table style = \"padding: 10px; width: 100%; max-width: 225px;\"><tr><td style = \"width: 30px\"><img title=\"".$medaile[$med['medaile']]['nazev'].' '.dt_ts_date_db2user($med['udeleno'])."\"
                 style=\"cursor: help;\"
                 alt=\"".$medaile[$med['medaile']]['nazev']."\"
                 src=\"".$IMG."/medaile/".$med['medaile'].".png\" /></td><td>";
  $html .= "<span style=\"position: relative; top: 0px;\">".$med['popis']."</span></td>";
  
  
  if($_SESSION['uzivatel']['admin']) {
    $html .= "<td style = \"text-align: right;\">" . html_href($ROOT_URL.'/hraci/detail.php?id='.$med['hrac'].'&amp;smaz_medaili='.$med['id'], 'Smazat','','','','button') . "</td>";
  }
  
  $html .= "</tr></table>";
  
  return $html;
}

function capital_first_letter($word) {
$old_first_letter = substr($word, 0, 1);
$new_first_letter = strtoupper($old_first_letter);
$output = ereg_replace("^." , $new_first_letter, $word);
return $output;
}


function capital_name($name) {
list ($name1, $name2) = split(" ", $name);

$name12 = capital_first_letter($name1);
$name22 = capital_first_letter($name2);

$output = $name12;

if ($name22 != "") $output .= ("&nbsp;".$name22);

return $output;

}

function mesto_en2cz($en) {

  $pole = array('castle' => 'hrad',
                'rampart' => 'bašta',
                'tower' => 'věž',
                'inferno' => 'peklo',
                'dungeon' => 'kobka',
                'necropolis' => 'nekropolis',
                'stronghold' => 'tvrz',
                'fortress' => 'pevnost',
                'conflux' => 'soutok',
                'cove' => 'zátoka');
  return $pole[$en];              
}

// slova zacinajici na www. nebo http:// nebo https:// nahradi linkama
function add_links($text) {

  $odstavce = explode("\n", $text);
  foreach($odstavce as $oi => $odstavec) {
    $slova = explode(" ", $odstavec);
    foreach($slova as $si => $slovo) {
      if(ereg("^http:\/\/.+$", $slovo) || ereg("^https:\/\/.+$", $slovo) || ereg("^www\..+$", $slovo)) {
        $newslovo = "<a href=\"".$slovo."\" >".$slovo."</a>";
        $slova[$si] = $newslovo;
      } 
    }
    $odstavce[$oi] = implode(" ", $slova);
  }
  $newtext = implode("\n", $odstavce); 
  return $newtext;
}

function update_pocet_prispevku($hrac_id) {
	$forum_prispevky = sql_getfield(sprintf("SELECT COUNT(*) AS count FROM forum_prispevky WHERE vlozil=%d", $hrac_id), 'count');
	$hry_komentare = sql_getfield(sprintf("SELECT COUNT(*) AS count FROM hry_komentare WHERE vlozil=%d", $hrac_id), 'count');
	$celkem_prispevky = $forum_prispevky + $hry_komentare;
	sql_query(sprintf("UPDATE hraci SET prispevku=%d WHERE id=%d", $celkem_prispevky, $hrac_id));
}

?>
