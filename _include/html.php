<?php
/*
* php funkce na vkladani html objektu.
*/

function html_href($url, $text, $title='', $id='', $onClick='', $class='',$blank='') {
  $ret = "<a href=\"".$url."\"";
  if(!empty($title)) $ret .= " title=\"".$title."\"";
  if(!empty($id)) $ret .= " id=\"".$id."\"";
  if(!empty($onClick)) $ret .= " onClick=\"".$onClick."\"";
  if(!empty($class)) $ret .= "class=\"".$class."\"";
  if(!empty($blank)) $ret .= "target=\"_blank\"";
  $ret .= ">".$text."</a>";
  return $ret;
}

function html_image($src = '', $alt = '', $title = '', $style='') {

  if (empty($alt))
    $alt = "img";

  if (empty($title))
    $title = $alt;
    
  if(!empty($style))
    $style = 'style="'.$style.'"';

  return sprintf('<img src="%s" alt="%s" title="%s" %s />', $src, $alt, $title, $style);
}

function table_start($class = 'tab', $action='')
{
  if(empty($action)) $action=$_SERVER['PHP_SELF'];

  echo '<form action="'.$action.'" method="post" enctype="multipart/form-data">'."\n";
  if($class===0) $class = 'tab';
  echo '<table class="'.$class.'">'."\n";
}

function table_end() {
  echo '</table>'."\n";
  echo '</form>'."\n";
}

function table_start_simple($class = 'tab')
{
  if($class===0) $class = 'tab';
  echo '<table class="'.$class.'">'."\n";
}

function table_end_simple() {
  echo '</table>'."\n";
}

function html_podklad_start () {
  echo '<div class = "podklad">';  
}

function html_podklad_end () {
  echo '</div><div style = "height: 5px;"></div>';   
}

function table_row($columns, $colspans = 0, $widths = 0,
  $aligns = array(), $valigns = array(),
  $header = false, $col_highlite = false, $footer = false, $classes = array(), $rowspans = 0)
{
  $rclass = '';
  if ($header) {
    $GLOBALS['table_odd_line'] = true;
  } elseif ($footer===true OR $footer===1) {
    $rclass = 'ftr';
  } elseif ($footer!==true AND $footer!==false) {
    $rclass = $footer;
  } else {
    $rclass = $GLOBALS['table_odd_line'] ? '' : 'even';
    $GLOBALS['table_odd_line'] = !$GLOBALS['table_odd_line'];
  }

  $str = '<tr'.($rclass != '' ? ' class="'.$rclass.'"' : '').'>'."\n";
  $first_td = true;

  foreach ($columns as $index => $column) {
    $row = ($header == TRUE ? '<th' : '<td');
    $class = '';
    if (isset($aligns) && isset($aligns[$index]) && !empty($aligns[$index])) {
      if (($header && strtolower($aligns[$index]) == 'center') ||
          (!$header && strtolower($aligns[$index]) == 'left')) {
        $class = '';
      } else {
        $class = $aligns[$index];
      }
    }
    if (isset($valigns) && isset($valigns[$index]) && !empty($valigns[$index])) {
      $class .= ' '.$valigns[$index];
    }    
    
    
    if ($col_highlite && $first_td && !$header) {
      if ($class != '')
        $class .= ' ';
      if ($first_td)
        $class .= 'first';
    }
    if (isset($classes[$index])) {
      if ($class != '')
        $class .= ' ';
      $class .= $classes[$index];
    }
    if ($class != '') {
      $row .= ' class="'.$class.'"';
    }
    if (isset($colspans) && isset($colspans[$index]) && !empty($colspans[$index])) {
      $row .= ' colspan="'.$colspans[$index].'"';
    }
    if (isset($rowspans) && isset($rowspans[$index]) && !empty($rowspans[$index])) {
      $row .= ' rowspan="'.$rowspans[$index].'"';
    }    
    /*if (isset($valigns) && isset($valigns[$index]) && !empty($valigns[$index])) {
      $row .= ' valign="'.(strtolower($valigns[$index]) != 'middle' ? $valigns[$index] : '').'"';
    }*/
    if (isset($widths) && isset($widths[$index])) {
      $row .= ' width="'.$widths[$index].'"';
    }
    $row .= '>'.$column.'</';
    $row .= ($header == TRUE ? 'th>' : 'td>')."\n";
    $str .= $row;
    $first_td = FALSE;
  }
  $str .= '</tr>'."\n";

  print $str;
}

function ramecek_start($sirka, $nadpis) {
  echo '<div class="ramecek">'."\n";
  echo '<div class="ramecek'.$sirka.'_1">'."\n";
  if(!empty($nadpis)) echo '<h2>'.$nadpis.'</h2>'."\n";
  echo '</div>'."\n";
  
  echo '<div class="ramecek_stred"><div class="ramecek'.$sirka.'_2">'."\n";
}

function ramecek_end($sirka) {
  echo '</div></div>'."\n";  

  echo '<div class="ramecek'.$sirka.'_3">'."\n";
  echo '</div>'."\n";
  echo '</div>'."\n";
}

function input_text($name, $value = '', $size = 15, $maxlength = 256, $disabled = false, $class = '', $id='', $onKeyUp='')
{
  $html = "\n<input type=\"text\" name=\"$name\" size=\"$size\" maxlength=\"$maxlength\" value=\"$value\"";
  $html .= ' class="textbox '.$class.'"';
  $html .= !empty($disabled) ? ' disabled="disabled"' : '';
  $html .= !empty($id) ? ' id="'.$id.'"' : '';
  $html .= !empty($onKeyUp) ? ' onKeyUp="'.$onKeyUp.'"' : '';
  $html .= " />\n";
  return($html);
}

function input_password($name, $value = '', $size = 15, $maxlength = 59, $class = '')
{
	$html = "\n<input type=\"password\" name=\"$name\" size=\"$size\" maxlength=\"$maxlength\" value=\"$value\" class=\"textbox $class\" />\n";
	return($html);
}

function input_submit($alt = 'Odeslat', $name = '',$class = 'submit', $width = '')
{  
  if (intVal($width)) {
    $style = "style=\"width: ".$width."px\"";
  }
  else $style = "style=\"width: ".max(30, strlen($alt)*10)."px\""; 
  
  return("<button class=\"".$class."\" ".$style." type=\"submit\" name=\"$name\" value=\"$alt\" title=\"$alt\" >".$alt."</button>");
}

function input_checkbox($name, $value, $checked = false, $disabled = false, $title = '', $onClick = '')
{
  if (empty($title)) {
    $title = 'Označit';
  }
  
  if($onClick==1) $onClick = 'this.form.submit();';
  
  $html = "\n<input class=\"checkbox\" type=\"checkbox\" name=\"$name\" value=\"$value\" title=\"$title\"";
  $html .= ($checked) ? ' checked="checked"' : '';
  $html .= ($disabled) ? ' disabled="disabled"' : '';
  $html .= empty($onClick) ? '' : ' onClick="'.$onClick.'"';
  $html .= " />\n";
  return($html);
}

function input_textarea($name, $value = '', $rows = 6, $cols = 60, $width = '')
{
  $style = '';
  if(!empty($width)) {
    $style = "style=\"width: ".intval($width)."px;\" ";
  }
  $html = "\n<textarea name=\"$name\" rows=\"$rows\" cols=\"$cols\" class=\"textarea\" $style >$value</textarea>\n";
  return($html);
}
function input_file($name, $size=40)
{
  $html = "\n<input type=\"file\" name=\"$name\" />\n";
  return($html);
}
function input_select($name, $data, $aktivni = array(), $size = 1, $multiple = false, $onchange='', $option_class = array(), $disabled = false, $n = true, $id = "")
{
  $html = ($n ? "\n" : "")."<select name=\"$name\"".(!empty($id) ? " id=\"$id\"" : "")." class=\"selectbox\" ";
  $html .= ($size != 1) ? " size=\"$size\" " : '';
  $html .= ($multiple) ? ' multiple="multiple"' : '';
  $html .= !empty($disabled) ? ' disabled="disabled"' : '';
  if($onchange===1) $onchange = 'this.form.submit();';
  $html .= !empty($onchange) ? " onChange=\"$onchange\"" : '';
  $html .= ">".($n ? "\n" : "");
  foreach ($data as $key => $value)
  {
    $html .= ($n ? "\t" : "")."<option value=\"$key\"";
    $html .= (in_array($key, $aktivni)) ? ' selected="selected"' : '';
    $html .= (!empty($option_class[$key])) ? ' class="'.$option_class[$key].'"' : '';
    $html .= ">$value</option>".($n ? "\n" : "");
  }
  $html .= "</select>".($n ? "\n" : "");
  return($html);
}
function input_hidden($name, $value)
{
  $html = "\n<input type=\"hidden\" name=\"$name\" value=\"$value\" />\n";
  return($html);
}

function sloupec_start($sirka='') {
  if(!empty($sirka)){
	echo "<div class=\"sloupec\" style=\"width: " . $sirka . "px\">";
  }else{
	echo "<div class=\"sloupec\">";
  }

}

function sloupec_end() {
  echo "</div>";
}

function avatar($id) {
  global $ROOT_URL, $ROOT_DIR;
  
  if(file_exists($ROOT_DIR.'/__big_data/hraci_avatar/'.$id.'.jpg')) 
    $url = $ROOT_URL.'/__big_data/hraci_avatar/'.$id.'.jpg';
  else 
    $url = $ROOT_URL.'/__big_data/hraci_avatar/default.jpg';

    return "<img src=\"".$url."\" alt=\"avatar\" class=\"avatar img_ramecek\" />";
}

function html_error($text) {
  return '<div class="isa_error">' . $text .
		 '</div>'; 
}

function html_galerie_video ($datum, $mesto, $hrdina, $YT_URL, $popis){
	global $IMG;
	$html = '<table class = "tab_video">
			 <tr>
				<td style = "width: 50px;"> 
					<span  >' . $datum  . '</span>
				</td>
				<td>
					<img class = "img_ramecek" style="height: 62px; width: 103px;" src="' . $IMG . '/mesta/' . $mesto  . '.jpg" alt="mesto" />
					<br/>
					<span class = "strong_video">' . $mesto .'</span>
				</td>
				<td style = "">
					<img class = "img_ramecek" style="height: 62px; width: 58px;" src="' . $IMG . '/hrdinove/' . $hrdina . '.jpg" alt="hrdina" />
					<br/>
					<span class = "strong_video">' . str_replace('%20', ' ', $hrdina) . '</span>
				</td>
				<td style = " padding-left: 10px;">
					<a class="yt" href="' . $YT_URL .  '">' . $popis . '</a>
				</td>
			 </tr>
			</table>';
	echo $html;
}

function html_nadpis($text) {
  $html = '';
  $html .= "<div class=\"nadpis\" style=\"width: ".(strlen($text)*13+120)."px;\">\n";
  $html .= "<div class=\"nadpis_1\"></div>\n";
  $html .= "<div class=\"nadpis_2\" style=\"width: ".(strlen($text)*13+10)."px;\" ><h1>".$text."</h1></div>\n";
  $html .= "<div class=\"nadpis_3\"></div>\n";
  $html .= "<br style=\"clear: left\"/>\n";
  $html .= "</div>\n";
  $html .= "<br/>\n";
  
  return $html;
}

function html_p($text) {
  $html = "<p class=\"paragraph\">".$text."</p>";
  return $html;
}

function h1($text) {
  $html = "<p class=\"h1\">".$text."</p>";
  return $html;
}

function h2($text) {
  $html = "<p class=\"h2\">".$text."</p>";
  return $html;
}

function h3($text) {
  $html = "<p class=\"h3\">".$text."</p>";
  return $html;
}

function h4($text) {
  $html = "<p class=\"h4\">".$text."</p>";
  return $html;
}

?>
