<?php
/*
* Sablona HTML stranek
*/

function html_start() {

  global $TITLE, $CSS, $JS, $IMG, $ROOT_URL;

  # posun ramecku dle prohlizece
  $brow = '_'.strtolower($_SERVER['HTTP_USER_AGENT']);
  $posun = false;
  if(strpos($brow, 'mozilla')) $posun = true;
  if(strpos($brow, 'safari')) $posun = true;
  if(strpos($brow, 'msie')) $posun = false;

  echo "<!DOCTYPE html>\n";
  echo "<html lang =\"cs\">\n";  
  echo "<head>\n";
  echo "<meta charset=\"UTF-8\" />\n";
  echo "<meta name=\"description\" content=\"Liga a turnaje v počítačové hře Heroes of Might and Magic III.\" />\n"; 
  echo "<meta name=\"keywords\" content=\"Heroes of Might and Magic 3, Heroes 3, liga, turnaje, online, hra, HoMaM, HMM, Hamachi, GameRanger\" />\n";
  echo "<title>Liga Heroes 3".(empty($TITLE) ? "" : " - ".$TITLE)."</title>\n";
  echo '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">';
  echo "<link rel=\"Stylesheet\" type=\"text/css\" href=\"".$CSS."/common.css\" />\n";
  echo "<link rel=\"Stylesheet\" type=\"text/css\" href=\"".$CSS."/menu.css\" />\n";
  echo "<link rel=\"Stylesheet\" type=\"text/css\" href=\"".$CSS."/footer.css\" />\n";
  echo "<link rel=\"Stylesheet\" type=\"text/css\" href=\"".$CSS."/ramecky.css\" />\n";
  echo "<link rel=\"Stylesheet\" type=\"text/css\" href=\"".$CSS."/nova_hra.css\" />\n";
  if($posun)
    echo "<link rel=\"Stylesheet\" type=\"text/css\" href=\"".$CSS."/posun.css\" />\n";
  echo "<link rel=\"shortcut icon\" href=\"".$ROOT_URL."/favicon.png\"  type=\"image/x-icon\" />\n";
  echo "</head>\n";
  echo "<body>\n";
  echo "<div id=\"div_logo\"></div>\n";
  echo "<div id=\"div_main\">\n";
  
  main_menu();
}

function main_menu() {

  global $MODUL, $ROOT_URL;

  echo "<div id=\"div_menu\">";
  echo "<ul>
  <li".($MODUL=='domu' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."\" title=\"\">Domů</a></li>
  <li".($MODUL=='hraci' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/hraci\" title=\"\">Hráči</a></li>
  <li".($MODUL=='hry' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/hry\" title=\"\">Hry</a></li>
  <li".($MODUL=='turnaje' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/turnaje\" title=\"\">Turnaje</a></li>
  <li".($MODUL=='diskuze' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/diskuze\" title=\"\">Diskuze</a></li>
  <li".($MODUL=='statistiky' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/statistiky\" title=\"\">Statistiky</a></li>			
  <li".($MODUL=='galerie' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/galerie\" title=\"\">Galerie</a></li>
  <li".($MODUL=='liga_info' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/liga_info\" title=\"\">Liga Info</a></li>
  <li".($MODUL=='jak_hrat_online' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/jak_hrat_online\" title=\"\">Jak hrát online</a></li>
  <li".($MODUL=='odkazy' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/odkazy\" title=\"\">Odkazy</a></li>			
  </ul>";
  echo "</div>";
  
}

function liga_info_menu() {

  global $MODUL2, $ROOT_URL;

  echo "<div id=\"div_menu2\">";
  echo "<ul style=\"margin-left: 225px;\">
  <li".($MODUL2=='obecne' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/liga_info/obecne\" title=\"\">Obecně</a></li>
  <li".($MODUL2=='pravidla' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/liga_info/pravidla\" title=\"\">Pravidla</a></li>
  <li".($MODUL2=='zebricek' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/liga_info/zebricek\" title=\"\">Žebříček</a></li>
  <li".($MODUL2=='oceneni' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/liga_info/oceneni\" title=\"\">Ocenění</a></li>	
  </ul>";
  echo "</div>";  
}

function galerie_menu() {

  global $MODUL2, $ROOT_URL;

  echo "<div id=\"div_menu2\">";
  echo "<ul style=\"margin-left: 225px;\">
  <li".($MODUL2=='videa' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/galerie/videa\" title=\"\">Videa</a></li>
  <li".($MODUL2=='hrbitov_her' ? " class=\"active\" " : "")."><a href=\"".$ROOT_URL."/galerie/hrbitov_her\" title=\"\"><div style=\"font-size: 78%; margin-top: 2px;\">Hřbitov her</div></a></li>	
  </ul>";
  echo "</div>";  
}


function html_end() {

  global $IMG, $WEB_VERSION;

  echo "\n<hr style=\"clear: left\" />";
  echo "\n<div id=\"div_footer\">\n";
  
  echo "<table style=\"width: 300px; margin: auto\">";
  table_row(array(
	html_href('http://validator.w3.org/check?uri=referer', html_image($IMG.'/misc/html5-validated.png', 'Valid HTML 5')),
	html_href('http://jigsaw.w3.org/css-validator/check/referer', html_image($IMG.'/misc/vcss.gif', 'Valid CSS 3')),
	"&nbsp;verze&nbsp;webu&nbsp;".$WEB_VERSION));
  echo "<table>";
    
  echo "\n</div>";
  echo "\n</div>";
  echo "\n</body>";
  echo "\n</html>";

    db_close();
}


?>
