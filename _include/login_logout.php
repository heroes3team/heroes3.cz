<?php
/*
* Toto je skript zarizujici login/logout
* 
* includuje se na zacatek kazde stranky, v ramci _common_start.php
* protoze login / logout je mozne provest na kazde strance
*
*/

function h3hash($heslo) {
 return sha1($heslo."e5fyHT59I75Yn1578XEh1CWh4Y6tVo");
}

function login($jmeno, $heslo) { 
  global $ROOT_URL;
  
  // test blokace jmena
  $sql = sql_query("SELECT * FROM hraci WHERE jmeno='".sql_string($jmeno)."'");
  if(sql_num_rows($sql)==1) {
    // otestujeme zablokovani uctu vysokym poctem nepovedenych prihlaseni
    $row = sql_fetch_array($sql);
    $hrac_id = $row['id'];
    $sql2 = sql_query("SELECT * FROM ip_denied WHERE hrac=".$row['id']);
    $chyb = sql_num_rows($sql2);
    $zbyva_pokusu = 6 - $chyb;
    if($zbyva_pokusu<1)
      return html_error('Účet byl zablokován z důvodu opakovaných pokusů o přihlášení pod špatným heslem.
                          Pro odblokování kontaktujte administrátora.');      
  }
  else {
    return html_error('Uživatel '.$jmeno.' neexistuje.');
  }
  // pokud je jmeno OK a neni blokle, login pokracuje
  
  // test jmena a hesla
  $hash = h3hash($heslo); 
  $sql = sql_query("SELECT h.*, s.nazev as skupina_nazev
			FROM hraci as h JOIN skupiny as s ON h.skupina=s.id
			WHERE h.jmeno='".sql_string($jmeno)."' AND h.heslo='".$hash."'");
  if(sql_num_rows($sql)==1) {   
    $row = sql_fetch_array($sql);
    
    // otestujeme ban
    if(!$row['ban']) {
      // neni - li ucet aktivni, nelze se prihalsit
      if(!$row['aktivni']) return html_error('Účet ještě nebyl aktivován.');
      // hrac nema ban a v poradku se prihlasil - OK!
      $_SESSION['uzivatel'] = $row;
      
      // smazeme zaznamy o nepovedenych pokusech o prihlaseni
      sql_query("DELETE FROM ip_denied WHERE hrac=".$row['id']);
      
      // zapiseme ip adresu
      if($row2 = sql_one_row("SELECT * FROM ip_login WHERE hrac=".$row['id']." AND ip='".get_cient_ip()."'")) {
        $novy_pocet = $row2['pocet']+1;
        sql_query("UPDATE ip_login SET pocet=".$novy_pocet." WHERE id=".$row2['id']);
      }
      else
        sql_query("INSERT INTO ip_login (hrac, ip) VALUES(".$row['id'].", '".get_cient_ip()."')");
        
      return 'ok';
    }
    else potrestej();
    return 'ban';    
  }
  else {
    // spatne heslo
    sql_query("INSERT INTO ip_denied (hrac, heslo, ip) VALUES(".$row['id'].", '".sql_string($heslo)."', '".get_cient_ip()."')");
    $zbyva_pokusu--;
    if($zbyva_pokusu>0) return html_error('Bylo zadáno špatné heslo. Zbývá ještě '.($zbyva_pokusu).' pokusů na přihlášení a poté bude účet
                          z bezpečnostních důvodů automaticky zablokován.')
                          .' '.html_href($ROOT_URL.'/zapomenute_heslo.php?hrac='.$hrac_id, 'Zapomněli jste heslo?');
    else return html_error('Účet byl zablokován z důvodu opakovaných pokusů o přihlášení pod špatným heslem.
                          Pro odblokování kontaktujte administrátora.');                        
  }
}

if(empty($_SESSION['uzivatel']['id']) and isset($_COOKIE["autologin_jmeno"]) and isset($_COOKIE["autologin_heslo"])) { 
  login($_COOKIE["autologin_jmeno"], $_COOKIE["autologin_heslo"]);
}

// login
if($_SERVER['REQUEST_METHOD']=='POST') {
  if(isset($_POST['Prihlasit']))  {
    $LOGIN_ERR = login($_POST['jmeno'], $_POST['heslo']);
    // auto login
    if(isset($_POST['trvale'])) {
      setcookie("autologin_jmeno", $_POST['jmeno'], time() + 20*86400, '/');
      setcookie("autologin_heslo", $_POST['heslo'], time() + 20*86400, '/');
    }  
  }
}

// logout
if(isset($_GET['odhlasit'])) {
  $_SESSION['uzivatel'] = array();
  // smazeme cookies
  setcookie("autologin_jmeno", $_POST['jmeno'], time() -60);
  setcookie("autologin_heslo", $_POST['heslo'], time() -60);  
}

// aktualizace last_loginu
if(!empty($_SESSION['uzivatel']['id'])) {
  sql_query("UPDATE hraci SET posledni_prihlaseni=DEFAULT WHERE id=".$_SESSION['uzivatel']['id']);
}

?>
