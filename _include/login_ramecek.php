<?php
/*
* Ramecek s formularem na prihaseni nebo info o uzivateli
* Includuje se na hlavni stranku, pripadne kamkoliv jinam
*/

$get_params = '';
foreach($_GET as $key => $val) {
  if($key!='odhlasit') {
    if(!empty($get_params)) $get_params .= '&amp;';
    $get_params .= $key.'='.$val;  
  }
}

ramecek_start(286, ($_SESSION['uzivatel']['id'] ? 'Uživatel' : 'Přihlášení'));

// neprihlasen
if(empty($_SESSION['uzivatel']['id'])) {
  table_start('nic', $_SERVER['PHP_SELF'].'?'.$get_params);
  echo "
  <tr>
   <td><i class=\"fas fa-user\"></i></td>
   <td>" . input_text('jmeno') . "</td>
   <td>" . input_submit('Přihlásit', 'Prihlasit', 'submit', 110) . "</td>
  </tr>
  <tr>
   <td><i class=\"fas fa-key\"></i></td>
   <td>" . input_password('heslo') . "</td>
   <td>" . input_checkbox('trvale', '1') . " trvalé přihlášení</td>
  </tr>
  <tr>
   <td>&nbsp;</i></td>
   <td align='center'>&nbsp;&nbsp;<br><a href='" . $ROOT_URL . "/registrace.php' style='text-decoration: underline;'>Založit nový účet </a></td>
   <td>&nbsp;</td>
  </tr> 
  ";
  table_end();
}
// prihlasen
else {
  table_start('nic');
  
  echo "
  <tr>
   <td>
	<div style = \" padding-right: 10px; padding-left: 20px;\" >
	 " . avatar($_SESSION['uzivatel']['id']) . "
	</div>
   </td>
   <td style = \"vertical-align: top;\">
    <table class = 'nic' style = 'marign-left: 5px;'>
	 <tr>
	  <td style = 'font-weight: bold; font-size: 15px; '>
		" . html_href($ROOT_URL . "/hraci/detail.php?id=" . $_SESSION['uzivatel']['id'], $_SESSION['uzivatel']['jmeno']) . "
	  </td>	  
	 </tr>
	 <tr>
	  <td>
	    " . $_SESSION['uzivatel']['skupina_nazev'] ."
	  </td>	  
	 </tr>		 
	 <tr>
	  <td>
	   ELO: " . $_SESSION['uzivatel']['elo'] . "
	  </td>	  
	 </tr>
	 <tr>
	  <td>
	   Hry: " . $_SESSION['uzivatel']['celkem_her'] . "
	  </td>	  
	 </tr>
	 <tr>
	  <td>
	   Diskuze: " . $_SESSION['uzivatel']['prispevku'] . "
	  </td>	  
	 </tr>
	</table>
   </td>
  </tr>
  ";
  table_end();
 
  echo "
	<br>
	<div style = \" padding-right: 10px; padding-left: 21px;\" >   
	   <a href = '" . $ROOT_URL . "/hry/pridat_hru.php' class = 'button' > <i class=\"fas fa-plus-square\"></i>&nbsp;&nbsp;Přidat hru </a>
	   &nbsp;&nbsp;
	   <a href = '" . $_SERVER['PHP_SELF'].'?odhlasit=1&amp;'.$get_params . "' class = 'button' > <i class=\"fas fa-sign-out-alt\"></i>&nbsp;&nbsp;Odhlásit </a>
	</div>
  ";

  
  /* stará tabulka
  table_row(array(html_href($ROOT_URL.'/hraci/detail.php?id='.$_SESSION['uzivatel']['id'], $_SESSION['uzivatel']['jmeno']).'<br/>'
                  .$_SESSION['uzivatel']['skupina_nazev'].'<br/>'
                  .'ELO: '.$_SESSION['uzivatel']['elo'].'<br/>'
                  .'hry: '.$_SESSION['uzivatel']['celkem_her'].'<br/>'
                  .'diskuze: '.$_SESSION['uzivatel']['prispevku'].'<br/>'
                  .html_href($_SERVER['PHP_SELF'].'?odhlasit=1&amp;'.$get_params, '[Odhlásit]'),
                  avatar($_SESSION['uzivatel']['id'])),0,0,array('right', 'left'), array('top', 'top'));
  */  


}

if(isset($LOGIN_ERR) and $LOGIN_ERR!='ok') echo $LOGIN_ERR;
ramecek_end(286);

?>

