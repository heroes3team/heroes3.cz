<?php

function get_cient_ip() {
	
	$ip = $_SERVER['REMOTE_ADDR'];
	
	if($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
	}
	
	return $ip;
}

?>
