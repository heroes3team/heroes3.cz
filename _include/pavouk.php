<?php

// vypise tabulku - pavouka

function sloupce($radek, $pocet_kol) {
  if($radek==0) return $pocet_kol+1;
  
  $dvojek = 0;
  $radek2 = $radek;
  while(($radek2/2)==floor($radek2/2)) {
  $radek2 /= 2;
  $dvojek++;
  }
  return $dvojek+1;
}

function pavouk($pocet_radku, $hraci = array(), $nadpis = false, $styl = '') {
  $pocet_kol = log($pocet_radku, 2);

  $html = "<table ".$styl." border=\"1\">";
  
  if($nadpis) {
    $html .= "\n<tr>";
  
    for($i=0; $i<$pocet_kol+1; $i++) {
      if($i==$pocet_kol-3) $html .= "<th style=\"color: #FF7000; text-align: center;\">Čvtrtfinále</th>";
      elseif($i==$pocet_kol-2) $html .= "<th style=\"color: #FF7000; text-align: center;\">Semifinále</th>";    
      elseif($i==$pocet_kol-1) $html .= "<th style=\"color: #FF7000; text-align: center;\">Finále</th>";    
      elseif($i==$pocet_kol) $html .= "<th style=\"color: #FF7000; text-align: center;\">Vítěz</th>";
      else $html .= "<th style=\"color: #FF7000; text-align: center;\">".($i+1).". kolo</th>";
    }
    $html .= "</tr>"; 
  }
  
  for($i=0; $i<$pocet_radku; $i++) {
    $html .= "\n<tr>";
    for($j=0; $j<sloupce($i, $pocet_kol); $j++) {
      $html .= "<td align=\"center\" rowspan=\"".pow(2, $j)."\" style=\"width: "."100pt;\">";
      //$html .= "<div style=\"width: ".(100/$pocet_kol+1)."%;\">";
      if(empty($hraci[$i][$j]))
        $html .= "&nbsp;?&nbsp;";
      else 
        $html .= "&nbsp;".$hraci[$i][$j]."&nbsp;";
      //$html .= "</div>";
      $html .= "</td>";        
    }
    $html .= "</tr>";
  }
  $html .= "</table>";
  return $html;
}

?>
