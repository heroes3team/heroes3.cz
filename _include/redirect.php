<?php

function  header_redirect($location, $relativne = 1)
{
  if ($relativne)
  {
    $uri = explode('?', $_SERVER['REQUEST_URI']);
    $path = SubStr($uri[0], 0, StrRPos($uri[0], '/'));
    $path .= '/' . $location;
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')
      $protocol = 'https';
    else
      $protocol = 'http';
    Header("Location: $protocol://$_SERVER[HTTP_HOST]$path");
  }
  else
  {
    Header("Location: $location");
  }
}

function javascript_redirect($url) {
echo "
<script type=\"text/javascript\">
//<!--
	 window.location.href=\"".$url."\";     
//-->
</script>	
";
}

function metatag_redirect($url) {
echo "
<meta http-equiv=\"refresh\" content=\"3;url=".$url."\">
";
}

?>
