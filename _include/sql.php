<?php

require($ROOT_DIR.'/dbaccess.php');

function db_open() {
  global $dbhost, $dbuser, $dbpassword, $dbname;
  mysql_connect($dbhost, $dbuser, $dbpassword) or die('nejde spojit '.mysql_error());
  mysql_select_db($dbname) or die(mysql_error());
  mysql_query ('SET NAMES UTF8');
}

function db_close() {
  mysql_close() or die(mysql_error());
}

function sql_query($command) {
  //$command = iconv("ISO-8859-2", "UTF-8", $command);
  $result = mysql_query($command) or die(mysql_error());
  return $result;
}

function sql_fetch_array($rows) {
  $array = mysql_fetch_array($rows);
  /*if(!empty($array)) {
    foreach($array as $key => $col) $array[$key] = iconv("UTF-8", "ISO-8859-2", $array[$key]);  
  }*/
  return $array;
}

function sql_num_rows($rows) {
  return mysql_num_rows($rows);
}

function sql_one_row($command) {
  $sql = sql_query($command);
  if(sql_num_rows($sql)>0) {
    $row = sql_fetch_array($sql);
    return $row;
  }
  else return false;
}

function sql_getfield($command, $field) {
  $row = sql_one_row($command);
  return $row[$field];
}

function sql_exists($command) {
  $sql = sql_query($command);
  if(sql_num_rows($sql)>0) return true;
  else return false;
}

function sql_begin() {
  return sql_query("BEGIN");
}
function sql_commit() {
  return sql_query("COMMIT");
}
function sql_rollback() {
  return sql_query("ROLLBACK");
}

function sql_string($s) {
  return mysql_real_escape_string($s);
}

?>
