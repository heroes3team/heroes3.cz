<?php
/**
 * Aktivace noveho uctu.
 **/

include('_common_start.php');

html_start();

sloupec_start();

ramecek_start(1024, 'Aktivace účtu');
echo "<div style=\"margin-left: 5px;\">";
if($hrac = sql_one_row("SELECT * FROM hraci WHERE id=".intval($_GET['hrac']))) {
  if(!$hrac['aktivni']) {
    if($hrac['popis']==$_GET['kod']) {
      if($upd = sql_query("UPDATE hraci SET aktivni=TRUE, popis='' WHERE id=".intval($_GET['hrac']))) {
        echo "Účet byl úspěšně aktivován. Nyní se můžete přihlásit";
      }
      else echo "Nepodařilo se aktivovat účet.";
    }
    else echo "Špatný aktivační kód.";
  }
  else echo "Účet je již aktivní.";
}
echo "</div>";
ramecek_end(1024);

sloupec_end();

html_end();

?>
