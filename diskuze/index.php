<?php
/*
* Domovska stranka ligy
*/

include('../_common_start.php');

$TITLE = 'Diskuze';
$MODUL = 'diskuze';

$sql = sql_query("SELECT * FROM  forum_vlakna ORDER BY id;");
$vlakna = array();
while($row = sql_fetch_array($sql)) {
	$vlakna[$row['id']] = $row;
}

if(isset($_GET['vlakno'])) {
	$vlakno = intval($_GET['vlakno']);
} else {
	$vlakno = 1;
}

if(isset($_GET['page'])) {
	$page = intval($_GET['page']);
} else {
	$page = 1;
}

# posts per page
$ppp = 20;
$total = sql_getfield(sprintf("SELECT COUNT(*) as count FROM forum_prispevky AS f JOIN hraci AS h ON h.id=f.vlozil WHERE f.vlakno=%d",$vlakno), "count");
$last_page = ceil($total / $ppp);

html_start();

# levy sloupec
sloupec_start(310);
# login / uzivatel.panel
include($INCLUDE_DIR.'/login_ramecek.php');

ramecek_start(286, 'Vlákna');
	foreach($vlakna as $id => $vlakno1) {
	 html_podklad_start();
	  table_start('nic');
		table_row(array("<strong>".html_href('?vlakno='.$vlakno1['id'], $vlakno1['nazev'], '','','','button2')."</strong>"," &nbsp; &nbsp;",$vlakno1['popis']));
	  table_end();
	 html_podklad_end();
	}
	
ramecek_end(286);
sloupec_end();

# pravy sloupec
sloupec_start();
ramecek_start(1024, $vlakna[$vlakno]['nazev']);
echo '<div style = "padding: 0px 10px 0px 10px; margin-right: 10px;">';
	# ulozit prispevek / nebo upravit
	if($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['submit'])) {

		$errors = '';

		# kontrola prihlaseni uzivatele
		if(empty($_SESSION['uzivatel']['id'])) {
			$errors .= "Nejste přihlášen. Pravděpodobně vypršela platnost vašeho přihlášení. Prosím, přihlašte se znovu. Odeslal jste tento text: ".htmlspecialchars($_POST['text']);
		}
		if(empty($errors)) {
	  
		$update = false;
		if(isset($_POST['update'])) {
		  $row = sql_one_row(sprintf("SELECT * FROM forum_prispevky WHERE vlakno=%d ORDER BY vlozeno DESC, id DESC LIMIT 1;", $vlakno));
		  if($row['vlozil']==$_SESSION['uzivatel']['id'] && $row['id']==$_POST['update'] || $_SESSION['uzivatel']['moderator']) {
			$update = true;
		  }    
		}
		
		if($update) {
		  sql_query(sprintf("UPDATE forum_prispevky SET text='%s', vlozeno=NOW() WHERE id=%d AND vlakno=%d",
					sql_string($_POST['text']), $_POST['update'], $vlakno));    
		} else {
			sql_begin();
			$hrac = sql_one_row(sprintf("SELECT * FROM hraci WHERE id=%d", $_SESSION['uzivatel']['id']));
			$ins = sql_query(sprintf("INSERT INTO forum_prispevky (vlozil, vlakno, text) VALUES (%d, %d, '%s')",
					$_SESSION['uzivatel']['id'], $vlakno, sql_string($_POST['text'])));
			$upd = update_pocet_prispevku($_SESSION['uzivatel']['id']);
			if($ins && $upd) {
				sql_commit();
			} else {
				sql_rollback();
			}    
		}
	  

		} else echo html_error($errors);
	}

	# smazat
	if(!empty($_SESSION['uzivatel']['id'])) {
	  if(isset($_GET['smazat'])) {
		$row = sql_one_row(sprintf("SELECT * FROM forum_prispevky WHERE vlakno=%d ORDER BY vlozeno DESC, id DESC LIMIT 1;", $vlakno));
		if($row['vlozil']==$_SESSION['uzivatel']['id'] && $row['id']==$_GET['smazat'] || $_SESSION['uzivatel']['moderator']) {
		  sql_query(sprintf("DELETE FROM forum_prispevky WHERE id=%d AND vlakno=%d", $_GET['smazat'], $vlakno));
		  update_pocet_prispevku($row['vlozil']);
		}
	  }
	}

	# editace
	$editace = false;
	$text = '';
	if(!empty($_SESSION['uzivatel']['id'])) {
	  if(isset($_GET['editovat'])) {
		$row = sql_one_row(sprintf("SELECT * FROM forum_prispevky WHERE vlakno=%d ORDER BY vlozeno DESC, id DESC LIMIT 1;", $vlakno));
		if($row['vlozil']==$_SESSION['uzivatel']['id'] && $row['id']==$_GET['editovat'] || $_SESSION['uzivatel']['moderator']) {
		  $editace = true;
		  $row = sql_one_row(sprintf("SELECT * FROM forum_prispevky WHERE vlakno=%d and id=%d", $vlakno, $_GET['editovat']));
		  $text = $row['text'];
		}
	  }
	}

	# move
	if(!empty($_SESSION['uzivatel']['id'])) {
	  if(isset($_GET['move']) && $_SESSION['uzivatel']['moderator']) {
		sql_query(sprintf("UPDATE forum_prispevky SET vlakno=%d, vlozeno=NOW()
							WHERE id=%d AND vlakno=%d", $_GET['to'], $_GET['move'], $vlakno));
	  }
	}


	$sql = sql_query(sprintf("SELECT f.*, h.jmeno
				FROM forum_prispevky AS f JOIN hraci AS h ON h.id=f.vlozil 
				WHERE f.vlakno=%d
				ORDER BY f.id DESC, f.vlozeno DESC
				LIMIT %d, %d",
				$vlakno,
				($page-1)*$ppp, $ppp));
	
	html_podklad_start();
	 table_start('nic', '.?vlakno='.$vlakno);
		if($_SESSION['uzivatel']['id'] && ($vlakno != 7 || $_SESSION['uzivatel']['admin']) ) {
			$colspan = 2;
			table_row(array(input_textarea('text', htmlspecialchars($text), 6, 90, 920)),array($colspan));
			table_row(array('<br>'));
			table_row(array(input_submit($editace ? 'Uložit změny' : 'Odeslat', 'submit').($editace ? input_hidden('update', $_GET['editovat']) : '')), array($colspan),0,array('center'));
			table_row(array('<br>'));

		}
	 table_end();
	html_podklad_end();
	
	$counter = 0;
	while($row = sql_fetch_array($sql)) {
	 html_podklad_start();
		table_start('nic', '.?vlakno='.$vlakno);
			  $counter++;
			  table_row(array(avatar($row['vlozil']).' '.html_href($ROOT_URL.'/hraci/detail.php?id='.$row['vlozil'],$row['jmeno']),    
				  '<span class="strong_diskuze"> #'.$row['id'].' | '.dt_ts_date_db2user($row['vlozeno']).' | '.dt_ts_time_db2user($row['vlozeno'],2).'</span>'.
				  '<div>'.
					'<div style="height: 78px; float: left;"></div>'.
					'<div class = "text_diskuze"><p class = "paragraph2">'.str_replace("\n", "<br/>", add_links(htmlspecialchars($row['text']))).'</p></div>'.
					'<div style="clear: left;"></div>'.
				  '</div>'.
				  '<div>'.
					($row['vlozil']==$_SESSION['uzivatel']['id']  && $counter==1 || $_SESSION['uzivatel']['moderator'] ? html_href($_SERVER['PHP_SELF'].'?vlakno='.$vlakno.'&amp;page='.$page.'&amp;smazat='.$row['id'], 'smazat', '', '', 'return confirm(\'Opravdu smazat?\');','button') : '').
					($row['vlozil']==$_SESSION['uzivatel']['id']  && $counter==1 || $_SESSION['uzivatel']['moderator'] ? ' '.html_href($_SERVER['PHP_SELF'].'?vlakno='.$vlakno.'&amp;page='.$page.'&amp;editovat='.$row['id'], 'upravit','','','','button') : '').                      
					($_SESSION['uzivatel']['moderator'] ? ' '.html_href($_SERVER['PHP_SELF'].'?vlakno='.$vlakno.'&amp;page='.$page.'&amp;move='.$row['id'].'&amp;to=1', 'move1','','','','button') : '').
					($_SESSION['uzivatel']['moderator'] ? ' '.html_href($_SERVER['PHP_SELF'].'?vlakno='.$vlakno.'&amp;page='.$page.'&amp;move='.$row['id'].'&amp;to=2', 'move2','','','','button') : '').                      
					($_SESSION['uzivatel']['moderator'] ? ' '.html_href($_SERVER['PHP_SELF'].'?vlakno='.$vlakno.'&amp;page='.$page.'&amp;move='.$row['id'].'&amp;to=3', 'move3','','','','button') : '').                      
					($_SESSION['uzivatel']['moderator'] ? ' '.html_href($_SERVER['PHP_SELF'].'?vlakno='.$vlakno.'&amp;page='.$page.'&amp;move='.$row['id'].'&amp;to=4', 'move4','','','','button') : '').                      
					($_SESSION['uzivatel']['moderator'] ? ' '.html_href($_SERVER['PHP_SELF'].'?vlakno='.$vlakno.'&amp;page='.$page.'&amp;move='.$row['id'].'&amp;to=5', 'move5','','','','button') : '').                      
					($_SESSION['uzivatel']['moderator'] ? ' '.html_href($_SERVER['PHP_SELF'].'?vlakno='.$vlakno.'&amp;page='.$page.'&amp;move='.$row['id'].'&amp;to=6', 'move6','','','','button') : '').                      
					($_SESSION['uzivatel']['moderator'] ? ' '.html_href($_SERVER['PHP_SELF'].'?vlakno='.$vlakno.'&amp;page='.$page.'&amp;move='.$row['id'].'&amp;to=7', 'move7','','','','button') : '').                      
				  '</div>'.
				  "<div style=\"width: 600px;\"></div>"),
			   0,array(110),array('center','left'),array('top','top'));
		table_end();
	 html_podklad_end();	
	}


	$last_page = ceil($total / $ppp);
	
	echo '<div style = "padding-left: 60px;"><table style="width: 100%; margin: 0 auto;">';
	table_row(array($page > 1 ? html_href('?vlakno='.$vlakno.'&amp;page=1', '&lt;&lt; první','','','','button') : '<div style = "width: 110px">&nbsp;</div>',
					$page > 1 ? html_href('?vlakno='.$vlakno.'&amp;page='.($page-1), '&lt; předchozí','','','','button') : '<div style = "width: 110px">&nbsp;</div>',
					"<div class = 'podklad' style = 'color: #f4e4ce; width: 85px; display:flex; align-items: center;justify-content:center;'> ".$page." / ".$last_page . "</div>",
					$page < $last_page ? html_href('?vlakno='.$vlakno.'&amp;page='.($page+1), 'další &gt;','','','','button') : '<div style = "width: 75px">&nbsp;</div>',
					$page < $last_page ? html_href('?vlakno='.$vlakno.'&amp;page='.$last_page, 'poslední &gt;&gt;','','','','button') : '<div style = "width: 75px">&nbsp;</div>'
	  ));
	echo '</table></div>';
echo '</div>';
ramecek_end(1024);
sloupec_end();
 
html_end();

?>
