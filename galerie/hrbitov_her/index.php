<?php
/*
* Domovska stranka ligy
*/

include('../../_common_start.php');

$TITLE = 'Galerie';
$MODUL = 'galerie';
$MODUL2 = 'hrbitov_her';

html_start();
sloupec_start(310);
	include($INCLUDE_DIR.'/login_ramecek.php');
	include('../menu.php');
sloupec_end ();

sloupec_start ();
	echo html_nadpis('Hřbitov her');

	$sql = sql_query("SELECT h.*, COUNT(k.id) AS komentare, vit.jmeno AS vitez_jmeno, por.jmeno AS por_jmeno
					  FROM hry AS h JOIN hry_komentare AS k ON k.hra=h.id JOIN hraci AS vit ON h.vitez=vit.id JOIN hraci AS por ON h.porazeny=por.id
					  WHERE souhlas_rozhodciho IS FALSE
					  GROUP BY h.id
					  UNION
					  SELECT h.*, 0 AS komentare, vit.jmeno AS vitez_jmeno, por.jmeno AS por_jmeno
					  FROM hry AS h JOIN hraci AS vit ON h.vitez=vit.id JOIN hraci AS por ON h.porazeny=por.id
					  WHERE souhlas_rozhodciho IS FALSE
						AND NOT EXISTS (SELECT * FROM hry_komentare WHERE hra=h.id)
					  ORDER BY id DESC
					  LIMIT 100;");

	while($hra = sql_fetch_array($sql)) {
	  html_hra($hra);
	}
sloupec_end;

html_end();

?>
