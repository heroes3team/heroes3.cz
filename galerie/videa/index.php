<?php

include('../../_common_start.php');

$TITLE = 'Galerie';
$MODUL = 'galerie';

html_start();

// galerie_menu();

# levy sloupec
sloupec_start(310);
	# login / uzivatel.panel
	include($INCLUDE_DIR.'/login_ramecek.php');
	include('../menu.php');
sloupec_end ();

# pravy sloupec
sloupec_start ();
	ramecek_start(1024, 'Videa');

		echo "<div style=\"margin-left: 5px; margin-right: 15px; height: 3500px\">";
		html_podklad_start();
			echo h2("Info");
			echo html_p("V této sekci najdete kompletní záznamy několika vybraných ligových her z pohledu našeho hráče H34D. Videa jsou doplněna o český komentář, který vysvětluje základy hry i pokročilé taktiky a triky.");
			echo '<br>';
		html_podklad_end ();

		# Vnitřní levý sloupec
		sloupec_start (490);
			html_podklad_start(); html_galerie_video ('30.1.2021','rampart','sephinroth','https://www.youtube.com/watch?v=09ZOpVvig-w','Druhá hra mezi finalisty turnaje World Rules');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('30.12.2020','cove','jeremy','https://www.youtube.com/watch?v=-AslatCv3ak','První hra mezi finalisty turnaje World Rules');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('2.12.2020','cove','cassiopeia','https://www.youtube.com/watch?v=3xYqGuIsK88','Jebus Cross téměř jak má být');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('31.10.2020','conflux','grindan','https://www.youtube.com/watch?v=RScdXjzNpzE','Když se na Jebus Cross nedaří');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('20.8.2020','dungeon','giselle','https://www.youtube.com/watch?v=s9gTRAMPDgc','představení templatu mt_wrzosy');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('1.7.2020','stronghold','gundula','https://www.youtube.com/watch?v=sM2VTnBBVXM','showmatch o 40 dolarů');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('1.6.2020','necropolis','galthran','https://www.youtube.com/watch?v=oBLB-f7C2sQ','Pokročilá hra na Jebus King');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('20.04.2020','stronghold','zubin','https://www.youtube.com/watch?v=Jn9gmOx2Ty4','Představení mapy Arena 2.6.3');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('05.04.2020','inferno','ignatius','https://www.youtube.com/watch?v=p6lTajoIZe4','Představení templatu Jebus Cross na velikosti L');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('14.03.2020','castle','kyrre','https://www.youtube.com/watch?v=cjI0WUfRuEE','Představení templatu Jebus King');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('27.11.2019', 'inferno', 'labetha', 'https://www.youtube.com/watch?v=ov4L0Qc6QmU', 'Jebus Cross se zahraničním hráčem'); html_podklad_end ();
			html_podklad_start(); html_galerie_video ('8.10.2019', 'dungeon', 'grindan', 'https://www.youtube.com/watch?v=uNc_evWJKZg', 'Jebus Cross se zahraničním hráčem'); html_podklad_end ();
			html_podklad_start(); html_galerie_video ('21.8.2019', 'tower', 'solmyr', 'https://www.youtube.com/watch?v=WU_PCt77ICw', 'Představení templatu Jebus Cross'); html_podklad_end ();
			html_podklad_start(); html_galerie_video ('19.5.2019', 'necropolis', 'vidomina', 'https://www.youtube.com/watch?v=iBLeBb2oulE', 'Pár triků pro rychlý vzestup za Necromanty'); html_podklad_end ();		
			html_podklad_start(); html_galerie_video ('24.10.2018', 'tower', 'eovacious', 'https://www.youtube.com/watch?v=yQvhdQI_v7Y', 'Zradlový templat a použití zajimavých kouzel'); html_podklad_end ();
			html_podklad_start(); html_galerie_video ('7.6.2018', 'dungeon', 'dace', 'https://www.youtube.com/watch?v=CcTuwZIYIvo', 'Představení zrcadlového templatu mt_Dom.'); html_podklad_end ();
			html_podklad_start(); html_galerie_video ('24.4.2018', 'cove', 'jeremy', 'https://www.youtube.com/watch?v=LUfrOpVw6ik', 'Představení zrcadlového templatu mt_skirmish.'); html_podklad_end ();
			html_podklad_start(); html_galerie_video ('18.2.2018', 'castle', 'orrin', 'https://www.youtube.com/watch?v=H7x9Z1cWFKs', 'Představení zrcadlového templatu h3dm1.'); html_podklad_end ();			
			html_podklad_start(); html_galerie_video ('31.12.2017', 'castle', 'tyris', 'https://www.youtube.com/watch?v=DTzHyyZS0ms', 'Finálová hra HOTA turnaje'); html_podklad_end ();
			html_podklad_start(); html_galerie_video ('30.10.2017', 'conflux', 'grindan', 'https://www.youtube.com/watch?v=d6LhD0yDUSQ', 'Jeden z nejdelších a nejvíce metodických soubojů v lize'); html_podklad_end ();
			html_podklad_start(); html_galerie_video ('23.9.2017', 'inferno', 'tyris', 'https://www.youtube.com/watch?v=-ITNBeqIbVE', 'Úmorné dobývání hradu'); html_podklad_end ();
			html_podklad_start(); html_galerie_video ('3.9.2017', 'conflux', 'luna', 'https://www.youtube.com/watch?v=ti6ol_MTVu8', 'Představení hry s časovým limitem 1 minuta'); html_podklad_end ();
			html_podklad_start(); html_galerie_video ('6.8.2017', 'tower', 'solmyr', 'https://www.youtube.com/watch?v=DcI_25S_jAU', 'Řada pěkných, dlouhých bitev mezi silnými hrdiny'); html_podklad_end ();
		sloupec_end ();
		
		# Vnitřní pravý sloupec
		sloupec_start (490);
			html_podklad_start(); html_galerie_video ('26.6.2017', 'stronghold', 'jeddite', 'https://www.youtube.com/watch?v=483LJmqtFOE', 'Těsná bitva do posledního muže'); html_podklad_end ();
			html_podklad_start(); html_galerie_video ('10.6.2017', 'cove', 'mephala', 'https://www.youtube.com/watch?v=YrgHXGfaYso', 'První video v datadisku Horn of the Abyss. Pozor: jedná se pouze o záznam živého streamu!'); html_podklad_end ();			
			html_podklad_start(); html_galerie_video ('4.6.2017','fortress','tazar','https://www.youtube.com/watch?v=tDNtVr8e1IA','Průvodce asi nejdelší bitvou se soupeřem, kterou můžete v 1. a 2. týden hry hrát.');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('8.5.2017','castle','kyrre','https://www.youtube.com/watch?v=ls5OZq1dDOY','Trošku rychlejší hra se zajímavou výměnou hradů zakončená obléháním.');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('7.4.2017','inferno','pyre','https://www.youtube.com/watch?v=wzz9zy87480','Poměření sil s poměrně kvalitním youtuberem ze Slovenska, který má na kontě téměř stovku Heroes videí.');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('13.3.2017','necropolis','galthran','https://www.youtube.com/watch?v=JH-Dp6kQOIE','Pro zpestření jsme vyzkoušeli souboj "zakázaného" Nekropolisu versus diplomacie.');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('28.1.2017','fortress','tazar','https://www.youtube.com/watch?v=_U7LOISZPpY','Extrémně vyrovnaná hra s napínavými bitvami.');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('11.1.2017','inferno','pyre','https://www.youtube.com/watch?v=0vX6-eTBh9E','Blesková válka po nečekaně brzkém střetu se soupeřem.');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('24.12.2016','stronghold','crag%20hack','https://www.youtube.com/watch?v=w6RWrWc5Ajs','Síla proti magii a hned několik vyrovnaných bitev včetně obléhání hradu');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('11.12.2016','tower','neela','https://www.youtube.com/watch?v=sjHh7962-1M','Něco málo k pohybu po mapě a k vyváženosti mezi silou a magií');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('24.11.2016','tower','solmyr','https://www.youtube.com/watch?v=rpjCY92h0pA','Napínavá přestřelka dvou mocných mágů až do poslední vteřiny!');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('15.5.2016','dungeon','shakti','https://www.youtube.com/watch?v=wI5XcYjQxxo','Souboj titánů, aneb ustojí Shakti hned několik střetů s mocným Galthranem?');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('2.5.2016','inferno','marius','https://www.youtube.com/watch?v=FvPH0FuV5nM','Ještě jednou harvestování demonů a tentokrát pořádně!');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('21.4.2016','conflux','ciele','https://www.youtube.com/watch?v=8dW-B4qKs3E','Odbočka k „zakázané“ rase ve spojení s pořádně nabroušenou magickou šipkou');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('8.4.2016','inferno','marius','https://www.youtube.com/watch?v=vOqD1i_OY6Y','Pokročilá taktika harvestování demonů za podceňované Peklo v akci');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('27.3.2016','fortress','tazar','https://www.youtube.com/watch?v=Hm5D0MYNSkM','Když největší obránce hry najde hned několik stromů na wyverny je z toho masakr');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('14.3.2016','tower','solmyr','https://www.youtube.com/watch?v=2y0DatuC1cc','Zákeřná a velice efektivní taktika hit and run v akci');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('6.3.2016','castle','tyris','https://www.youtube.com/watch?v=f1a2KnEwsiE','Zářný příklad toho, proč nikdy není radno podcenit soupeře');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('26.2.2016','castle','loynis','https://www.youtube.com/watch?v=1gW7qo91uig','Zábavná oddychovka s brzkým střetem se soupeřem');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('14.2.2016','dungeon','shakti','https://www.youtube.com/watch?v=r3hiKblTuG4','Shakti by měl být jasná volba pro každého, kdo se Heroes 3 teprve učí…');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('23.1.2016','rampart','ivor','https://www.youtube.com/watch?v=PZpSV4_4pRE','Tady se mrkneme zejména na to, jak v základech zvládnout pohyb po mapě');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('19.1.2016','rampart','ivor','https://www.youtube.com/watch?v=0_vYBCha978','Video s tipy a triky spíše pro začínající hráče');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('4.7.2012','stronghold','crag%20hack','https://www.youtube.com/watch?v=B2iqsuEwvcg','Názorná ukázka, jak to vypadá, když vám jde mapa ve všem na ruku');   html_podklad_end ();
			html_podklad_start(); html_galerie_video ('29.6.2012','necropolis','galthran','https://www.youtube.com/watch?v=sJk-rQtpjU4','První zkušební video, které bylo vytvořeno pouze jako záznam živého streamu.');   html_podklad_end ();
		sloupec_end ();

		echo "</div>";

	ramecek_end(1024);
sloupec_end ();

html_end();

?>
<script>
/* Načtení originálu */
function youTube() {
	var ifr = document.createElement("iframe");
	ifr.width = 180;
	ifr.height = 120;
	ifr.style = "  border: 1px solid #3d3021; outline: 2px solid #685338;";
	ifr.src = "https://www.youtube.com/embed/" + getYtCode(this.href);
	this.parentNode.appendChild(ifr);
	this.parentNode.removeChild(this);
	
}

/* Vytažení kódu videa */
function getYtCode(url) {
    return url.match(/\?v=([A-z0-9_-]*)/)[1];
}

/* Procházení odkazy a přilepení funkčnosti */
var a = document.links;
for (var i = 0; i < a.length; i++) {
  if (a[i].className == "yt") {
    a[i].className = "";
    var div = document.createElement("div");
    div.className = "yt";
    div.style.backgroundImage = "url(https://img.youtube.com/vi/" + getYtCode(a[i].href) + "/sddefault.jpg)";
    a[i].parentNode.insertBefore(div, a[i]);   
    var span = document.createElement("span");
    span.appendChild(document.createTextNode(a[i].innerHTML));
    a[i].innerHTML = "";
    a[i].appendChild(span);
    div.appendChild(a[i]);
    a[i].onmouseover = youTube;
  }
}
</script>
