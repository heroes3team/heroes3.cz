<?php
/*
* Domovska stranka ligy
*/

include('../_common_start.php');

$TITLE = 'Informace o hře';
$MODUL = 'h3_info';

html_start();

ramecek_start(1024, 'Heroes 3 Info');
echo "<div style=\"margin-left: 10px; margin-right: 10px;\">";

echo html_p("Tady by v budoucnu mělo být info o hře. Než bude doplněno, můžete si ho najít třeba zde:");
echo html_href("http://heroes.thelazy.net/", "http://heroes.thelazy.net/");

echo "</div>";
ramecek_end(1024);

html_end();

?>
