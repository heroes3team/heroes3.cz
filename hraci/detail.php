<?php
/*
* Detail hrace, upravy profilu
*/

include('../_common_start.php');

$TITLE = 'Hráči';
$MODUL = 'hraci';

$page = 1;
if(intval($_GET['page'])>0) $page = intval($_GET['page']);

$hra_id=intval($_GET['id']);

# ulozeni zmen profilu
if($_SERVER['REQUEST_METHOD']=='POST' and isset($_POST['submit']) and ($_SESSION['uzivatel']['id']==$hra_id or $_SESSION['uzivatel']['admin'])) {

  $errors = '';

  # kontrola hesla
  if(!empty($_POST['heslo1'])) {
    if($_POST['heslo1']!=$_POST['heslo2']) {
      $errors .= "Heslo a kontrola hesla se neshodují.<br/>";
    }
  }
  
  # kontrola avatara
  $file = $_FILES['avatar']['tmp_name'];
  if(is_uploaded_file($file)) {
    $info = GetImageSize($file);
    if($info[2]!=2)
      $errors .= "Špatný formát obrázku.<br/>";
    $size = filesize($file);
    if($size>50*1024)
      $errors .= "Obrázek je příliš velký.<br/>"; 
  }
  
  # UPDATE
  if(empty($errors)) {
  
    sql_query("UPDATE hraci SET 
      email='".sql_string($_POST['email'])."',
      icq='".sql_string($_POST['icq'])."',
      hamachi='".sql_string($_POST['hamachi'])."',
      gameranger='".sql_string($_POST['gameranger'])."',
      popis='".sql_string($_POST['popis'])."'
      WHERE id=".$hra_id);
      
    if(!empty($_POST['heslo1'])) {
      sql_query("UPDATE hraci SET 
        heslo='".h3hash($_POST['heslo1'])."'
        WHERE id=".$hra_id);
      sql_query(sprintf("DELETE from ip_denied WHERE hrac=%d", $hra_id));
    }
    
    if($_SESSION['uzivatel']['admin']) {
      sql_query("UPDATE hraci SET 
        admin=".(isset($_POST['admin']) ? 'TRUE' : 'FALSE').",
        moderator=".(isset($_POST['moderator']) ? 'TRUE' : 'FALSE').",
        rozhodci=".(isset($_POST['rozhodci']) ? 'TRUE' : 'FALSE').",
        ban=".(isset($_POST['ban']) ? 'TRUE' : 'FALSE')."
        WHERE id=".$hra_id);    
    }
    
    # refresh
    $_SESSION['uzivatel'] = sql_one_row("SELECT * FROM hraci WHERE id=".intval($_SESSION['uzivatel']['id']));
    
    if(is_uploaded_file($file)) {
      $oldfile = $ROOT_DIR.'/__big_data/hraci_avatar/'.$hra_id.'.jpg';
      if(file_exists($oldfile)) unlink($oldfile);
      $newfile = fopen($ROOT_DIR.'/__big_data/hraci_avatar/'.$hra_id.".jpg", "w+") or javascript_alert("soubor nelze vytvorit");
      $fp = fopen($file, "rb") or javascript_alert("nelze otevrit docasny soubor");
      $content = fread($fp, $size) or javascript_alert("nelze precist docasny soubor");
      fwrite($newfile, $content) or javascript_alert("nelze vepsat data do noveho souboru");
      fclose($newfile);
      fclose($fp);
    }        
  }
}

# ulozeni medaile
if($_SERVER['REQUEST_METHOD']=='POST' and isset($_POST['submit2']) and $_SESSION['uzivatel']['admin']) {

  sql_query("INSERT into hraci_medaile (udelil, hrac, medaile, popis)
              VALUES (".$_SESSION['uzivatel']['id'].", ".$hra_id.", '".sql_string($_POST['oceneni'])."', '".sql_string($_POST['popis'])."')");

}

# smazani medaile
if(isset($_GET['smaz_medaili']) and $_SESSION['uzivatel']['admin']) {
  sql_query("DELETE FROM hraci_medaile WHERE id=".intval($_GET['smaz_medaili']));
}


$hrac = sql_one_row("SELECT h.*, s.nazev AS skupina_nazev 
                     FROM hraci AS h JOIN skupiny AS s ON s.id=h.skupina
                     WHERE h.id=".$hra_id);
                     
if(!$hrac) header_redirect($ROOT_URL.'/chyba.php', 0);

# poradi
$poradi = 1;
$sql = sql_query("SELECT id FROM hraci ORDER BY skupina, body DESC, elo DESC, id");
while($row = sql_fetch_array($sql) and $row['id']!=$hra_id) {
  $poradi++;
}

html_start();

# levy sloupec
sloupec_start(340);
# login / uzivatel.panel
include($INCLUDE_DIR.'/login_ramecek.php');

# Udělit ocenění / medaile

if($_SESSION['uzivatel']['admin'] and isset($_GET['editovat'])) {
 ramecek_start(286,'Udělit ocenění');	 
 $oceneni_select = array();
  foreach($medaile as $m) $oceneni_select[$m['zkratka']]=$m['nazev'];
	  table_start('nic', $_SERVER['PHP_SELF'].'?id='.$hra_id);
		  table_row(array('ocenění:', input_select('oceneni', $oceneni_select)),array(1,1),0, array('right', 'left'));
		  table_row(array('popis:', input_textarea('popis', '',6,22)),array(1,2),0, array('right', 'left'));
		  table_row(array(input_submit('Udělit', 'submit2')),array(2),0,0,0,1);
	  table_end();
	ramecek_end (286);
}

# seznam IP adres - jen pro admina/rozhodciho/moderatora
if($_SESSION['uzivatel']['admin'] or $_SESSION['uzivatel']['rozhodci'] or $_SESSION['uzivatel']['moderator']) {
	ramecek_start(286,'Seznam IP adres');	 
	$sql = sql_query(sprintf("SELECT ip, pocet FROM ip_login WHERE hrac=%d ORDER BY pocet DESC", $hra_id));
	table_start_simple('tab_top');
			table_row(array('IP adresa', '# přihlášení'),array(1,1),0,0,0,1);
			while($row = sql_fetch_array($sql)) {
				table_row(array($row['ip'], $row['pocet']+1),array(1,1),0, array('left', 'right'));
			}			
	table_end_simple();
	ramecek_end (286);
}


sloupec_end();

# prostredni slopec
sloupec_start();
echo html_nadpis($hrac['jmeno']);
ramecek_start(490, 'Hráčský profil');

if(!empty($errors)) echo html_error($errors);

if(isset($_GET['editovat']) and ($_SESSION['uzivatel']['id']==$hra_id or $_SESSION['uzivatel']['admin'])) {

  if(isset($_GET['odstranit_avatara']) and file_exists($ROOT_DIR.'/hraci/avatar/'.$hra_id.'.jpg'))
    unlink($ROOT_DIR.'/hraci/avatar/'.$hra_id.'.jpg');

  table_start('tab', $_SERVER['PHP_SELF'].'?id='.$hra_id);
  table_row(array('OBECNÉ'),array(3),0,0,0,1);
  table_row(array('jméno:',  html_href($_SERVER['PHP_SELF'].'?id='.$hrac['id'],$hrac['jmeno']),avatar($hra_id).
    (file_exists($ROOT_DIR.'/hraci/avatar/'.$hra_id.'.jpg') ? '<br/>'.html_href($_SERVER['PHP_SELF'].'?id='.$hra_id.'&amp;editovat=1&amp;odstranit_avatara=1','[odstranit]') : '').'<br/>vložit avatara<br/>'.input_file('avatar',15).'<br/>pouze formát jpeg a velikost do 50 kB'),array(1,1,1),0, array('right', 'left', 'center'),0,0,0,0,0,array(1,1,5));
  table_row(array('registrace:', dt_ts_date_db2user($hrac['registrace_cas'])),array(1,1),0, array('right', 'left'));
  table_row(array('poslední&nbsp;přihlášení:', dt_ts_date_db2user($hrac['posledni_prihlaseni']).' '.dt_ts_time_db2user($hrac['posledni_prihlaseni'])),array(1,1),0, array('right', 'left'));
  table_row(array('změnit heslo:', input_password('heslo1', '')),array(1,1),0, array('right', 'left'));
  table_row(array('kontrola hesla:', input_password('heslo2', '')),array(1,1),0, array('right', 'left'));
  
  table_row(array('HERNÍ STATISTIKA'),array(3),0,0,0,1);
  table_row(array('pořadí v žebříčku:', $poradi),array(1,2),0, array('right', 'left'));
  table_row(array('skupina:', $hrac['skupina_nazev'].' '.html_image($IMG.'/skupiny/'.$hrac['skupina'].'.png', $hrac['skupina_nazev'], $hrac['skupina_nazev'], "cursor: help;")),array(1,2),0, array('right', 'left'));
  table_row(array('body:', $hrac['body']),array(1,2),0, array('right', 'left'));
  table_row(array('ELO:', $hrac['elo']),array(1,2),0, array('right', 'left'));
  table_row(array('vyhraných her:', $hrac['vyhranych_her']),array(1,2),0, array('right', 'left'));
  table_row(array('celkem her:', $hrac['celkem_her'].' ('.zkusenost($hrac['celkem_her']).')'),array(1,2),0, array('right', 'left'));
  table_row(array('úspěšnost:', round($hrac['uspesnost']).'%'),array(1,2),0, array('right', 'left'));
  
  table_row(array('KONTAKT'),array(3),0,0,0,1);
  table_row(array('registrační e-mail:', $hrac['registrace_mail']),array(1,2),0, array('right', 'left'));
  table_row(array('e-mail:', input_text('email', $hrac['email'])),array(1,2),0, array('right', 'left'));
  table_row(array('ICQ:', input_text('icq', $hrac['icq'])),array(1,2),0, array('right', 'left'));
  table_row(array('Hamachi:', input_text('hamachi', $hrac['hamachi'])),array(1,2),0, array('right', 'left'));
  table_row(array('GameRanger:', input_text('gameranger', $hrac['gameranger'])),array(1,2),0, array('right', 'left'));
  table_row(array('popis:', input_textarea('popis', $hrac['popis'],6,40)),array(1,2),0, array('right', 'left'));
  
  if($_SESSION['uzivatel']['admin']) {
    table_row(array(input_checkbox('admin', 1, $hrac['admin']).' admin&nbsp;&nbsp;'.input_checkbox('moderator', 1, $hrac['moderator']).' moderátor&nbsp;&nbsp;'.input_checkbox('rozhodci', 1, $hrac['rozhodci']).' rozhodčí&nbsp;&nbsp;'.input_checkbox('ban', 1, $hrac['ban']).' ban'),array(3),0, array('left'));
  }
  
  table_row(array(input_submit('Uložit', 'submit')),array(3),0,0,0,1);

  
  table_end();
}
else {

  
  if($hrac['ban']) echo '<img style="position:absolute; top: 450px;  margin-left: 50px;  width:200px;" alt="ban" src="'.$IMG.'/misc/ban.png" />';
  
  html_podklad_start ();
  table_start_simple('tab_top');

	  table_row(array('OBECNÉ'),array(3),0,0,0,1);
	  table_row(array('jméno:', html_href($_SERVER['PHP_SELF'].'?id='.$hrac['id'],$hrac['jmeno']),avatar($hra_id)),array(1,1,1),0, array('right', 'left', 'center'),0,0,0,0,0,array(1,1,5));
	  table_row(array('registrace:', dt_ts_date_db2user($hrac['registrace_cas'])),array(1,1),0, array('right', 'left'));
	  table_row(array('poslední&nbsp;přihlášení:', dt_ts_date_db2user($hrac['posledni_prihlaseni']).' '.dt_ts_time_db2user($hrac['posledni_prihlaseni'])),array(1,1),0, array('right', 'left'));
	  table_row(array('příspěvků:', $hrac['prispevku'].' ('.ukecanost($hrac['prispevku']).')'),array(1,1),0, array('right', 'left'));
	  table_row(array('karma:', $hrac['karma'].' ('.karma($hrac['karma']).')'),array(1,1),0, array('right', 'left'));

  table_end_simple();
  html_podklad_end ();

  html_podklad_start ();
  table_start_simple('tab_top');
	  
	  table_row(array('HERNÍ STATISTIKA'),array(3),0,0,0,1);
	  table_row(array('pořadí v žebříčku:', $poradi),array(1,2),0, array('right', 'left'));
	  table_row(array('skupina:', html_image($IMG.'/skupiny/'.$hrac['skupina'].'.png', $hrac['skupina_nazev'], $hrac['skupina_nazev'], "cursor: help;")),array(1,2),0, array('right', 'left'));
	  table_row(array('body:', $hrac['body']),array(1,2),0, array('right', 'left'));
	  table_row(array('ELO:', $hrac['elo']),array(1,2),0, array('right', 'left'));
	  table_row(array('vyhraných her:', $hrac['vyhranych_her']),array(1,2),0, array('right', 'left'));
	  table_row(array('celkem her:', $hrac['celkem_her'].' ('.zkusenost($hrac['celkem_her']).')'),array(1,2),0, array('right', 'left'));
	  table_row(array('úspěšnost:', round($hrac['uspesnost']).'%'),array(1,2),0, array('right', 'left'));
  
  table_end_simple();
  html_podklad_end (); 
  
  html_podklad_start ();
  table_start_simple('tab_top');  
  
	  table_row(array('KONTAKT'),array(3),0,0,0,1);
	  if($_SESSION['uzivatel']['id']==$hra_id or $_SESSION['uzivatel']['admin'])
		table_row(array('registrační e-mail:', $hrac['registrace_mail']),array(1,2),0, array('right', 'left'));
	  table_row(array('e-mail:', $hrac['email']),array(1,2),0, array('right', 'left'));
	  table_row(array('ICQ:', $hrac['icq']),array(1,2),0, array('right', 'left'));
	  table_row(array('Hamachi:', $hrac['hamachi']),array(1,2),0, array('right', 'left'));
	  table_row(array('GameRanger:', $hrac['gameranger']),array(1,2),0, array('right', 'left'));
	  table_row(array('popis:', str_replace("\n", "<br/>", add_links(htmlspecialchars($hrac['popis'])))),array(1,2),0, array('right', 'left'));
  
  table_end_simple();
  html_podklad_end ();  
  
  html_podklad_start ();
  table_start_simple('tab_top');  
  
	  # získaná ocenění
	  table_row(array('ZÍSKANÁ OCENĚNÍ'),array(3),0,0,0,1);
  
  $sql = sql_query("SELECT * FROM hraci_medaile WHERE hrac=".$hra_id." ORDER BY udeleno");
  while($row = sql_fetch_array($sql)) {
   echo html_medaile($row)."<br/>";
  }
  
  $dalsi_funkce = dalsi_funkce($hrac);
  if(!empty($dalsi_funkce))
    table_row(array('funkce:', implode(', ', $dalsi_funkce)),array(1,2),0, array('right', 'left'));
  
  table_end_simple();
  html_podklad_end ();
  echo '<br>';
  if($_SESSION['uzivatel']['id']==$hra_id or $_SESSION['uzivatel']['admin']) {
    echo "<a href ='" . $_SERVER['PHP_SELF'] . "?id=" . $hra_id . "&amp;editovat=1" . "' class = 'button' style = 'margin-left: 180px; '> Upravit profil </a>";
  }
}

ramecek_end(490);

sloupec_end();


# pravy sloupec
sloupec_start();

echo html_nadpis("Odehrané hry").'<br>';



$sql = sql_query("SELECT
				h.*,
				COUNT(k.id) AS komentare,
				vit.jmeno AS vitez_jmeno,
				por.jmeno AS por_jmeno
				FROM
				hry AS h
				JOIN hry_komentare AS k ON k.hra=h.id
				JOIN hraci AS vit ON h.vitez=vit.id
				JOIN hraci AS por ON h.porazeny=por.id
				WHERE
				potvrzeno IS NOT NULL and vitez = " . $hra_id . "
				or potvrzeno IS NOT NULL and porazeny = " . $hra_id . "
				GROUP BY h.id
				
				UNION
				
				SELECT h.*,
				0 AS komentare,
				vit.jmeno AS vitez_jmeno,
				por.jmeno AS por_jmeno
				FROM
				hry AS h
				JOIN hraci AS vit ON h.vitez=vit.id
				JOIN hraci AS por ON h.porazeny=por.id
				WHERE
				potvrzeno IS NOT NULL and vitez = " . $hra_id . " AND NOT EXISTS (SELECT * FROM hry_komentare WHERE hra=h.id)
				or potvrzeno IS NOT NULL and porazeny = " . $hra_id . "	AND NOT EXISTS (SELECT * FROM hry_komentare WHERE hra=h.id)
				ORDER BY potvrzeno DESC, id DESC");

$her = sql_num_rows($sql);
$limit = 5;
$last_page = ceil($her / $limit);
# nechame odjet prvnich (page-1) * limit zaznamu.
for($i=0; $i<($page-1)*$limit; $i++) {
  sql_fetch_array($sql);
}

$i = 0;               
while($i<$limit and $hra = sql_fetch_array($sql)) {
  $i++;
  html_hra($hra);
  echo '<br>';
}

echo '<div class="podklad2">';
echo '<table style="width: 100%">';
table_row(array($page > 1 ? html_href($_SERVER['PHP_SELF'].'?page=1&id='.$hra_id, '&lt;&lt; první', '','','','button') : '<div style = "width: 75px">&nbsp;</div>',
                $page > 1 ? html_href($_SERVER['PHP_SELF'].'?page='.($page-1).'&id='.$hra_id, '&lt; předchozí','','','','button') : '<div style = "width: 75px">&nbsp;</div>',
				"<div class = 'podklad' style = 'color: #f4e4ce'>strana ".$page." z ".$last_page . "</div>",
                $page < $last_page ? html_href($_SERVER['PHP_SELF'].'?page='.($page+1).'&id='.$hra_id, 'další &gt;','','','','button') : '<div style = "width: 75px">&nbsp;</div>',
                $page < $last_page ? html_href($_SERVER['PHP_SELF'].'?page='.$last_page.'&id='.$hra_id, 'poslední &gt;&gt;','','','','button') : '<div style = "width: 75px">&nbsp;</div>'
  ));
echo '</table></div>';

sloupec_end();

?>

<script type="text/javascript">
<!-- smazeme prohlizecem vyplnene heslo -->
var elements = document.getElementsByName('heslo1');
var element = elements[0];
element.value = '';
</script>


<?php

html_end();

?>



