<?php
/*
* Domovska stranka ligy
*/

include('../_common_start.php');

$TITLE = 'Hráči';
$MODUL = 'hraci';

$razeni = 'DESC';
$opacne_razeni = 'ASC';
if($_GET['razeni']=='ASC') {
  $razeni = 'ASC';
  $opacne_razeni = 'DESC';
}
$page = intval($_GET['page']) ? intval($_GET['page']) : 1;


switch($_GET['order']) {
  case 'jmeno': $sql_order = 'h.jmeno '.$razeni.', h.id ASC'; break;
  case 'elo': $sql_order = 'h.elo '.$razeni.', h.id ASC'; break;
  case 'vitezstvi': $sql_order = 'h.vyhranych_her '.$razeni.', h.id ASC'; break;
  case 'celkem_her': $sql_order = 'h.celkem_her '.$razeni.', h.id ASC'; break;
  case 'uspesnost': $sql_order = 'h.uspesnost '.$razeni.', h.id ASC'; break;
  case 'registrace': $sql_order = 'h.registrace_cas '.$razeni.', h.id ASC'; break;
  case 'karma': $sql_order = 'h.karma '.$razeni.', h.id ASC'; break;
  case 'prispevky': $sql_order = 'h.prispevku '.$razeni.', h.id ASC'; break;
  default: $sql_order = "h.skupina ".$opacne_razeni.", h.body ".$razeni.", h.elo ".$razeni.", h.id ASC";
}

html_start();

# levy sloupec
sloupec_start(310);
# login / uzivatel.panel
include($INCLUDE_DIR.'/login_ramecek.php');

# reklama
/*ramecek_start(203, 'Reklama');
echo html_p("Správní hráči se nebojí zahrát si hru <a href=\"http://www.hazardnihrac.com/ruleta\">ruleta online</a>.");
ramecek_end(203);*/

# ramecek na hledani
ramecek_start(286, 'Hledání uživatele');

table_start('nic');
table_row(array('<i class="fas fa-user"></i>&nbsp;', input_text('hledat', $_POST['hledat']),input_submit('Vyhledat', 'submit_hledat', 'submit', 110)));
table_end();

# omezeni na vysledky hledani
if($_SERVER['REQUEST_METHOD']=='POST' and isset($_POST['submit_hledat']) and !empty($_POST['hledat'])) {
  $sql = sql_query("SELECT h.*, s.nazev FROM hraci AS h JOIN skupiny AS s ON h.skupina=s.id WHERE h.jmeno LIKE '%".sql_string($_POST['hledat'])."%' ORDER BY ".$sql_order);
  
  if(sql_num_rows($sql)==0) {
    echo html_error('Hráč '.$_POST['hledat'].' nebyl nalezen.');
    $sql = sql_query("SELECT h.*, s.nazev FROM hraci AS h JOIN skupiny AS s ON h.skupina=s.id ORDER BY ".$sql_order);
  }
  elseif(sql_num_rows($sql)==1) {
    # je li vysledek jediny, tak rovnou presmerujeme na profil
    $row = sql_fetch_array($sql);
    javascript_redirect('detail.php?id='.$row['id']);
  }
}
else {
  $sql = sql_query("SELECT h.*, s.nazev FROM hraci AS h JOIN skupiny AS s ON h.skupina=s.id ORDER BY ".$sql_order);
} 

$hracu = sql_num_rows($sql);
$poradi = 0;
$limit = 40;
$last_page = ceil($hracu / $limit);

# nechame odjet prvnich (page-1) * limit zaznamu.
for($i=0; $i<($page-1)*$limit; $i++) {
  sql_fetch_array($sql);
  $poradi++;
}

ramecek_end(286);

sloupec_end();

# pravy sloupec
sloupec_start();

# tabulka hracu
ramecek_start(1024, 'Tabulka hráčů');

table_start('tab_top');
table_row(array(html_href($_SERVER['PHP_SELF'].'?page='.$page.'&amp;order=0&amp;razeni='.($_GET['order']=='0' ? $opacne_razeni : 'DESC'),'POŘADÍ'),
                html_href($_SERVER['PHP_SELF'].'?page='.$page.'&amp;order=0&amp;razeni='.($_GET['order']=='0' ? $opacne_razeni : 'DESC'),'SKUPINA'),
                html_href($_SERVER['PHP_SELF'].'?page='.$page.'&amp;order=jmeno&amp;razeni='.($_GET['order']=='jmeno' ? $opacne_razeni : 'DESC'),'JMÉNO'),
                html_href($_SERVER['PHP_SELF'].'?page='.$page.'&amp;order=0&amp;razeni='.($_GET['order']=='0' ? $opacne_razeni : 'DESC'),'BODY'),
                html_href($_SERVER['PHP_SELF'].'?page='.$page.'&amp;order=elo&amp;razeni='.($_GET['order']=='elo' ? $opacne_razeni : 'DESC'),'ELO'),
                html_href($_SERVER['PHP_SELF'].'?page='.$page.'&amp;order=vitezstvi&amp;razeni='.($_GET['order']=='vitezstvi' ? $opacne_razeni : 'DESC'),'VÍTĚZSTVÍ'),
                html_href($_SERVER['PHP_SELF'].'?page='.$page.'&amp;order=celkem_her&amp;razeni='.($_GET['order']=='celkem_her' ? $opacne_razeni : 'DESC'),'CELKEM HER'),
                html_href($_SERVER['PHP_SELF'].'?page='.$page.'&amp;order=uspesnost&amp;razeni='.($_GET['order']=='uspesnost' ? $opacne_razeni : 'DESC'),'ÚSPĚŠNOST'),
                html_href($_SERVER['PHP_SELF'].'?page='.$page.'&amp;order=registrace&amp;razeni='.($_GET['order']=='registrace' ? $opacne_razeni : 'DESC'),'REGISTRACE'),
                html_href($_SERVER['PHP_SELF'].'?page='.$page.'&amp;order=karma&amp;razeni='.($_GET['order']=='karma' ? $opacne_razeni : 'DESC'),'KARMA'),
                html_href($_SERVER['PHP_SELF'].'?page='.$page.'&amp;order=prispevky&amp;razeni='.($_GET['order']=='prispevky' ? $opacne_razeni : 'DESC'),'PŘÍSPĚVKY')),0,0,0,0,1);

$i = 0;
while($i<$limit and $hrac = sql_fetch_array($sql)) {
  $i++;
  $poradi++;
  table_row(array($poradi.'.',
                  '<div style="margin-top: -3px; margin-bottom: -3px; margin-right: 1px;">'.html_image($IMG.'/skupiny/'.$hrac['skupina'].'.png', $hrac['nazev'], $hrac['nazev'], "cursor: help;").'</div>',
                  html_href($ROOT_URL.'/hraci/detail.php?id='.$hrac['id'], $hrac['jmeno']),
                  $hrac['body'],
                  $hrac['elo'],
                  $hrac['vyhranych_her'],
                  $hrac['celkem_her'],
                  round($hrac['uspesnost']).'%',
                  dt_ts_date_db2user($hrac['registrace_cas']),
                  $hrac['karma'],
                  $hrac['prispevku']
                   ),0,0,
                   array('right','right', 'left', 'right', 'right', 'right', 'right', 'right', 'right', 'right', 'right')
                   );
}
table_end();
echo '<br><br>';
table_start('tab_top');
	table_row(array($page > 1 ? html_href($_SERVER['PHP_SELF'].'?page=1&amp;order='.$_GET['order'].'&amp;razeni='.$razeni, '&lt;&lt; první', '','','','button') : '<div style = "width: 75px">&nbsp;</div>',
					$page > 1 ? html_href($_SERVER['PHP_SELF'].'?page='.($page-1).'&amp;order='.$_GET['order'].'&amp;razeni='.$razeni, '&lt; předchozí', '','','','button') : '<div style = "width: 75px">&nbsp;</div>',
					"<span class='podklad'> ".$page." / ".$last_page . "</span>",
					$page < $last_page ? html_href($_SERVER['PHP_SELF'].'?page='.($page+1).'&amp;order='.$_GET['order'].'&amp;razeni='.$razeni, 'další &gt;','','','','button') : '<div style = "width: 75px">&nbsp;</div>',
					$page < $last_page ? html_href($_SERVER['PHP_SELF'].'?page='.$last_page.'&amp;order='.$_GET['order'].'&amp;razeni='.$razeni, 'poslední &gt;&gt;','','','','button') : '<div style = "width: 75px">&nbsp;</div>',
	  ));

	table_end();
ramecek_end(1024);


sloupec_end();

html_end();

?>
