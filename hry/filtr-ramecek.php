<?php

if(!isset($_SESSION['filtr'])) {
	$_SESSION['filtr'] = array();
}

if(isset($_POST['filtr_button'])) {
	
	if($_POST['filtr_button']=='submit') {
		$filtr = array();
		$post_filtr = $_POST['filtr'];
		foreach($post_filtr as $key => $val) {
			if(!empty($val)) {
				$filtr[$key] = $val;
			}
		}
		$_SESSION['filtr'] = $filtr;		
	}
	
	if($_POST['filtr_button']=='reset') {
		$_SESSION['filtr'] = array();
	}
	
}

ramecek_start(286, 'Nastavení filtru');

?>

<form method="post">
<div style = "margin-left: 10px;">
<table class="nic">
<tr>
 <td>
 <span class = "strong_ng2" style = "margin-left: 35px;">Hráč 1</span>
 </td>
 <td>
 <span class = "strong_ng2" style = "margin-left: 35px;">Hráč 2</span>
 </td>
</tr>
<tr>
 <td>
	<table class="nic">
	<tr>
	  <td>
	   <input id="hrac1_search" type="text" name="filtr[hrac1]" placeholder="Hledat..." onkeyup="hrac1_jmeno_change();" class="textbox" <?php if(!empty($_SESSION['filtr']['hrac1'])) echo "value=\"".$_SESSION['filtr']['hrac1']."\""; ?> />
	  </td>
	</tr>
	<tr>
	  <td>
		<select id="hrac1_select" class="textbox" style="display: none;"></select>
		<table id="neex_fhrac1_warn" class="nic error" style="display: none;">
		 <tr>
		  <td style="vertical-align: middle;">Takový hráč není v seznamu!</td>
		 </tr>
		</table>
		<table id="ok_fhrac1_confirm" class="nic" style="display: none;">
		 <tr>
		 </tr>
		</table>
	  </td>
	</tr>
	</table>
 </td>
 <td>
  <table class="nic">
	<tr>
	  <td>
	   <input id="hrac2_search" type="text" name="filtr[hrac2]" placeholder="Hledat..." onkeyup="hrac2_jmeno_change();" class="textbox" <?php if(!empty($_SESSION['filtr']['hrac2'])) echo "value=\"".$_SESSION['filtr']['hrac2']."\""; ?> />
	  </td>
	</tr>
	<tr>
	  <td>
		<select id="hrac2_select" class="textbox" style="display: none;"></select>
		<table id="neex_fhrac2_warn" class="nic error" style="display: none;">
		 <tr>
		  <td style="vertical-align: middle;">Takový hráč není v seznamu!</td>
		 </tr>
		</table>
		<table id="ok_fhrac2_confirm" class="nic" style="display: none;">
		 <tr>
		 </tr>
		</table>
	  </td>
	</tr>
  </table>
 </td>
</tr>
<tr>
 <td>
	<button class="button" type="submit" name="filtr_button" value="submit" style = "margin-left: 5px;"><i class="fas fa-filter"></i> Filtrovat</button>
 </td>
 <td>
	<button class="button" type="submit" name="filtr_button" value="reset" style = "margin-left: 5px;">Resetovat filtr</button>
 </td>
</tr>
</table>
<br>




</div>
</form>
<script type="text/javascript" >
	var fhraci = new Array();

	<?php
	$sql = sql_query("SELECT id, jmeno
					  FROM hraci
					  WHERE ban is FALSE
					  ORDER BY jmeno");
	while($row = sql_fetch_array($sql)) {
	  echo "fhraci.push('".$row['jmeno']."');\n";
	}
	?>

	function hrac1_jmeno_change() {
	  
	  var search = document.getElementById('hrac1_search');
	  var text = search.value;
	  var box = document.getElementById('hrac1_select');
	  
	  document.getElementById('neex_fhrac1_warn').style.display = 'none';
	  document.getElementById('ok_fhrac1_confirm').style.display = 'none';
	  box.style.display = 'block';
	  
	  while(box.options.length) {
		box.remove(0);
	  }
	  
	  
	  if(text != '') {
		for(var i = 0; i < fhraci.length; i++) {
		  if(fhraci[i] != null) {
			if(fhraci[i].toUpperCase().indexOf(text.toUpperCase()) == 0) {
		
			  var option = document.createElement("option");
			  option.text = fhraci[i];
			  option.setAttribute("onClick", "javascript: document.getElementById('hrac1_search').value = '"+fhraci[i]+"'; hrac1_jmeno_change();");
			  box.add(option);
			  display = 'block';
			   
			}    
		  }
		}
		
		if(box.options.length==0) {
		  document.getElementById('neex_fhrac1_warn').style.display = 'block';
		  box.style.display = "none";
		}
		if(box.options.length==1 && box.options[0].text.toUpperCase()==text.toUpperCase()) {
		  document.getElementById('ok_fhrac1_confirm').style.display = 'block';
		  box.style.display = "none";
		}
		
		var size = box.options.length;
		if(size < 2) size = 2;
		if(size > 10) size = 10;
		box.size = size;
	  }
	  else {
		box.style.display = "none";
	  }
	   
	}

	function hrac2_jmeno_change() {
	  
	  var search = document.getElementById('hrac2_search');
	  var text = search.value;
	  var box = document.getElementById('hrac2_select');
	  
	  document.getElementById('neex_fhrac2_warn').style.display = 'none';
	  document.getElementById('ok_fhrac2_confirm').style.display = 'none';
	  box.style.display = 'block';
	  
	  while(box.options.length) {
		box.remove(0);
	  }
	  
	  
	  if(text != '') {
		for(var i = 0; i < fhraci.length; i++) {
		  if(fhraci[i] != null) {
			if(fhraci[i].toUpperCase().indexOf(text.toUpperCase()) == 0) {
		
			  var option = document.createElement("option");
			  option.text = fhraci[i];
			  option.setAttribute("onClick", "javascript: document.getElementById('hrac2_search').value = '"+fhraci[i]+"'; hrac2_jmeno_change();");
			  box.add(option);
			  display = 'block';
			   
			}    
		  }
		}
		
		if(box.options.length==0) {
		  document.getElementById('neex_fhrac2_warn').style.display = 'block';
		  box.style.display = "none";
		}
		if(box.options.length==1 && box.options[0].text.toUpperCase()==text.toUpperCase()) {
		  document.getElementById('ok_fhrac2_confirm').style.display = 'block';
		  box.style.display = "none";
		}
		
		var size = box.options.length;
		if(size < 2) size = 2;
		if(size > 10) size = 10;
		box.size = size;
	  }
	  else {
		box.style.display = "none";
	  }
	   
	}
</script>

<?php

ramecek_end(286);

?>