<?php
/*
* Funkce na vypis ramceku se zaznamem hry. Vsechna data jsou jiz na vstupu
* druha funke je pro detailni vypis, komentare atd.
*/

function html_hra($h) {
  
  global $ROOT_URL, $IMG, $page;

  //nadpis
  // ramecek_start(490, 'A vs B');
  echo '<div class="ramecek">'."\n";
  echo '<div class="ramecek490_1">'."\n";
  echo '<table style="width: 490px;"><tr>';
  echo '<td style="width: 226px; text-align: right; "><h2 style="padding-top: 3px;">'.html_href($ROOT_URL.'/hraci/detail.php?id='.$h['vitez'], $h['vitez_jmeno']).'</h2></td>';
  echo '<td style="width: 20px; text-align: center;"><h2 style="padding-top: 3px;">vs</h2></td>';
  echo '<td style="text-align: left; "><h2 style="padding-top: 3px;">'.html_href($ROOT_URL.'/hraci/detail.php?id='.$h['porazeny'], $h['por_jmeno']).'</h2></td>';
  echo '</tr></table>';
  echo '</div>'."\n"; 
  echo '<div class="ramecek_stred"><div class="ramecek490_2">'."\n"; 
  // konec nadpisu

  echo '<table style="margin-top: -0px; margin-bottom: -0px;">';
  
  echo '<tr>';
  echo '<td colspan="2" style="text-align: left;">';
  echo html_href($ROOT_URL.'/hry/?detail='.$h['id'], '# '.$h['id']);
  echo '</td>';
  echo '<td style="text-align: right;">';
  echo '<span style="font-size: 18px; font-weight: bold; ">1</span>';
  echo '</td>';  
  echo '<td style="text-align: center;">';
  echo '<span style="font-size: 18px; font-weight: bold; ">:</span>';
  echo '</td>';
  echo '<td style="text-align: left;">';
  echo '<span style="font-size: 18px; font-weight: bold; ">0</span>';
  echo '</td>';
  echo '<td colspan="2" style="text-align: right;">';
  echo dt_ts_date_db2user($h['vytvoreno']);
  echo '</td>';    
  echo '</tr>';
  
  echo '<tr>';
  echo '<td rowspan="2"><img style= "margin-bottom: -25px; margin-left: 3px;" alt="moralka" src="'.$IMG.'/moralka/'.($h['vitez_fairplay'] ? '+1.png' : '-1.png').'"/>
            <img style= "margin-left: -4px; margin-right: -4px;" src="'.$IMG.'/barvy/'.($h['vitez_cerveny'] ? 'cerveny' : 'modry').'.png" alt="barva_vitez" /><br/><br/> </td>';
  echo '<td>  <img style="height: 62px; width: 103px;" src="'.$IMG.'/mesta/'.$h['vitez_hlavni_mesto'].'.jpg" alt="mesto_vitez" /><img style="margin-top: -64px; position: relative; top: -14px;" src="'.$IMG.'/misc/mesto_ramecek.png" alt="mesto_ramecek" /> </td>';
  echo '<td> <img style="height: 62px; width: 58px;" src="'.$IMG.'/hrdinove/'.str_replace(' ', '%20', $h['vitez_hlavni_hrdina']).'.jpg" alt="hrdina_vitez" /><img style="margin-top: -64px; position: relative; top: -14px;" src="'.$IMG.'/misc/hrdina_ramecek.png" alt="hrdina_ramecek" /> </td>';
  echo '<td>&nbsp;</td>';
  echo '<td> <img style="height: 62px; width: 58px;" src="'.$IMG.'/hrdinove/'.str_replace(' ', '%20', $h['porazeny_hlavni_hrdina']).'.jpg" alt="hrdina_vitez" /><img style="margin-top: -64px; position: relative; top: -14px;" src="'.$IMG.'/misc/hrdina_ramecek.png" alt="hrdina_ramecek" /> </td>';
  echo '<td> <img style="height: 62px; width: 103px;" src="'.$IMG.'/mesta/'.$h['porazeny_hlavni_mesto'].'.jpg" alt="mesto_vitez" /><img style="margin-top: -64px; position: relative; top: -14px;" src="'.$IMG.'/misc/mesto_ramecek.png" alt="mesto_ramecek" />   </td>';
  echo '<td rowspan="2"><img style= "margin-bottom: -25px; margin-left: 3px;" alt="moralka" src="'.$IMG.'/moralka/'.($h['porazeny_fairplay'] ? '+1.png' : '-1.png').'"/>
            <img style= "margin-left: -4px; margin-right: -4px;"  src="'.$IMG.'/barvy/'.($h['vitez_cerveny'] ? 'modry' : 'cerveny').'.png" alt="barva_porazeny" /><br/><br/> </td>';
  echo '</tr>';
  
  echo '<tr>';
  //echo '<td>&nbsp;</td>';
  echo '<td style="text-align: center;"><span class = "strong_hra">  '.mesto_en2cz($h['vitez_hlavni_mesto']).' </span></td>';
  echo '<td style="text-align: center;"><span class = "strong_hra">  '.capital_name($h['vitez_hlavni_hrdina']).' </span></td>';
  echo '<td>&nbsp;</td>';
  echo '<td style="text-align: center;"><span class = "strong_hra">  '.capital_name($h['porazeny_hlavni_hrdina']).' </span></td>';
  echo '<td style="text-align: center;"><span class = "strong_hra">  '.mesto_en2cz($h['porazeny_hlavni_mesto']).' </span></td>';
  //echo '<td>&nbsp;</td>';
  echo '</tr>';  
  
  
  
  if ($h["delka"]==9) $delka = "(9+). týden";  
  else $delka = $h["delka"].". týden"; 
  if ($h["typ_mapy"]=='classic') $typ = "klasická";  
  if ($h["typ_mapy"]=='random') $typ = "random";   
  
  echo '<tr>';
  echo '<td colspan="2" style="text-align: left;"><div style="margin-top:-25px;">';
  echo '<span style="position: relative; top: -15px;">'.$typ.'</span> <img src="'.$IMG.'/velikosti/'.$h['velikost_mapy'].'.png" alt="velikost"/> &nbsp;&nbsp;&nbsp; <img style = "margin-top: 4px;"src="'.$IMG.'/obtiznosti/'.$h['obtiznost'].'.png" alt="obtiznost"/>';
  echo '</div></td>';
  echo '<td colspan="3" style="text-align: left;"><div style="margin-top:-15px;">';
  echo '<img src="'.$IMG.'/misc/cas.png" alt="cas" /><span style="position: relative; top: -15px;">'.$delka.'</span>';
  echo '</div></td>';
  echo '<td colspan="2" style="text-align: right;"><div style="margin-top:-15px;">';
  echo html_href($ROOT_URL.'/hry/?detail='.$h['id'].'&amp;page='.$page, 'komentáře ('.$h['komentare'].')','','','','button');
  echo '</div></td>';  
  echo '</tr>';  
  
  echo '</table>';
  
  # TLACITKA - jen pro prihlaseneho uzivatele
  if($_SESSION['uzivatel']['id']) {
    
    table_start(); 
    echo input_hidden('hra', $h['id']);

    # nepotvrzena hra, ke ktere se porazeny jeste nevyjadril
    if(empty($h['potvrzeno']) and $h['souhlas_porazeneho']===null) {
      # vitez ji muze smazat
      if($_SESSION['uzivatel']['id']==$h['vitez']) {       
        echo input_submit('Smazat', 'smazat');
      }
      # porazeny ji muze potvrdit x2 nebo odmitnout
      if($_SESSION['uzivatel']['id']==$h['porazeny']) {       
        echo "<table style=\"margin-left: 2px; width: 450px; text-align: center;\"><tr>";   
        echo "<td style=\"width: 150px;\"><button class=\"submit\" style=\"height: 42px; width: 120px\" type=\"submit\" name=\"potvrdit_plus\"  value=\"Potvrdit\" title=\"Soupeř hrál čestně\" ><img style=\"float: left;\" src=\"".$IMG."/moralka/+1_small.png\" alt=\"Potvrdit\" /><div style=\" margin-top: 3px;\" >Potvrdit</div></button></td>";
        echo "<td style=\"width: 150px;\"><button class=\"submit\" style=\"height: 42px; width: 120px\" type=\"submit\" name=\"potvrdit_minus\" value=\"Potvrdit\" title=\"Soupeř nehrál čestně\" ><img style=\"float: left;\" src=\"".$IMG."/moralka/-1_small.png\" alt=\"Potvrdit\" /><div style=\" margin-top: 3px;\" >Potvrdit</div></button></td>";
        echo "<td style=\"width: 150px;\"><button class=\"submit\" style=\"height: 42px; width: 130px; vertical-align: bottom\" type=\"submit\" name=\"odmitnout\" value=\"Odmítnout\" title=\"Odmítnout\" ><img style=\"float: left;\" src=\"".$IMG."/misc/ne.png\" alt=\"Odmítnout\" /><div style=\" margin-top: 7px;\" >Odmítnout</div></button></td>";
        echo "</tr></table>";
      }
    }

    # nepotvrzena hra, kterou porazeny odmitl a rozhodci se jeste nevyjadril
    if(empty($h['potvrzeno']) and $h['souhlas_porazeneho'].""=="0" and $h['souhlas_rozhodciho'].""!=="0") { 
      # rozhodci ji muze potvrdit nebo smazat
      if($_SESSION['uzivatel']['rozhodci']) {
        echo "<table style=\"margin-left: 2px; width: 450px; text-align: center;\"><tr>";   
        echo "<td style=\"width: 150px;\"><button class=\"submit\" style=\"height: 42px; width: 200px; vertical-align: bottom\" type=\"submit\" name=\"rozhodci_potvrdit\" value=\"Potvrdit jako rozhodčí\" title=\"Potvrdit\" ><img style=\"float: left;\" src=\"".$IMG."/misc/ano.png\" alt=\"Potvrdit\" /><div style=\" margin-top: 7px;\" >Potvrdit jako rozhodčí</div></button></td>";
        echo "<td style=\"width: 150px;\"><button class=\"submit\" style=\"height: 42px; width: 200px; vertical-align: bottom\" type=\"submit\" name=\"rozhodci_smazat\" value=\"Smazat jako rozhodčí\" title=\"Smazat\" ><img style=\"float: left;\" src=\"".$IMG."/misc/ne.png\" alt=\"Smazat\" /><div style=\" margin-top: 7px;\" >Smazat jako rozhodčí</div></button></td>";
        echo "</tr></table>";
      }
    }
    
    table_end();
    
  }
  
  ramecek_end(490);

}

function html_hra_detail($h) {
  
  global $ROOT_URL, $IMG, $SCREENY_URL, $errors, $page;

  //nadpis
  // ramecek_start(490, 'A vs B');
  echo '<div class="ramecek">'."\n";
  echo '<div class="ramecek490_1">'."\n";
  echo '<table style="width: 490px;"><tr>';
  echo '<td style="width: 226px; text-align: right; "><h2 style="padding-top: 3px;">'.html_href($ROOT_URL.'/hraci/detail.php?id='.$h['vitez'], $h['vitez_jmeno']).'</h2></td>';
  echo '<td style="width: 20px; text-align: center;"><h2 style="padding-top: 3px;">vs</h2></td>';
  echo '<td style="text-align: left; "><h2 style="padding-top: 3px;">'.html_href($ROOT_URL.'/hraci/detail.php?id='.$h['porazeny'], $h['por_jmeno']).'</h2></td>';
  echo '</tr></table>';
  echo '</div>'."\n"; 
  echo '<div class="ramecek_stred"><div class="ramecek490_2">'."\n"; 
  // konec nadpisu

  echo '<table style="margin-top: -0px; margin-bottom: -0px;">';
  
  echo '<tr>';
  echo '<td colspan="2" style="text-align: left;">';
  echo html_href($ROOT_URL.'/hry/?detail='.$h['id'], '# '.$h['id']);
  echo '</td>';
  echo '<td style="text-align: right;">';
  echo '<span style="font-size: 18px; font-weight: bold; ">1</span>';
  echo '</td>';  
  echo '<td style="text-align: center;">';
  echo '<span style="font-size: 18px; font-weight: bold; ">:</span>';
  echo '</td>';
  echo '<td style="text-align: left;">';
  echo '<span style="font-size: 18px; font-weight: bold; ">0</span>';
  echo '</td>';
  echo '<td colspan="2" style="text-align: right;">';
  echo dt_ts_date_db2user($h['vytvoreno']);
  echo '</td>';    
  echo '</tr>';
  
  // pocatecni mesto a hrdina - obrazek
  echo '<tr>';
  echo '<td>&nbsp;</td>';
  echo '<td style="text-align: center;"><span style="position: relative; top: 0px;">Startovní město</span></td>';
  echo '<td style="text-align: center;"><span style="position: relative; top: 0px;">Startovní hrdina</span></td>';
  echo '<td>&nbsp;</td>';
  echo '<td style="text-align: center;"><span style="position: relative; top: 0px;">Startovní hrdina</span></td>';
  echo '<td style="text-align: center;"><span style="position: relative; top: 0px;">Startovní město</span></td>';
  echo '<td>&nbsp;</td>';
  echo '</tr>';   
  echo '<tr>';
  echo '<td rowspan="5"><img style= "margin-bottom: -25px; margin-left: 3px;" alt="moralka" src="'.$IMG.'/moralka/'.($h['vitez_fairplay'] ? '+1.png' : '-1.png').'"/>
                        <img style= "margin-left: -4px; margin-right: -4px;" src="'.$IMG.'/barvy/'.($h['vitez_cerveny'] ? 'cerveny' : 'modry').'.png" alt="barva_vitez" /><br/><br/> </td>';
  echo '<td>  <img style="height: 62px; width: 103px;" src="'.$IMG.'/mesta/'.$h['vitez_start_mesto'].'.jpg" alt="mesto_vitez" /><img style="margin-top: -64px; position: relative; top: -14px;" src="'.$IMG.'/misc/mesto_ramecek.png" alt="mesto_ramecek" /> </td>';
  echo '<td> <img style="height: 62px; width: 58px;" src="'.$IMG.'/hrdinove/'.$h['vitez_start_hrdina'].'.jpg" alt="hrdina_vitez" /><img style="margin-top: -64px; position: relative; top: -14px;" src="'.$IMG.'/misc/hrdina_ramecek.png" alt="hrdina_ramecek" /> </td>';
  echo '<td>&nbsp;</td>';
  echo '<td> <img style="height: 62px; width: 58px;" src="'.$IMG.'/hrdinove/'.$h['porazeny_start_hrdina'].'.jpg" alt="hrdina_vitez" /><img style="margin-top: -64px; position: relative; top: -14px;" src="'.$IMG.'/misc/hrdina_ramecek.png" alt="hrdina_ramecek" /> </td>';
  echo '<td> <img style="height: 62px; width: 103px;" src="'.$IMG.'/mesta/'.$h['porazeny_start_mesto'].'.jpg" alt="mesto_vitez" /><img style="margin-top: -64px; position: relative; top: -14px;" src="'.$IMG.'/misc/mesto_ramecek.png" alt="mesto_ramecek" />   </td>';
  echo '<td rowspan="5"><img style= "margin-bottom: -25px; margin-left: 3px;" alt="moralka" src="'.$IMG.'/moralka/'.($h['porazeny_fairplay'] ? '+1.png' : '-1.png').'"/>
                        <img style= "margin-left: -4px; margin-right: -4px;"  src="'.$IMG.'/barvy/'.($h['vitez_cerveny'] ? 'modry' : 'cerveny').'.png" alt="barva_porazeny" /><br/><br/> </td>';
  echo '</tr>'; 
  // pocatecni mesto a hrdina - popis
  echo '<tr>';
  //echo '<td>&nbsp;</td>';
  echo '<td style="text-align: center;"><span class = "strong_hra">  '.mesto_en2cz($h['vitez_start_mesto']).' </span></td>';
  echo '<td style="text-align: center;"><span class = "strong_hra">  '.capital_name($h['vitez_start_hrdina']).' </span></td>';
  echo '<td>&nbsp;</td>';
  echo '<td style="text-align: center;"><span class = "strong_hra">  '.capital_name($h['porazeny_start_hrdina']).' </span></td>';
  echo '<td style="text-align: center;"><span class = "strong_hra">  '.mesto_en2cz($h['porazeny_start_mesto']).' </span></td>';
  //echo '<td>&nbsp;</td>';
  echo '</tr>';  
  
  // hlavni mesto a hrdina - obrazek
  echo '<tr>';
  //echo '<td>&nbsp;</td>';
  echo '<td style="text-align: center;"><span style="position: relative; top: 0px;">Hlavní město</span></td>';
  echo '<td style="text-align: center;"><span style="position: relative; top: 0px;">Hlavní hrdina</span></td>';
  echo '<td>&nbsp;</td>';
  echo '<td style="text-align: center;"><span style="position: relative; top: 0px;">Hlavní hrdina</span></td>';
  echo '<td style="text-align: center;"><span style="position: relative; top: 0px;">Hlavní město</span></td>';
  //echo '<td>&nbsp;</td>';
  echo '</tr>';  
  echo '<tr>';
  //echo '<td> <img style= "margin-left: -4px; margin-right: -4px;" src="'.$IMG.'/barvy/'.($h['vitez_cerveny'] ? 'cerveny' : 'modry').'.png" alt="barva_vitez" /><br/><br/> </td>';
  echo '<td>  <img style="height: 62px; width: 103px;" src="'.$IMG.'/mesta/'.$h['vitez_hlavni_mesto'].'.jpg" alt="mesto_vitez" /><img style="margin-top: -64px; position: relative; top: -14px;" src="'.$IMG.'/misc/mesto_ramecek.png" alt="mesto_ramecek" /> </td>';
  echo '<td> <img style="height: 62px; width: 58px;" src="'.$IMG.'/hrdinove/'.$h['vitez_hlavni_hrdina'].'.jpg" alt="hrdina_vitez" /><img style="margin-top: -64px; position: relative; top: -14px;" src="'.$IMG.'/misc/hrdina_ramecek.png" alt="hrdina_ramecek" /> </td>';
  echo '<td>&nbsp;</td>';
  echo '<td> <img style="height: 62px; width: 58px;" src="'.$IMG.'/hrdinove/'.$h['porazeny_hlavni_hrdina'].'.jpg" alt="hrdina_vitez" /><img style="margin-top: -64px; position: relative; top: -14px;" src="'.$IMG.'/misc/hrdina_ramecek.png" alt="hrdina_ramecek" /> </td>';
  echo '<td> <img style="height: 62px; width: 103px;" src="'.$IMG.'/mesta/'.$h['porazeny_hlavni_mesto'].'.jpg" alt="mesto_vitez" /><img style="margin-top: -64px; position: relative; top: -14px;" src="'.$IMG.'/misc/mesto_ramecek.png" alt="mesto_ramecek" />   </td>';
  //echo '<td> <img style= "margin-left: -4px; margin-right: -4px;"  src="'.$IMG.'/barvy/'.($h['vitez_cerveny'] ? 'modry' : 'cerveny').'.png" alt="barva_porazeny" /><br/><br/> </td>';
  echo '</tr>';
  // hlavni mesto a hrdina - popis
  echo '<tr>';
  //echo '<td>&nbsp;</td>';
  echo '<td style="text-align: center;"><span class = "strong_hra">  '.mesto_en2cz($h['vitez_hlavni_mesto']).' </span></td>';
  echo '<td style="text-align: center;"><span class = "strong_hra">  '.capital_name($h['vitez_hlavni_hrdina']).' </span></td>';
  echo '<td>&nbsp;</td>';
  echo '<td style="text-align: center;"><span class = "strong_hra">  '.capital_name($h['porazeny_hlavni_hrdina']).' </span></td>';
  echo '<td style="text-align: center;"><span class = "strong_hra">  '.mesto_en2cz($h['porazeny_hlavni_mesto']).' </span></td>';
  //echo '<td>&nbsp;</td>';
  echo '</tr>';    
  
  
  if ($h["delka"]==9) $delka = "(9+). týden";  
  else $delka = $h["delka"].". týden"; 
  if ($h["typ_mapy"]=='classic') $typ = "klasická";  
  if ($h["typ_mapy"]=='random') $typ = "random";   
  
  echo '<tr>';
  echo '<td colspan="2" style="text-align: left;"><div style="margin-top:-25px;">';
  echo '<span style="position: relative; top: -15px;">'.$typ.'</span> <img src="'.$IMG.'/velikosti/'.$h['velikost_mapy'].'.png" alt="velikost"/> &nbsp;&nbsp;&nbsp; <img src="'.$IMG.'/obtiznosti/'.$h['obtiznost'].'.png" alt="obtiznost"/>';
  echo '</div></td>';
  echo '<td colspan="3" style="text-align: left;"><div style="margin-top:-15px;">';
  echo '<img src="'.$IMG.'/misc/cas.png" alt="cas" /><span style="position: relative; top: -15px;">'.$delka.'</span>';
  echo '</div></td>';
  echo '<td colspan="2" style="text-align: right;"><div style="margin-top:-15px;">';
  //echo html_href($ROOT_URL.'/hry/?detail='.$h['id'], 'zobrazit komentáře ('.$h['komentare'].')');
  echo '</div></td>';  
  echo '</tr>';  
  
  echo '</table>';
  
    # TLACITKA - jen pro prihlaseneho uzivatele
  if($_SESSION['uzivatel']['id']) {
    
    table_start(); 
    echo input_hidden('hra', $h['id']);

    # nepotvrzena hra, ke ktere se porazeny jeste nevyjadril
    if(empty($h['potvrzeno']) and $h['souhlas_porazeneho']===null) {
      # vitez ji muze smazat
      if($_SESSION['uzivatel']['id']==$h['vitez']) {  
        echo "<table style=\"margin-left: 2px; width: 100%; text-align: center;\"><tr>";   
        echo "<td><button class=\"submit\" style=\"width: 100px\" type=\"submit\" name=\"smazat\" value=\"Smazat\" title=\"Smazat\"><i class=\"fas fa-trash-alt\"></i> Smazat</button></td>";
        echo "</tr></table>";	  
      }
      # porazeny ji muze potvrdit x2 nebo odmitnout
      if($_SESSION['uzivatel']['id']==$h['porazeny']) {       
        echo "<table style=\"margin-left: 2px; width: 450px; text-align: center;\"><tr>";   
        echo "<td style=\"width: 150px;\"><button class=\"submit\" style=\"height: 42px; width: 120px\" type=\"submit\" name=\"potvrdit_plus\"  value=\"Potvrdit\" title=\"Soupeř hrál čestně\" ><img style=\"float: left;\" src=\"".$IMG."/moralka/+1_small.png\" alt=\"Potvrdit\" /><div style=\" margin-top: 3px;\" >Potvrdit</div></button></td>";
        echo "<td style=\"width: 150px;\"><button class=\"submit\" style=\"height: 42px; width: 120px\" type=\"submit\" name=\"potvrdit_minus\" value=\"Potvrdit\" title=\"Soupeř nehrál čestně\" ><img style=\"float: left;\" src=\"".$IMG."/moralka/-1_small.png\" alt=\"Potvrdit\" /><div style=\" margin-top: 3px;\" >Potvrdit</div></button></td>";
        echo "<td style=\"width: 150px;\"><button class=\"submit\" style=\"height: 42px; width: 130px; vertical-align: bottom\" type=\"submit\" name=\"odmitnout\" value=\"Odmítnout\" title=\"Odmítnout\" ><img style=\"float: left;\" src=\"".$IMG."/misc/ne.png\" alt=\"Odmítnout\" /><div style=\" margin-top: 7px;\" >Odmítnout</div></button></td>";
        echo "</tr></table>";
      }
    }

    # nepotvrzena hra, kterou porazeny odmitl a rozhodci se jeste nevyjadril
    if(empty($h['potvrzeno']) and $h['souhlas_porazeneho'].""=="0" and $h['souhlas_rozhodciho'].""!=="0") { 
      # rozhodci ji muze potvrdit nebo smazat
      if($_SESSION['uzivatel']['rozhodci']) {
        echo "<table style=\"margin-left: 2px; width: 450px; text-align: center;\"><tr>";   
        echo "<td style=\"width: 150px;\"><button class=\"submit\" style=\"height: 42px; width: 200px; vertical-align: bottom\" type=\"submit\" name=\"rozhodci_potvrdit\" value=\"Potvrdit jako rozhodčí\" title=\"Potvrdit\" ><img style=\"float: left;\" src=\"".$IMG."/misc/ano.png\" alt=\"Potvrdit\" /><div style=\" margin-top: 7px;\" >Potvrdit jako rozhodčí</div></button></td>";
        echo "<td style=\"width: 150px;\"><button class=\"submit\" style=\"height: 42px; width: 200px; vertical-align: bottom\" type=\"submit\" name=\"rozhodci_smazat\" value=\"Smazat jako rozhodčí\" title=\"Smazat\" ><img style=\"float: left;\" src=\"".$IMG."/misc/ne.png\" alt=\"Smazat\" /><div style=\" margin-top: 7px;\" >Smazat jako rozhodčí</div></button></td>";
        echo "</tr></table>";
      }
    }
    
    table_end();
    
  }
  // ted se podivame na screeny
  echo "<div class=\"center\">";    
  $sql = sql_query("SELECT * FROM hry_screeny WHERE hra=".$h['id']);
  if(sql_num_rows($sql)>0) {
   echo "<h2>Screenshoty</h2>";
	$i=0;
	while($row=sql_fetch_array($sql)) {
		$i++;
		echo $i==sql_num_rows($sql) || sql_num_rows($sql) % 2 == 0 ? "<div class = 'podklad_screen' style = 'float: center; margin-left: 125px; '><div>" : "<div class = 'podklad_screen' ><div>";
		echo "<a href='".$SCREENY_URL.'/'.$row["id"].".jpg' onclick=\"return !window.open('".$SCREENY_URL.'/'.$row["id"].".jpg'".", 'okenko', 'left=150, top=50, width=840, height=640')\"><img class='screenshot' alt='screenshot' src='".$SCREENY_URL.'/'.$row["id"].".jpg'/></a>";
		if($_SESSION['uzivatel']['id']==$row['vlozil'] or $_SESSION['uzivatel']['admin'])
		  echo "<br/>".html_href($_SERVER['PHP_SELF'].'?detail='.$_GET['detail'].'&amp;page='.$page.'&amp;delete_screen='.$row["id"], 'smazat','button');
		echo "</div>";
	 echo "</div>";	
	}
  }
  echo "<div style=\"clear: left\"></div>";
  echo "</div>";
  
  // a ted komentare 
  $sql = sql_query("SELECT k.*, h.jmeno, h.prispevku
                    FROM hry_komentare AS k JOIN hraci AS h ON h.id=k.vlozil
                    WHERE k.hra=".intval($_GET['detail'])."
                    ORDER BY k.id, k.vlozeno");
  if(sql_num_rows($sql)>0 or $_SESSION['uzivatel']['id']) {
    echo "<div class=\"center\"><h2>Komentáře</h2></div>";

    $colspan = 1;
    $counter = sql_num_rows($sql);
    while($row=sql_fetch_array($sql)) {
     $counter--;
     $colspan = 2;
	  html_podklad_start ();
		table_start('nic', $_SERVER['PHP_SELF'].'?detail='.$_GET['detail']);
			  table_row(array(avatar($row['vlozil']).html_href($ROOT_URL.'/hraci/detail.php?id='.$row['vlozil'],$row['jmeno']),
				  '<span class="strong_diskuze">'.dt_ts_date_db2user($row['vlozeno']).' | '.dt_ts_time_db2user($row['vlozeno'],2).'</span><br/>'.
				  '<div>'.
					'<div style="height: 78px; float: left;"></div><div class = "text_diskuze"><p class = "paragraph2">'.
					str_replace("\n", "<br/>", add_links(htmlspecialchars($row['text']))).
					'</div><div style="clear: left;"></p></div>'.
				  '</div><br>'.
				  '<div>'.
					($row['vlozil']==$_SESSION['uzivatel']['id'] && $counter==0 || $_SESSION['uzivatel']['moderator'] ? html_href($_SERVER['PHP_SELF'].'?detail='.$h['id'].'&amp;page='.$page.'&amp;smazat_komentar='.$row['id'], 'smazat', '', '', 'return confirm(\'Opravdu smazat?\');', "button") : '').
					($row['vlozil']==$_SESSION['uzivatel']['id'] && $counter==0 || $_SESSION['uzivatel']['moderator'] ? '  '.html_href($_SERVER['PHP_SELF'].'?detail='.$h['id'].'&amp;page='.$page.'&amp;edit_komentar='.$row['id'], 'upravit', '', '', '', "button") : '').                                            
				  '</div>'.
				  "<div style=\"width: 300px;\"></div>"),                        
			  0,array(110),array('center','left'),array('top','top'));
		  table_end ();
	  html_podklad_end ();
    }
    $text = ''; 
    $editace = false;   
    if($_SESSION['uzivatel']['id']) {
      if(isset($_GET['edit_komentar'])) { 
        $row = sql_one_row(sprintf("SELECT * FROM hry_komentare WHERE hra=%d ORDER BY vlozeno DESC, id DESC LIMIT 1;", $_GET['detail']));
        if($row['vlozil']==$_SESSION['uzivatel']['id'] && $row['id']==$_GET['edit_komentar'] || $_SESSION['uzivatel']['moderator']) {
          $editace = true;
          $row =  sql_one_row(sprintf("SELECT * FROM hry_komentare WHERE hra=%d and id=%d;", $_GET['detail'], $_GET['edit_komentar']));
          $text = $row['text'];
        }       
      }
	 html_podklad_start ();
		table_start('nic', $_SERVER['PHP_SELF'].'?detail='.$_GET['detail']);
		  table_row(array(input_textarea('text', $text, 6, 56)),array($colspan));
		  
		  echo '<tr style = " padding: 10px;">
				 <td style = "border: 1px dashed; text-align: center;">
					<br/> Přidat screenshot pouze ve formátu jpeg a velikost do 400 kB!</br></br>'.input_file('screen', 30).
				'<br/><br/></td>
				</tr>';
		  table_row(array('<br>'));
		  table_row(array(input_submit($editace ? 'Uložit změny' : 'Odeslat', 'submit').($editace ? input_hidden('update_komentar', $_GET['edit_komentar']) : '')), array($colspan),0,array('center'));
		table_end ();
	 html_podklad_end ();
	} 
  }
  
  if(!empty($errors)) echo html_error($errors);
  ramecek_end(490);

}

?>
