
<?php
/*
*    Formular pro vlozeni nove hry
*/

// selecbox pro hrdiny je tak velky ze ja radsi zvlast
include_once('selectbox_hrdina.php');

// kotva pro skakani nahoru
echo "<a name=\"new_game_top\"></a>\n";

ramecek_start(1024, 'Nový záznam');
echo "<form action=\"".$_SERVER['PHP_SELF']."\" method=\"post\">";
// Obecné informace o hře
	html_podklad_start();
	echo "<div style = 'margin-left: 385px;'>";
	echo h2("Obecné nastavení hry");
	echo "</div><br>";
	echo "
	<table style =\"width: 100%; valign: top;\">
		<tr>
			<td style = \"width: 75px\">
			&nbsp;
			</td>		
			
			<td style =\"vertical-align: top;\">
			  <span class = \"strong_ng2\">typ mapy:</span>
			  <br><br>
			  <a href=\"#\" style=\"margin-left: 7px; \" onclick=\"typ_mapy_change(); return false;\">
				<span class = \"text_ng\"  id=\"typ_mapy_popis\">random</span>
			  </a>
			  <input type=\"hidden\" id=\"typ_mapy\" name=\"typ_mapy\" value=\"random\">
			</td>
	
			<td style =\"vertical-align: top;\">
			<span class = \"strong_ng2\" style=\"position: relative;\">velikost:</span>
			<br>
			  <a href=\"#\" style=\"margin-left: 12px;\" onclick=\"rozklik('selectbox_velikost_mapy'); return false;\" >
				<img id=\"img_velikost\" class=\"hover_border\" style=\"\" src=\"".$IMG."/velikosti/L.png\" alt=\"\" />
			  </a>
			  <input type=\"hidden\" name=\"velikost_mapy\" id=\"velikost_mapy\" value=\"L\" />

			  <div class = \"podklad\" id=\"selectbox_velikost_mapy\" style=\"display: none; max-width: 150px;\">
				  <a  href=\"#\" onclick=\"set_velikost_mapy('S'); return false;\" ><img class=\"hover_border\" src=\"".$IMG."/velikosti/S.png\" alt=\"\" /></a>
				  <a  href=\"#\" onclick=\"set_velikost_mapy('M'); return false;\" ><img class=\"hover_border\" src=\"".$IMG."/velikosti/M.png\" alt=\"\" /></a>
				  <a  href=\"#\" onclick=\"set_velikost_mapy('L'); return false;\" ><img class=\"hover_border\" src=\"".$IMG."/velikosti/L.png\" alt=\"\" /></a>
				  <a  href=\"#\" onclick=\"set_velikost_mapy('XL'); return false;\" ><img class=\"hover_border\" src=\"".$IMG."/velikosti/XL.png\" alt=\"\" /></a>
			  </div>
			</td>
			<td>
			&nbsp;&nbsp;
			</td>
			<td style =\"vertical-align: top;\">      
			  <span class = \"strong_ng2\" style=\"position: relative;\">obtížnost:</span>
			  <br>
			  <a href=\"#\" style=\"margin-left: 20px;\" onclick=\"rozklik('selectbox_obtiznost'); return false;\" >
				<img id=\"img_obtiznost\" class=\"hover_border\" style=\"\" src=\"".$IMG."/obtiznosti/expert.png\" alt=\"\" />
			  </a>
			  <input type=\"hidden\" name=\"obtiznost\" id=\"obtiznost\" value=\"expert\" />

			  <div class = \"podklad\" id=\"selectbox_obtiznost\" style=\"display: none; max-width: 150px;\">
				  <a  href=\"#\" onclick=\"set_obtiznost('easy'); return false;\" ><img class=\"hover_border\" src=\"".$IMG."/obtiznosti/easy.png\" alt=\"\" /></a>
				  <a  href=\"#\" onclick=\"set_obtiznost('medium'); return false;\" ><img class=\"hover_border\" src=\"".$IMG."/obtiznosti/medium.png\" alt=\"\" /></a>
				  <a  href=\"#\" onclick=\"set_obtiznost('hard'); return false;\" ><img class=\"hover_border\" src=\"".$IMG."/obtiznosti/hard.png\" alt=\"\" /></a>
				  <a  href=\"#\" onclick=\"set_obtiznost('expert'); return false;\" ><img class=\"hover_border\" src=\"".$IMG."/obtiznosti/expert.png\" alt=\"\" /></a>
				  <a  href=\"#\" onclick=\"set_obtiznost('impossible'); return false;\" ><img class=\"hover_border\" src=\"".$IMG."/obtiznosti/impossible.png\" alt=\"\" /></a>
			  </div>     
		    </td>
			<td>
			&nbsp;&nbsp;
			</td>
			<td style =\"vertical-align: top;\">      
			  <span class = \"strong_ng2\" style=\"position: relative;\">konec hry: </span>
			  <br><br>
			  <select name=\"delka\" class=\"textbox_ng\">
				<option value=\"1\">1. týden</option>
				<option value=\"2\">2. týden</option>
				<option value=\"3\">3. týden</option>
				<option value=\"4\">4. týden</option>
				<option value=\"5\">5. týden</option>
				<option value=\"6\">6. týden</option>
				<option value=\"7\">7. týden</option>
				<option value=\"8\">8. týden</option>
				<option value=\"9\">9+. týden</option>
			  </select>
		    </td>
			<td>
			&nbsp;&nbsp;
			</td>
			<td style =\"vertical-align: top;\">      
			<span class = \"strong_ng2\" style=\"position: relative;\">fair play soupeře:</span>
			<br><br>
			  <a href=\"#\" onclick=\"swap_porazeny_fairplay(); return false;\" >
				<img id=\"img_porazeny_fairplay\" class=\"hover_border\"  style=\"margin-left: 45px;\" src=\"".$IMG."/moralka/+1_small.png\" alt=\"\" />
			  </a>
			  <input type=\"hidden\" name=\"porazeny_fairplay\" id=\"porazeny_fairplay\" value=\"1\" />
		    </td>
		</tr>
	</table>
	";
	html_podklad_end();
// Hráč 1 - levá strana	
	sloupec_start(465);
		html_podklad_start();
			echo h2("Vítězný hráč " . $_SESSION['uzivatel']['jmeno']);
			echo "<br>";
			// vlajka 
			echo "
			<table style = 'width: 100%'>
			 <tr>
			  <td style=\"text-align: center;\">
				  <a href=\"#\" onclick=\"swap_colors(); return false;\" >
					<img src=\"".$IMG."/barvy/cerveny.png\" alt=\"Barva vítěze\" title=\"Barva vítěze\" id=\"img_vitez_barva\" />
				  </a>
			  <input type=\"hidden\" id=\"vitez_cerveny\" name=\"vitez_cerveny\" value=\"1\" />
   			  </td>
			 </tr>
			 <tr>
			  <td>
			   <input type=\"hidden\" disabled=\"disabled\" class=\"textbox_ng\"  value=\"".$_SESSION['uzivatel']['jmeno']."\" />
			  </td>
			 </tr>
			 </table>
			 <div style = 'height: 5px;'></div>";
			 
			 // výběr hradu
			 echo "
			 <br>
			 <table style = 'width: 100%'>
			 <tr>
				<td style = 'width: 50%'><span class = 'strong_ng2'>Startovní město:</span> <span id=\"vitez_start_mesto_popis\"  style = \"text-transform: capitalize;\"></span>
				<br>
					<div style=\"height: 10px;\"></div>

					<a href=\"#\" onclick=\"rozklik('selectbox_vitez_start_mesto'); return false;\" >
					  <img id=\"img_vitez_start_mesto\" style=\"height: 62px; width: 103px; margin-bottom: -10px;\" src=\"".$IMG."/mesta/otaznik.jpg\" alt=\"\" /><br>
					  <img style=\"margin-top: -64px; position: relative; top: -14px; margin-bottom: -10px;\" src=\"".$IMG."/misc/mesto_ramecek.png\" alt=\"\" title=\"\" />
					</a>
					
					<input type=\"hidden\" id=\"vitez_start_mesto\" name=\"vitez_start_mesto\" value=\"\" />
				  
					<table class=\"podklad\"  id=\"selectbox_vitez_start_mesto\" style=\"display: none; max-width: 200px;'\">
					  <tr>
						<td>
						  &nbsp;
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_start_mesto', 'random'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/random.jpg\" alt=\"\" />
							random
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_start_mesto', 'cove'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/cove.jpg\" alt=\"\" />
							zátoka
						  </a>
						</td>
					  </tr>
					  <tr>
						<td style=\"width: 33.33%\">
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_start_mesto', 'castle'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/castle.jpg\" alt=\"\" />
							hrad
						  </a>
						</td>
						<td style=\"width: 33.33%\">
						   <a href=\"#\" onclick=\"vyber_hodnotu('vitez_start_mesto', 'rampart'); return false;\" > 
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/rampart.jpg\" alt=\"\" />
							bašta
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_start_mesto', 'tower'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/tower.jpg\" alt=\"\" />
							věž
						  </a>
						</td>
					  </tr>
					  <tr>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_start_mesto', 'dungeon'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/dungeon.jpg\" alt=\"\" />
							kobka
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_start_mesto', 'inferno'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/inferno.jpg\" alt=\"\" />
							peklo
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_start_mesto', 'necropolis'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/necropolis.jpg\" alt=\"\" />
							<span class=\"mini\">nekropolis</span>
						  </a>
						</td>
					  </tr>
					  <tr>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_start_mesto', 'stronghold'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/stronghold.jpg\" alt=\"\" />
							tvrz
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_start_mesto', 'fortress'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/fortress.jpg\" alt=\"\" />
							pevnost
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_start_mesto', 'conflux'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/conflux.jpg\" alt=\"\" />
							soutok
						  </a>
						</td>
					  </tr>
					</table>
				  </td>
				  <td style = 'width: 50%'><span class = 'strong_ng2'>Hlavní město:</span> <span id=\"vitez_hlavni_mesto_popis\"  style = \"text-transform: capitalize;\"></span>
				  <br>
					<div style=\"height: 10px;\"></div>
					<a href=\"#\" onclick=\"rozklik('selectbox_vitez_hlavni_mesto'); return false;\" >
					  <img id=\"img_vitez_hlavni_mesto\" style=\"height: 62px; width: 103px; margin-bottom: -10px;\" src=\"".$IMG."/mesta/otaznik.jpg\" alt=\"\" /><br>
					  <img style=\"margin-top: -64px; position: relative; top: -14px; margin-bottom: -10px;\" src=\"".$IMG."/misc/mesto_ramecek.png\" alt=\"\" title=\"\" />
					</a>
					<input type=\"hidden\" id=\"vitez_hlavni_mesto\" name=\"vitez_hlavni_mesto\" value=\"\" />
					
					<table class=\"podklad\" id=\"selectbox_vitez_hlavni_mesto\" style=\"display: none; max-width: 200px;\">
					  <tr>
						<td style=\"width: 33.33%\">
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_hlavni_mesto', 'castle'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/castle.jpg\" alt=\"\" />
							hrad
						  </a>
						</td>
						<td style=\"width: 33.33%\">
						   <a href=\"#\" onclick=\"vyber_hodnotu('vitez_hlavni_mesto', 'rampart'); return false;\" > 
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/rampart.jpg\" alt=\"\" />
							bašta
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_hlavni_mesto', 'tower'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/tower.jpg\" alt=\"\" />
							věž
						  </a>
						</td>
					  </tr>
					  <tr>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_hlavni_mesto', 'dungeon'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/dungeon.jpg\" alt=\"\" />
							kobka
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_hlavni_mesto', 'inferno'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/inferno.jpg\" alt=\"\" />
							peklo
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_hlavni_mesto', 'necropolis'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/necropolis.jpg\" alt=\"\" />
							<span class=\"mini\">nekropolis</span>
						  </a>
						</td>
					  </tr>
					  <tr>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_hlavni_mesto', 'stronghold'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/stronghold.jpg\" alt=\"\" />
							tvrz
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_hlavni_mesto', 'fortress'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/fortress.jpg\" alt=\"\" />
							pevnost
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_hlavni_mesto', 'conflux'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/conflux.jpg\" alt=\"\" />
							soutok
						  </a>
						</td>
					  </tr>
					  <tr>
						<td>
					&nbsp;
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('vitez_hlavni_mesto', 'cove'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/cove.jpg\" alt=\"\" />
							zátoka
						  </a>
						</td>
						<td>
					&nbsp;
						</td>
					  </tr>
					</table>
				  </td>
			 </tr>
			</table>
			";
			
			// výběr hrdiny
			echo "
			 <table style = 'width: 100%'>
			  <tr>
			   <td style = 'width: 50%'><span class = 'strong_ng2'>Startovní hrdina:</span> <span id=\"vitez_start_hrdina_popis\" ></span>
				<br>
				<div style=\"height: 10px;\"></div>
				<a href=\"#\" onclick=\"rozklik('selectbox_vitez_start_hrdina'); return false;\" >
				  <img id=\"img_vitez_start_hrdina\" style=\"height: 62px; width: 58px; margin-bottom: -10px;\" src=\"".$IMG."/hrdinove/otaznik.jpg\" alt=\"\" />
				  <br/>
				  <img  style=\"margin-top: -64px; position: relative; top: -14px; margin-bottom: -10px; margin-left: -1px;\" src=\"".$IMG."/misc/hrdina_ramecek.png\" alt=\"\" title=\"\" />
				</a>
				<input type=\"hidden\" id=\"vitez_start_hrdina\" name=\"vitez_start_hrdina\" value=\"\" />
				
				".selectbox_hrdina('vitez_start_hrdina')."
			   </td>
			   <td style = 'width: 50%'><span class = 'strong_ng2'>Hlavní hrdina:</span> <span id=\"vitez_hlavni_hrdina_popis\" ></span>
				<br>
				<div style=\"height: 10px;\"></div>
				<a href=\"#\" onclick=\"rozklik('selectbox_vitez_hlavni_hrdina'); return false;\" >
				  <img id=\"img_vitez_hlavni_hrdina\" style=\"height: 62px; width: 58px; margin-bottom: -10px;\" src=\"".$IMG."/hrdinove/otaznik.jpg\" alt=\"\" />
				  <br/>
				  <img  style=\"margin-top: -64px; position: relative; top: -14px; margin-bottom: -10px; margin-left: -1px;\" src=\"".$IMG."/misc/hrdina_ramecek.png\" alt=\"\" title=\"\" />
				</a>
				<input type=\"hidden\" id=\"vitez_hlavni_hrdina\" name=\"vitez_hlavni_hrdina\" value=\"\" />
				
				".selectbox_hrdina('vitez_hlavni_hrdina', false)."        
				
			   </td>
			  </tr>
			 </table>
			 <br><br>
			";
		html_podklad_end();
	sloupec_end ();
	
// Změna stran
	sloupec_start();	
		echo "<a  href=\"#\" onclick=\"swap_colors(); return false;\" >
		<img style = 'padding-top: 50px;' src=\"".$IMG."/misc/switch_arrows.png\" alt=\"Vyměnit barvy\" title=\"Vyměnit barvy\" id=\"img_vymenit\" />
		</a>
		<br>
		Změna barev";
	sloupec_end ();	

// Hráč 2 - pravá strana
	sloupec_start(464);
		html_podklad_start();
			echo h2("Poražený hráč <input required id=\"porazeny_search\" type=\"text\" name=\"porazeny_jmeno\" placeholder=\"Hledat...\" onkeyup=\"porazeny_jmeno_change();\" class=\"textbox_ng_nazev\" />");
			echo "<br>";
			echo "
			<table style = 'width: 100%'>
			 <tr>
				<td>
				  <a href=\"#\" onclick=\"swap_colors(); return false;\" >
					<img src=\"".$IMG."/barvy/modry.png\" alt=\"Barva poraženého\" title=\"Barva poraženého\" id=\"img_porazeny_barva\" />
				  </a>
				</td>
			 </tr>
			 <tr>
			  <td>
			  <table class=\"nic\" style = 'width: 100%'>
				<tr>
				 <td>
					<select id=\"porazeny_select\" class=\"textbox\" >
					</select>
					<table id=\"neex_hrac_warn\" class=\"nic error\" style=\"display: none;\">
						<tr>
							<td>".html_image($IMG."/misc/ne.png", 'ne', 'ne', 'float: left;')."</td>
							<td style=\"vertical-align: middle;\">Takový hráč není v seznamu!</td>
						</tr>
					</table>
					<div id=\"ok_hrac_confirm\" class=\"nic\" style=\"display: none;\">
					</div>
				  </td>
				</tr>
			  </table>
			  </td>
			 </tr>
			 </table>";
			 // vyber město
			 echo "
			 <br>
			 <table style = 'width: 100%'>
			 <tr>
				<td style = 'width: 50%'>
					<span class = 'strong_ng2'>Startovní město:</span> <span id=\"porazeny_start_mesto_popis\"  style = \"text-transform: capitalize;\"></span>
				<br>
					<div style=\"height: 10px;\"></div>
					<a href=\"#\" onclick=\"rozklik('selectbox_porazeny_start_mesto'); return false;\" >
					  <img id=\"img_porazeny_start_mesto\" style=\"height: 62px; width: 103px; margin-bottom: -10px;\" src=\"".$IMG."/mesta/otaznik.jpg\" alt=\"\" /><br>
					  <img style=\"margin-top: -64px; position: relative; top: -14px; margin-bottom: -10px; margin-left: 1px; \" src=\"".$IMG."/misc/mesto_ramecek.png\" alt=\"\" title=\"\" />
					</a>
					<input type=\"hidden\" id=\"porazeny_start_mesto\" name=\"porazeny_start_mesto\" value=\"\" />
					  
					<table class=\"podklad\" id=\"selectbox_porazeny_start_mesto\" style=\"display: none; max-width: 200px;\">
					  <tr>
						<td>
						  &nbsp;
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_start_mesto', 'random'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/random.jpg\" alt=\"\" />
							random
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_start_mesto', 'cove'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/cove.jpg\" alt=\"\" />
							zátoka
						  </a>
						</td>
					  </tr>
					  <tr>
						<td style=\"width: 33.33%\">
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_start_mesto', 'castle'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/castle.jpg\" alt=\"\" />
							hrad
						  </a>
						</td>
						<td style=\"width: 33.33%\">
						   <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_start_mesto', 'rampart'); return false;\" > 
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/rampart.jpg\" alt=\"\" />
							bašta
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_start_mesto', 'tower'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/tower.jpg\" alt=\"\" />
							věž
						  </a>
						</td>
					  </tr>
					  <tr>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_start_mesto', 'dungeon'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/dungeon.jpg\" alt=\"\" />
							kobka
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_start_mesto', 'inferno'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/inferno.jpg\" alt=\"\" />
							peklo
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_start_mesto', 'necropolis'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/necropolis.jpg\" alt=\"\" />
							<span class=\"mini\">nekropolis</span>
						  </a>
						</td>
					  </tr>
					  <tr>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_start_mesto', 'stronghold'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/stronghold.jpg\" alt=\"\" />
							tvrz
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_start_mesto', 'fortress'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/fortress.jpg\" alt=\"\" />
							pevnost
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_start_mesto', 'conflux'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/conflux.jpg\" alt=\"\" />
							soutok
						  </a>
						</td>
					  </tr>
					</table>
				</td>
				  <td style = 'width: 50%'><span class = 'strong_ng2'>Hlavní město:</span> <span id=\"porazeny_hlavni_mesto_popis\"  style = \"text-transform: capitalize;\"></span>
				  <br>
					<div style=\"height: 10px;\"></div>
					<a href=\"#\" onclick=\"rozklik('selectbox_porazeny_hlavni_mesto'); return false;\" >
					  <img id=\"img_porazeny_hlavni_mesto\" style=\"height: 62px; width: 103px; margin-bottom: -10px;\" src=\"".$IMG."/mesta/otaznik.jpg\" alt=\"\" /><br>
					  <img style=\"margin-top: -64px; position: relative; top: -14px; margin-bottom: -10px; margin-left: 1px;\" src=\"".$IMG."/misc/mesto_ramecek.png\" alt=\"\" title=\"\" />
					</a>
					<input type=\"hidden\" id=\"porazeny_hlavni_mesto\" name=\"porazeny_hlavni_mesto\" value=\"\" />
					
					<table class=\"podklad\" id=\"selectbox_porazeny_hlavni_mesto\" style=\"display: none; max-width: 200px;\">
					  <tr>
						<td style=\"width: 33.33%\">
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_hlavni_mesto', 'castle'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/castle.jpg\" alt=\"\" />
							hrad
						  </a>
						</td>
						<td style=\"width: 33.33%\">
						   <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_hlavni_mesto', 'rampart'); return false;\" > 
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/rampart.jpg\" alt=\"\" />
							bašta
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_hlavni_mesto', 'tower'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/tower.jpg\" alt=\"\" />
							věž
						  </a>
						</td>
					  </tr>
					  <tr>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_hlavni_mesto', 'dungeon'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/dungeon.jpg\" alt=\"\" />
							kobka
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_hlavni_mesto', 'inferno'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/inferno.jpg\" alt=\"\" />
							peklo
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_hlavni_mesto', 'necropolis'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/necropolis.jpg\" alt=\"\" />
							<span class=\"mini\">nekropolis</span>
						  </a>
						</td>
					  </tr>
					  <tr>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_hlavni_mesto', 'stronghold'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/stronghold.jpg\" alt=\"\" />
							tvrz
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_hlavni_mesto', 'fortress'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/fortress.jpg\" alt=\"\" />
							pevnost
						  </a>
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_hlavni_mesto', 'conflux'); return false;\" >                
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/conflux.jpg\" alt=\"\" />
							soutok
						  </a>
						</td>
					  </tr>
					  <tr>
						<td>
					&nbsp;
						</td>
						<td>
						  <a href=\"#\" onclick=\"vyber_hodnotu('porazeny_hlavni_mesto', 'cove'); return false;\" >
							<img class=\"selectbox_mesto img_ramecek\" style=\"height: 40px;\" src=\"".$IMG."/mesta/cove.jpg\" alt=\"\" />
							zátoka
						  </a>
						</td>
						<td>
					&nbsp;
						</td>
					  </tr>
					</table>
				  </td>				
			 </tr>
			</table>
			";
			// vyber hrdinu
			echo "
			 <table style = 'width: 100%'>
			 <tr> 
			  <td style = 'width: 50%'><span class = 'strong_ng2'>Startovní hrdina:</span> <span id=\"porazeny_start_hrdina_popis\" ></span>
			  <br>
				<div style=\"height: 10px;\"></div>
				<a href=\"#\" onclick=\"rozklik('selectbox_porazeny_start_hrdina'); return false;\" >
				  <img id=\"img_porazeny_start_hrdina\" style=\"height: 62px; width: 58px; margin-bottom: -10px;\" src=\"".$IMG."/hrdinove/otaznik.jpg\" alt=\"\" />
				  <br/>
				  <img  style=\"margin-top: -64px; position: relative; top: -14px; margin-bottom: -10px; margin-left: -1px;\" src=\"".$IMG."/misc/hrdina_ramecek.png\" alt=\"\" title=\"\" />
				</a>
				<input type=\"hidden\" id=\"porazeny_start_hrdina\" name=\"porazeny_start_hrdina\" value=\"\" />
				
				".selectbox_hrdina('porazeny_start_hrdina')."
				
			    </td>
				<td style = 'width: 50%'><span class = 'strong_ng2'>Hlavní hrdina:</span> <span id=\"porazeny_hlavni_hrdina_popis\" ></span>          
				<br>
				<div style=\"height: 10px;\"></div>
				<a href=\"#\" onclick=\"rozklik('selectbox_porazeny_hlavni_hrdina'); return false;\" >
				  <img id=\"img_porazeny_hlavni_hrdina\"  style=\"height: 62px; width: 58px; margin-bottom: -10px;\" src=\"".$IMG."/hrdinove/otaznik.jpg\" alt=\"\" />
				  <br/>
				  <img  style=\"margin-top: -64px; position: relative; top: -14px; margin-bottom: -10px; margin-left: -1px;\" src=\"".$IMG."/misc/hrdina_ramecek.png\" alt=\"\" title=\"\" />
				</a>
				<input type=\"hidden\" id=\"porazeny_hlavni_hrdina\" name=\"porazeny_hlavni_hrdina\" value=\"\" />
				
				".selectbox_hrdina('porazeny_hlavni_hrdina', false)."         
				
				</td>
			   </tr>
			  </table>
			 <br><br>
			";
		html_podklad_end();
	sloupec_end ();
	


echo "<table id=\"nova_hra_table\">";
echo "<tr>";
echo "
      
      <td>&nbsp;</td>
      
";
echo "</tr>";

echo "<tr>";
echo "<td colspan=\"5\">
        
       
        
      </td>";
echo "</tr>";

echo "<tr><td colspan=\"5\" style=\"text-align: center;\">";
echo '<button class="submit" style="width: 100px" type="submit" name="ulozit" value="Uložit" title="Uložit"><i class="fas fa-save"></i>&nbsp;&nbsp;Uložit</button>';

echo "</table></form>";                                                                  
ramecek_end(1024);
echo "<br><br>";

?>

<?php
/*
* Javascript pro obsluhu formulare
*/
?>

<script type="text/javascript" >

var hraci = new Array();

<?php
$sql = sql_query("SELECT id, jmeno
                  FROM hraci
                  WHERE id<>".$_SESSION['uzivatel']['id']."
                    AND ban is FALSE
                  ORDER BY jmeno");
while($row = sql_fetch_array($sql)) {
  echo "hraci.push('".$row['jmeno']."');\n";
}
?>

function porazeny_jmeno_change() {
  
  var search = document.getElementById('porazeny_search');
  var text = search.value;
  var box = document.getElementById('porazeny_select');
  
  document.getElementById('neex_hrac_warn').style.display = 'none';
  document.getElementById('ok_hrac_confirm').style.display = 'none';
  box.style.display = 'block';
  
  while(box.options.length) {
    box.remove(0);
  }
  
  
  if(text != '') {
    for(var i = 0; i < hraci.length; i++) {
      if(hraci[i] != null) {
        if(hraci[i].toUpperCase().indexOf(text.toUpperCase()) == 0) {
    
          var option = document.createElement("option");
          option.text = hraci[i];
          option.setAttribute("onClick", "javascript: document.getElementById('porazeny_search').value = '"+hraci[i]+"'; porazeny_jmeno_change();");
          box.add(option);
          display = 'block';
           
        }    
      }
    }
    
    if(box.options.length==0) {
      document.getElementById('neex_hrac_warn').style.display = 'block';
      box.style.display = "none";
    }
    if(box.options.length==1 && box.options[0].text.toUpperCase()==text.toUpperCase()) {
      document.getElementById('ok_hrac_confirm').style.display = 'block';
      box.style.display = "none";
    }
    
    var size = box.options.length;
    if(size < 2) size = 2;
    if(size > 10) size = 10;
    box.size = size;
  }
  else {
    box.style.display = "none";
  }
   
}

function swap_colors() {

  var vc = document.getElementById('vitez_cerveny').value;
  if(vc == '1') {
    document.getElementById('vitez_cerveny').value = '0';
    document.getElementById('img_vitez_barva').src = '<?php echo $IMG."/barvy/modry.png"; ?>';
    document.getElementById('img_porazeny_barva').src = '<?php echo $IMG."/barvy/cerveny.png"; ?>';
    document.getElementById('vitez_barva_popis').innerHTML = 'modrý';
    document.getElementById('porazeny_barva_popis').innerHTML = 'červený';
  }
  else {
    document.getElementById('vitez_cerveny').value = '1';
    document.getElementById('img_vitez_barva').src = '<?php echo $IMG."/barvy/cerveny.png"; ?>';
    document.getElementById('img_porazeny_barva').src = '<?php echo $IMG."/barvy/modry.png"; ?>';
    document.getElementById('vitez_barva_popis').innerHTML = 'červený';
    document.getElementById('porazeny_barva_popis').innerHTML = 'modrý';
  } 
}

function swap_porazeny_fairplay() {

  var fp = document.getElementById('porazeny_fairplay').value;
  if(fp == '1') {
    document.getElementById('porazeny_fairplay').value = '0';
    document.getElementById('img_porazeny_fairplay').src = '<?php echo $IMG."/moralka/-1_small.png"; ?>';
  }
  else {
    document.getElementById('porazeny_fairplay').value = '1';
    document.getElementById('img_porazeny_fairplay').src = '<?php echo $IMG."/moralka/+1_small.png"; ?>';
  } 
}

function typ_mapy_change() {

  var tm = document.getElementById('typ_mapy').value;
  if(tm == 'random') {
    document.getElementById('typ_mapy').value = 'classic';
    document.getElementById('typ_mapy_popis').innerHTML = 'klasická';
  }
  else {
    document.getElementById('typ_mapy').value = 'random';
    document.getElementById('typ_mapy_popis').innerHTML = 'random';
  } 
}

function set_velikost_mapy(size) {
  document.getElementById('velikost_mapy').value = size;
  document.getElementById('img_velikost').src = '<?php echo $IMG."/velikosti/' + size + '.png"; ?>';
  document.getElementById('selectbox_velikost_mapy').style.display = 'none';  
}

function set_obtiznost(diff) {
  document.getElementById('obtiznost').value = diff;
  document.getElementById('img_obtiznost').src = '<?php echo $IMG."/obtiznosti/' + diff + '.png"; ?>';
  document.getElementById('selectbox_obtiznost').style.display = 'none';  
}

function rozklik(element_id) {
  document.getElementById(element_id).style.display = 'block';
}

function mesto_en2cz(en) {
  var mesto_en2cz = {};
  mesto_en2cz['random'] = 'random';
  mesto_en2cz['castle'] = 'hrad';
  mesto_en2cz['rampart'] = 'bašta';
  mesto_en2cz['tower'] = 'věž';
  mesto_en2cz['inferno'] = 'peklo';
  mesto_en2cz['dungeon'] = 'kobka';
  mesto_en2cz['necropolis'] = 'nekropolis';
  mesto_en2cz['fortress'] = 'pevnost';
  mesto_en2cz['stronghold'] = 'tvrz';
  mesto_en2cz['conflux'] = 'soutok';
  mesto_en2cz['cove'] = 'zátoka';
  return mesto_en2cz[en];
}

function capitaliseFirstLetter(str) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function vyber_hodnotu(element_id, hodnota) {
  var selectbox = document.getElementById('selectbox_' + element_id);
  
  // naplnime hidden input
  document.getElementById(element_id).value = hodnota;
  
  
    // mesto
    if(element_id.indexOf("mesto") > -1 ) {
      // zmenime obrazek
      document.getElementById("img_" + element_id).src = '<?php echo $IMG."/mesta/' + hodnota + '.jpg"; ?>';
      // zmenime label obrazku
      document.getElementById(element_id + "_popis").innerHTML = mesto_en2cz(hodnota);
    }
    // hrdina 
    if(element_id.indexOf("hrdina") > -1 ) {
      // zmenime obrazek
      document.getElementById("img_" + element_id).src = '<?php echo $IMG."/hrdinove/' + hodnota + '.jpg"; ?>';
      // zmenime label obrazku
      var popis =  hodnota;
      if(popis != 'random') popis = capitaliseFirstLetter(popis);
      document.getElementById(element_id + "_popis").innerHTML = popis;
    }

  selectbox.style.display = 'none';

  // pokud to byl vyber startovniho mesta nebo hrdiny, udelame totez pro hlavniho
  if(element_id == 'vitez_start_mesto' && hodnota != 'random') vyber_hodnotu('vitez_hlavni_mesto', hodnota);
  if(element_id == 'porazeny_start_mesto' && hodnota != 'random') vyber_hodnotu('porazeny_hlavni_mesto', hodnota);
  if(element_id == 'vitez_start_hrdina' && hodnota != 'random') vyber_hodnotu('vitez_hlavni_hrdina', hodnota);
  if(element_id == 'porazeny_start_hrdina' && hodnota != 'random') vyber_hodnotu('porazeny_hlavni_hrdina', hodnota);
  

}



</script>

