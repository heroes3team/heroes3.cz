<?php
/*
*  stranka na hry
*/

include('../_common_start.php');

$TITLE = 'Hry';
$MODUL = 'hry';
$SCREENY_URL = 'http://heroes3.cz/__big_data/hry_screenshoty';
$SCREENY_DIR = $ROOT_DIR.'/__big_data/hry_screenshoty';

html_start();

# levy sloupec
sloupec_start(310);

# login / uzivatel.panel
include($INCLUDE_DIR.'/login_ramecek.php');

# filtr her - ramecek + update data
include('filtr-ramecek.php');

sloupec_end();
 
# prostřední sloupec
sloupec_start(525);


# pokud neni zadan detail, vypisujeme dale tabulky:
if(!isset($_GET['detail'])) {
  # formular + zpracovani dat
  if($_SESSION['uzivatel']['id']) { 
    include('smazat_hru.php');
    include('rozhodci_smazat_hru.php');
    include('potvrdit_hru.php');
    include('odmitnout_hru.php');
  }
  
  
  # sporne zaznamy
  
  ramecek_start (490, 'Sporné záznamy');
    $sql = sql_query("SELECT h.*, COUNT(k.id) AS komentare, vit.jmeno AS vitez_jmeno, por.jmeno AS por_jmeno
                    FROM hry AS h JOIN hry_komentare AS k ON k.hra=h.id JOIN hraci AS vit ON h.vitez=vit.id JOIN hraci AS por ON h.porazeny=por.id
                    WHERE potvrzeno IS NULL
                      AND souhlas_porazeneho IS FALSE
                      AND souhlas_rozhodciho IS NULL
                    GROUP BY h.id
                    UNION
                    SELECT h.*, 0 AS komentare, vit.jmeno AS vitez_jmeno, por.jmeno AS por_jmeno
                    FROM hry AS h JOIN hraci AS vit ON h.vitez=vit.id JOIN hraci AS por ON h.porazeny=por.id
                    WHERE potvrzeno IS NULL
                      AND souhlas_porazeneho IS FALSE
                      AND souhlas_rozhodciho IS NULL
                      AND NOT EXISTS (SELECT * FROM hry_komentare WHERE hra=h.id)
                    ORDER BY vytvoreno DESC, id DESC;");
	
	if(sql_num_rows($sql)>0) {
		table_start ('tab_top');
			echo "<tr>
					<th style = 'width: 20%'>ID</th>
					<th style = 'width: 20%'>DATUM</th>
					<th style = 'width: 20%'>VÍTĚZ</th>
					<th style = 'width: 20%'>PORAŽENÝ</th>
					<th style = 'width: 10%'>KOMENTÁŘE</th>
					<th style = 'width: 15%'></th> 							
				</tr>
				";
			while($hra = sql_fetch_array($sql)) {
				table_row(array(html_href($ROOT_URL.'/hry/?detail=' . $hra['id'],'# ' . $hra['id']),date('d.m.Y', strtotime($hra['vytvoreno'])),html_href($ROOT_URL.'/hraci/detail.php?id='.$hra['vitez'], $hra['vitez_jmeno']),html_href($ROOT_URL.'/hraci/detail.php?id='.$hra['porazeny'], $hra['por_jmeno']),$hra['komentare'],html_href($ROOT_URL.'/hry/?detail=' . $hra['id'],'Zobrazit', '', '', '', 'button3')));
			}
		table_end ();
	} else {
	echo html_p('Žádné sporné záznamy. Hráči jsou vzorní. :-)');
	}	
  ramecek_end (490);
  
  echo '<br>';
  
  ramecek_start (490, 'Nepotvrzené záznamy');
	$sql = sql_query("SELECT h.*, COUNT(k.id) AS komentare, vit.jmeno AS vitez_jmeno, por.jmeno AS por_jmeno
                    FROM hry AS h JOIN hry_komentare AS k ON k.hra=h.id JOIN hraci AS vit ON h.vitez=vit.id JOIN hraci AS por ON h.porazeny=por.id
                    WHERE potvrzeno IS NULL
                      AND souhlas_porazeneho IS NULL
                      AND souhlas_rozhodciho IS NULL
                    GROUP BY h.id
                    UNION
                    SELECT h.*, 0 AS komentare, vit.jmeno AS vitez_jmeno, por.jmeno AS por_jmeno
                    FROM hry AS h JOIN hraci AS vit ON h.vitez=vit.id JOIN hraci AS por ON h.porazeny=por.id
                    WHERE potvrzeno IS NULL
                      AND souhlas_porazeneho IS NULL
                      AND souhlas_rozhodciho IS NULL
                      AND NOT EXISTS (SELECT * FROM hry_komentare WHERE hra=h.id)
                    ORDER BY vytvoreno DESC, id DESC;");
	
	if(sql_num_rows($sql)>0) {
		table_start ('tab_top');
			echo "<tr>
					<th style = 'width: 20%'>ID</th>
					<th style = 'width: 20%'>DATUM</th>
					<th style = 'width: 20%'>VÍTĚZ</th>
					<th style = 'width: 20%'>PORAŽENÝ</th>
					<th style = 'width: 10%'>KOMENTÁŘE</th>
					<th style = 'width: 15%'></th> 					
				</tr>
				";
			while($hra = sql_fetch_array($sql)) {
				table_row(array(html_href($ROOT_URL.'/hry/?detail=' . $hra['id'],'# ' . $hra['id']),date('d.m.Y', strtotime($hra['vytvoreno'])),html_href($ROOT_URL.'/hraci/detail.php?id='.$hra['vitez'], $hra['vitez_jmeno']),html_href($ROOT_URL.'/hraci/detail.php?id='.$hra['porazeny'], $hra['por_jmeno']),$hra['komentare'],html_href($ROOT_URL.'/hry/?detail=' . $hra['id'],'Zobrazit', '', '', '', 'button3')));
			}
		table_end ();
	} else {
		echo "<p>Žádné nepotvrzené záznamy. Hráči jsou vzorní. :-)</p>"; 
	}	
  ramecek_end (490);
}

else {
	  # formular + zpracovani dat
  if($_SESSION['uzivatel']['id']) { 
    include('smazat_hru.php');
    include('rozhodci_smazat_hru.php');
    include('potvrdit_hru.php');
    include('odmitnout_hru.php');
  }
	
  // detail zaznamu
  
  // upravy:
  // GET - mazani
  if(isset($_GET['delete_screen'])) {
    // pred smazanim screenu zkontrolujeme, ze na to uzivatel ma opravneni
    $s = sql_one_row("SELECT * FROM hry_screeny WHERE id=".intval($_GET['delete_screen']));
    if($s['vlozil']==$_SESSION['uzivatel']['id'] or $_SESSION['uzivatel']['admin']) {
      sql_query("DELETE FROM hry_screeny WHERE id=".intval($_GET['delete_screen']));
      unlink($SCREENY_DIR.'/'.$_GET['delete_screen'].".jpg");
    }
  }
  
  // smazat komentar
  if(!empty($_SESSION['uzivatel']['id'])) {
    if(isset($_GET['smazat_komentar'])) {
      $row = sql_one_row(sprintf("SELECT * FROM hry_komentare WHERE hra=%d ORDER BY vlozeno DESC, id DESC LIMIT 1;", $_GET['detail']));
      if($row['vlozil']==$_SESSION['uzivatel']['id'] && $row['id']==$_GET['smazat_komentar'] || $_SESSION['uzivatel']['moderator']) {
        sql_query(sprintf("DELETE FROM hry_komentare WHERE id=%d AND hra=%d", $_GET['smazat_komentar'], $_GET['detail']));
		update_pocet_prispevku($row['vlozil']);
      }
    }
  }
  
  // POST - komentar, screen, potvrzovani
  if($_SERVER['REQUEST_METHOD']=='POST' and $_SESSION['uzivatel']['id']) {
  
      $errors .= '';
      
      // pridani screenu a komentar, update
      if(isset($_POST['submit'])) {
        sql_begin();
      
        //screen      
        $screen = $_FILES['screen']['tmp_name'];
        if(is_uploaded_file($screen)) {
          $info = GetImageSize($screen);
          $screen_size = filesize($screen); 
          if($info[2]==2 && $screen_size<=(400*1024)) {
            sql_query("INSERT INTO hry_screeny (vlozil, hra) VALUES(".$_SESSION['uzivatel']['id'].", ".intval($_GET['detail']).")");
            $screen_id = sql_getfield("SELECT id FROM hry_screeny ORDER BY id DESC LIMIT 1", 'id');
            $newfile = fopen($SCREENY_DIR."/".$screen_id.".jpg", "w+") or $errors .= "Soubor nelze vytvořit.";
            $fp = fopen($screen, "rb") or $errors .= "Nelze otevřít dočasný soubor.";
            $content = fread($fp, $screen_size) or $errors .= "Nelze přečíst dočasný soubor.";
        		fwrite($newfile, $content) or $errors .= "Nelze vepsat data do nového souboru.";          
            fclose($newfile);
            fclose($fp);            
          }
          else $errors .= 'Špatný formát nebo velikost obrázku.';         
        }
        
        // text
        if(!empty($_POST['text'])) {
          // insert
          if(empty($_POST['update_komentar'])) {
            sql_query("INSERT INTO hry_komentare (vlozil, hra, text) VALUES(".$_SESSION['uzivatel']['id'].", ".intval($_GET['detail']).", '".sql_string($_POST['text'])."')");
	    $hrac = sql_one_row(sprintf("SELECT * FROM hraci WHERE id=%d", $_SESSION['uzivatel']['id']));
	    $upd = sql_query(sprintf("UPDATE hraci SET prispevku=%d WHERE id=%d", $hrac['prispevku']+1, $_SESSION['uzivatel']['id']));
		update_pocet_prispevku($_SESSION['uzivatel']['id']);

          } 
          // update
          else {
            $row = sql_one_row(sprintf("SELECT * FROM hry_komentare WHERE hra=%d ORDER BY vlozeno DESC, id DESC LIMIT 1;", $_GET['detail']));
            if($row['vlozil']==$_SESSION['uzivatel']['id'] && $row['id']==$_POST['update_komentar'] || $_SESSION['uzivatel']['moderator']) {
              sql_query(sprintf("UPDATE hry_komentare SET text='%s', vlozeno=NOW() WHERE id=%d AND hra=%d", sql_string($_POST['text']), $_POST['update_komentar'], $_GET['detail']));
            }
          }
        }
        
        if(empty($errors)) {
          sql_commit();
        } else {
          sql_rollback();
        }
        
        
      }
 
  }
  
  echo html_nadpis("Detail záznamu");
  
  $hra = sql_one_row("SELECT h.*, COUNT(k.id) AS komentare, vit.jmeno AS vitez_jmeno, por.jmeno AS por_jmeno
                  FROM hry AS h JOIN hry_komentare AS k ON k.hra=h.id JOIN hraci AS vit ON h.vitez=vit.id JOIN hraci AS por ON h.porazeny=por.id
                  WHERE h.id=".intval($_GET['detail'])."
                  GROUP BY h.id
                  UNION
                  SELECT h.*, 0 AS komentare, vit.jmeno AS vitez_jmeno, por.jmeno AS por_jmeno
                  FROM hry AS h JOIN hraci AS vit ON h.vitez=vit.id JOIN hraci AS por ON h.porazeny=por.id
                  WHERE h.id=".intval($_GET['detail'])."
                    AND NOT EXISTS (SELECT * FROM hry_komentare WHERE hra=h.id)");
  if(!$hra) javascript_redirect($ROOT_URL.'/chyba.php');
  html_hra_detail($hra);
}
sloupec_end();


# pravy sloupec
sloupec_start();

$page = 1;
if(intval($_GET['page'])>0) $page = intval($_GET['page']);


echo html_nadpis("Odehrané hry");

$filtr_klauzule="";
if(!empty($_SESSION['filtr']['hrac1'])) {
	$filtr_klauzule .= " AND (vit.jmeno='".sql_string($_SESSION['filtr']['hrac1'])."' OR por.jmeno='".sql_string($_SESSION['filtr']['hrac1'])."')";
}
if(!empty($_SESSION['filtr']['hrac2'])) {
	$filtr_klauzule .= " AND (vit.jmeno='".sql_string($_SESSION['filtr']['hrac2'])."' OR por.jmeno='".sql_string($_SESSION['filtr']['hrac2'])."')";
}

$sql = sql_query("SELECT h.*, COUNT(k.id) AS komentare, vit.jmeno AS vitez_jmeno, por.jmeno AS por_jmeno
                  FROM hry AS h JOIN hry_komentare AS k ON k.hra=h.id JOIN hraci AS vit ON h.vitez=vit.id JOIN hraci AS por ON h.porazeny=por.id
                  WHERE potvrzeno IS NOT NULL
					".$filtr_klauzule."
                  GROUP BY h.id
                  UNION
                  SELECT h.*, 0 AS komentare, vit.jmeno AS vitez_jmeno, por.jmeno AS por_jmeno
                  FROM hry AS h JOIN hraci AS vit ON h.vitez=vit.id JOIN hraci AS por ON h.porazeny=por.id
                  WHERE potvrzeno IS NOT NULL
                    AND NOT EXISTS (SELECT * FROM hry_komentare WHERE hra=h.id)
					".$filtr_klauzule."
                  ORDER BY potvrzeno DESC, id DESC;");

$her = sql_num_rows($sql);
$limit = 10;
$last_page = ceil($her / $limit);
# nechame odjet prvnich (page-1) * limit zaznamu.
for($i=0; $i<($page-1)*$limit; $i++) {
  sql_fetch_array($sql);
}

$i = 0;               
while($i<$limit and $hra = sql_fetch_array($sql)) {
  $i++;
  html_hra($hra);
  echo '<br>';
}

echo '<div class="podklad2">';
echo '<table style="width: 100%">';
table_row(array($page > 1 ? html_href($_SERVER['PHP_SELF'].'?page=1', '&lt;&lt; první','','','','button') : '<div style = "width: 75px">&nbsp;</div>',
                $page > 1 ? html_href($_SERVER['PHP_SELF'].'?page='.($page-1), '&lt; předchozí','','','','button') : '<div style = "width: 75px">&nbsp;</div>',
				"<div class = 'podklad' style = 'color: #f4e4ce;'> ".$page." / ".$last_page . "</div>",
                $page < $last_page ? html_href($_SERVER['PHP_SELF'].'?page='.($page+1), 'další &gt;','','','','button') : '<div style = "width: 75px">&nbsp;</div>',
                $page < $last_page ? html_href($_SERVER['PHP_SELF'].'?page='.$last_page, 'poslední &gt;&gt;','','','','button') : '<div style = "width: 75px">&nbsp;</div>'
  ));
echo '</table></div>';
sloupec_end();
html_end();

?>


