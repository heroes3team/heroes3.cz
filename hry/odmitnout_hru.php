<?php
  
if($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['odmitnout'])) {
   
  $errors = "";
  
  $h = sql_one_row(sprintf("SELECT * FROM hry WHERE id=%d", $_POST['hra']));
   
  # kontrola opravneni
  
  # nelze odmitnout potvrzenou hru
  if($h['potvrzeno']!==null) {
    $errors .= "Hra už je potvrzena. Nemůžete ji tedy potvrdit.";
  }
  
  # jen porazeny muze odmitnout
  if($_SESSION['uzivatel']['id'] != $h['porazeny']) {
    $errors .= "Pouze poražený hráč může odmítnout hru";
  }
  
  if(empty($errors)) {
    # odmitneme hru
    sql_begin();
    
    # odmitnuti = souper nehral fer
    $r1 = sql_query(sprintf("UPDATE hry SET vitez_fairplay=0, souhlas_porazeneho=FALSE
                            WHERE id=%d",
                            $_POST['hra']));      

    
    if($r1) {
      sql_commit();
      echo html_error("Hra byla odmítnuta.");
    } else {
      echo html_error("Chyba při odmítnutí hry.");
      sql_rollback();
    }               
  } else {
    echo html_error($errors);
  }        
}
      

?>
