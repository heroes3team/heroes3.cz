<?php
  
if($_SERVER['REQUEST_METHOD']=='POST' && 
    (   isset($_POST['potvrdit_plus'])
    ||  isset($_POST['potvrdit_minus'])
    ||  isset($_POST['rozhodci_potvrdit']))) {
   
      
  $errors = "";
  
  $h = sql_one_row(sprintf("SELECT * FROM hry WHERE id=%d", $_POST['hra']));
   
  # kontrola opravneni
  
  # nelze potvrdit potvrzenou hru
  if($h['potvrzeno']!==null) {
    $errors .= "Hra už je potvrzena. Nemůžete ji tedy potvrdit.";
  }
  
  # jen porazeny muze udelat potvrdit plus nebo potvrdit minus
  if(isset($_POST['potvrdit_plus']) || isset($_POST['potvrdit_minus'])) {
    if($_SESSION['uzivatel']['id'] != $h['porazeny']) {
      $errors .= "Pouze poražený hráč může potvrdit hru";
    }
  }
  
  # jen rozhodci muze potvrdit hru jako rozhodci
  if(isset($_POST['rozhodci_potvrdit'])) {
    if(!$_SESSION['uzivatel']['rozhodci']) {
      $errors .= "Pouze rozhodčí může potvrdit hru";
    }
  }
  
  if(empty($errors)) {
    # potvrdime hru
    sql_begin();
    
    # fairplay a karma
    $vitez_fairplay = 1;
    $porazeny_fairplay = $h['porazeny_fairplay'];
    if(isset($_POST['potvrdit_minus'])) $vitez_fairplay = 0;  
    $karma_vitez = 1;
    $karma_porazeny = 1;
    if($vitez_fairplay == 0) $karma_vitez = -3;
    if($porazeny_fairplay == 0) $karma_porazeny = -3;
    if(isset($_POST['rozhodci_potvrdit'])) $karma_porazeny = -3;
    
    $r1 = sql_query(sprintf("UPDATE hry SET vitez_fairplay=%s, porazeny_fairplay=%s
                            WHERE id=%d",
                            $vitez_fairplay, $porazeny_fairplay,
                            $_POST['hra']));   
     
    
    
    $vitez = sql_one_row(sprintf("SELECT * FROM hraci WHERE id=%d", $h['vitez']));
    $porazeny = sql_one_row(sprintf("SELECT * FROM hraci WHERE id=%d", $h['porazeny']));
	$vitez_celkem_her_count = sql_getfield(sprintf("SELECT COUNT(*) AS count FROM hry WHERE vitez=%d OR porazeny=%d AND potvrzeno IS NOT NULL", $vitez['id'], $vitez['id']), 'count');
	$porazeny_celkem_her_count = sql_getfield(sprintf("SELECT COUNT(*) AS count FROM hry WHERE vitez=%d OR porazeny=%d AND potvrzeno IS NOT NULL", $porazeny['id'], $porazeny['id']), 'count');
	$vitez_vyhranych_her_count = sql_getfield(sprintf("SELECT COUNT(*) AS count FROM hry WHERE vitez=%d AND potvrzeno IS NOT NULL", $vitez['id'], $vitez['id']), 'count');
	$porazeny_vyhranych_her_count = sql_getfield(sprintf("SELECT COUNT(*) AS count FROM hry WHERE vitez=%d AND potvrzeno IS NOT NULL", $porazeny['id']), 'count');	
	
	
	# update karmy	
    $r3 = sql_query(sprintf("UPDATE hraci SET karma=%d WHERE id=%d", $vitez['karma']+$karma_vitez, $h['vitez']));
    $r4 = sql_query(sprintf("UPDATE hraci SET karma=%d WHERE id=%d", $porazeny['karma']+$karma_porazeny, $h['porazeny'])); 
    
    # update statistik hracu
    $vitez_celkem_her = $vitez_celkem_her_count+1;
    $porazeny_celkem_her = $porazeny_celkem_her_count+1;
    $vitez_vyhranych_her = $vitez_vyhranych_her_count+1;
    $porazeny_vyhranych_her = $porazeny_vyhranych_her_count;
    $vitez_uspesnost = 100 * $vitez_vyhranych_her / $vitez_celkem_her;
    $porazeny_uspesnost = 100 * $porazeny_vyhranych_her / $porazeny_celkem_her;

    # elo
    $rozdil = $vitez['elo']-$porazeny['elo'];
    $presun = 1;
    if($rozdil<=550) $presun = 2;
    if($rozdil<=450) $presun = 5;    
    if($rozdil<=300) $presun = 10;
    if($rozdil<=150) $presun = 15;
    if($rozdil<=100) $presun = 20;
    if($rozdil<= 50) $presun = 25;
    if($rozdil< -50) $presun = 30;
    if($rozdil<-100) $presun = 35;
    if($rozdil<-150) $presun = 40;
    if($rozdil<-300) $presun = 45;
    if($rozdil<-450) $presun = 48;
    if($rozdil<-550) $presun = 49;
    if($h['delka']==1) $presun = ceil($presun / 2);
    
    $vitez_elo = $vitez['elo'] + $presun;
    $porazeny_elo = $porazeny['elo'] - $presun;
    
    $deadline = '1970-01-01 00:00:00';
    $uzaverka = sql_one_row("SELECT * FROM uzaverky ORDER BY cas DESC, id DESC LIMIT 1;");
    if($uzaverka) $deadline = $uzaverka['cas'];


    
    # body
    $vitez_body = $vitez['body'];
    $porazeny_body = $porazeny['body'];
    $predchozi_hra = sql_one_row(sprintf("SELECT * FROM hry WHERE ((vitez=%d AND porazeny=%d) OR (vitez=%d AND porazeny=%d)) AND potvrzeno IS NOT NULL AND potvrzeno>'%s'", $vitez['id'], $porazeny['id'], $porazeny['id'], $vitez['id'], $deadline));

    if($vitez['skupina']==$porazeny['skupina'] || ($vitez['skupina']==1 && $porazeny['skupina']==2) || ($vitez['skupina']==2 && $porazeny['skupina']==1)  ) {  
	//die ("stejna skupina");
      if($predchozi_hra == null) { 
	//die ("predchozi hra neni");
        $vitez_body += 50;
        $porazeny_body += 10;
      } else { 
	//print_r($predchozi_hra);
	//die("predchozi hra je: ".$predchozi_hra );
	}
    } else {
	//die ("ruzna skupina");
      if($predchozi_hra == null) { 
	//die ("predchozi hra neni");
        $vitez_body += 5;
        $porazeny_body += 1;
      } else { 
	//print_r($predchozi_hra);
	//die("predchozi hra je: ".$predchozi_hra );
	}
    }
    
    # karma
    $vitez_karma = $vitez['karma'];
    $porazeny_karma = $porazeny['karma'];
    
    if(isset($_POST['potvrdit_plus'])) $vitez_karma += 1;
    if(isset($_POST['potvrdit_minus'])) $vitez_karma -= 3;
    if(isset($_POST['rozhodci_potvrdit'])) $vitez_karma += 1;
    
    if($h['porazeny_fairplay']==0 || isset($_POST['rozhodci_potvrdit'])) {
      $porazeny_karma -= 3;
    } else {
      $porazeny_karma += 1;
    }
         
    $r1 = sql_query(sprintf("UPDATE hry SET
                              souhlas_porazeneho=%s,
                              souhlas_rozhodciho=%s,
                              potvrzeno=NOW()
                              WHERE id=%d",
                              (isset($_POST['potvrdit_plus']) || isset($_POST['potvrdit_minus']) ? 'TRUE' : 'FALSE'),
                              (isset($_POST['rozhodci_potvrdit']) ? 'TRUE' : 'NULL'),
                              $_POST['hra']
                              ));
                               

    $r2 = sql_query(sprintf("UPDATE hraci SET
                              vyhranych_her=%d,
                              celkem_her=%d,
                              uspesnost=%f,
                              elo=%d,
                              body=%d,
                              karma=%d
                             WHERE id=%d",
                             $vitez_vyhranych_her,
                             $vitez_celkem_her,
                             $vitez_uspesnost,
                             $vitez_elo,
                             $vitez_body,
                             $vitez_karma,
                             $vitez['id']
                             ));

    $r3 = sql_query(sprintf("UPDATE hraci SET
                              vyhranych_her=%d,
                              celkem_her=%d,
                              uspesnost=%f,
                              elo=%d,
                              body=%d,
                              karma=%d
                             WHERE id=%d",
                             $porazeny_vyhranych_her,
                             $porazeny_celkem_her,
                             $porazeny_uspesnost,
                             $porazeny_elo,
                             $porazeny_body,
                             $porazeny_karma,
                             $porazeny['id']
                             ));    
    
    if($r1 && $r2 && $r3) {
      sql_commit();
      echo html_error("Hra byla úspěšně potvrzena.");
    } else {
      echo html_error("Chyba při potvrzování hry.");
      sql_rollback();
    }          
     
     
  } else {
    echo html_error($errors);
  }
       
}
      

?>
