<?php
/*
* Domovska stranka ligy
*/

include('../_common_start.php');

$TITLE = 'Jak hrát online';
$MODUL = 'h3_info';

html_start();

# levy sloupec
sloupec_start(310);
	# login / uzivatel.panel
	include($INCLUDE_DIR.'/login_ramecek.php');
sloupec_end ();

# pravy sloupec
sloupec_start();
	# pokud neni zadan detail, vypisujeme dale tabulky:
	if(!isset($_GET['detail'])) {
	  # formular + zpracovani dat
	  if($_SESSION['uzivatel']['id']) { 
		include('ulozit_novou_hru.php');
		include('html_nova_hra_formular.php'); 
	  }
	}
sloupec_end();


html_end();

?>
