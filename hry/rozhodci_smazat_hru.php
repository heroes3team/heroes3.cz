<?php

if($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['rozhodci_smazat'])) {
  //echo "POST: ";
  //print_r($_POST);
 
  $errors = '';
  
  # kontrola prihlaseni
  if(!$_SESSION['uzivatel']['id']) {
    $errors .= " Nejste přihlášen. Zřejmě vypršela doba vašeho přihlášení. Musíte se znovu přihlásit.<br/>";
  }
  
  # najdeme hru v DB
  $h = sql_one_row(sprintf("SELECT * FROM hry WHERE id=%d", $_POST['hra']));
  
  if(!$h) {
    $errors .= " Hra nebyla nalezena.<br/>";
  }
  
  # Jsi rozhodci
  if(!$_SESSION['uzivatel']['rozhodci']) {
    $errors .= " Nemáte oprávnění smazat tuto hru.<br/>";
  }
  
  # Hra neni potvrzena
  if(!empty($h['potvrzeno'])) {
    $errors .= " Hra už je potvrzená.<br/>";
  }  
  
  # porazeny se jeste nevyjadril
  if($h['souhlas_porazeneho']."" !== "0") {
    $errors .= " Poražený hráč se ke hře ještě vyjádřil.<br/>";
  }
 
  # Hra neni potvrzena
  if(!empty($h['potvrzeno'])) {
    $errors .= " Hra už je potvrzená.<br/>";
  }  
  
  # DELETE
  if(empty($errors)) {
    sql_begin();  
    //$result1 = sql_query(sprintf("DELETE FROM hry WHERE id=%d", $_POST['hra']));
    $result1 = sql_query(sprintf("UPDATE hry SET souhlas_rozhodciho=FALSE WHERE id=%d", $_POST['hra']));
    $porazeny = sql_one_row(sprintf("SELECT * FROM hraci WHERE id=%d", $h['porazeny']));
    $karma = $porazeny['karma'] - 3;
    $result2 = sql_query(sprintf("UPDATE hraci SET karma=%d WHERE id=%d", $karma, $h['porazeny']));
    sql_commit();                                                  
    if($result1 && $result2) {
      sql_commit();
      echo html_error("Hra byla úspěšně smazána");  
    } else {
      sql_rollback();
      echo html_error("Chyba při mazání hry.");
    }                                                 
  } else {
    echo html_error($errors);
  }
}

?>
