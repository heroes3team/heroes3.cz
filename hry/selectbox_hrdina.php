<?php

function selectbox_hrdina($id, $random = true) {

  global $IMG;

  return "
  
        <table class=\"podklad\" id=\"selectbox_".$id."\" style=\"display: none; font-size: 10px; max-width: 210px; padding: 0; \">
            ".($random ? "
          <tr>
            <td >
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'random'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/random.jpg\" alt=\"\" />
                random
              </a>
            </td>
            <td >
               &nbsp;
            </td>
            <td >
               &nbsp;
            </td>
            <td>
               &nbsp;
            </td>
          </tr>
          " : "")." 
          
          <tr>
            <td style=\"width: 25%\">
              <a name=\"castle_hero\" href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'edric'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/edric.jpg\" alt=\"\" />
                Edric
              </a>
            </td>
            <td style=\"width: 25%\">
               <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'lord haart'); return true;\" > 
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/lord%20haart.jpg\" alt=\"\" />
                L. Haart
              </a>
            </td>
            <td style=\"width: 25%\">
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'orrin'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/orrin.jpg\" alt=\"\" />
                Orrin
              </a>
            </td>
            <td>
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'beatrice'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/beatrice.jpg\" alt=\"\" />
                Beatrice
              </a>
            </td>
		</tr>
          
          <tr>
            <td>
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'christian'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/christian.jpg\" alt=\"\" />
                Christian
              </a>
            </td>
		  <td>
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'sorsha'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/sorsha.jpg\" alt=\"\" />
                Sorsha
              </a>
            </td>
            <td >
               <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'sylvia'); return true;\" > 
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/sylvia.jpg\" alt=\"\" />
                Sylvia
              </a>
            </td>
            <td >
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'tyris'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/tyris.jpg\" alt=\"\" />
                Tyris
              </a>
            </td>
          </tr>
          
          <tr>
            <td>
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'valeska'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/valeska.jpg\" alt=\"\" />
                Valeska
              </a>
            </td>
            <td >
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'adela'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/adela.jpg\" alt=\"\" />
                Adela
              </a>
            </td>
            <td >
               <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'adelaide'); return true;\" > 
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/adelaide.jpg\" alt=\"\" />
                Adelaide
              </a>
            </td>
            <td >
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'caitlin'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/caitlin.jpg\" alt=\"\" />
                Caitlin
              </a>
            </td>
          </tr>          
        
          <tr>
            <td>
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'cuthbert'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/cuthbert.jpg\" alt=\"\" />
                Cuthbert
              </a>
            </td>
            <td >
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'ingham'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/ingham.jpg\" alt=\"\" />
                Ingham
              </a>
            </td>
            <td >
               <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'loynis'); return true;\" > 
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/loynis.jpg\" alt=\"\" />
                Loynis
              </a>
            </td>
            <td >
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'rion'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/rion.jpg\" alt=\"\" />
                Rion
              </a>
            </td>
          </tr>
		  
          <tr>		  
            <td>
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'sanya'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/sanya.jpg\" alt=\"\" />
                Sanya
              </a>
            </td>          
            <td >
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'sir mullich'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/sir%20mullich.jpg\" alt=\"\" />
                Sir Mullich
              </a>
            </td>
          </tr>
            <td >
            </br></br>
            </td>		  
          
          <tr>
            <td style=\"width: 25%\">
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'clancy'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/clancy.jpg\" alt=\"\" />
                Clancy
              </a>
            </td>
            <td style=\"width: 25%\">
               <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'giselle'); return true;\" > 
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/giselle.jpg\" alt=\"\" />
                Giselle
              </a>
            </td>
            <td style=\"width: 25%\">
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'ivor'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/ivor.jpg\" alt=\"\" />
                Ivor
              </a>
            </td>
            <td>
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'jenova'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/jenova.jpg\" alt=\"\" />
                Jenova
              </a>
            </td>
          </tr>          
       
          <tr>
            <td style=\"width: 25%\">
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'kyrre'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/kyrre.jpg\" alt=\"\" />
                Kyrre
              </a>
            </td>
            <td style=\"width: 25%\">
               <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'mephala'); return true;\" > 
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/mephala.jpg\" alt=\"\" />
                Mephala
              </a>
            </td>
            <td style=\"width: 25%\">
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'ryland'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/ryland.jpg\" alt=\"\" />
                Ryland
              </a>
            </td>
            <td>
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'thorgrim'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/thorgrim.jpg\" alt=\"\" />
                Thorgrim
              </a>
            </td>
          </tr>        
          
          <tr>
            <td style=\"width: 25%\">
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'ufretin'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/ufretin.jpg\" alt=\"\" />
                Ufretin
              </a>
            </td>
            <td style=\"width: 25%\">
               <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'aeris'); return true;\" > 
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/aeris.jpg\" alt=\"\" />
                Aeris
              </a>
            </td>
            <td style=\"width: 25%\">
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'alagar'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/alagar.jpg\" alt=\"\" />
                Alagar
              </a>
            </td>
            <td>
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'coronius'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/coronius.jpg\" alt=\"\" />
                Coronius
              </a>
            </td>
          </tr> 
     
          <tr>
            <td style=\"width: 25%\">
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'elleshar'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/elleshar.jpg\" alt=\"\" />
                Elleshar
              </a>
            </td>
            <td style=\"width: 25%\">
               <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'gem'); return true;\" > 
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/gem.jpg\" alt=\"\" />
                Gem
              </a>
            </td>
            <td style=\"width: 25%\">
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'malcom'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/malcom.jpg\" alt=\"\" />
                Malcom
              </a>
            </td>
            <td>
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'melodia'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/melodia.jpg\" alt=\"\" />
                Melodia
              </a>
            </td>
          </tr>
         <tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'uland'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/uland.jpg\" alt=\"\" />
              Uland
            </a>
          </td>
        </tr> 		  
            <td >
            </br></br>
            </td>
			
          <tr>
            <td style=\"width: 25%\">
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'fafner'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/fafner.jpg\" alt=\"\" />
                Fafner
              </a>
            </td>
            <td style=\"width: 25%\">
               <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'iona'); return true;\" > 
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/iona.jpg\" alt=\"\" />
                Iona
              </a>
            </td>
            <td style=\"width: 25%\">
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'josephine'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/josephine.jpg\" alt=\"\" />
                Josephine
              </a>
            </td>
            <td>
              <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'neela'); return true;\" >
                <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/neela.jpg\" alt=\"\" />
                Neela
              </a>
            </td>
          </tr>          
          
         <tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'piquedram'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/piquedram.jpg\" alt=\"\" />
              Piquedram
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'rissa'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/rissa.jpg\" alt=\"\" />
              Rissa
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'thane'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/thane.jpg\" alt=\"\" />
              Thane
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'torosar'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/torosar.jpg\" alt=\"\" />
              Torosar
            </a>
          </td>
        </tr>         
          
         <tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'aine'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/aine.jpg\" alt=\"\" />
              Aine
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'astral'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/astral.jpg\" alt=\"\" />
              Astral
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'cyra'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/cyra.jpg\" alt=\"\" />
              Cyra
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'daremyth'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/daremyth.jpg\" alt=\"\" />
              Daremyth
            </a>
          </td>
        </tr>
 
         <tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'halon'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/halon.jpg\" alt=\"\" />
              Halon
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'serena'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/serena.jpg\" alt=\"\" />
              Serena
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'solmyr'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/solmyr.jpg\" alt=\"\" />
              Solmyr
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'theodorus'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/theodorus.jpg\" alt=\"\" />
              Theodorus
            </a>
          </td>
        </tr>          
            <td >
            </br></br>
            </td>

			<tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'calh'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/calh.jpg\" alt=\"\" />
              Calh
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'fiona'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/fiona.jpg\" alt=\"\" />
              Fiona
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'ignatius'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/ignatius.jpg\" alt=\"\" />
              Ignatius
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'marius'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/marius.jpg\" alt=\"\" />
              Marius
            </a>
          </td>
        </tr>   
       
         <tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'nymus'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/nymus.jpg\" alt=\"\" />
              Nymus
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'octavia'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/octavia.jpg\" alt=\"\" />
              Octavia
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'pyre'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/pyre.jpg\" alt=\"\" />
              Pyre
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'rashka'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/rashka.jpg\" alt=\"\" />
              Rashka
            </a>
          </td>
        </tr>       
          
         <tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'ash'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/ash.jpg\" alt=\"\" />
              Ash
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'axsis'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/axsis.jpg\" alt=\"\" />
              Axsis
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'ayden'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/ayden.jpg\" alt=\"\" />
              Ayden
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'calid'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/calid.jpg\" alt=\"\" />
              Calid
            </a>
          </td>
        </tr>
          
         <tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'olema'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/olema.jpg\" alt=\"\" />
              Olema
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'xarfax'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/xarfax.jpg\" alt=\"\" />
              Xarfax
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'xyron'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/xyron.jpg\" alt=\"\" />
              Xyron
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'zydar'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/zydar.jpg\" alt=\"\" />
              Zydar
            </a>
          </td>

		  </tr>          
            <td >
            </br></br>
            </td>          
         <tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'ajit'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/ajit.jpg\" alt=\"\" />
              Ajit
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'arlach'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/arlach.jpg\" alt=\"\" />
              Arlach
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'dace'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/dace.jpg\" alt=\"\" />
              Dace
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'damacon'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/damacon.jpg\" alt=\"\" />
              Damacon
            </a>
          </td>
        </tr>           
          
         <tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'gunnar'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/gunnar.jpg\" alt=\"\" />
              Gunnar
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'lorelei'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/lorelei.jpg\" alt=\"\" />
              Lorelei
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'shakti'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/shakti.jpg\" alt=\"\" />
              Shakti
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'synca'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/synca.jpg\" alt=\"\" />
              Synca
            </a>
          </td>
        </tr>         
        
         <tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'alamar'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/alamar.jpg\" alt=\"\" />
              Alamar
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'darkstorn'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/darkstorn.jpg\" alt=\"\" />
              Darkstorn
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'deemer'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/deemer.jpg\" alt=\"\" />
              Deemer
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'geon'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/geon.jpg\" alt=\"\" />
              Geon
            </a>
          </td>
        </tr>         
          
         <tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'jaegar'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/jaegar.jpg\" alt=\"\" />
              Jaegar
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'jeddite'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/jeddite.jpg\" alt=\"\" />
              Jeddite
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'malekith'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/malekith.jpg\" alt=\"\" />
              Malekith
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'sephinroth'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/sephinroth.jpg\" alt=\"\" />
              Sephinroth
            </a>
          </td>
        </tr>           
            <td >
            </br></br>
            </td>          

			<tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'charna'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/charna.jpg\" alt=\"\" />
              Charna
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'clavius'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/clavius.jpg\" alt=\"\" />
              Clavius
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'ranloo'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/ranloo.jpg\" alt=\"\" />
              Ranloo
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'galthran'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/galthran.jpg\" alt=\"\" />
              Galthran
            </a>
          </td>
        </tr>          

         <tr>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'isra'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/isra.jpg\" alt=\"\" />
              Isra
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'moandor'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/moandor.jpg\" alt=\"\" />
              Moandor
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'straker'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/straker.jpg\" alt=\"\" />
              Straker
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'tamika'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/tamika.jpg\" alt=\"\" />
              Tamika
            </a>
          </td>
        </tr> 
         
         <tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'vokial'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/vokial.jpg\" alt=\"\" />
              Vokial
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'aislinn'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/aislinn.jpg\" alt=\"\" />
              Aislinn
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'nagash'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/nagash.jpg\" alt=\"\" />
              Nagash
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'nimbus'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/nimbus.jpg\" alt=\"\" />
              Nimbus
            </a>
        </tr>          
    
         <tr>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'sandro'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/sandro.jpg\" alt=\"\" />
              Sandro
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'septienna'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/septienna.jpg\" alt=\"\" />
              Septienna
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'thant'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/thant.jpg\" alt=\"\" />
              Thant
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'vidomina'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/vidomina.jpg\" alt=\"\" />
              Vidomina
            </a>
          </td>
        </tr> 
         <tr>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'xsi'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/xsi.jpg\" alt=\"\" />
              Xsi
            </a>
          </td>
        </tr>
            <td >
            </br></br>
            </td>		
        
         <tr>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'alkin'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/alkin.jpg\" alt=\"\" />
              Alkin
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'broghild'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/broghild.jpg\" alt=\"\" />
              Broghild
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'bron'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/bron.jpg\" alt=\"\" />
              Bron
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'drakon'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/drakon.jpg\" alt=\"\" />
              Drakon
            </a>
          </td>
        </tr>         

         <tr>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'gerwulf'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/gerwulf.jpg\" alt=\"\" />
              Gerwulf
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'korbac'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/korbac.jpg\" alt=\"\" />
              Korbac
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'tazar'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/tazar.jpg\" alt=\"\" />
              Tazar
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'wystan'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/wystan.jpg\" alt=\"\" />
              Wystan
            </a>
          </td>
        </tr>         

         <tr>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'andra'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/andra.jpg\" alt=\"\" />
              Andra
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'merist'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/merist.jpg\" alt=\"\" />
              Merist
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'mirlanda'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/mirlanda.jpg\" alt=\"\" />
              Mirlanda
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'rosic'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/rosic.jpg\" alt=\"\" />
              Rosic
            </a>
          </td>
        </tr>  
			
         <tr>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'styg'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/styg.jpg\" alt=\"\" />
              Styg
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'tiva'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/tiva.jpg\" alt=\"\" />
              Tiva
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'kinkeria'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/kinkeria.jpg\" alt=\"\" />
              Kinkeria
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'verdish'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/verdish.jpg\" alt=\"\" />
              Verdish
            </a>
          </td>
        </tr>
         <tr>		
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'voy'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/voy.jpg\" alt=\"\" />
              Voy
            </a>
          </td>		  
        </tr>
            <td >
            </br></br>
            </td>
			
         <tr>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'crag hack'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/crag%20hack.jpg\" alt=\"\" />
              Crag Hack
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'gretchin'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/gretchin.jpg\" alt=\"\" />
              Gretchin
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'gurnisson'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/gurnisson.jpg\" alt=\"\" />
              Gurnisson
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'jabarkas'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/jabarkas.jpg\" alt=\"\" />
              Jabarkas
            </a>
          </td>
        </tr> 

         <tr>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'krellion'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/krellion.jpg\" alt=\"\" />
              Krellion
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'shiva'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/shiva.jpg\" alt=\"\" />
              Shiva
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'tyraxor'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/tyraxor.jpg\" alt=\"\" />
              Tyraxor
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'yog'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/yog.jpg\" alt=\"\" />
              Yog
            </a>
          </td>
        </tr> 

         <tr>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'dessa'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/dessa.jpg\" alt=\"\" />
              Dessa
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'tiva'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/gird.jpg\" alt=\"\" />
              Gird
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'gundula'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/gundula.jpg\" alt=\"\" />
              Gundula
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'oris'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/oris.jpg\" alt=\"\" />
              Oris
            </a>
          </td>
        </tr> 
          
         <tr>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'saurug'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/saurug.jpg\" alt=\"\" />
              Saurug
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'terek'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/terek.jpg\" alt=\"\" />
              Terek
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'vey'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/vey.jpg\" alt=\"\" />
              Vey
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'zubin'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/zubin.jpg\" alt=\"\" />
              Zubin
            </a>
          </td>
        </tr>           
            <td >
            </br></br>
            </td>    
			
         <tr>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'lacus'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/lacus.jpg\" alt=\"\" />
              Lacus
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'kalt'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/kalt.jpg\" alt=\"\" />
              Kalt
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'ignissa'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/ignissa.jpg\" alt=\"\" />
              Ignissa
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'fiur'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/fiur.jpg\" alt=\"\" />
              Fiur
            </a>
          </td>
        </tr>         
     
         <tr>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'thunar'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/thunar.jpg\" alt=\"\" />
              Thunar
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'erdamon'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/erdamon.jpg\" alt=\"\" />
              Erdamon
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'pasis'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/pasis.jpg\" alt=\"\" />
              Pasis
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'monere'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/monere.jpg\" alt=\"\" />
              Monere
            </a>
          </td>
        </tr>     
 
         <tr>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'brissa'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/brissa.jpg\" alt=\"\" />
              Brissa
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'aenain'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/aenain.jpg\" alt=\"\" />
              aenain
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'gelare'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/gelare.jpg\" alt=\"\" />
              Gelare
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'ciele'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/ciele.jpg\" alt=\"\" />
              Ciele
            </a>
          </td>
        </tr> 

         <tr>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'inteus'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/inteus.jpg\" alt=\"\" />
              Inteus
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'luna'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/luna.jpg\" alt=\"\" />
              Luna
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'grindan'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/grindan.jpg\" alt=\"\" />
              Grindan
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'labetha'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/labetha.jpg\" alt=\"\" />
              Labetha
            </a>
          </td>
        </tr>
            <td >
            </br></br>
            </td>    
			
         <tr>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'anabel'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/anabel.jpg\" alt=\"\" />
              Anabel
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'andal'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/andal.jpg\" alt=\"\" />
              Andal
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'astra'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/astra.jpg\" alt=\"\" />
              Astra
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'casmetra'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/casmetra.jpg\" alt=\"\" />
              Casmetra
            </a>
          </td>
        </tr>

         <tr>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'cassiopeia'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/cassiopeia.jpg\" alt=\"\" />
              Cassiopeia
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'corkes'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/corkes.jpg\" alt=\"\" />
              Corkes
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'dargem'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/dargem.jpg\" alt=\"\" />
              Dargem
            </a>
          </td>
		  <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'derek'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/derek.jpg\" alt=\"\" />
              Derek
            </a>
          </td>
        </tr>
		
         <tr>
		  <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'elmore'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/elmore.jpg\" alt=\"\" />
              Elmore
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'eovacious'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/eovacious.jpg\" alt=\"\" />
              Eovacious
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'illor'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/illor.jpg\" alt=\"\" />
              Illor
            </a>
          </td>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'jeremy'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/jeremy.jpg\" alt=\"\" />
              Jeremy
            </a>
          </td>
        </tr>

         <tr>
		  <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'leena'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/leena.jpg\" alt=\"\" />
              Leena
            </a>
          </td>
          <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'manfred'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/manfred.jpg\" alt=\"\" />
              Manfred
            </a>
          </td>
		  <td style=\"width: 25%\">
             <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'miriam'); return true;\" > 
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/miriam.jpg\" alt=\"\" />
              Miriam
            </a>
          </td>
          <td style=\"width: 25%\">
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'spint'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/spint.jpg\" alt=\"\" />
              Spint
            </a>
          </td>
		</tr>

         <tr>
          <td>
            <a href=\"#new_game_top\" onclick=\"vyber_hodnotu('".$id."', 'zilare'); return true;\" >
              <img class=\"selectbox_mesto img_ramecek\" style=\"height: 45px; width: 42px\" src=\"".$IMG."/hrdinove/zilare.jpg\" alt=\"\" />
              Zilare
            </a>
          </td>
          <td style=\"width: 25%\">
		&nbsp;
          </td>
        </tr>

        </table>
  
  ";

}

?>
