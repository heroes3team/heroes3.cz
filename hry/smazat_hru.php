<?php

if($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['smazat'])) {
  //echo "POST: ";
  //print_r($_POST);
 
  $errors = '';
  
  # kontrola prihlaseni
  if(!$_SESSION['uzivatel']['id']) {
    $errors .= " Nejste přihlášen. Zřejmě vypršela doba vašeho přihlášení. Musíte se znovu přihlásit.<br/>";
  }
  
  # najdeme hru v DB
  $h = sql_one_row(sprintf("SELECT * FROM hry WHERE id=%d", $_POST['hra']));
  
  if(!$h) {
    $errors .= " Hra nebyla nalezena.<br/>";
  }
  
  # Jsi majitelem hry
  if($_SESSION['uzivatel']['id'] != $h['vitez']) {
    $errors .= " Nemáte oprávnění smazat tuto hru.<br/>";
  }
  
  # Hra neni potvrzena
  if(!empty($h['potvrzeno'])) {
    $errors .= " Hra už je potvrzená.<br/>";
  }  
  
  # porazeny se jeste nevyjadril
  if($h['souhlas_porazeneho'] !== null ) {
    $errors .= " Poražený hráč se již ke hře vyjádřil. Musíte počkat na rozhodnutí rozhodčího.<br/>";
  }
 
  # Hra neni potvrzena
  if(!empty($h['potvrzeno'])) {
    $errors .= " Hra už je potvrzená.<br/>";
  }  
  
  # DELETE
  if(empty($errors)) {  
    $result = sql_query(sprintf("DELETE FROM hry WHERE id=%d", $_POST['hra']));                                                  
    if($result) {
      echo html_error("Hra byla úspěšně smazána");  
    } else {
      echo html_error("Chyba při mazání hry.");
    }                                                 
  } else {
    echo html_error($errors);
  }
}


?>