<?php

if($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['ulozit'])) {
  //echo "POST: ";
  //print_r($_POST);
 
  $errors = '';
  
  # kontrola prihlaseni
  if(!$_SESSION['uzivatel']['id']) {
    $errors .= " Nejste přihlášen. Zřejmě vypršela doba vašeho přihlášení. Musíte se znovu přihlásit.<br/>";
  }

  # kontrola 'sam se sebou'
  if($_SESSION['uzivatel']['jmeno']==$_POST['porazeny_jmeno']) {
    $errors .= " Nemůžete ukládat hry 'sám se sebou'.<br/>";
  }  

  # kontrola jmena porazeneho
  $porazeny = 0;  
  $row = sql_one_row(sprintf("SELECT * FROM hraci WHERE jmeno='%s'", $_POST['porazeny_jmeno']));  
  if($row == false) {
    $errors .= "Hráč ".$_POST['porazeny_jmeno']." nebyl nalezen.<br/>";
  } elseif ($row['aktivni'] == false) {
    $errors .= " Hráč ".$_POST['porazeny_jmeno']." je zablokovaný. Nemůže se tedy účastnit ligy.<br/>"; 
  } else {
    $porazeny = $row['id'];
  }

  # kontrola mest a hrdinu
  $all_ok = true;
  if(empty($_POST['vitez_start_mesto'])) $all_ok = false;
  if(empty($_POST['vitez_hlavni_mesto'])) $all_ok = false;
  if(empty($_POST['vitez_start_hrdina'])) $all_ok = false;
  if(empty($_POST['vitez_hlavni_hrdina'])) $all_ok = false;
  if(empty($_POST['porazeny_start_mesto'])) $all_ok = false;
  if(empty($_POST['porazeny_hlavni_mesto'])) $all_ok = false;
  if(empty($_POST['porazeny_start_hrdina'])) $all_ok = false;
  if(empty($_POST['porazeny_hlavni_hrdina'])) $all_ok = false;

  if(!$all_ok)    $errors .= " Musíte vyplnit všchna města a hrdiny.<br/>";

  # INSERT
  if(empty($errors)) {  
    $result = sql_query(sprintf("INSERT INTO hry (vitez_cerveny, vitez, vitez_start_mesto, vitez_hlavni_mesto, vitez_start_hrdina, vitez_hlavni_hrdina, vitez_fairplay,
                                                  porazeny, porazeny_start_mesto, porazeny_hlavni_mesto, porazeny_start_hrdina, porazeny_hlavni_hrdina, porazeny_fairplay,
                                                  typ_mapy, velikost_mapy, obtiznost, delka)
                                          VALUES (%d, %d, '%s', '%s', '%s', '%s', 1,
                                                  %d, '%s', '%s', '%s', '%s', %d,
                                                  '%s', '%s', '%s', %d)",
                                                  $_POST['vitez_cerveny'], $_SESSION['uzivatel']['id'], $_POST['vitez_start_mesto'], $_POST['vitez_hlavni_mesto'], $_POST['vitez_start_hrdina'], $_POST['vitez_hlavni_hrdina'],
                                                  $porazeny, $_POST['porazeny_start_mesto'], $_POST['porazeny_hlavni_mesto'], $_POST['porazeny_start_hrdina'], $_POST['porazeny_hlavni_hrdina'], $_POST['porazeny_fairplay'],
                                                  $_POST['typ_mapy'], $_POST['velikost_mapy'], $_POST['obtiznost'], $_POST['delka']));                                                  
    if($result) {
      echo html_error("Hra byla úspěšně uložena");  
    } else {
      echo html_error("Chyba při ukládání hry.");
    }                                                 
  } else {
    echo html_error($errors);
  }
}


?>
