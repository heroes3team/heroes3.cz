<?php
/*
* Domovska stranka ligy
*/
include('_common_start.php');

$TITLE = '';
$MODUL = 'domu';

html_start();

# levy sloupec
sloupec_start(310);
# login / uzivatel.panel
include($INCLUDE_DIR.'/login_ramecek.php');


# oceneni
ramecek_start(286, 'Ocenění');

$sql = sql_query("SELECT m.*, h.jmeno FROM hraci_medaile AS m JOIN hraci AS h ON h.id=m.hrac ORDER BY udeleno DESC LIMIT 3");
if(sql_num_rows($sql)>0) {
  while($row = sql_fetch_array($sql)) {
	echo "<div class = 'podklad'>";
	echo html_href('hraci/detail.php?id='.$row['hrac'], $row['jmeno'])." - ".$medaile[$row['medaile']]['nazev'].'<br/>';
	echo html_medaile($row) . '</div> <div style = "height: 5px;"></div>';
	
  }
}

ramecek_end(286);


# stat.
ramecek_start(286, 'Info');

	$hracu = sql_getfield("SELECT COUNT(*) AS hracu FROM hraci;", 'hracu');
	$her = sql_getfield("SELECT COUNT(*) AS her FROM hry WHERE potvrzeno IS NOT NULL;", 'her');
	$prispevku = sql_getfield("SELECT COUNT(*) AS prispevku FROM forum_prispevky;", 'prispevku');
	$komentaru = sql_getfield("SELECT COUNT(*) AS komentaru FROM hry_komentare;", 'komentaru');

	$deadline = date("Y-m-d H:i:s", time() - 60*5);
	$sql = sql_query($dotaz = "SELECT id, jmeno FROM hraci WHERE posledni_prihlaseni>'".$deadline."' ORDER BY jmeno;");
	$online = array();
	while($row = sql_fetch_array($sql)) {
	  $online[] = html_href($ROOT_URL.'/hraci/detail.php?id='.$row['id'], $row['jmeno']);
	}
	$online = implode(', ', $online);

	$sql = sql_query($dotaz = "SELECT id, jmeno FROM hraci WHERE admin IS TRUE ORDER BY jmeno;");
	$admini = array();
	while($row = sql_fetch_array($sql)) {
	  $admini[] = html_href($ROOT_URL.'/hraci/detail.php?id='.$row['id'], $row['jmeno']);
	}
	$admini = implode(', ', $admini);

	$sql = sql_query($dotaz = "SELECT id, jmeno FROM hraci WHERE moderator IS TRUE ORDER BY jmeno;");
	$moderatori = array();
	while($row = sql_fetch_array($sql)) {
	  $moderatori[] = html_href($ROOT_URL.'/hraci/detail.php?id='.$row['id'], $row['jmeno']);
	}
	$moderatori = implode(', ', $moderatori);

	$sql = sql_query($dotaz = "SELECT id, jmeno FROM hraci WHERE rozhodci IS TRUE ORDER BY jmeno;");
	$rozhodci = array();
	while($row = sql_fetch_array($sql)) {
	  $rozhodci[] = html_href($ROOT_URL.'/hraci/detail.php?id='.$row['id'], $row['jmeno']);
	}
	$rozhodci = implode(', ', $rozhodci);
	html_podklad_start ();
		echo '<span class = "strong">Zodpovědné osoby:</span><br/> ';
		echo '<div class ="text_n"> <span class="admin">admin</span>: '.$admini.'<br/>';
		echo '<span class="moderator">moderátor</span>: '.$moderatori.'<br/>';
		echo '<span class="rozhodci">rozhodčí</span>: '.$rozhodci.'</div><br/>';
	html_podklad_end ();
	html_podklad_start ();
		echo '<span class = "strong">Právě jsou online:</span><br/><div class ="text_n">' . $online . '</div>';
	html_podklad_end ();
	echo "
		<a href = 'https://www.facebook.com/groups/134142730021026/' target='_blank' > 
			<img src = 'fb-icon.png' class = 'obrazek-fb'/>
		</a>
	";

ramecek_end(286);
sloupec_end();


# prostredni sloupec
sloupec_start(525);

# Obecné informace
ramecek_start(490, 'Obecné informace');
	echo '<div style="margin-left: 5px; padding-right: 5px;">';
		echo h2('Úvodní slova');
		echo html_p('Smyslem těchto stránek je umožnit hráčům Heroes 3 v první řadě přehledně porovnávat jejich schopnosti a herní úspěchy, v druhé řadě pak navázat nové kontaky a najít si další kamarády a soupeře pro online hraní.');
		echo '<br>';
		echo h2('Doporučení pro nové hráče');
		echo '<p class = "paragraph">';
		echo '1. Předtím než začněte hrát pečlivě si pročíst sekci <a class = "strong2" href = "/jak_hrat_online"> Jak hrát online </a> a <a class = "strong2" href = "/liga_info">Pravidla</a>.';
		echo '<br>';
		echo '2. Spoustu tipů a rad jak se stát dobrým hráčem naleznete v sekci <a class = "strong2" href = "/galerie"> Galerie</a>.';
		echo '<br>';
		echo '3. Komunikace mezi hráči, včetně domlouvání zápasů, probíhá především v Heroes 3 Skype skupině. Pro přidání, klikněte na <a class = "strong2" href = "https://join.skype.com/GL0PMbhINcy5">tento odkaz</a>, případně napište do veřejné <a class = "strong2" href = "/diskuze"> Diskuze</a>. Alternativní způsob komunikace probíhá na <a class = "strong2" href = "https://discord.gg/2eK3BK7">Discordu</a>. ';
		echo '</p>';		
	echo '</div>';
ramecek_end(490);

# Novinky
ramecek_start(490, 'Novinky');
	echo '<div style="margin-left: 5px; padding-right: 5px;">';
	echo h2('World Cup 2019');
	echo html_p('9.10.2019 | Tomis');
	echo '<img class = "img_ramecek img_novinky" src="/_include/img/ostatni/world-cup-2019.jpg" /><br>';
	echo html_p('Blíží se letošní světový pohár v Heroes 3. Kdo se účastní, jaká jsou pravidla a jaké ceny? Pro více informací sledujte diskuzi <span class="strong">'.html_href($ROOT_URL.'/diskuze?vlakno='.'6', "Turnaje").'</span>'.
		' a oficiální stránky turnaje <span class="strong">'.html_href('http://forum.heroesworld.ru/showthread.php?p=1161927', "ZDE").'</span">.');
	echo '</div>';
ramecek_end(490);

# posledni prispevky
$max = 100;
ramecek_start(490, 'Poslední příspěvky');
$sql = sql_query("SELECT * FROM  forum_vlakna ORDER BY id;");
$vlakna = array();
while($row = sql_fetch_array($sql)) {
	$vlakna[$row['id']] = $row;
}
foreach($vlakna as $id => $vlakno1) {
	//if($id>1) echo "<div style=\"margin-right: 7px;\"></div>";
	$row = sql_one_row(sprintf("SELECT P.*, H.jmeno
							  FROM forum_prispevky AS P JOIN hraci AS H ON P.vlozil=H.id
							  WHERE P.vlakno=%d ORDER BY P.id DESC, P.vlozeno DESC", $vlakno1['id']));
	echo "<div class = 'podklad'> <span class = 'strong' style = 'border-bottom: 1px solid;'>".html_href($ROOT_URL.'/diskuze?vlakno='.$vlakno1['id'], $vlakno1['nazev'])."</strong>".' | '.dt_ts_date_db2user($row['vlozeno']).' '.dt_ts_time_db2user($row['vlozeno'],2).' | '.html_href($ROOT_URL.'/hraci/detail.php?id='.$row['vlozil'],$row['jmeno']).": </span><br>";

	if(strlen($row['text']) > $max) {
		$row['text'] = substr($row['text'], 0, $max-3).'...';
	}

	echo '<div class = "text_n"><p class = "paragraph2">' . add_links(htmlspecialchars($row['text'])).'</p></div></div><div style = "height: 5px;"></div>';
}

ramecek_end(490);

# posledni komentare
ramecek_start(490, 'Poslední komentáře');
$sql = sql_query("SELECT K.*, H.jmeno
                  FROM  hry_komentare AS K JOIN hraci AS H ON K.vlozil=H.id
                  ORDER BY K.id DESC, K.vlozeno DESC
                  LIMIT 5;");
$counter = 0;
while($row = sql_fetch_array($sql)) {
	$counter++;
	echo "<div class = 'podklad'> <span class = 'strong' style = 'border-bottom: 1px solid;'>ID hry: " . html_href($ROOT_URL.'/hry?detail='.$row['hra'], $row['hra']) . ' | ' . dt_ts_date_db2user($row['vlozeno']) . ' ' . dt_ts_time_db2user($row['vlozeno'],2) . ' | ' . html_href($ROOT_URL.'/hraci/detail.php?id='.$row['vlozil'],$row['jmeno']).': </span>';
  if(strlen($row['text']) > $max) $row['text'] = substr($row['text'], 0, $max-3).'...';
  echo '<div class = "text_n"><p class = "paragraph2">'.add_links(htmlspecialchars($row['text'])).'</p></div></div><div style = "height: 5px;"></div>';  
}                 
                  
ramecek_end(490);

sloupec_end();

# pravy sloupec
sloupec_start(490);

# top 10
ramecek_start(490, 'Top 10');


$sql = sql_query("SELECT h.*, s.nazev FROM hraci AS h JOIN skupiny AS s ON h.skupina=s.id ORDER BY skupina ASC, body DESC, elo DESC, id ASC LIMIT 10");

echo '<div style="margin-left: 0px;">';
table_start('tab_top');
table_row(array('POŘADÍ',
                'SKUPINA',
                'JMÉNO',
                'BODY',
                'ELO',
                'WIN',
                'CELKEM HER',
                'ÚSPĚŠNOST'),0,0,0,0,1);

$poradi=0;
while($hrac = sql_fetch_array($sql)) {
  $poradi++;
  table_row(array($poradi.'.',
                  '<div style="margin-top: -3px; margin-bottom: -3px; margin-right: 1px;">'.html_image($IMG.'/skupiny/'.$hrac['skupina'].'.png', $hrac['nazev'], $hrac['nazev'], "cursor: help;").'</div>',
                  html_href($ROOT_URL.'/hraci/detail.php?id='.$hrac['id'], $hrac['jmeno']),
                  $hrac['body'],
                  $hrac['elo'],
                  $hrac['vyhranych_her'],
                  $hrac['celkem_her'],
                  round($hrac['uspesnost']).'%'
                   ),0,0,
                   array('right','right', 'left', 'right', 'right', 'right', 'right', 'right')
                   );
}

table_end();
echo '</div>';
ramecek_end(490);

echo '<br>';

echo html_nadpis('Nejnovější potvrzené hry');

$sql = sql_query("SELECT h.*, COUNT(k.id) AS komentare, vit.jmeno AS vitez_jmeno, por.jmeno AS por_jmeno
                  FROM hry AS h JOIN hry_komentare AS k ON k.hra=h.id JOIN hraci AS vit ON h.vitez=vit.id JOIN hraci AS por ON h.porazeny=por.id
                  WHERE potvrzeno IS NOT NULL
                  GROUP BY h.id
                  UNION
                  SELECT h.*, 0 AS komentare, vit.jmeno AS vitez_jmeno, por.jmeno AS por_jmeno
                  FROM hry AS h JOIN hraci AS vit ON h.vitez=vit.id JOIN hraci AS por ON h.porazeny=por.id
                  WHERE potvrzeno IS NOT NULL
                    AND NOT EXISTS (SELECT * FROM hry_komentare WHERE hra=h.id)
                  ORDER BY potvrzeno DESC, id DESC
                  LIMIT 4;");

while($hra = sql_fetch_array($sql)) {
  html_hra($hra);
  echo "<br>";
}

sloupec_end();


html_end();

?>