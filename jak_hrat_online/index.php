<?php
/*
* Domovska stranka ligy
*/

include('../_common_start.php');

$TITLE = 'Jak hrát online';
$MODUL = 'h3_info';

html_start();

# levy sloupec
sloupec_start(310);
# login / uzivatel.panel
include($INCLUDE_DIR.'/login_ramecek.php');
sloupec_end ();

# pravy sloupec
echo '<a name="home"></a>';
sloupec_start();

	ramecek_start(1024, 'Jak hrát online');

	echo "<div style=\"margin-left: 10px; margin-right: 10px;\">";
	
	html_podklad_start ();
	echo h1('Jak hrát online - Rozcestník');
	
	echo '
		<div style = "padding: 10px">
			<a href="#1" class = "strong2">1. Příprava hry</a>
			<br>
			<a href="#2" class = "strong2">2. Hrajeme online bez použití dalšího softwaru</a>
			<br>
			<a href="#3" class = "strong2">3. Hrajeme přes online lobby</a>
			<br>
			<a href="#4" class = "strong2">4. Hrajeme online přes GameRanger</a>
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="#5" class = "strong3">a. Problém s kombinací GameRangeru a Heroes 3 HD modu</a>
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="#6" class = "strong3">b. Problém s kombinací GameRangeru a datadisku Horn of the Abyss</a>
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="#7" class = "strong3">c. Možné problémy se spojením na GameRangeru</a>
			<br>
			<a href="#8" class = "strong2">8. Hrajeme online přes Hamachi</a>
		</div>
		';
	html_podklad_end ();

	echo '<a name="1"></a>';
	html_podklad_start ();
		echo h1('Příprava hry');

		echo html_p("K hraní Heroes III online potřebujete verzi Heroes III Complete anebo Heroes III: Restoration of Erathia s oběma datadisky Armageddon's Blade a The Shadow of Death ve verzi 4.0 tak, aby byla s Heroes III Complete kompatibilní. Dále doporučujeme nainstalovat neoficiální datadisk Horn of the Abyss. Přítomnost neoficiálního datadisku In The Wake of Gods není překážkou.");

		echo "<br/>";
		echo html_p("Pro pohodlné hraní je vhodné nainstalovat Heroes 3 HD mod vždy v aktuální (latest) verzi – dostupný zde: ".html_href("https://sites.google.com/site/heroes3hd/", "https://sites.google.com/site/heroes3hd/").". Heroes 3 HD mod přidává do hry Online Lobby, opravuje občasné pády hry, umožňuje hrát hru v okně v libovolném rozlišení a přidává mnoho zjednodušujících prvků do ovládání. Pro nastavení funkcí HD modu spusťte nejprve „HD_launcher.exe“.", '', '', "strong2");

		echo "<br/>";

		echo html_p("Pro hraní původní hry bez datadisku Horn of the Abyss doporučujeme do hry přidat patch D3 pro rychlý pohyb jednotek na bojišti – dostupný zde: ".html_href("http://randommaps.narod.ru/text.html", "http://randommaps.narod.ru/text.html", '', '', '', "strong2")." (pravým klikem „uložit jako“ a nahrát do složky „DATA“).");
		echo "<br/>";
		echo '<a class = "button" href = "#home">Zpět</a>';
		echo "<br/><br/>";
	html_podklad_end ();
	
	echo '<a name="2"></a>';
	html_podklad_start ();
		echo h1('Hrajeme online bez použití dalšího softwaru');

		echo html_p("Pokud chcete hrát Heroes po síti (LAN), anebo máte veřejnou IP adresu, můžete hrát Heroes bez použití dalšího softwaru. V menu Multiplayer zvolíte položku „TCP/IP“ a zatímco jeden hráč založí hru, druhý ji může dle zadání IP adresy vyhledat a připojit se do ní. Zda máte veřejnou IP adresu a tedy zda je váš PC viditelný a přístupný z internetu můžete ověřit tím, že porovnáte IP adresu, kterou vám Heroes napíše po rozkliknutí položky „TCP/IP“ s IP adresou, kterou vám zobrazí web typu ".html_href("https://www.whatismyip.com", "https://www.whatismyip.com").". Pokud se IP adresy shodují, máte IP adresu veřejnou, ale v drtivé většině případů tomu tak nebude.", '', '', "strong2");

		echo "<br/>";
		echo '<a class = "button" href = "#home">Zpět</a>';
		echo "<br/><br/>";
	html_podklad_end ();
	
	echo '<a name="3"></a>';
	html_podklad_start ();
		echo h1('Hrajeme přes online lobby');

		echo html_p("Od verze 5.0 Heroes 3 HD modu (19.08.2017) je možné hrát Heroes 3, ať už s datadiskem Horn of the Abyss nebo bez něj, s různými hráči z celého světa jako každou jinou moderní hru, a to tak, že v menu multiplayer zvolíte položku „ONLINE-LOBBY“, vytvoříte si uživatelský účet a následně buď přímo vytvoříte hru, anebo se připojíte do nějaké otevřené hry založené jiným hráčem. Online Lobby je nejjednodušším a nejpoužívanějším způsobem jak hrát Heroes 3 po internetu. Online Lobby disponuje také žebříčkem a ratingem všech registrovaných hráčů.");

		echo "<br/>";
		echo '<a class = "button" href = "#home">Zpět</a>';
		echo "<br/><br/>";
	html_podklad_end ();
	
	echo '<a name="4"></a>';
	html_podklad_start ();
		echo h1('Hrajeme online přes GameRanger');

		echo html_p("Nejběžnější způsob, jak se spojit s dalšími hráči online je program GameRanger. Po instalaci a registraci v GameRangeru je vašemu uživatelskému účtu vygenerováno tzv. ID číslo. Toto ideálně s funkčním e-mailem zadejte do svého profilu v Heroes 3 lize, abyste mohli s ostatními hráči komunikovat. Další hráče si podle GameRanger ID můžete přidávat do friend listu. Systém spojení je jednoduchý, nejprve v nastavení GameRangeru navolte cestu k „heroes3.exe“ a to tak, že v hlavním okně zvolíte „Edit“ a pak „Options“. Následně jeden z hráčů vytvoří místnost (v hlavním okně položky „File“ a „Host Game“) a druhý se může připojit.");

		echo "<br/>";
		echo '<a class = "button" href = "#home">Zpět</a>';
		echo "<br/><br/>";
	html_podklad_end ();
	
	
	echo '<a name="5"></a>';
	html_podklad_start ();
		echo h1('Problém s kombinací GameRangeru a Heroes 3 HD modu');

		echo html_p("Při používání Heroes 3 HD modu a GameRangeru je třeba si uvědomit, že GameRanger jako spouštěcí soubor hry akceptuje pouze název „heroes3.exe“, což je problém, protože Heroes 3 s HD modem se spouští buď prostřednictvím „HD_Launcher.exe“ a tlačítka „Play“, anebo tak, že v HD_Launcheru si pomocí tlačítka „Create HD exe“ vytvoříte vlastní spouštěcí soubor s názvem „heroes3 HD.exe“ (tento soubor se také nachází ve složce „_HD3_Data“, kterou vám vytvořila instalace Heroes 3 HD modu).");

		echo "<br/>";

		echo html_p("Pro hraní na GameRangeru s aktivovaným HD modem tedy vezměte původní „heroes3.exe“ (velikost 344 KB, modrá ikona), přejmenujte ho, nebo ho někam přesuňte/zálohujte a na jeho místo umístěte nový spouštěcí soubor „heroes3.exe“, popř. „heroes3 HD.exe“ (velikost 2,7 MB, modro-červená ikona). U toho je však třeba mít na paměti, že s tímto novým spouštěcím souborem již nebude možné měnit nastavení v „HD_Launcher.exe“, tedy např. aktivovat/deaktivovat nadstavbu „HD+“. Pro povolení změn v „HD_Launcher.exe“ je třeba soubory „heroes3.exe“ dočasně prohodit nazpět, nastavení uložit a výměnu souborů „heroes3.exe“ opakovat. Jinými slovy se starým „heroes3.exe“ (velikost 344 KB, modrá ikona) můžete nastavovat, ale nemůžete hrát, s novým „heroes3.exe“ (velikost 2,7 MB, modro-červená ikona) můžete hrát, ale nemůžete nastavovat.");

		echo "<br/>";

		echo html_p("Pokud se vám při hraní na GameRangeru zobrazí nějaká varovná hláška a hra se nespustí, anebo se neaktivují funkce Heroes 3 HD modu, ujistěte se, že v nastavení GameRangeru je jako spouštěcí soubor hry nastaven nový „heroes3.exe“ (velikost 2,7 MB, modro-červená ikona), jmenuje se přesně „heroes3.exe“ a je načítán přímo ze složky se hrou (Heroes 3 Complete), nikoli ze složky „_HD3_Data“.");

		echo "<br/>";

		echo html_p("Pokud používáte zvláštní edici \"Heroes of Might and Magic III Complete HD\", tedy Heroes III Complete s již integrovaným Heroes 3 HD modem, dejte si pozor na vaši verzi Heroes 3 HD modu, neboť s největší pravděpodobností nebude aktuální. Ve starších verzích Heroes 3 HD modu je třeba smazat soubor DPWSOCKX.DLL ze složky „_HD3_DATA“, jinak nebude možné se na GameRangeru spojit s hráči, kteří používají jinou (novější) verzi Heroes 3 HD modu.");

		echo "<br/>";
		echo '<a class = "button" href = "#home">Zpět</a>';
		echo "<br/><br/>";
	html_podklad_end ();
	
	echo '<a name="6"></a>';
	html_podklad_start ();
		echo h1('Problém s kombinací GameRangeru a datadisku Horn of the Abyss');

		echo html_p("Po instalaci datadisku Horn of the Abyss se vám ve složce se hrou vytvoří spouštěcí soubor „h3hota.exe“ a jak je uvedeno výše, GameRanger akceptuje pouze název „heroes3.exe“. Pokud nepoužíváte Heroes 3 HD mod, postačí soubor „h3hota.exe“ přejmenovat na „heroes3.exe“. Pokud chcete zároveň hrát s aktivovaným Heroes 3 HD modem, je třeba nejprve spustit „HD_Launcher.exe“, kliknout na „Create HD exe“, čímž se vám ve složce vytvoří soubor „h3hota HD.exe“ a tento následně přejmenovat na „heroes3.exe“.");

		echo "<br/>";

		echo html_p("Mějte na paměti, že „HD_Launcher.exe“ mění nastavení zvlášť pro Horn of the Abyss a zvlášť pro původní hru. Pro hraní HOTA na GameRangeru by váš „HD_Launcher.exe“ měl vypadat takto (název spouštěcího souboru je heroes3.exe, ale ikonka je z datadisku HOTA, neboť se jedná o přejmenovaný „soubor h3hota HD.exe“).");

		echo "<br/>";

		echo html_image($ROOT_URL."/jak_hrat_online/img/img10.jpg");

		echo "<br/>";

		echo "<br/>";
		echo '<a class = "button" href = "#home">Zpět</a>';
		echo "<br/><br/>";
	html_podklad_end ();
	
	
	echo '<a name="7"></a>';
	html_podklad_start ();
		echo h1('Možné problémy se spojením na GameRangeru');

		echo html_p("1. Ať už jste pokročilý uživatel PC nebo ne, nejčastější problém spočívá v tom, že jeden z hráčů nemá hru, nebo GameRanger (popř. Hamachi Client, viz dále) povolen ve Firewallu. Po prvním spuštění hry, vyčkejte dialogových oken s dotazem o povolení přístupu k internetu, následně ověřte nastavení v Bráně Firewall v ovládacích panelech Windows.");

		echo "<br/>";

		echo html_p("2. GameRanger při připojování do místnosti zobrazí následující upozornění:");

		echo "<br/>";

		echo html_image($ROOT_URL."/jak_hrat_online/img/img1.jpg");

		echo "<br/>";
		echo "<br/>";

		echo html_p("Znamená to, že nemáte veřejnou IP adresu (případ většiny dnešních uživatelů) a navíc nemáte nastaveno přesměrování portů do vašeho PC, tudíž váš PC není přístupný pro P2P spojení z internetu. Po kliknutí na „Learn More“ se dozvíte víc. Pokud se zde ztrácíte, připravili jsme pro vás jednoduchý návod, jak přesměrování portů nastavit.");

		echo "<br/>";

		echo html_p("Nejprve potřebujete zjistit IP adresu vašeho PC a IP adresu vašeho routeru (nebo modemu). Proklikejte se do Centra síťových připojení a sdílení, viz obrázek a zde se dozvíte, jak IP adresu svého PC („IPv4 adresa“), tak IP adresu vašeho routeru („Výchozí brána IPv4“).");

		echo "<br/>";

		echo html_image($ROOT_URL."/jak_hrat_online/img/img2.jpg", "", "", "width: 95%;");

		echo "<br/>";
		echo "<br/>";

		echo html_p("Následně napište IP adresu vašeho routeru do internetového prohlížeče, tím otevřete nastavení routeru. Při dotazu o jméno a heslo, si vygooglete typ vašeho routeru a jeho defaultní (výchozí) heslo, popř. se fyzicky podívejte na zadní stranu routeru, kde jsou většinou přístupové údaje napsány. Nejčastěji je jméno i heslo „admin1234“, popř. heslo je prázdné.");

		echo "<br/>";

		echo html_p("V nastavení routeru hledejte položku „Virtual servers“, kterou vyplňte podle obrázku (resp. podle návodu, který poskytl GameRanger po kliknutí na „Learn More“) a to tak, že jako IP adresu použijete IP adresu svého PC. V případě, že potíže přetrvají, můžete zkusit také vyplnit položku DMZ (demilitarized zone).");

		echo "<br/>";

		echo html_image($ROOT_URL."/jak_hrat_online/img/img3.jpg", "", "", "width: 95%;");

		echo "<br/>";
		echo "<br/>";

		echo html_p("Pozor: Výše uvedené nastavení nemusí nutně eliminovat vyskakující upozornění GameRangeru a neznamená, že bude možné se spojit s úplně každým hráčem.");

		echo "<br/>";

		echo html_p("3. V místnosti GameRangeru se jméno některého z hráčů zobrazuje kurzívou, anebo je u něj tmavý obdélník, viz obrázek.");

		echo "<br/>";

		echo html_image($ROOT_URL."/jak_hrat_online/img/img4.jpg");

		echo "<br/>";
		echo "<br/>";

		echo html_p("Jedná se kombinaci chyb se zapnutým Firewallem, anebo s nedostupností vnějších IP adres některých hráčů v místnosti. Paradoxně velké procento tohoto problému se dá vyřešit tím, že místnost vytvoří jiný hráč, popř. hráči restartují GameRanger. Pokud v určité kombinaci hráčů problém přetrvává, nezbývá nic jiného, než vyzkoušet Hamachi.");

		echo "<br/>";
		echo '<a class = "button" href = "#home">Zpět</a>';
		echo "<br/><br/>";
	html_podklad_end ();
	
	
	echo '<a name="8"></a>';
	html_podklad_start ();
		echo h1('Hrajeme online přes Hamachi');

		echo html_p("Hamachi vám může pomoci v případech, kdy není možné se s určitými spoluhráči spojit pomocí jiných programů, např. v případech, kdy je jeden z hráčů připojen k internetu přes mobilní telefon, používá VPN, anebo nemá přístup k nastavení internetové brány (router/modem). Hamachi vytváří virtuální síť LAN mezi hráči, kteří se připojí do totožné Hamachi místnosti. Název místnosti včetně hesla můžete zadávat libovolně. Pro připojení do hry postupujte stejně jako při připojování na síti LAN.");

		echo "<br/>";

		echo h2('Možné problémy se spojením na Hamachi');

		echo html_p("1) Heroes musí po otevření menu multiplayer a TCP/IP načíst IP adresu Hamachi a nikoli vnitřní IP adresu vašeho PC, viz obrázek.");

		echo "<br/>";

		echo html_image($ROOT_URL."/jak_hrat_online/img/img5.jpg");

		echo "<br/>";
		echo "<br/>";

		echo html_p("Heroes vždy načte IP adresu vašeho primárního síťového připojení, kterým se tedy musí stát Hamachi. Nastavení změníte tím, že v centru sítových připojení a sdílení přejdete vlevo na „změnit nastavení adaptéru“, poté zmáčknete levý ALT, aby se vám v okně objevily další možnosti. V nich zvolíte „Upřesnit“ a poté „Upřesnit nastavení“. Následně zelenou šipkou posunete Hamachi na první místo, viz obrázek.");

		echo "<br/>";

		echo html_image($ROOT_URL."/jak_hrat_online/img/img6.jpg");

		echo "<br/>";
		echo "<br/>";

		echo html_p("2) Hamachi u jednotlivých členů místnosti ukazuje vykřičník nebo červenou, či žlutou tečku. Zkontrolujte v Centru síťových připojení a sdílení ve Windows, zda Hamachi vůbec vytvořilo nové síťové připojení a zkontrolujte ve Správci zařízení ve Windows, zda ovladač síťového připojení Hamachi nevykazuje nějakou chybu, viz obrázek.");

		echo "<br/>";

		echo html_image($ROOT_URL."/jak_hrat_online/img/img7.jpg");

		echo "<br/>";
		echo "<br/>";

		echo html_p("Pod Windows 7 64 bit se obvykle setkáte s problémem digitálního podpisu ovladače (Kód 52). Jelikož Hamachi nemá digitálně podepsaný ovladač, daří se tento problém obejít pouze tím, že restartujete PC a při bootování (před naběhnutím loga Windows) zmáčknete F8, čímž vyvoláte nabídku bootování. V ní zvolíte „Zakázat vynucení podpisu ovladače“, viz obrázek. Bohužel tímto umožníte funkci síťového ovladače Hamachi pouze jednorázově.");

		echo "<br/>";

		echo html_image($ROOT_URL."/jak_hrat_online/img/img8.jpg", "", "", "width: 95%;");

		echo "<br/>";
		echo "<br/>";

		echo html_p("3) V případě přetrvávajících problémů, vyzkoušejte pravým kliknutím na spoluhráče v menu Hamachi, zda na něj funguje ping (tzn. vrací odezvu v milisekundách). Pokud není ping funkční, opět prověřte povolení ve firewallu na obou stranách. Zejména v případech Windows XP také ověřte, zda je zapnutá služba „Windows Firewall/Internet Connection Sharing (ICS)“, což provedete tak, že kliknete na nabídku START -> spustit -> napíšete „services.msc“, potvrdíte a následně výše uvedenou službu v nabídce najdete a spustíte.");
		echo "<br/>";		
		echo "<br/>";
		echo '<a class = "button" href = "#home">Zpět</a>';
		echo "<br/><br/>";
	html_podklad_end ();
	
	echo "</div>";

	ramecek_end(1024);
sloupec_end();


html_end();

?>