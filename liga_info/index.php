<?php
include('../_common_start.php');

$TITLE = 'Informace o lize';
$MODUL = 'liga_info';

html_start();

# levy sloupec
sloupec_start(310);
	# login / uzivatel.panel
	include($INCLUDE_DIR.'/login_ramecek.php');
sloupec_end ();

# pravý sloupec
echo '<a name="home"></a>';
sloupec_start();
	ramecek_start(1024, 'Informace o lize');
	html_podklad_start ();

		echo h1("Liga Info - Rozcestník");
		echo '
			<div style = "padding: 10px">
				<a href="#1" class = "strong2">1. Obecné informace</a>
				<br>
				<a href="#2" class = "strong2">2. Registrace a účast v lize</a>
				<br>
				<a href="#3" class = "strong2">3. Ocenění</a>
				<br>
				<a href="#4" class = "strong2">4. Žebříček</a>
				<br>
			</div>
			';
	html_podklad_end ();
	echo '<a name="1"></a>';
	html_podklad_start ();
		echo h1("Obecné informace");
		echo html_p("Smyslem těchto stránek je umožnit hráčům Heroes 3 v první řadě přehledně porovnávat jejich schopnosti a herní úspěchy, v druhé řadě pak navázat nové kontaky a najít si další kamarády a soupeře pro online hraní. Přestože Heroes 3 je stále velmi oblíbenou hrou a má tisíce vášnivých příznivců, až do založení ligy v ČR nikdo nevytvořil žádnou dlouhobou online soutěž. Proběhly pouze dva jendorázové turnaje, u kterých se jako velký problém ukázala časová synchronizace hráčů. Liga oproti turnajům nabízí časově nezávazný systém hraní a porovnávání síly hráčů.");
		echo "<br/>";
		echo html_p("Liga běží nepřetržitě od svého založení 5. 11. 2008.");
		echo "<br/>";
		echo h3("O fungování ligy se starají");
		echo html_p("
		<span class=\"admin\">admin</span> - Má nejvyšší oprávnění. Může upravovat hráčské profily, udělovat funkce, medaile, měnit hesla atd.<br/>
		<span class=\"moderator\">moderátor</span> - Moderuje příspěvky v diskuzi a komentáře her. Může příspěvky mazat, editovat a přesouvat a zamykat záznamy her proti přidávání příspěvků.<br/>
		<span class=\"rozhodci\">rozhodčí</span> - Rozhoduje o smazání nebo potvrzení sporných her, tzn. těch, které poražený hráč odmítá potvrdit.<br/>
		");
		echo "<br/>";
		echo h3("Poděkování");
		echo html_p("Všem, kteří se podílelli nebo podílejí na tvorbě a udržování ligy, zejména Macwarovi a Nivenovi. Dále všem donátorům, kteří přispěli na provoz ligy v době, kdy už měla placený hosting ale ještě si na provoz nevydělala reklamou. :-)");
		echo "<br/>";
		echo h3("Webmaster");
		echo html_p("Jakub Tomek (Tomis)<br/>
		<a href=\"mailto:liga@heroes3.cz\">liga@heroes3.cz</a>");
		echo "<br/>";
		echo '<a class = "button" href = "#home">Zpět</a>';
		echo "<br/><br/>";
	html_podklad_end ();
	echo '<a name="2"></a>';
	html_podklad_start ();
		echo h1("Registrace a účast v lize");
		echo html_p("
		Do ligy se může zaregistrovat kdokoliv, bez jakýchkoliv podmínek. Jediné omezení je, že každý hráč se smí zaregistrovat jen jednou. Registrace i členství v lize jsou bezplatné. Herní aktvita je zcela libovolná, je jedno jestli budete hrát každý den, dvakrát do roka, nebo vůbec nikdy. Z odehraných her se tvoří statistiky a hráči se tak sami postupně profilují.
		");

		echo "<br/>";

		echo h2("Ligové hry");
		echo html_p("
		Do ligy se počítají jen duely, čili hry 1 vs 1. Každá hra má právě jednoho vítěze a jednoho poraženého, remízy neexistují. Pokud se z nějakého závažného důvodu nedá rozhodnout o vítězi, hra se nehlásí. Hrát se mohou mpy s libovolnými parametry, na kterých se musí hráči domluvit předem. Hráči se také musí předem domluvit, kolik času mohou hře věnovat. Nedomluvený odchod uprostřed hry znamená kontumační prohru!<br/>
		<br/>
		Při hře je zakázáno:
		<ul class = 'paragraph'>
		<li>analyzovat rozehranou mapu prostřednictvím příslušného h3m souboru</li>
		<li>analyzovat rozehranou hru prostřednictvím příslušného save souboru</li>
		<li>zdržovat</li>
		<li>jakkoliv jinak podvádět</li>
		</ul>");
		echo html_p("Kterýkoliv z těchto zákazů se nemusí dodržovat, pokud se na tom hráči předem výslovně dohodnou. Stejně tak se mohou hráči dohodnout na nějakém vlastním omezení nebo pravidle. (bez cestovních kouzel, bez dračích utopií, restart v případě absence dřeva/kamení u hradu atp.) Možnost neregulérnosti však v tom přpadě berou na vlastní zodpovědnost. ");
		echo "<br/>";
		echo h2("Volitelná pravidla");
		echo html_p("Pravidla, na kterých se hráči mohou libovolně domluvit, ale vždy před začátkem hry.");
		echo "<br/>";
		echo h3("Fight Misplaced Guard (známé jako Monsters)");
		echo html_p("<ul class = 'paragraph'>
		<li>Jakýkoliv objekt na mapě(artefakt, vězení, knihovna, svitek...), který je hlídaný neutrálem, se smí sebrat/navštívit až po poražení neutrála. Neutrální hlídka se považuje za hlídající daný objekt, pokud od něj stojí 1, 2 nebo 3 políčka jakýmkoliv směrem.</li>
		<li>Pokud průchod do jiné části mapy hlídá neutrální hlídka, tak se smí obejít jedině ve vzálenosti větší než 3 políčka od ní. (to znamená, že k hlídce se chováme, jako by měla dosah 3 políčka místo 1) Toto pravidlo nebrání v přeletu do jiné části mapy pomocí kouzel/artefaktů. </li>
		<li>Pokud je objekt/průchod hlídán dvěma a více bojovníky zároveň, tak stačí porazit jednoho. Pozor! Toto platí pouze pokud obě hlídky evidentně patří k danému objektu/průhodu. Nemůžete se vymluvit, že pro sebrání meče práva nemusíte porazit archanděly, když jste už porazili kostlivce, kteří náhodou stáli o 3 políčka vedle :-) </li>
		<li>Je zakázáno sbírat pandořiny skříňky a artefakty jiného než 1. stupně, které jsou úplně nehlídané.</li>
		</ul>");
		echo "<br/>";
		echo h3("Grál");
		echo html_p("Je zakázáno postavit svatostánek grálu.");
		echo "<br/>";
		echo h3("Diplomacie");
		echo html_p("
		<ul>
		<li>Hráč si nesmí při přechodu na vyšší úroveň vybrat diplomacii, pokud má jinou možnost.</li>
		<li>Pokud se jakýmkoliv způsobem stane, že hráčův hrdina má diplomacii, nesmí tento hrdina přijímat neutrální hlídky do své armády.</li>
		</ul>");
		echo "<br/>";
		echo h2("Hlášení výsledků her");
		echo html_p("Po každé hře dvou zaregistrovaných hráčů má vítěz právo nahlásit své vítěství v oddílu záznamy her a to bez zbytečného odkladu. Poražený má povinnost svou prohru potvrdit na tomtéž místě bez zbytečného odkladu. Případné spory řeší rozhodčí. Po potvrzení hry od obou soupeřů se výsledek okamžitě automaticky započítá do statistik.");
		echo "<br/>";
		echo h2("Závěrem");
		echo html_p("Tato pravidla nejsou dokonalá a nechávají volnost zdravému lidskému rozumu.
		Takže se chovejte slušně a vše bude OK. ;)");
		echo "<br/>";
		echo '<a class = "button" href = "#home">Zpět</a>';
		echo "<br/><br/>";
	html_podklad_end ();
	echo '<a name="3"></a>';
	html_podklad_start ();
		echo h1("Ocenění");
		echo html_p("Hráči mohou svým působením v lize získávat celou řadu ocenění titulů.
		Jedná se jednak o ocenění za herní úpěchy a jednak za chování, které se hrou přímo nesouvisí - aktivita na fóru, fair play apod.");
		echo "<br/>";

		foreach(array('sampion', 'zlato', 'stribro', 'bronz', 'zasluhy', 'repre', 'varovani', 'dobyvatel') as $nazev) {
		  echo h3($medaile[$nazev]['nazev']);
		  echo html_p(html_image($IMG.'/medaile/'.$nazev.'.png', $medaile[$nazev]['nazev'], $medaile[$nazev]['nazev'], "cursor: help;").' '.$medaile[$nazev]['popis']);
		  echo "<br/>";
		}

		/*echo h2("Dobyvatel challenge");
		echo html_p("Meč dobyvatele je speciální ocenění, které se uděluje za rozdrcení nejlepších hráčů ligy.
		Jak ho získáš? Napřed musíš porazit všechny čtyři hráče z Elitní čtyřky.
		Může ti to trvat libovolně dlouho a nevadí když některý ze zápasů prohraješ - můžeš to zkusit znovu.
		Pokud se členové Elitní čtyřky změní, nevadí. Důležité je, abys nakonec nasbíral 4 vítězství s různými hráči z této skupiny.
		Vítězství se počítá i poté, co hráč Elitní čtyřku opustí.
		Až pokoříš všechny 4 členy Elitní čtyřky, přijde čas vyzvat na souboj samotného Šampiona.
		V zápase s ním máš však pouze jeden pokus. Pokud vyhraješ, Meč dobyvatele je tvůj.
		Když však neuspěješ, tvé vítěství nad Elitní čtyřkou se smaže a ty musíš začít zase od začátku."); */
		echo "<br/>";

		echo h2("Karma");
		echo html_p("Karma je hodnocení hráčovo fair play, které se tvoří na zálkadě hodnocení od soupeřů v jednotlivých hrách.
		Každý hráč začíná s fair play 0. Při kladném ohodnocení fair play od soupeře dostane 1 bod karmy. Při záporném hodnocení zratí 3 body.
		Pokud je zápas sporný a musí ho rozhodnout rozhodčí, potom hráč v jehož neprospěch bylo rozhodnuto ztratí 3 body.");
		echo "<br/>";
		echo html_p("Podle počtů bodů karmy hráč dostává titul:");
		echo "<br/>";
		echo "<table style=\"margin-left: 5px;\"><tr><td>";
		table_start();
		table_row(array('Kladné body<br/>'.html_image($IMG.'/moralka/+1.png')), array(2), 0, array('center'));
		table_row(array('0 - 9&nbsp;', 'slušňák'), 0, 0, array('right', 'left'));
		table_row(array('10 - 49&nbsp;', 'dobrák'), 0, 0, array('right', 'left'));
		table_row(array('50 - 250&nbsp;', 'dobrodinec'), 0, 0, array('right', 'left'));
		table_row(array('250+&nbsp;', 'spasitel'), 0, 0, array('right', 'left'));
		table_end();
		echo "</td><td>&nbsp;</td><td style=\"vertical-align: top;\">";
		table_start();
		table_row(array('Záporné body<br/>'.html_image($IMG.'/moralka/-1.png')), array(2), 0, array('center'));
		table_row(array('1 - 9&nbsp;', 'rošťák'), 0, 0, array('right', 'left'));
		table_row(array('10 - 49&nbsp;', 'darebák'), 0, 0, array('right', 'left'));
		table_row(array('50+&nbsp;', 'zlosyn'), 0, 0, array('right', 'left'));
		table_end();
		echo "</td></tr></table>";
		echo "<br/>";

		echo h2("Aktivita v lize");
		echo html_p("Hráč získává titul podle počtu odehraných her:");
		echo "<br/>";
		echo "<div style=\"margin-left: 5px;\">";
		table_start();
		table_row(array('0 - 9&nbsp;', 'nováček'), 0, 0, array('right', 'left'));
		table_row(array('10 - 24&nbsp;', 'pionýr'), 0, 0, array('right', 'left'));
		table_row(array('25 - 49&nbsp;', 'bojovník'), 0, 0, array('right', 'left'));
		table_row(array('50 - 99&nbsp;', 'ostřílený bojovník'), 0, 0, array('right', 'left'));
		table_row(array('100 - 249&nbsp;', 'mazák'), 0, 0, array('right', 'left'));
		table_row(array('250 - 999&nbsp;', 'veterán'), 0, 0, array('right', 'left'));
		table_row(array('1000+&nbsp;', 'legenda'), 0, 0, array('right', 'left'));
		table_end();
		echo "</div>";
		echo "<br/>";

		echo h2("Aktivita na fóru");
		echo html_p("Hráč získává titul podle počtu příspěvků a komentářů:");
		echo "<br/>";
		echo "<div style=\"margin-left: 5px;\">";
		table_start();
		table_row(array('0 - 9&nbsp;', 'mlčoun'), 0, 0, array('right', 'left'));
		table_row(array('10 - 49&nbsp;', 'sváteční pisálek'), 0, 0, array('right', 'left'));
		table_row(array('50 - 99&nbsp;', 'diskutér'), 0, 0, array('right', 'left'));
		table_row(array('100 - 499&nbsp;', 'kecálek'), 0, 0, array('right', 'left'));
		table_row(array('500 - 999&nbsp;', 'řečník'), 0, 0, array('right', 'left'));
		table_row(array('1000+&nbsp;', 'spisovatel'), 0, 0, array('right', 'left'));
		table_end();
		echo "</div>";
		echo "<br/>";
		echo '<a class = "button" href = "#home">Zpět</a>';
		echo "<br/><br/>";
	html_podklad_end ();
	echo '<a name="4"></a>';
	html_podklad_start ();
		echo h1("Žebříček");
		echo html_p("Pořadí hráčů v žebříčku je jednoznačně určené. Nováčci začínají u konce žeříbčku a snaží se propracovat nahoru. Nejlepší hráči jsou na předních místech a bojují o pozici šampiona. Neaktivní hráči postupem času propadají žebříčkem dolů, zatímco aktivní hráči je předbíhají.
		Pořadí v hráče v žeříčku je určené následujícími faktory:
		<ol>
		<li>Skupina</li>
		<li>Body</li>
		<li>Elo</li>
		</ol>");
		echo h2("Skupiny");
		echo html_p("Žebříček je rozdělen do několika skupin, univnitř kterých jsou přibližně stejně silní hráči. Nejlepší hráči z každé skupiny jednou za čas postoupí do vyšší skupiny a nejslabší hráči naopak sestoupí do nižší skupiny.<br/>
		<br/>
		".html_image($IMG.'/skupiny/1.png')." Šampion - První hráč v žebříčku. Toto je speciální skupina s jediným hráčem. Na konci každého období se novým šampionem stane nejlepší hráč ze skupiny Elitní 4, nebo stávající šampion obhájí svou pozici, pokud je lepší než kdokoliv z Elitní 4. Pokud stávající šampion svou pozici neobhájí, sestupuje do skupiny Elitní 4, případně rovnou do TOP 10, pokud je hoší než kdokoliv z Elitní 4.
		<br/> 
		".html_image($IMG.'/skupiny/2.png')." Elitní 4 - Čtyři nejlepší hráči po Šampionovi a zároveň kandidáti na Šampiona. Tato skupina je částečně spojená se Šampionem. Při získávání bodů se Šampoin chová jako člen Elitní 4. Nejhoší hráč z Elitní 4 včetně šampiona na konci období sestupuje do skupiny TOP 10. Nejlepší hráč se naopak stává novým Šampionem. Může to být i stávající Šampion, který obhájil svou pozici.
		<br/> 
		".html_image($IMG.'/skupiny/3.png')." Top 10 - Hráči do desátého místa v žebříčku. Bez Šampiona a Elitní 4 je jich celkem 5. Toto je Elita Ligy. Schopní hráči, kteří hru dokonale ovládají, mají mnoho zkušeností a není snadné je porazit. Nejlepší hráč postupuje do Elitní 4, dva nejhorší hráči sestupují do  1. ligy.
		<br/>
		".html_image($IMG.'/skupiny/4.png')." 1. liga - Nejlepší ze základních skupin. V této skupině je 20 hráčů. Dva nejlepší hráčí postupují do TOP 10, Tři nejhorší hráči sestupují do 2. ligy.
		<br/>
		".html_image($IMG.'/skupiny/5.png')." 2. liga - Hráčský zlatý střed. V této skupině je 50 hráčů. Tři nejlepší hráčí postupují do 1. ligy, čtyři nejhorší hráči sestupují do 3. ligy.
		<br/>
		".html_image($IMG.'/skupiny/6.png')." 3. liga - Skupina začátečníků a méně zkušených hráčů. V této skupině je 100 hráčů. Čtyři nejlepší hráčí postupují do 2. ligy, pět nejhorších hráčů sestupuje do Pralesní ligy.
		<br/>
		".html_image($IMG.'/skupiny/7.png')." Pralesní liga - Poslední a velikostně neomezená skupina. V této skupině jsou všichni hráči, kteří se nevešli nikam jinam a začínají v ní nováčci. Pět nejlepších hráčů postupuje do 3. ligy.
		");
		echo "<br/>";
		echo h2("Hrací období a uzávěrky");
		echo html_p("Na konci každého hracího období se provede uzávěrka a vyhodnocení výsledků. Hráči se pak jednorázově přesunou mezi skupinami a začne další období. Uzávěrka se provádí kvartálně 4x za rok na konci kadždého čtvrtletí. Výsledky uzáverek se automaticky zveřejňují v Diskuzi s sekci Oznámení.<br/>
		<br/>
		Termíny uzávěrek:<br/>
		<br/>
		Q1: 31. 3.<br/>
		Q2: 30. 6.<br/>
		Q3: 30. 9.<br/>
		Q4: 31. 12.<br/>
		");
		echo "<br/>";
		echo h2("Body");
		echo html_p("Body určují pořadí hráčů uvnitř skupiny.
		Hráči začínaji každé hrací období s 0 body a během hracího období získávají body výhrami proti ostatním hrůčům.
		Množství bodů určuje, kdo na konci období postoupí resp. sestoupí do vyšší resp. nižší skupiny.
		Hráč získává 50 bodů za výhru nad kterýmkoliv hráčem ze své skupiny a 10 bodů za prohru s hráčem ze své skupiny.
		Při hře s hráčem mimo svou skupinu získává hráč 5 bodů v případě výhry a 1 bod v případě prohry.
		Body je možné získat pouze při první hře s každým soupeřem v rámci hracího období.
		Pokud hráč v první hře prohraje, nemůže již v daném období získat body výhrou nad tímto soupeřem.");
		echo "<br/>";
		echo h2("Elo");
		echo html_p("Elo je pomocný systém hodnocení, který se použije pro určení pořadí hráčů ve skupině v případě, že mají stejné množství bodů.<br/>
		<br/>
		Elo je systém hodnocení založený na matematické statistice. Odhaduje pravděpodobnost vítězství hráče na základě jeho výsledků v předchozích zápasech a výsledků jeho soupeře.
		Síla hráče je vyjádřena Elo - hodnocením a pravděpodobnost vítězství hráče odpovídá rozdílu mezi jeho Elo a Elo soupeře. Např. rozdíl 0 bodů znamená rovné šance na vítězství,
		rozdíl 150 bodů znamená 70% pravděpodobnost výhry silnějšího, rozdíl 350 znamená bodů 90% pravděpodobnost výhry silnějšího atd.<br/>
		<br/>
		Elo body se upravují po každém zápase a zpřesňují tak hodnocení síly hráčů. Vítěz získá určité množství bodů a poražený stejné množství bodů ztratí.
		Čím déle běží žebříček, tím je přesnější hodnocení účastníků. Elo žebříček je dimenzován na neomezenou dobu, nikdy nebude vrácen nazpět.
		Účastník může kdykoli později do žebříčku vstoupit aniž by musel mít strach, že nikdy nedostihne hráče na špici žebříčku, což je dost podstatná výhoda oproti klasickým žebříčkům.");
		echo "<br/>";

		$row = sql_one_row("SELECT AVG(elo) as avg FROM hraci WHERE skupina=7 and aktivni IS TRUE;");
		$avg = round($row['avg']);

		echo html_p("Nově příchozí hráči dosávají nasazovací Elo rovné průměrnému Elu v jejich skupině - Pralesní lize.<br/>Aktuální nasazovací Elo je: ". html_href($ROOT_URL."/hraci", $avg));
		echo "<br/>";

		echo h3("Tabulka úprav Elo");

		echo "<table style=\"margin-left: 5px;\"><tr><td>";

		table_start();
		table_row(array("Výhra silnějšího"), array(3), 0, array('center'));
		table_row(array("Rozdíl Elo", "Šance na výhru" , "Zisk bodů"));
		table_row(array("551+&nbsp;", "98 - 100%&nbsp;", "1&nbsp;"), array(1,1,1), 0, array('right', 'right', 'right'));
		table_row(array("451 - 550&nbsp;", "95 - 98%&nbsp;", "2&nbsp;"), array(1,1,1), 0, array('right', 'right', 'right'));
		table_row(array("301 - 450&nbsp;", "85 - 95%&nbsp;", "5&nbsp;"), array(1,1,1), 0, array('right', 'right', 'right'));
		table_row(array("151 - 300&nbsp;", "70 - 85%&nbsp;", "10&nbsp;"), array(1,1,1), 0, array('right', 'right', 'right'));
		table_row(array("101 - 150&nbsp;", "60 - 70%&nbsp;", "15&nbsp;"), array(1,1,1), 0, array('right', 'right', 'right'));
		table_row(array("51 - 100&nbsp;", "55 - 60%&nbsp;", "20&nbsp;"), array(1,1,1), 0, array('right', 'right', 'right'));
		table_row(array("0 - 50&nbsp;", "50 - 55%&nbsp;", "25&nbsp;"), array(1,1,1), 0, array('right', 'right', 'right'));
		table_end();
		echo "</td><td>&nbsp;</td><td>";
		table_start();
		table_row(array("Výhra slabšího"), array(3), 0, array('center'));
		table_row(array("Rozdíl Elo", "Šance na výhru" , "Zisk bodů"));
		table_row(array("0 - 50&nbsp;", "45 - 50&nbsp;%", "25&nbsp;"), array(1,1,1), 0, array('right', 'right', 'right'));
		table_row(array("51 - 100&nbsp;", "40 - 45%&nbsp;", "30&nbsp;"), array(1,1,1), 0, array('right', 'right', 'right'));
		table_row(array("101 - 150&nbsp;", "30 - 40%&nbsp;", "35&nbsp;"), array(1,1,1), 0, array('right', 'right', 'right'));
		table_row(array("151 - 300&nbsp;", "15 - 30%&nbsp;", "40&nbsp;"), array(1,1,1), 0, array('right', 'right', 'right'));
		table_row(array("301 - 450&nbsp;", "5 - 15%&nbsp;", "45&nbsp;"), array(1,1,1), 0, array('right', 'right', 'right'));
		table_row(array("451 - 550&nbsp;", "2 - 5%&nbsp;", "48&nbsp;"), array(1,1,1), 0, array('right', 'right', 'right'));
		table_row(array("551+&nbsp;", "0 - 2%&nbsp;", "49&nbsp;"), array(1,1,1), 0, array('right', 'right', 'right'));
		table_end();
		echo "</td></tr></table>";
		echo "<br/>";
		echo html_p("Poznámka: Pokud hra skončí v prvním týdnu, je bodový zisk resp. ztráta poloviční, zaokrouhleno nahoru.");
		echo "<br/>";
		echo '<a class = "button" href = "#home">Zpět</a>';
		echo "<br/><br/>";
	html_podklad_end ();
	ramecek_end(1024);
sloupec_end ();
html_end();

?>
