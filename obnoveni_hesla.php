<?php

include('_common_start.php');

$deadline = Date("Y-m-d h:i:s", time()-86400);

$h = sql_one_row(sprintf("SELECT * FROM hraci WHERE id=%d and reset_token='%s' and posledni_prihlaseni>'%s' and ban=0",$_GET['hrac'],$_GET['reset_token'],$deadline));

if($h) {
	sql_query(sprintf("UPDATE hraci SET reset_token=NULL WHERE id=%d", $_GET['hrac']));
	$_SESSION['uzivatel'] = $h;
	header_redirect('.');
} else {
	header_redirect('chyba.php');
}

?>
