<?php
/*
* Domovska stranka ligy
*/

include('../_common_start.php');

$TITLE = 'Odkazy';
$MODUL = 'odkazy';

html_start();

# levy sloupec
sloupec_start(310);
	# login / uzivatel.panel
	include($INCLUDE_DIR.'/login_ramecek.php');
sloupec_end ();

# pravý sloupec
sloupec_start (523);
	ramecek_start(490, 'Ke stažení');
	echo '<div style = "padding: 5px;">';
echo h4('Horn of the Abyss - neoficiální datadisk');
echo html_href("http://heroescommunity.com/viewthread.php3?TID=39830", "http://heroescommunity.com/viewthread.php3?TID=39830");
echo "<br/>";
echo "<br/>";

echo h4('ERA 2 (WOG) - neoficiální mod');
echo html_href("http://heroescommunity.com/viewthread.php3?TID=37208", "http://heroescommunity.com/viewthread.php3?TID=37208");
echo "<br/>";
echo "<br/>";

echo h4('Heroes 3 HD Mod <i>(Alexander Barinov)</i>');
echo html_href("https://sites.google.com/site/heroes3hd/", "https://sites.google.com/site/heroes3hd/");
echo "<br/>";
echo "<br/>";

echo h4('Template náhodné mapy Jebus King <i>(pouze pro HOTA, rozbalit do „Heroes 3 Complete\HotA_RMGTemplates\“)</i>');
echo html_href("https://www.h3templates.com/mkc/jebus-king", "https://www.h3templates.com/mkc/jebus-king");
echo "<br/>";
echo "<br/>";

echo h4('Template náhodné mapy mt_Jebus King <i>(pouze pro HOTA, rozbalit do „Heroes 3 Complete\HotA_RMGTemplates\“)</i>');
echo html_href("https://www.h3templates.com/machberet/mt-king", "https://www.h3templates.com/machberet/mt-king");
echo "<br/>";
echo "<br/>";

echo h4('Template náhodné mapy mt_Jebus <i>(pouze pro HOTA, rozbalit do „Heroes 3 Complete\HotA_RMGTemplates\“)</i>');
echo html_href("https://forum.heroesworld.ru/showthread.php?t=14797", "https://forum.heroesworld.ru/showthread.php?t=14797");
echo "<br/>";
echo "<br/>";

echo h4('Template náhodné mapy Jebus Outcast <i>(pouze pro HOTA, rozbalit do „Heroes 3 Complete\HotA_RMGTemplates\“)</i>');
echo html_href("https://www.h3templates.com/mkc/jebus-outcast", "https://www.h3templates.com/mkc/jebus-outcast");
echo "<br/>";
echo "<br/>";

echo h4('Template náhodné mapy Arena <i>(pouze pro HOTA, rozbalit do „Heroes 3 Complete\Maps\“)</i>');
echo html_href("http://mapy.heroes.net.pl/szukaj/11", "http://mapy.heroes.net.pl/szukaj/11");
echo "<br/>";
echo "<br/>";

echo h4('Template náhodné mapy Dragons Underground <i>(pro HOTA rozbalit do „Heroes 3 Complete\HotA_RMGTemplates\“, pro SoD rozbalit do „Heroes 3 Complete\_HD3_DATA\Templates\“)</i>');
echo html_href($ROOT_URL.'/odkazy/dragons_underground_1.1.zip', $ROOT_URL.'/odkazy/dragons_underground_1.1.zip');
echo "<br/>";
echo "<br/>";

echo h4('Acidův template 7sb0b <i>(pro HOTA rozbalit do „Heroes 3 Complete\HotA_RMGTemplates\“, pro SoD rozbalit do „Heroes 3 Complete\_HD3_DATA\Templates\“)</i>');
echo html_href($ROOT_URL.'/odkazy/7sb0b_by_Acid.zip', $ROOT_URL.'/odkazy/7sb0b_by_Acid.zip');
echo "<br/>";
echo "<br/>";

echo h4('Zrcadlová verze templatu mt_2sm4d(3) <i>(pro HOTA rozbalit do „Heroes 3 Complete\HotA_RMGTemplates\“, pro SoD rozbalit do „Heroes 3 Complete\_HD3_DATA\Templates\“)</i>');
echo html_href($ROOT_URL.'/odkazy/mt_2sm4d(3).zip', $ROOT_URL.'/odkazy/mt_2sm4d(3).zip');
echo "<br/>";
echo "<br/>";

echo h4('Starší balík templatů náhodných map <i>(pro HOTA rozbalit do „Heroes 3 Complete\HotA_RMGTemplates\“, pro SoD rozbalit do „Heroes 3 Complete\_HD3_DATA\Templates\“)</i>');
echo html_href("http://forum.heroesworld.ru/showpost.php?p=564869&postcount=57", "http://forum.heroesworld.ru/showpost.php?p=564869&postcount=57");
echo "<br/>";
echo "<br/>";

echo h4('Templaty náhodných map pro WorldCup 2018 (např. mt_Dom) <i>(pro HOTA rozbalit do „Heroes 3 Complete\HotA_RMGTemplates\“, pro SoD rozbalit do „Heroes 3 Complete\_HD3_DATA\Templates\“)</i>');
echo html_href("https://yadi.sk/d/GC027OoL3T3hmh", "https://yadi.sk/d/GC027OoL3T3hmh");
echo "<br/>";
echo "<br/>";

echo h4('Templaty náhodných map z Polské Heroes 3 ligy (např. mt_Wrzosy) <i>(pro HOTA rozbalit do „Heroes 3 Complete\HotA_RMGTemplates\“, pro SoD rozbalit do „Heroes 3 Complete\_HD3_DATA\Templates\“)</i>');
echo html_href("http://h3.heroes.net.pl/szablony", "http://h3.heroes.net.pl/szablony");
echo "<br/>";
echo "<br/>";

echo h4('Mapy (singleplayer)');
echo html_href("http://www.maps4heroes.com/heroes3/maps.php", "http://www.maps4heroes.com/heroes3/maps.php");
echo "<br/>";
echo "<br/>";
	echo "</div>";
	ramecek_end (490);
	ramecek_start(490, 'Jiné ligy a turnajové portály');
	echo '<div style = "padding: 5px;">';
		echo h4('WCL (mezinárodní liga, Sergey Goulyaev)');
		echo "&nbsp;&nbsp;" . html_href("http://www.heroes-iii.com/", "http://www.heroes-iii.com/",0,0,0,"strong3","1");
		echo "<br/>";
		echo "<br/>";

		echo h4('World of Heroes (polská liga)');
		echo "&nbsp;&nbsp;" . html_href("http://www.h3.ligaheroes.pl", "http://www.h3.ligaheroes.pl",0,0,0,"strong3","1");
		echo "<br/>";
		echo "<br/>";

		echo h4('Heroes3.tv (ruský turnajový portál, streamy)');
		echo "&nbsp;&nbsp;" . html_href("http://heroes3.tv", "http://heroes3.tv",0,0,0,"strong3","1");
		echo "<br/>";
		echo "<br/>";

		echo h4('Heroes Portal (ruský turnajový portál)');
		echo "&nbsp;&nbsp;" . html_href("http://heroesportal.net", "http://heroesportal.net",0,0,0,"strong3","1");
		echo "<br/>";
		echo "<br/>";
	echo "</div>";
	ramecek_end(490);
sloupec_end ();

sloupec_start ();
	ramecek_start(490, 'Heroes 3 komunity');
	echo '<div style = "padding: 5px;">';
		echo h4('České Heroes Centrum');
		echo "&nbsp;&nbsp;" . html_href("http://www.heroes-centrum.com", "http://www.heroes-centrum.com",0,0,0,"strong3","1");
		echo "<br/>";
		echo "<br/>";

		echo h4('Velmi slušné detailní info o hře v angličtině');
		echo "&nbsp;&nbsp;" . html_href("http://www.heroesofmightandmagic.com/heroes3/", "http://www.heroesofmightandmagic.com/heroes3/",0,0,0,"strong3","1");
		echo "<br/>";
		echo "<br/>";

		echo h4('Ještě lepší info o hře v angličtině');
		echo "&nbsp;&nbsp;" . html_href("http://heroes.thelazy.net/", "http://heroes.thelazy.net/",0,0,0,"strong3","1");
		echo "<br/>";
		echo "<br/>";

		echo h4('Asi největší světová komunita');
		echo "&nbsp;&nbsp;" . html_href("http://heroescommunity.com", "http://heroescommunity.com",0,0,0,"strong3","1");
		echo "<br/>";
		echo "<br/>";

		echo h4('Jaskinia Behemota - největší polský portál o sérii heroes');
		echo "&nbsp;&nbsp;" . html_href("http://heroes.net.pl/", "http://heroes.net.pl/",0,0,0,"strong3","1");
		echo "<br/>";
		echo "<br/>";
	echo "</div>";
	ramecek_end(490);
	ramecek_start(490, 'Ostatní');	
	echo '<div style = "padding: 5px;">';
		echo h4('Heroes 3 Liga - Facebook stránka');
		echo "&nbsp;&nbsp;" . html_href("https://www.facebook.com/groups/134142730021026/", "https://www.facebook.com/groups/134142730021026/",0,0,0,"strong3","1");
		echo "<br/>";
		echo "<br/>";
		
		echo h4('Heroes 4 liga (Niven)');
		echo "&nbsp;&nbsp;" . html_href("http://www.heroes4league.clanweb.eu/", "http://www.heroes4league.clanweb.eu/",0,0,0,"strong3","1");
	echo "</div>";
	ramecek_end(490);
sloupec_end ();
html_end();

?>