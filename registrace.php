<?php
/*
* registrace noveho hrace
*/

include('_common_start.php');

$TITLE = 'Registrace';

$data = array('jmeno' => '', 'heslo1' => '', 'heslo2' => '', 'email' => '@', 'souhlas' => false);

html_start();

sloupec_start();

ramecek_start(1024, 'Nový uživatel');

# provedeni registrace
if($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['registrovat'])) {

  $data = $_POST;
  $data['souhlas'] = isset($_POST['souhlas']);
  $errors = '';
  
  # kontrola jmena
  # unikatnost
  if(sql_exists("SELECT jmeno FROM hraci WHERE jmeno='".sql_string($data['jmeno'])."'"))
    $errors .= "Uživatel ".$data['jmeno']." už existuje.<br/>\n";
  # delka
  if(strlen($data['jmeno'])<3 or strlen($data['jmeno'])>15)
    $errors .= "Jméno musí být dlouhé 3-15 znaků.<br/>\n";
  # slozeni
  if(!preg_match('/^[a-zA-Z0-9 .\-_ěščřžýáíéúůďťňóĚŠČŘŽÝÁÍÉÚŮĎŤŇÓ]+$/', $data['jmeno']))
    $errors .= "Jméno smí být složeno pouze z písmen, číslic a znaků mezera, tečka, pomlčka a podtržítko.<br/>\n";
    
  # kontrola hesla
  # delka
  if(strlen($data['heslo1'])<3)
    $errors .= "Heslo musí být alespoň 3 znaky dlouhé.<br/>\n";
  # shoda
  if($data['heslo1']!=$data['heslo2'])
    $errors .= "Hesla v prvním a druhém poli se neshodují.<br/>\n";

  # kontrola mailu
  # je to vubec mail?
  if(!preg_match('/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $data['email']))
    $errors .= $data['email']." není správně zadaný e-mail.<br/>\n";
  # delka
  if(strlen($data['email'])<5 or strlen($data['email'])>50)
    $errors .= "E-mail musí být dlouhý 5-50 znaků.<br/>\n";
  # unikatnost
  if(sql_exists("SELECT registrace_mail FROM hraci WHERE registrace_mail='".sql_string($data['email'])."'"))
    $errors .= "Pod emailem ".$data['email']." už je jeden uživatel registrován.<br/>\n";  
  
  # souhlas
  if(!$data['souhlas']) {
    $errors .= "Před registrací si musíte přečíst pravidla ligy.<br/>\n";
  }
  
  # pokud je vse ok, provedeme registraci
  if(empty($errors)) {
  
	# elo
	$row = sql_one_row("SELECT AVG(elo) as avg FROM hraci WHERE skupina=7 and aktivni IS TRUE;");
	$elo = round($row['avg']);

    # vygenerujeme aktivacni kod
    $kod = md5(time()*mt_rand());

    $ins = sql_query("INSERT INTO hraci (registrace_cas, registrace_mail, registrace_ip, jmeno, heslo, elo, popis)
                              VALUES (
                              '".date('Y-m-d H:i:s')."',
                              '".sql_string($data['email'])."',
                              '".get_cient_ip()."',
                              '".sql_string($data['jmeno'])."',
                              '".h3hash($data['heslo1'])."',
			      '".$elo."',
                              '".$kod."')");
                              
    # zjistim si id
    $id = sql_getfield("SELECT id FROM hraci WHERE jmeno='".sql_string($data['jmeno'])."'", 'id');
                              
    # pokude se povedl insert, posleme mail                          
    if($ins) {
      $predmet = "Liga Heroes 3: registrace nového uživatele";
      $telo = "Dobrý den,\n\nzaregistrovali jste se na České Lize Heroes 3.\n\nvaše přihlašovací údaje:\n\njméno: ".$data['jmeno']."\nheslo: ".$data['heslo1']."\n\nPřed prvním přihlášením musíte svůj účet aktivovat kliknutím na následující odkaz:\n".$ROOT_URL."/aktivace.php?hrac=".$id.'&kod='.$kod."\n\nPlatnost odkazu je 24 hodin.\n\nS pozdravem,\nČeská Liga Heroes of Might and Magic III\n";
        
        if(send_email($data['email'], $predmet, $telo)) {
          echo "<div style=\"margin-left: 5px;\">Registrace proběhla úspěšně.<br/>Na zadaný e-mail byl odeslán aktivační odkaz, pomocí kterého musíte účet aktivovat, než se budete moci přihlásit.<br/>Platnost aktivačního odkazu je 24 hodin.</div>";
          $data = array('jmeno' => '', 'heslo1' => '', 'heslo2' => '', 'email' => '@', 'souhlas' => false);
        } else {
			$errors .= "Uživatel byl uložen ale nepodařilo se odeslat aktivační e-mail."; 			
		}

    }
    else {
      $errors .= "Chyba při ukládání uživatele.";
    }
	
	if(!empty($errors)) {
		echo html_error($errors);
	}
  
  }
  else {
    echo html_error($errors);
  }
  
}

table_start('tab automargin');
table_row(array('jméno:', input_text('jmeno', $data['jmeno'])),0,0,array('right', 'left'));
table_row(array('heslo:', input_password('heslo1', $data['heslo1'])),0,0,array('right', 'left'));
table_row(array('heslo znovu:', input_password('heslo2', $data['heslo2'])),0,0,array('right', 'left'));
table_row(array('e-mail:', input_text('email', $data['email'])),0,0,array('right', 'left'));
table_row(array(input_checkbox('souhlas', 1, $data['souhlas']).'Přečetl jsem si '.html_href('liga_info/pravidla', 'pravidla').' ligy.'),array(2),0,array('center'));
table_row(array(input_submit('Odeslat', 'registrovat')),array(2),0,array('center'));
table_end();
ramecek_end(1024);

sloupec_end();

html_end();

?>
