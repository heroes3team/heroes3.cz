<?php
/*
* Domovska stranka ligy
*/

include('../_common_start.php');

$TITLE = 'Statistiky';
$MODUL = 'statistiky';

html_start();
# Levy sloupec
sloupec_start(310);
# login / uzivatel.panel
include($INCLUDE_DIR.'/login_ramecek.php');

# statisika
ramecek_start(286, 'Statistika');

	$hracu = sql_getfield("SELECT COUNT(*) AS hracu FROM hraci;", 'hracu');
	$her = sql_getfield("SELECT COUNT(*) AS her FROM hry WHERE potvrzeno IS NOT NULL;", 'her');
	$prispevku = sql_getfield("SELECT COUNT(*) AS prispevku FROM forum_prispevky;", 'prispevku');
	$komentaru = sql_getfield("SELECT COUNT(*) AS komentaru FROM hry_komentare;", 'komentaru');

	$deadline = date("Y-m-d H:i:s", time() - 60*5);
	$sql = sql_query($dotaz = "SELECT id, jmeno FROM hraci WHERE posledni_prihlaseni>'".$deadline."' ORDER BY jmeno;");
	$online = array();
	while($row = sql_fetch_array($sql)) {
	  $online[] = html_href($ROOT_URL.'/hraci/detail.php?id='.$row['id'], $row['jmeno']);
	}
	$online = implode(', ', $online);

	$sql = sql_query($dotaz = "SELECT id, jmeno FROM hraci WHERE admin IS TRUE ORDER BY jmeno;");
	$admini = array();
	while($row = sql_fetch_array($sql)) {
	  $admini[] = html_href($ROOT_URL.'/hraci/detail.php?id='.$row['id'], $row['jmeno']);
	}
	$admini = implode(', ', $admini);

	$sql = sql_query($dotaz = "SELECT id, jmeno FROM hraci WHERE moderator IS TRUE ORDER BY jmeno;");
	$moderatori = array();
	while($row = sql_fetch_array($sql)) {
	  $moderatori[] = html_href($ROOT_URL.'/hraci/detail.php?id='.$row['id'], $row['jmeno']);
	}
	$moderatori = implode(', ', $moderatori);

	$sql = sql_query($dotaz = "SELECT id, jmeno FROM hraci WHERE rozhodci IS TRUE ORDER BY jmeno;");
	$rozhodci = array();
	while($row = sql_fetch_array($sql)) {
	  $rozhodci[] = html_href($ROOT_URL.'/hraci/detail.php?id='.$row['id'], $row['jmeno']);
	}
	$rozhodci = implode(', ', $rozhodci);

	html_podklad_start ();
		echo '<span class = "strong" >Obecné statistiky:</span><br/> ';
		echo '<table style = "width: 100%; padding-top: 5px;">';
		echo '<tr><td>Registrovaných hráčů: </td><td style = "text-align: right;">' . $hracu . '</td></tr>';
		echo '<tr><td>Odehraných her: </td><td style = "text-align: right;">' . $her .'</td></tr>';
		echo '<tr><td>Diskuzních příspěvků: </td><td style = "text-align: right;"> ' . ($komentaru+$prispevku) . '</td></tr>';
		echo '</table>';
	html_podklad_end ();

	html_podklad_start ();
	echo '<span class = "strong">Právě jsou online:</span><br/><div class ="text_n">' . $online . '</div>';
	html_podklad_end ();

ramecek_end(286);
sloupec_end();

# Pravý sloupec
sloupec_start();
	$mesto_cz = $mesta[$_GET['mesto']];

	if(empty($mesto_cz)) {

	ramecek_start(1024, 'Statistiky');

	$sql = sql_query("SELECT vitez_hlavni_mesto, COUNT(*) as count FROM hry
	WHERE potvrzeno IS NOT NULL
	GROUP BY vitez_hlavni_mesto;");
	$wins = array();
	$sum = 0;
	while($row = sql_fetch_array($sql)) {
		$wins[$row['vitez_hlavni_mesto']] = $row['count'];
		$sum += $row['count'];
	}

	$sql = sql_query("SELECT porazeny_hlavni_mesto, COUNT(*) as count FROM hry
	WHERE potvrzeno IS NOT NULL
	GROUP BY porazeny_hlavni_mesto;");
	$losts = array();
	while($row = sql_fetch_array($sql)) {
		$losts[$row['porazeny_hlavni_mesto']] = $row['count'];
	}

	$succ = array();
	$total = array();
	$sum = 0;
	foreach($mesta as $en => $cz) {
		$succ[$en] = 100 * $wins[$en] / ($wins[$en] + $losts[$en]); 
		$total[$en] = $wins[$en] + $losts[$en];
		$sum += $total[$en];
	}
	arsort($succ);
	arsort($total);
	

	echo "<table>";
	echo "<tr>";
	echo "<td >";
	html_podklad_start ();
		echo h2("Nejúspěšnější Města");
		echo "<table style = \"padding: 5px;\">";
		foreach($succ as $en => $val) {
			$cz = $mesta[$en];
			echo "<tr>";	
			echo "<td>";
			echo '<img class = "img_ramecek" style="height: 31px; width: 51px;" src="'.$IMG.'/mesta/'.$en.'.jpg" alt="'.$cz.'" />';
			echo "</td>";
			echo "<td style=\"width: 375px; text-align: left;\"><div class = \"graf\" style=\"background-image: url('".$IMG."/misc/fireice.jpg'); width: ".(1.2*round($val))."%;\"></div> &nbsp;".round($val)."%</td>";
			echo "</tr>";
			echo "<tr>";
			echo '<td style=" padding-bottom: 5px; text-align: center;"><span style="position: relative; top: -0px;">  '.html_href('?mesto='.$en, $cz).' </span></td>';
			echo "<td>&nbsp;</td>";
			echo "</tr>";
		}
		echo "</table>";
	html_podklad_end ();
	echo "</td>";
	echo "<td >	";
	html_podklad_start ();
		echo h2("Nejoblíbenější Města");
		echo "<table style = \"padding: 5px;\">";
		foreach($total as $en => $val) {
			$cz = $mesta[$en];
			$fav = round(100 * $val / $sum);
			echo "<tr>";	
			echo "<td>";
			echo '<img class = "img_ramecek" style="height: 31px; width: 51px;" src="'.$IMG.'/mesta/'.$en.'.jpg" alt="'.$cz.'" />';
			echo "</td>";
			echo "<td style=\"width: 375px; text-align: left;\"><div class = \"graf\" style=\"background-image: url('".$IMG."/misc/fireice.jpg');width: ".(3*$fav)."%;\"></div> &nbsp;".$fav."%</td>";
			echo "</tr>";
			echo "<tr>";
			echo '<td style=" padding-bottom: 5px; text-align: center;"><span style="position: relative; top: -0px;">  '.html_href('?mesto='.$en, $cz).' </span></td>';
			echo "<td>&nbsp;</td>";
			echo "</tr>";
		}
		echo "</table>";
	html_podklad_end ();
	echo "</td>";
	echo "</tr>";
	echo "<tr>";
	echo "<td colspan=\"2\">";
	echo "<br/>";
	echo html_p ('TIP: Kliknutím na název hradu zobrazíte podrobnou statistiku.');


	echo "<br/>";
	html_podklad_start ();
		echo h2("Úspěšnost Barev");

		$sql = sql_query("SELECT vitez_cerveny, COUNT(*) as count FROM hry
		WHERE potvrzeno IS NOT NULL
		GROUP BY vitez_cerveny;");
		$barvy = array();
		$sum = 0;
		while($row = sql_fetch_array($sql)) {
			$barvy[$row['vitez_cerveny']] = $row['count'];
			$sum += $row['count'];
		}
		$cerveny = round(100*$barvy[1]/$sum);
		$modry = round(100*$barvy[0]/$sum);
		echo "<table>";
			echo "<tr>";	
			echo "<td>";
			echo '<img style="" src="'.$IMG.'/barvy/cerveny.png" alt="cerveny" />';
			echo "</td>";
			echo "<td  style=\"width: 375px; text-align: left;\"><div class = \"graf\" style=\"background-image: url('".$IMG."/misc/fireice.jpg'); height: 20px; width: ".(1.2*$cerveny)."%; \"></div> &nbsp;".($cerveny)."%</td>";
			echo "</tr>";
			echo "<tr>";
			echo '<td style="text-align: center;"><span style="position: relative; top: -0px;">  červený </span></td>';
			echo "<td>&nbsp;</td>";
			echo "</tr>";
			echo "<tr>";	
			echo "<td>";
			echo '<img style="" src="'.$IMG.'/barvy/modry.png" alt="modry" />';
			echo "</td>";
			echo "<td style=\"width: 375px; text-align: left;\"><div class = \"graf\" style=\"background-image: url('".$IMG."/misc/fireice.jpg'); height: 20px; width: ".(1.2*$modry)."%; \"></div> &nbsp;".($modry)."%</td>";
			echo "</tr>";
			echo "<tr>";
			echo '<td style="text-align: center;"><span style="position: relative; top: -0px;">  modrý </span></td>';
			echo "<td>&nbsp;</td>";
			echo "</tr>";
		echo "</table>";
		echo "</td>";
		echo "</tr>";
		echo "</table></div>";
	


	ramecek_end(1024);

	} else {

	ramecek_start(1024, 'Statistiky - '.capital_first_letter($mesto_cz));

	$mesto_en = $_GET['mesto'];

	$sql = sql_query("SELECT porazeny_hlavni_mesto, COUNT(*) as count FROM hry
	WHERE potvrzeno IS NOT NULL and vitez_hlavni_mesto='".sql_string($mesto_en)."'
	GROUP BY porazeny_hlavni_mesto;");
	$wins = array();
	$wins_total = 0;
	while($row = sql_fetch_array($sql)) {
		$wins[$row['porazeny_hlavni_mesto']] = $row['count'];
		$wins_total += $row['count'];
	}

	$sql = sql_query("SELECT vitez_hlavni_mesto, COUNT(*) as count FROM hry
	WHERE potvrzeno IS NOT NULL and porazeny_hlavni_mesto='".sql_string($mesto_en)."'
	GROUP BY vitez_hlavni_mesto;");
	$losts = array();
	$losts_total = 0;
	while($row = sql_fetch_array($sql)) {
		$losts[$row['vitez_hlavni_mesto']] = $row['count'];	
		$losts_total += $row['count'];
	}

	$succ = array();
	$total = array();
	$sum = 0;
	foreach($mesta as $en => $cz) {
		$succ[$en] = 100 * $wins[$en] / ($wins[$en] + $losts[$en]); 
		$total[$en] = $wins[$en] + $losts[$en];
		$sum += $total[$en];
	}
	arsort($succ);
	arsort($total);

	echo "<table style = \"width: 100%\">";
	echo "<tr>";	
	echo "<td>";
	html_podklad_start ();
	echo h2("Úspěšnost proti Ostatním Městům");

	echo "<table style = \"padding: 5px;\">";
	foreach($succ as $en => $val) {
		$cz = $mesta[$en];
		echo "<tr>";	
		echo "<td>";
		echo '<img class = "img_ramecek" style="height: 31px; width: 50px;" src="'.$IMG.'/mesta/'.$mesto_en.'.jpg" alt="'.$mesto_cz.'" />';
		echo "</td>";
		echo "<td>vs</td>";
		echo "<td>";
		echo '<img class = "img_ramecek" style="height: 31px; width: 50px;" src="'.$IMG.'/mesta/'.$en.'.jpg" alt="'.$cz.'" />';
		echo "</td>";
		echo "<td  style=\"width: 250px; text-align: left;\"><div class = \"graf\" style=\"background-image: url('".$IMG."/misc/fireice.jpg');width: ".(1.2*round($val))."%; \"></div> &nbsp;".round($val)."%</td>";
		echo "</tr>";
		echo "<tr>";
		echo '<td style=" padding-bottom: 5px; text-align: center;"><span style="position: relative; top: -0px;">  '.html_href('?mesto='.$mesto_en, $mesto_cz).' </span></td>';
		echo "<td>&nbsp;</td>";
		echo '<td style="text-align: center;"><span style="position: relative; top: -0px;">  '.html_href('?mesto='.$en, $cz).' </span></td>';
		echo "<td>&nbsp;</td>";
		echo "</tr>";
	}
	echo "</table>";

	echo "</div></td><td style = \"width: 50%\">";
	html_podklad_start ();
	echo h2("Nejběžnější soupeř");
	echo "<table style = \"padding: 5px;\">";
	foreach($total as $en => $val) {
		$cz = $mesta[$en];
		$fav = round(100 * $val / $sum);
		echo "<tr>";	
		echo "<td>";
		echo '<img class = "img_ramecek" style="height: 31px; width: 50px;" src="'.$IMG.'/mesta/'.$en.'.jpg" alt="'.$cz.'" />';
		echo "</td>";
		echo "<td style=\"width: 300px; text-align: left;\"><div class = \"graf\" style=\"background-image: url('".$IMG."/misc/fireice.jpg');  width: ".(3*$fav)."%; \"></div> &nbsp;".$fav."%</td>";
		echo "</tr>";
		echo "<tr>";
		echo '<td style=" padding-bottom: 5px; text-align: center;"><span style="position: relative; top: -0px;">  '.html_href('?mesto='.$en, $cz).' </span></td>';
		echo "<td>&nbsp;</td>";
		echo "</tr>";
	}
	echo "</table>";

	echo "</div></td>";
	echo "</tr>";
	echo "</table>";

	ramecek_end(1024);

	}
sloupec_end ();
html_end();

?>
