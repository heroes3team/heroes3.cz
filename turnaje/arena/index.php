<?php


require('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

ramecek_start(1024, 'Karantaréna');

echo "<div style=\"margin-left: 10px;\">";

echo "

&nbsp;
	<h1>Pravidla</h1>

      <h4>Systém turnaje</h4> 

      <div>
    <ul>
	 <li>Přihlášení hráči budou rozlosování do pavouka v systému challonge dle svého počtu ELO bodů v době začátku turnaje (20.04.2020).</li>
     <li>Pavouk ve formátu double elimination - každý zápas na 2 vítězné hry (BO3).</li>
     <li>Velké finále se bude hrát na 3 vítězné zápasy (BO5), hráč postupující z winners bracket vede 1:0.</li>
    </ul>
	 </div>
    
    <h4>Pravidla</h4>

    <div>	
    <ul>
     <li>Hraje se datadisk Horn of the Abyss ve své aktuální verzi s HD+ modem.</li>
     <li>Herní mapa: Arena, vždy ve své aktuální verzi dle odkazu ke stažení na webu ligy, v době začátku turnaje verze 2.6.3.</li>
     <li>Odehraný zápas se zapíše do ligy jako jeden záznam. Tedy i v případě výhry 2:1 se zavede pouze jeden záznam ve prospěch hráče, který vyhrál celou sérii.</li>
     <li>Každá mapa se hraje jako ranked do online lobby, nedohodnou-li se hráči jinak.</li>
     <li>Simultánní tahy nejméně 114, časový limit - chess timer - 4-4-2, nedohodnou-li se hráči jinak.</li> 
     <li>Nejpozději 4. den hry je modrý hráč povinen zajet svým hrdinou na jedno z bojových polí uprostřed mapy, nejpozději 5. den hry je červený povinen zaútočit.</li>
     <li>Hráči jsou povinni dodržovat pravidla nastavené mapou, tedy např. vybrat pouze armádu svého města a pouze 1 jednotku z každého levelu, vybrat pouze 1 skill v univerzitě, neskládat kombinované artefakty apod.</li>
	 <li>Hra se spouští obvykle bez hesla. V případě použití hesla jsou soupeři povinni si hesla po skončení hry vyměnit, např. pro kontrolu, zda jeden z hráčů nevzal více než 1 skill z university.</li>
    </ul>

    Výběr měst a hrdinů:
    <ul>	 
     <li>Jeden hráč založí hru a druhý si vybere 0 nebo 1, hostující hráč zvolí v PvP options coin flip. Hráč, který vyhrál coin flip si volí první barvu. Startovní bonus je zlato.</li>
     <li>Následně se v PvP options zvolí random vs random, přičemž první vygenerované město automaticky náleží červenému a druhé modrému hráči. Hraní obou stejných měst není povoleno.</li>
     <li>V druhé hře v sérii si hráči prohodí barvy a města zvolená v první hře.</li>
     <li>V třetí hře v sérii hráči volí města a barvu stejným způsobem jako v první hře v sérii (tedy nový coin flip, nový random).</li>
    </ul>

    Přerušení hry:
    <ul>
     <li>Hra se přerušuje jen v nejnutnějších, odůvodněných případech. Hra se z žádných důvodů nerestartuje.</li>
     <li>V případě, že je nutné hru přerušit, hráč pokud možno kompletně dokončí svůj tah.</li>
     <li>Za splnění výše uvedené podmínky hráči načítají již dokončený tah (např. autosave), v kterém není dovoleno cokoli měnit.</li>        
     <li>V případě náhlého odchodu hráče ze hry, např. vlivem technické chyby, či přerušení připojení k internetu, je tento hráč na požádání soupeře povinen ihned zaslat savy rozhodčímu. 
	 Za tímto účelem je třeba mít v HD_Launcheru zaškrtnuto „save before each battle“ a „save all days (for tournaments)“</li> 
     <li>V případě pádu hry při hlavní bitvě hráči načtou save „battle.gm2.“ a je-li to možné, zopakují boj tak, jak se odehrál před pádem hry.</li>
    </ul>

	 </div>
 
      <h4>Kontumace neaktivních hráčů</h4>
      
    <div>
    <ul>
	<li>Pokud určitý zápas není odehrán v přiměřeném termínu, kterým se rozumí 1 týden, a zjevně se prodlužuje trvání celého turnaje, je udělena hráčům kontumační prohra.</li>
     <li>V případě že je jeden hráč aktivní a jeho soupeř není k sehnání, udělí se kontumační prohra jen neaktivnímu hráči.</li>
     <li>Po první kontumaci v horní části pavouka (winners bracket) se hráč přesouvá do spodní části pavouka (losers bracket), odkud může pokračovat.</li>
     <li>Kontumací zápasu vzniká vítězi právo na zápis výhry nad daným hráčem do ligy. Hráč, který se nechal vykontumovat je povinen svoji prohru potvrdit.</li>	 
    </ul>
	 </div>  

	<h1>Ceny turnaje</h1>

      <div>
    <ul>
      1. místo - 200,- Kč<br/> 
    </ul>
      </div> 

	<h1>Přihlášky</h1>

    <ul>
     <li>Přihlášky pište do diskuse, sekce turnaje na webu ligy, anebo organizátorovi turnaje (H34D) na Skype, či e-mail.<br/>
     <li>Pro přihlášení do turnaje musíte být registrovaní v lize.</li>
     <li>Konec přihlášek a začátek turnaje je plánovan na 20.04.2020.</li>
    </ul>
	 
      <table style=\"border-spacing: 15pt;\"><tr>
      <td valign=\"top\">
      <h3>Přihlášení hráči</h3>
      
      <p>
        1. <a href=http://heroes3.cz/hraci/detail.php?id=222><font color=#ff6633>H34D</font></a><br/>
		2. <a href=http://heroes3.cz/hraci/detail.php?id=5271><font color=#ff6633>Bonca</font></a><br/>
		3. <a href=http://heroes3.cz/hraci/detail.php?id=5321><font color=#ff6633>Lord Slayer</font></a><br/>
		4. <a href=http://heroes3.cz/hraci/detail.php?id=5307><font color=#ff6633>Naoblaku</font></a><br/>
        5. <a href=http://heroes3.cz/hraci/detail.php?id=3473><font color=#ff6633>Skalimse</font></a><br/>
		6. <a href=http://heroes3.cz/hraci/detail.php?id=823><font color=#ff6633>Karcma</font></a><br/>
        7. <a href=http://heroes3.cz/hraci/detail.php?id=343><font color=#ff6633>DT.Azgard</font></a><br/>
        8. <a href=http://heroes3.cz/hraci/detail.php?id=5320><font color=#ff6633>Siska96</font></a><br/>
		9. <a href=http://heroes3.cz/hraci/detail.php?id=5322><font color=#ff6633>gabo</font></a><br/>
		10. <a href=http://heroes3.cz/hraci/detail.php?id=5360><font color=#ff6633>Helmut</font></a><br/>
		11. <a href=http://heroes3.cz/hraci/detail.php?id=5619><font color=#ff6633>Marian Hero</font></a><br/>
		12. <a href=http://heroes3.cz/hraci/detail.php?id=2762><font color=#ff6633>maGe</font></a><br/>
		13. <a href=http://heroes3.cz/hraci/detail.php?id=3113><font color=#ff6633>Scooby</font></a><br/>
		14. <a href=http://heroes3.cz/hraci/detail.php?id=3><font color=#ff6633>Kacer</font></a><br/>
		15. <a href=http://heroes3.cz/hraci/detail.php?id=3758><font color=#ff6633>uniqFazer</font></a><br/>
		16. <a href=http://heroes3.cz/hraci/detail.php?id=2755><font color=#ff6633>CrossGuard</font></a><br/>
		17. <a href=http://heroes3.cz/hraci/detail.php?id=5149><font color=#ff6633>Liut</font></a><br/>
		18. <a href=http://heroes3.cz/hraci/detail.php?id=200><font color=#ff6633>Empair</font></a><br/>
		19. <a href=http://heroes3.cz/hraci/detail.php?id=5659><font color=#ff6633>juraj.krepavy</font></a> (registrovaný po termínu)<br/>
		</p>
      
      </td>
      
      <td valign=\"top\">
      <h3>Nasazení do pavouka dle ELO k 20.04.2020</h3>
        1. <a href=http://heroes3.cz/hraci/detail.php?id=222><font color=#ff6633>H34D</font></a> - ELO - 2299<br/>
		2. <a href=http://heroes3.cz/hraci/detail.php?id=3><font color=#ff6633>Kacer</font></a> - ELO - 2226<br/>
		3. <a href=http://heroes3.cz/hraci/detail.php?id=3113><font color=#ff6633>Scooby</font></a> - ELO - 2136<br/>
		4. <a href=http://heroes3.cz/hraci/detail.php?id=5271><font color=#ff6633>Bonca</font></a> - ELO - 2065<br/>
        5. <a href=http://heroes3.cz/hraci/detail.php?id=343><font color=#ff6633>DT.Azgard</font></a> - ELO - 2038<br/>
		6. <a href=http://heroes3.cz/hraci/detail.php?id=2762><font color=#ff6633>maGe</font></a> - ELO - 2027<br/>
        7. <a href=http://heroes3.cz/hraci/detail.php?id=823><font color=#ff6633>Karcma</font></a> - ELO - 1941<br/>
        8. <a href=http://heroes3.cz/hraci/detail.php?id=3473><font color=#ff6633>Skalimse</font></a> - ELO - 1941<br/>
		9. <a href=http://heroes3.cz/hraci/detail.php?id=5322><font color=#ff6633>gabo</font></a> - ELO - 1892<br/>
		10. <a href=http://heroes3.cz/hraci/detail.php?id=5320><font color=#ff6633>Siska96</font></a> - ELO - 1882<br/>
		11. <a href=http://heroes3.cz/hraci/detail.php?id=3758><font color=#ff6633>uniqFazer</font></a> - ELO - 1746<br/>
		12. <a href=http://heroes3.cz/hraci/detail.php?id=5321><font color=#ff6633>Lord Slayer</font></a> - ELO - 1738<br/>
		13. <a href=http://heroes3.cz/hraci/detail.php?id=200><font color=#ff6633>Empair</font></a> - ELO - 1662<br/>
		14. <a href=http://heroes3.cz/hraci/detail.php?id=5360><font color=#ff6633>Helmut</font></a> - ELO - 1661<br/>
		15. <a href=http://heroes3.cz/hraci/detail.php?id=5619><font color=#ff6633>Marian Hero</font></a> - ELO - 1632<br/>
		16. <a href=http://heroes3.cz/hraci/detail.php?id=5307><font color=#ff6633>Naoblaku</font></a> - ELO - 1598<br/>
		17. <a href=http://heroes3.cz/hraci/detail.php?id=2755><font color=#ff6633>CrossGuard</font></a> - ELO - 1516<br/>
		18. <a href=http://heroes3.cz/hraci/detail.php?id=5149><font color=#ff6633>Liut</font></a> - ELO - 1484<br/>
		19. <a href=http://heroes3.cz/hraci/detail.php?id=5659><font color=#ff6633>juraj.krepavy</font></a> - ELO - 1522 (registrovaný po termínu)<br/>
      </td> 
      
      <td valign=\"top\">
      <h5></h5>
              
      </td>       
      
      </tr></table>

";

echo "

</br>Odkaz na pavouka v systému challonge: <a href=https://challonge.com/karantarena><font color=#ff6633>https://challonge.com/karantarena</font></a>

<center><font size=3><h3>Velké finále</h3>
<table border=\"1\">
  <tbody>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">siska96</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">siska96</br>1:0, <a href=\"".$ROOT_URL."/hry/?detail=17899\"><font color=#ff6633>3:0</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">H34D</td>
    </tr>
  </tbody>
</table></font></center>

<center><h4>Winners bracket</h4></center>
<table border=\"1\">
  <tbody>
    <tr>
      <th style=\"color: rgb(255, 112, 0);\">1. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">2. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">3. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">4. kolo</th>
	  <th style=\"color: rgb(255, 112, 0);\">5. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">Vítěz WB</th>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">H34D</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">H34D</td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">H34D</br><a href=\"".$ROOT_URL."/hry/?detail=17761\"><font color=#ff6633>2:1</a></td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\">H34D</br><a href=\"".$ROOT_URL."/hry/?detail=17801\"><font color=#ff6633>2:0</a></td>
      <td rowspan=\"16\" style=\"width: 100pt;\" align=\"center\">H34D</br><a href=\"".$ROOT_URL."/hry/?detail=17825\"><font color=#ff6633>2:0</a></td>
	  <td rowspan=\"32\" style=\"width: 100pt;\" align=\"center\">siska96</br><a href=\"".$ROOT_URL."/hry/?detail=17871\"><font color=#ff6633>0:2</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">naoblaku</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">naoblaku</br><a href=\"".$ROOT_URL."/hry/?detail=17757\"><font color=#ff6633>2:1</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">CrossGuard</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Skalimse</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Skalimse</td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Skalimse</br><a href=\"".$ROOT_URL."/hry/?detail=17779\"><font color=#ff6633>2:0</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">gabo</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">gabo</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Bonca</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Bonca</td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Bonca</br><a href=\"".$ROOT_URL."/hry/?detail=17733\"><font color=#ff6633>2:0</a></td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\">Lord Slayer</br><a href=\"".$ROOT_URL."/hry/?detail=17797\"><font color=#ff6633>0:2</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Empair</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Empair</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">DT.Azgard</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">DT.Azgard</td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Lord Slayer</br><a href=\"".$ROOT_URL."/hry/?detail=17789\"><font color=#ff6633>0:2</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Lord Slayer</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Lord Slayer</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Kacer</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Kacer</td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Marian Hero</br><i>kontumace</i></td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\">siska96</br><a href=\"".$ROOT_URL."/hry/?detail=17807\"><font color=#ff6633>1:2</a></td>
      <td rowspan=\"16\" style=\"width: 100pt;\" align=\"center\">siska96</br><a href=\"".$ROOT_URL."/hry/?detail=17851\"><font color=#ff6633>2:0</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Marian Hero</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Marian Hero</br><a href=\"".$ROOT_URL."/hry/?detail=17727\"><font color=#ff6633>2:0</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Liut</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Karcma</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Karcma<br>
  </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">siska96</br><a href=\"".$ROOT_URL."/hry/?detail=17781\"><font color=#ff6633>0:2</a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">siska96</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">siska96</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Scooby</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Scooby</td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Scooby</br><a href=\"".$ROOT_URL."/hry/?detail=17787\"><font color=#ff6633>2:1</a></td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\">Scooby</br><a href=\"".$ROOT_URL."/hry/?detail=17821\"><font color=#ff6633>2:1</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Helmut</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">juraj.krepavy</br><a href=\"".$ROOT_URL."/hry/?detail=17753\"><font color=#ff6633>1:2</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">juraj.krepavy</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">maGe</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">maGe</td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">maGe</br><a href=\"".$ROOT_URL."/hry/?detail=17751\"><font color=#ff6633>2:0</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">uniqFazer</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">uniqFazer</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>	
  </tbody>
</table>
<br/><br/>

<center><h4>Losers Bracket</h4></center>
<table border=\"1\">
  <tbody>
    <tr>
      <th style=\"color: rgb(255, 112, 0);\">1. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">2. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">3. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">4. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">5. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">6. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">7. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">finále LB</th>
      <th style=\"color: rgb(255, 112, 0);\">vítěz LB</th>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">uniqFazer</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">uniqFazer</br><a href=\"".$ROOT_URL."/hry/?detail=17765\"><font color=#ff6633>2:1</a></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Bonca</td>   
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">juraj.krepavy</br><a href=\"".$ROOT_URL."/hry/?detail=17845\"><font color=#ff6633>0:2</a></td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Scooby</td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\">juraj.krepavy</br><a href=\"".$ROOT_URL."/hry/?detail=17883\"><font color=#ff6633>0:2</a></td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\">H34D</td>
      <td rowspan=\"16\" style=\"width: 70pt;\" align=\"center\">H34D</br><a href=\"".$ROOT_URL."/hry/?detail=17895\"><font color=#ff6633>2:1</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Crossguard</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Crossguard</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">juraj.krepavy</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">juraj.krepavy</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">juraj.krepavy</br><a href=\"".$ROOT_URL."/hry/?detail=17803\"><font color=#ff6633>1:2</a></td>
		</tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">karcma</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">karcma</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Skalimse</td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">karcma</br><a href=\"".$ROOT_URL."/hry/?detail=17837\"><font color=#ff6633>1:2</a></td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">juraj.krepavy</br><a href=\"".$ROOT_URL."/hry/?detail=17877\"><font color=#ff6633>0:2</a></td>
      <span
 style=\"color: rgb(255, 102, 0);\"></span></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Kacer</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Kacer</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Karcma</br><i><a href=\"".$ROOT_URL."/hry/?detail=17841\"><font color=#ff6633>kontumace</a></i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">DT.Azgard</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">DT.Azgard</br><a href=\"".$ROOT_URL."/hry/?detail=17827\"><font color=#ff6633>2:0</a></td>    
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">maGe</td>     
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">maGe</br><a href=\"".$ROOT_URL."/hry/?detail=17853\"><font color=#ff6633>1:0</a></td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Lord Slayer</td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\">Marian Hero</br><a href=\"".$ROOT_URL."/hry/?detail=17883\"><font color=#ff6633>1:2</a></td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\">juraj.krepavy</br><a href=\"".$ROOT_URL."/hry/?detail=17889\"><font color=#ff6633>2:0</a></td>		
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Liut</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Liut</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Empair</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Empair</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">DT.Azgard</br><a href=\"".$ROOT_URL."/hry/?detail=17829\"><font color=#ff6633>2:0</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">gabo</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">gabo</br><a href=\"".$ROOT_URL."/hry/?detail=17809\"><font color=#ff6633>2:1</a></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Marian Hero</td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Marian Hero</br><a href=\"".$ROOT_URL."/hry/?detail=17833\"><font color=#ff6633>2:1</a></td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Marian Hero</br><a href=\"".$ROOT_URL."/hry/?detail=17867\"><font color=#ff6633>1:2</a></td>
      <span
 style=\"color: rgb(255, 102, 0);\"></span></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">helmut</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">helmut</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">naoblaku</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">naoblaku</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">gabo</br><a href=\"".$ROOT_URL."/hry/?detail=17815\"><font color=#ff6633>2:0</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
	
  </tbody>
</table>

"; 
	  
echo "</div>";

ramecek_end(1024);



html_end();

?>
