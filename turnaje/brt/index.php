<?php


require('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

ramecek_start(1024, 'Brutal Random Tournament');

echo "<div style=\"margin-left: 10px;\">";

echo "

&nbsp;
	<h1>Pravidla</h1>


      <h4>Systém</h4> 

       
      <div>
        Systém turnaje je klasický vyřazovací pavouk. Počet kol se určí podle počtu přihlášených.
        Počáteční rozmístění hráčů v pavouku se určí tak, aby byly obě poloviny každého (pod)pavouka vyrovnané.<br/>
        Na každé kolo bude limit 1 týden. Kdo nestihne odehrát, má kontumační prohru.
      </div>
               
    
    <h4>Turnajové hry</h4>
    
    <div>

    Nastavení hry je následující:
    <ul>
     <li>mapa: random M s pozdemím, voda žádná, neutrálové strong</li>        
     <li>časový limit: 2 minuty</li>
     <li>obtížnost: expert (160%)</li>
     <li>oba hráči začínají s náhodným výběrem města a hrdiny</li>
    </ul>

    Jelikož se jedná o Brutal Random Tournament, je povolené využívat všech aspektů hry, tzn. včetně diplomacie atd.
    <br/>
    Odehrané duely se ukládají do záznamů, jako každá jiná hra. Na začátek komentáře k turnajové hře napište *TURNAJ*.
    Odehrané duely je nutné nahlásit organizátorovi(Tomis), abych je zapsal do pavouka.<br/>
      
    </div>

      <h4>Reklamace mapy</h4>
    <div>
      Hráč má nárok na novou hru v pouze případě, že je mapa naprosto nehratelná, (tzn. například cestu k hradu blokuje parta titánů)
      určitě nestačí absence kamenolomu.
      <br/>
      <br/>
      Kromě toho je možné restartovat hru v případě, že v městech chybí tvrz, hráčí mají více měst, města jsou na špatném povrchu apod.
      <br/>
      <br/>
      V případě sporu rozhodne o hratelnosti/nehratelnosti mapy nestranná porota složená z organizátora a možná i někoho dalšího.
    </div>
    
      <h4>Kontumace neaktivních hráčů</h4>
      
    <div>
    
    Pokud se do konce termínu daného kola neozve ani jeden z hráčů, kteří mají hrát turnajovou hru, dostanou oba kontumační prohru.
    
    V případě že je jeden hráč aktivní a jeho soupeř není k sehnání, dostane kontumaci jen soupeř. V tom případě musí být vidět
    snaha aktivního hráče o sehnání soupeře. Aktivní hráč by proto měl několik dní před termínem upozornit organizátora nebo veřejnost v diskuzi.
    
    </div>  
    
    <h4>Nasazení náhradníků do turnaje</h4>
    
    <div>
      Z důvodu současného nezájmu některých přihlášených hráčů a zájmu nepřihlášených hráčů přidávám toto speciální pravidlo:<br/>
      Poslední den prvního kola turnaje může nepřihlášený hráč nahradit v turnaji neaktivního přihlášeného hráče.<br/>
            Toto je možné provést za předpokladu, že:
      <ul>
      <li>hráč, který má být nahrazen prokazatelně není schopen odehrát svůj zápas ve stanoveném termínu</li>
      <li>volný soupeř neaktivního hráče a příchozí náhradník se dokážou dohodnout na zápase do konce termínu prvního kola
      (po dohodě s organizátorem je možné udělat výjimku a odehrát zápas o den později)</li>
      </ul>
      
      V tom případě bude náhradník zapsán do turnaje jako plnohodnotný účastník na místo vyřazeného neaktivního hráče.
    </div>

	<h1>Přihlášky</h1>

      <p>
      Přihlášky pište Tomisovi na ICQ: 258-431-157 nebo na mail: liga@heroes3.cz<br/>
      Konec přihlášek je v pátek 6. listopadu 2009, poté bude sestaven pavouk a od soboty 7.11. se začne hrát.<br/>
      Pro přihlášení do turnaje musíte být registrovaní v lize.
      </p>
      
      <table style\"border-spacing: 15pt;\"><tr>
      <td valign=\"top\">
      <h4>Přihlášení hráči</h4>
      
      <p>
        1: Tomis<br/>
        2: Afroman<br/>
        3: jukufa<br/>
        4: Matian<br/>
        5: Or1<br/>
        6: Tornádo<br/>
        7: DavidHakl<br/>
        8: KekelZpekel<br/>
        9: Bůh<br/>
        10: Spectral<br/>
        11: Kafuboss<br/>
        12: Gigiman<br/>
        13: Paolos<br/>
        14: Gaborn<br/>
        15: kaZ<br/>
        16: Lord.Alex<br/>
        17: Corwin<br/>
        18: Vik<br/>
        19: CD Cido<br/>
        20: Soki<br/>
        21: red joc<br/>
        22: Haklbery<br/>
        23: hujerOZ<br/>
        24: chucklorn<br/>
        25: Hannibal<br/>
        26: Nightwright<br/>
        27: Case<br/>
        28: CD lavino<br/>
        29: Skay<br/>
        30: tuatua222<br/>
        31: LuKi.22|SV|<br/>
        32: Montezuma3<br/>
        <br/>
        <br/>
        náhradník: Jess
      </p>
      </td>

      
      <td valign=\"top\">
      <h5>Seřazeno podle ELO</h5>
      
      1: KekelZpekel (2127)<br/>
      2: Tornádo (2111)<br/>
      3: Bůh (2045)<br/>
      4: Afroman (2022)<br/>
      5: Tomis (1974)<br/>
      6: Gaborn (1944)<br/>
      7: Or1 (1920)<br/>
      8. Nightwright (1896)<br/>
      9: Haklbery (1850)<br/>
     10: Gigiman (1836)<br/>
     11: CD Cido (1831)<br/>
     12: CD lavino (1799)<br/>
     13: DavidHakl (1747)<br/>
     14: Soki (1721)<br/>
     15: Lord.Alex (1720)<br/>
     16: Matian (1711)<br/>
     17: Case (1696)<br/>
     18: Vik (1696)<br/>
     19: hujerOZ (1670)<br/>
     20: red joc (1653)<br/>
     21: Corwin (1605)<br/>
     22: kaZ (1561)<br/>
     23: Hannibal (1561)<br/>
     24: Paolos (1550)<br/>
     25: Skay (1536)<br/>
     26: Spectral (1473)<br/>
     27: jukufa (1441)<br/>
     28: kafuboss (0)<br/>
     29: chucklorn (0)<br/>
     30: tuatua222 (0)<br/>
     31: LuKi.22|SV| (0)<br/>
     32: Montezuma3 (0)<br/>
      </td>
   
      <td valign=\"top\">
      <h5>Bitové schéma</h5>
      
      11111 &nbsp;&nbsp;&nbsp;&nbsp;= 31<br/>
      01111 &nbsp;&nbsp;&nbsp;&nbsp;= 15<br/>
      10111 &nbsp;&nbsp;&nbsp;&nbsp;= 23<br/>
      00111 &nbsp;&nbsp;&nbsp;&nbsp;= 7<br/>
      11011 &nbsp;&nbsp;&nbsp;&nbsp;= 27<br/>
      01011 &nbsp;&nbsp;&nbsp;&nbsp;= 11<br/>
      10011 &nbsp;&nbsp;&nbsp;&nbsp;= 19<br/>
      00011 &nbsp;&nbsp;&nbsp;&nbsp;= 3<br/>
      11101 &nbsp;&nbsp;&nbsp;&nbsp;= 29<br/>
      01101 &nbsp;&nbsp;&nbsp;&nbsp;= 13<br/>
      10101 &nbsp;&nbsp;&nbsp;&nbsp;= 21<br/>
      00101 &nbsp;&nbsp;&nbsp;&nbsp;= 5<br/>
      11001 &nbsp;&nbsp;&nbsp;&nbsp;= 25<br/>
      01001 &nbsp;&nbsp;&nbsp;&nbsp;= 9<br/>
      10001 &nbsp;&nbsp;&nbsp;&nbsp;= 17<br/>
      00001 &nbsp;&nbsp;&nbsp;&nbsp;= 1<br/>     
      11110 &nbsp;&nbsp;&nbsp;&nbsp;= 30<br/>
      01110 &nbsp;&nbsp;&nbsp;&nbsp;= 14<br/>
      10110 &nbsp;&nbsp;&nbsp;&nbsp;= 22<br/>
      00110 &nbsp;&nbsp;&nbsp;&nbsp;= 6<br/>
      11010 &nbsp;&nbsp;&nbsp;&nbsp;= 26<br/>
      01010 &nbsp;&nbsp;&nbsp;&nbsp;= 10<br/>
      10010 &nbsp;&nbsp;&nbsp;&nbsp;= 18<br/>
      00010 &nbsp;&nbsp;&nbsp;&nbsp;= 2<br/>
      11100 &nbsp;&nbsp;&nbsp;&nbsp;= 28<br/>
      01100 &nbsp;&nbsp;&nbsp;&nbsp;= 12<br/>
      10100 &nbsp;&nbsp;&nbsp;&nbsp;= 20<br/>
      00100 &nbsp;&nbsp;&nbsp;&nbsp;= 4<br/>
      11000 &nbsp;&nbsp;&nbsp;&nbsp;= 24<br/>
      01000 &nbsp;&nbsp;&nbsp;&nbsp;= 8<br/>
      10000 &nbsp;&nbsp;&nbsp;&nbsp;= 16<br/>
      00000 &nbsp;&nbsp;&nbsp;&nbsp;= 0<br/>               
      </td>   
      
      <td valign=\"top\">
      <h5>Seřazeno pro pavouka</h5>
      
      31 KekelZpekel<br/>
      30 Case <br/>
      29 Haklbery<br/>
      28 Skay<br/>
      27 Tomis<br/>
      26 Corwin<br/>
      25 DavidHakl<br/>
      24 chucklorn<br/>
      23 Bůh<br/>
      22 hujerOZ<br/>
      21 CD Cido<br/>
      20 jukufa<br/>
      19 Or1<br/>
      18 Hannibal<br/>
      17 Lord.Alex<br/>
      16 LuKi.22|SV|<br/>
      15 Tornádo<br/>
      14 Vik<br/>
      13 Gigiman<br/>
      12 Spectral<br/>
      11 Gaborn<br/>
      10 kaZ<br/>
       9 Soki<br/>
       8 tuatua222<br/>
       7 Afroman<br/>
       6 red joc<br/>
       5 CD lavino<br/>
       4 kafuboss<br/>
       3 Nightwright<br/>
       2 Paolos<br/>
       1 Matian<br/>
       0 Montezuma3<br/>
      
                    
      </td>       
      
      
      
      </tr></table>

	<h1>Průběh</h1>

      <div>
      1. kolo: sobota 7.11. - pátek 13.11.<br/>
      2. kolo: sobota 14.11. - pátek 20.11.<br/> 
      3. kolo(čtvrtfinále): sobota 21.11. - pátek 27.11.<br/>
      4. kolo(semifinále): sobota 28.11. - pátek 4.12.<br/>
      5. kolo(finále): sobota 5.12. - pátek 11.12.<br/>
      vyhlášení: sobota 12.12
      </div> 

";

echo "<h3>O třetí místo</h3>";

       echo pavouk(2, array(
        array("KekelZpekel",
          "KekelZpekel<br/><em>kontumační výhra</em>"),
        array("Tornádo")
        ), 0);


echo "<h3>Hlavní pavouk</h3>";

      echo pavouk(32,  array(
        array("KekelZpekel", "KekelZpekel<br/><a href=\"".$ROOT_URL."/hry/?detail=1965\">1965</a>",
          "KekelZpekel<br/><a href=\"".$ROOT_URL."/hry/?detail=1978\">1978</a>",
          "KekelZpekel<br/><a href=\"".$ROOT_URL."/hry/?detail=2022\">2022</a><br/><img src='".$IMG."/medaile/bronz.png' />",
          "Bůh<br/><a href=\"".$ROOT_URL."/hry/?detail=2120\">2120</a><br/><img src='".$IMG."/medaile/stribro.png' />",
          "Nightwright<br/><a href=\"".$ROOT_URL."/hry/?detail=2170\">2170</a><br/><img src='".$IMG."/medaile/zlato.png' />"),
        array("Case"),
        array("Haklbery", "Haklbery<br/><a href=\"".$ROOT_URL."/hry/?detail=1961\">1961</a>"),
        array("Skay"),
        array("Tomis", "Tomis<br/><a href=\"".$ROOT_URL."/hry/?detail=1936\">1936</a>",
          "Tomis<br/><a href=\"".$ROOT_URL."/hry/?detail=1988\">1988</a>"),
        array("Corwin"),
        array("DavidHakl", "DavidHakl<br/><em>kontumační výhra</em>"),
        array("chucklorn"),
        array("Bůh", "Bůh<br/><a href=\"".$ROOT_URL."/hry/?detail=1943\">1943</a>",
          "Bůh<br/><a href=\"".$ROOT_URL."/hry/?detail=1991\">1991</a>",
          "Bůh<br/><a href=\"".$ROOT_URL."/hry/?detail=2041\">2041</a>"),
        array("hujerOZ"),
        array("CD Cido", "CD Cido<br/><a href=\"".$ROOT_URL."/hry/?detail=1969\">1969</a>"),
        array("jukufa"),
        array("Or1", "Or1<br/><em>kontumační výhra</em>",
          "Lord.Alex<br/><a href=\"".$ROOT_URL."/hry/?detail=2034\">2034</a>"),
        array("Hannibal"),
        array("Lord.Alex", "Lord.Alex<br/><a href=\"".$ROOT_URL."/hry/?detail=1959\">1959</a>"),
        array("LuKi.22|SV|"),
        array("Tornádo", "Tornádo<br/><a href=\"".$ROOT_URL."/hry/?detail=1933\">1933</a>",
          "Tornádo<br/><a href=\"".$ROOT_URL."/hry/?detail=2005\">2005</a>",
          "Tornádo<br/><a href=\"".$ROOT_URL."/hry/?detail=2089\">2089</a>",
          "Nightwright<br/><a href=\"".$ROOT_URL."/hry/?detail=2122\">2122</a>"),
        array("Vik"),
        array("Gigiman", "Gigiman<br/><a href=\"".$ROOT_URL."/hry/?detail=1973\">1973</a>"),
        array("Spectral"),
        array("Gaborn", "Gaborn<br/><a href=\"".$ROOT_URL."/hry/?detail=1968\">1968</a>",
          "Gaborn<br/><a href=\"".$ROOT_URL."/hry/?detail=2016\">2016</a>"),
        array("Jess"),
        array("Soki", "Soki<br/><a href=\"".$ROOT_URL."/hry/?detail=1944\">1944</a>"),
        array("tuatua222"),
        array("Afroman", "Afroman<br/><a href=\"".$ROOT_URL."/hry/?detail=1927\">1927</a>",
          "Afroman<br/><a href=\"".$ROOT_URL."/hry/?detail=2011\">2011</a>",
          "Nightwright<br/><a href=\"".$ROOT_URL."/hry/?detail=2020\">2020</a>"),
        array("red joc"),
        array("CD lavino", "CD lavino<br/><a href=\"".$ROOT_URL."/hry/?detail=1937\">1937</a>"),
        array("kafuboss"),
        array("Nightwright", "Nightwright<br/><a href=\"".$ROOT_URL."/hry/?detail=1930\">1930</a>",
          "Nightwright<br/><a href=\"".$ROOT_URL."/hry/?detail=2012\">2012</a>"),
        array("Paolos"),
        array("Matian", "Matian<br/><a href=\"".$ROOT_URL."/hry/?detail=1951\">1951</a>"),
        array("Montezuma3")          
      ), 1); // "style=\"width: 95%\""

echo "</div>";

ramecek_end(1024);



html_end();

?>
