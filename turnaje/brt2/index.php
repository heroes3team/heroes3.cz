<?php


require('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

ramecek_start(1024, 'Brutal Random Tournament 2');

echo "<div style=\"margin-left: 10px;\">";

echo "

&nbsp;
	<h1>Pravidla</h1>


       <h4>Systém</h4>        
      <p>
        Systém turnaje je klasický vyřazovací pavouk. Počet kol se určí podle počtu přihlášených.
        Počáteční rozmístění hráčů v pavouku se určí tak, aby byly obě poloviny každého (pod)pavouka vyrovnané.<br/>
        Na každé kolo bude limit 1 týden. Kdo nestihne odehrát, má kontumační prohru.
      </p>
               
    
    <h4>Turnajové hry</h4>
    
    <p>

    Nastavení hry je následující:
    <ul>
     <li>mapa: náhodná L bez podzemí, množství vody: náhodné, síla bojovníků: vysoká</li>        
     <li>časový limit: 2 minuty</li>
     <li>obtížnost: expert (160%)</li>
     <li>oba hráči začínají s náhodným výběrem města a hrdiny</li>
    </ul>

    Jelikož se jedná o Brutal Random Tournament, je povolené využívat všech aspektů hry, tzn. včetně diplomacie atd.
    <br/>
    Odehrané duely se ukládají do záznamů, jako každá jiná hra. Na začátek komentáře k turnajové hře napište *TURNAJ*.
    Odehrané duely je nutné nahlásit organizátorovi, aby mohli být zapsány do pavouka.<br/>
      
    </p>

      <h4>Reklamace mapy</h4>
    <p>
      Hráč má nárok na novou hru v pouze případě, že je mapa naprosto nehratelná. (tzn. například cestu k hradu blokuje parta titánů)
      <br/>
      <br/>
      Kromě toho je možné restartovat hru v případě, že v městech chybí tvrz, hráčí mají více měst, města jsou na špatném povrchu apod.
      <br/>
      <br/>
      V případě sporu rozhodne o hratelnosti/nehratelnosti mapy nestranná porota složená z organizátora nebo jímž pověřených asistentů.
    </p>
    
      <h4>Kontumace neaktivních hráčů</h4>
      
    <p>
    
    Pokud se do konce termínu daného kola neozve ani jeden z hráčů, kteří mají hrát turnajovou hru, dostanou oba kontumační prohru.
    
    V případě že je jeden hráč aktivní a jeho soupeř není k sehnání, dostane kontumaci jen soupeř. V tom případě musí být vidět
    snaha aktivního hráče o sehnání soupeře. Aktivní hráč by proto měl několik dní před termínem upozornit organizátora nebo veřejnost v diskuzi.
    
    </p>  
    
    <h4>Nasazení náhradníků do turnaje</h4>
    
    <p>
      Kdyby došlo k nezájmu některých přihlášených hráčů a zájmu nepřihlášených hráčů bude přidáno toto speciální pravidlo:<br/>
      Poslední den prvního kola turnaje může nepřihlášený hráč nahradit v turnaji neaktivního přihlášeného hráče.<br/>
            Toto je možné provést za předpokladu, že:
      <ul>
      <li>hráč, který má být nahrazen prokazatelně není schopen odehrát svůj zápas ve stanoveném termínu</li>
      <li>volný soupeř neaktivního hráče a příchozí náhradník se dokážou dohodnout na zápase do konce termínu prvního kola
      (po dohodě s organizátorem je možné udělat výjimku a odehrát zápas o den později)</li>
      </ul>
      
      V tom případě bude náhradník zapsán do turnaje jako plnohodnotný účastník na místo vyřazeného neaktivního hráče.<br/>
      
      Pokud je náhradníků více než 1, tak nasazováni bude chronologické dle přihlášek do turnaje, samozřejmě roli bude hrát aktivnost a dosavadní ELO.<br/>
    </p>

	<h1>Přihlášky</h1>

   <p>
      Dotazy či připomínky pište Tornádovi (210-517-708) nebo Tornado@heroes3.cz ;)<br/>
      Konec registrace a datum zahájení byl 4.8.2010 v 22:30h.<br/>
      </p>
      
      <table style=\"border-spacing: 15pt;\"><tr>
      <td valign=\"top\">
      <h3>Přihlášení hráči</h3>
      
      <p>
        1: Nightwright<br/>
        2: Jakbe<br/>
        3: Unreal<br/>
        4: Corwin<br/>
        5: Fífa<br/>
        6: Niven<br/>
        7: Magi<br/>
        8: Nethos<br/>
        9: Spectral<br/>
        10: Fuckinek<br/>
        11: H34D<br/>
        12: Bernau<br/>
        13: Gaborn<br/>
        14: DanEvil<br/>
        15: Lord.Alex<br/>
        16: Gigiman<br/>
        17: Acid<br/>
        18: Peterson<br/>
        19: CD Lavino<br/>
        20: CD Cido<br/>
        21: Paolos<br/>
        22: Siam<br/>
        23: Kisane<br/>
        24: Pagy<br/>
        25: Pagi<br/>
        26: Haklbery<br/>
        27: DavidHakl<br/>
        28: PetoPalo<br/>
        29: Borosenior<br/>
        30: Marekrocky<br/>
        31: Aersen<br/>
        32: HujerOZ<br/>
        33: Afroman<br/>
        34: Mrchicken<br/>
        35: Skay<br/>
        36: Karcma<br/>
        37: Jess<br/>
        38: Royal.Flush<br/>
        39: Phoenixisnc<br/>
        40: Bync<br/>
        
        <br/>
        náhradníci: 1. Duša, 2. Imposieble, 3. Matian, 4. Vitkov, 5. Red Joc
      </p>
      
      
      </td>

      
      <td valign=\"top\">
      <h5>Seřazeno podle ELO</h5>
      
      1: Nightwright (2122)<br/>
      2: Afroman (2021)<br/>
      3: Gaborn (1990)<br/>
      4: DavidHakl (1957)<br/>
      5: Pagi (1951)<br/>
      6: Gigiman (1935)<br/>
      7: Haklbery (1934)<br/>
      8: Nethos (1933)<br/>
      9: H34D (1899)<br/>
     10: Jess (1866)<br/>
     11: Jakbe (1854)<br/>
     12: Peterson (1847)<br/>
     13: CD Cido (1834)<br/>
     14: Acid (1827)<br/>
     15: CD Lavino (1814)<br/>
     16: Aersen (1813)<br/>
     17: HujerOZ (1759)<br/>
     18: Niven (1742)<br/>
     19: DanEvil (1741)<br/>
     20: Lord.Alex (1731)<br/>
     21: Unreal (1717)<br/>
     22: Karcma (1699)<br/>
     23: Pagy (1675)<br/>
     24: PetoPalo (1667)<br/>
     25: Borosenior (1654)<br/> 
     26: Siam (1634)<br/>
     27: Corwin (1621)<br/>
     28: Fífa (1616)<br/>
     29: Bernau (1578)<br/>
     30: Bync (1574)<br/>
     31: Skay (1540)<br/>
     32: Royal.Flush (1524)<br/>
     33: Kisane (1522)<br/>
     34: Mrchicken (1514)<br/>
     35: Magi (1502)<br/>
     36: Paolos (1478)<br/>
     37: Marekrocky (1378)<br/>
     38: Phoenixisnc (1349)<br/>
     39: Fuckinek (1321)<br/>
     40: Spectral (1251)<br/>
      </td>
   
      <td valign=\"top\">
      <h5></h5>
                 
      </td>   
      
      <td valign=\"top\">
      <h5></h5>
      
                    
      </td>       
      
      
      
      </tr></table>


	<h1>Průběh</h1>

   <p>
      Kvalifikace: středa 4.8 - sobota 7.8.<br/>
      1. kolo: neděle 8.8. - pátek 13.8.<br/>
      2. kolo: sobota 14.8. - pátek 20.8.<br/>
      3. kolo(čtvrtfinále): sobota 21.8 - pátek 27.8.<br/>
      4. kolo(semifinále): sobota 28.8 - pátek 3.9<br/>
      5. kolo(finále): sobota 4.9 - pátek 10.9<br/>
      </p> 
      
      Zde se můžete podívat na systém, který byl použit pro tento turnaj: <a href=\"../pavouk.php\">pavouk</a>
      

      <h3>o třetí místo</h3>";
  
       echo pavouk(2, array(
        array("Matian", "Bernau<br/><a href=\"".$ROOT_URL."/hry/?detail=5044\">5044</a>"),
        array("Bernau")
        ), 0);
      
      ?>
      
      <h3>hlavní pavouk</h3>
      
      <?php
      echo pavouk(32,  array(
        array("Nightwright", "Matian<br/><a href=\"".$ROOT_URL."/hry/?detail=4655\">4655</a>", "Matian<br/><a href=\"".$ROOT_URL."/hry/?detail=4693\">4693</a>", 
        "Matian<br/><a href=\"".$ROOT_URL."/hry/?detail=4742\">4742</a>", "Pagi<br/><a href=\"".$ROOT_URL."/hry/?detail=4966\">4966</a><br/><img src='".$IMG."/medaile/stribro.png' />", 
        "Haklbery<br/><a href=\"".$ROOT_URL."/hry/?detail=4992\">4992</a><br/><img src='".$IMG."/medaile/zlato.png' />"),
        array("Matian"),
        array("Imposieble", "HujerOZ<br/><a href=\"".$ROOT_URL."/hry/?detail=4635\">4635</a>"),
        array("HujerOZ"),
        array("Nethos", "Nethos<br/><a href=\"".$ROOT_URL."/hry/?detail=4567\">4567</a>", "Nethos<br/><a href=\"".$ROOT_URL."/hry/?detail=4720\">4720</a>"),
        array("Borosenior"),
        array("H34D", "PetoPalo<br/><a href=\"".$ROOT_URL."/hry/?detail=4557\">4557</a>"),
        array("PetoPalo"),
        array("DavidHakl", "DavidHakl<br/><a href=\"".$ROOT_URL."/hry/?detail=4607\">4607</a>", "DavidHakl<br/><a href=\"".$ROOT_URL."/hry/?detail=4672\">4672</a>", 
        "Pagi<br/><a href=\"".$ROOT_URL."/hry/?detail=4788\">4788</a>"),
        array("Pagy"),
        array("CD Cido", "Lord.Alex<br/><a href=\"".$ROOT_URL."/hry/?detail=4536\">4536</a>"),
        array("Lord.Alex"),
        array("Pagi", "Pagi<br/><a href=\"".$ROOT_URL."/hry/?detail=4631\">4631</a>", "Pagi<br/><a href=\"".$ROOT_URL."/hry/?detail=4697\">4697</a>"),
        array("Skay"),
        array("Peterson", "Unreal<br/><a href=\"".$ROOT_URL."/hry/?detail=4582\">4582</a>"),
        array("Unreal"),
        array("Afroman", "Afroman<br/><a href=\"".$ROOT_URL."/hry/?detail=4592\">4592</a>", "CD Lavino<br/><a href=\"".$ROOT_URL."/hry/?detail=4768\">4768</a>", 
        "Haklbery<br/><a href=\"".$ROOT_URL."/hry/?detail=4860\">4860</a>", "Haklbery<br/><a href=\"".$ROOT_URL."/hry/?detail=4868\">4868</a>"),
        array("Mrchicken"),
        array("CD Lavino", "CD Lavino<br/><a href=\"".$ROOT_URL."/hry/?detail=4593\">4593</a>"),
        array("Niven"),
        array("Haklbery", "Haklbery<br/><a href=\"".$ROOT_URL."/hry/?detail=4623\">4623</a>", "Haklbery<br/><em>kontumační výhra</em>"),
        array("Fífa"),
        array("Jess", "Jess<br/><a href=\"".$ROOT_URL."/hry/?detail=4620\">4620</a>"),
        array("Duša"),
        array("Gaborn", "Kisane<br/><a href=\"".$ROOT_URL."/hry/?detail=4574\">4574</a>", "Acid<br/><a href=\"".$ROOT_URL."/hry/?detail=4681\">4681</a>",
        "Bernau<br/><a href=\"".$ROOT_URL."/hry/?detail=4755\">4755</a><br/><img src='".$IMG."/medaile/bronz.png' />"),
        array("Kisane"),
        array("Acid", "Acid<br/><a href=\"".$ROOT_URL."/hry/?detail=4598\">4598</a>"),
        array("DanEvil"),
        array("Gigiman", "Bernau<br/><a href=\"".$ROOT_URL."/hry/?detail=4586\">4586</a>", "Bernau<br/><a href=\"".$ROOT_URL."/hry/?detail=4643\">4643</a>"),
        array("Bernau"),
        array("Jakbe", "Jakbe<br/><a href=\"".$ROOT_URL."/hry/?detail=4583\">4583</a>"),
        array("Karcma")
      ), 1); // "style=\"width: 95%\""
      ?>
     

      <div>
	<br/>
      Kvalifikace:<br/>
      1. Siam        vs Mrchicken /-/ Mrchicken <a href="".$ROOT_URL."/hry/?detail=4517">4517</a> <br/>
      2. Corwin      vs Magi /-/ Magi <a href="".$ROOT_URL."/hry/?detail=4537&">4537</a> <br/>
      3. Fífa        vs Paolos /-/ Fífa <a href="".$ROOT_URL."/hry/?detail=4523">4523</a> <br/>
      4. Bernau      vs Marekrocky /-/ Bernau <a href="/games/?game_id=4532">4532</a> <br/>
      5. Bync        vs Borosenior /-/ Borosenior <a href="/games/?game_id=4538">4538</a> <br/>
      6. Skay        vs Phoenixisnc /-/ Skay <a href="".$ROOT_URL."/hry/?detail=4515">4515</a> <br/>     
      7. Royal.Flush vs Fuckinek /-/ Royal.Flush <a href="".$ROOT_URL."/hry/?detail=4511">4511</a> <br/>
      8. Kisane      vs Spectral /-/  Kisane <a href="".$ROOT_URL."/hry/?detail=4508">4508</a> <br/>
      <br/>
      25.Borosenior<br/>
      26.Fífa<br/>
      27.Bernau<br/>
      28.Skay<br/>
      29.Royal.Flush<br/>
      30.Kisane<br/>
      31.Mrchicken<br/>
      32.Magi<br/>
      </div>
     

<?php
echo "</div>";

ramecek_end(1024);



html_end();

?>
