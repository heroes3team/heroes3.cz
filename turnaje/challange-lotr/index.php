<?php
/*
* Domovska stranka ligy
*/

include('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

# levy sloupec
sloupec_start(310);

	# login / uzivatel.panel
	include($INCLUDE_DIR.'/login_ramecek.php');
	
	// tabulka držitelů
	
	ramecek_start (286,'Seznam držitelů');
		echo '<table class = "tab_top" style = "width: 200px; margin-left: 20px;" >';
			echo '<tr>';
				echo '<th>JMÉNO HRÁČE</td>';
				echo '<th>DATUM</td>';
				echo '<th>SKÓRE</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>AbsoluteZero</td>';
				echo '<td>18.5.2018</td>';
				echo '<td>455 dní</td>';
			echo '</tr>';
		echo '</table>';
	ramecek_end (286);
sloupec_end();

sloupec_start();

	ramecek_start(1024, 'Jeden prsten vládně všem');
		echo "<div style = \"padding: 10px;\">";
				echo '<table style = "width: 97%; padding-right: 15 px;"><tr><td>';
				echo h1("Jeden prsten vládne všem ...");
				echo '</tr></td><td style = "width: 600px;">';
				echo html_p("Pro opravdové Heroes 3 nadšence jsme připravili novou výzvu! Jedná se o mapu speciálně vytvořenou podle předlohy světoznámého knižního bestselleru \"Pán prstenů\". ");	
				echo html_p("Vzhledem k tomu, že zpracovanání mapy je promyšleno do nejmenšího detailu, mapa zaručuje hráči mnoho hodin zábavy, radosti a trápení.");
				echo "<br><br>";
				echo '<a class = "button" style = "margin-left: 7px;" href = "lord_of_the_rings_3_974.rar" download >Stáhnout mapu</a>' ;
				echo "<br><br><br>";
				echo h2("Pravidla");
				echo html_p("Ten kdo dohraje mapu za nejmenší počet dní získává odměnu v podobně ocenění <b>Prstenu moci</b>.");
				echo html_p("Ovšem pozor! Prsten zůstává v držitelovi moci pouze do té doby, než se najde někdo kdo mapu dohraje za méně dní!");
				echo html_p("Na internetu je možno shlédnout <a style = \"text-decoration: underline;\" href = \"https://www.youtube.com/watch?v=pd-ssjW35eY&list=PLqyBKiHbM__FZsyMgFi3DoZ0BbopnNJvU\" target = _blank >playtrought video</a> od YouTubera AbsoluteZero, který mapu dohrál za 455 dní.") ;
				echo "<br>";
				echo h2("Registrace");
				echo html_p("<ol style = \"font-family: sans-serif\">
							  <li>Hráč který překoná aktuálního šampiona odešle zabalený (.rar nebo .zip) save ze hry na pořadatele výzvy <a style = \"text-decoration: underline;\" href = \"/../hraci/detail.php?id=884\">Macwar</a>.</li>
							  <li>Hra musí být ukládána po dni. Takže v případě, že hráč dohrál mapu za 300 dní, finální archiv bude obsahovat 300 savů.</li>
							  <li>Pořadatel savy zkontroluje a v případě, že je vše v pořádku přidělí ocenění.</li>
							</ol>") ;
				echo "<br>";
				echo '</td><td style = "text-align: center; vertical-align: top;">';
				echo "<img style = \"margin-left: 7px; height: 250px;\" class = \"img_ramecek\" src = \"https://fannysschmuck.de/wp-content/uploads/2016/02/Screenshot-1-The-One-Ring-3D-Screensaver-1024x819-01f5bfea4b178bf1.jpg\" />";
				echo '<br><br>';
				echo '</td></tr></table>';

		echo "</div>";
	ramecek_end(1024);
	

sloupec_end();


html_end();

?>
