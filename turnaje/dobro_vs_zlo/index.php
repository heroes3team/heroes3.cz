<?php


require('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

ramecek_start(1024, 'Dobro Versus Zlo');

echo "<div style=\"margin-left: 10px;\">";

echo "<img style=\"width: 500px; height: 300px;\" alt=\"\" src=\"logo_dvz.jpg\" />";
?>
  
<div><h1>Pravidla</h1></div>

<br />

</big><big><small>Ve hře dobro versus zlo jsou hrady rozděleny n&aacute;sledovně:<br />

&nbsp;<span style="font-weight: bold;">Dobro</span>: Hrad, Ba&scaron;ta, Věž, Tvrz<br />

&nbsp;<span style="font-weight: bold;">Zlo</span>: Kobka, Peklo, Necropolis, Pevnost<br />

<br />

Soutok byl vypu&scaron;těn, jelikož hradů mus&iacute; b&yacute;t
sud&yacute; počet, a soutok je hodně nevyrovnan&yacute; oproti
ostatn&iacute;m.<br />

<br />

Syst&eacute;m je klasick&yacute; vyřazovac&iacute; pavouk, kde
soupeř&iacute; pouze mezi sebou jakmile hr&aacute;č prohraje tak
vypad&aacute;v&aacute; ze skupiny. 2 hr&aacute;či -
v&iacute;tězov&eacute; skupin se pak utkaj&iacute; ve fin&aacute;le o
celkov&eacute; v&iacute;tězstv&iacute;. Každ&yacute; hr&aacute;č se
dobrovolně a libovolně přihl&aacute;s&iacute; do skupiny, za kterou by
chtěl hr&aacute;t. Nen&iacute; možn&eacute; v průběhu turnaje
přej&iacute;t na druhou stranu. Hr&aacute;č co si vybere dobro nebo
zlo, tak hraje za hrady určen&eacute; k dan&eacute; straně. Pokud bude
v jedn&eacute; skupině
vět&scaron;&iacute; počet hr&aacute;čů, jak ve druh&eacute;, tak se
automaticky přiděl&iacute; naposledy přihl&aacute;&scaron;en&iacute;
hr&aacute;či do skupiny druh&eacute;, aby byl stejn&yacute; počet
z&uacute;častněn&yacute;ch hr&aacute;čů na každ&eacute; straně.<span style="font-weight: bold;"><br />

</span></small></big><big><small><span style="font-weight: bold;"><br />


Syst&eacute;m přidělov&aacute;n&iacute; měst<br />

<br />

</span></small></big><big><small>Proti sobě budou vždycky st&aacute;t města v t&eacute;to podobě: <br />

&nbsp;1)Ba&scaron;ta vs. Kobka<br />

&nbsp;2)Věž vs. Peklo<br />

&nbsp;3)Hrad vs. Necropolis<br />

&nbsp;4)Tvrz vs. Pevnost<br />

<br />


Pokud se do turnaje dostanou strany dobro versus dobro, či zlo versus zlo, tak dvojice hradů, budou vypadat n&aacute;sledovně:<br />

&nbsp;1)Hrad vs. Ba&scaron;ta<br />

&nbsp;2)Věž vs. Tvrz<br />

&nbsp;3)Pevnost vs. Peklo<br />

&nbsp;4)Kobka vs. Necropolis<br />

<br />


Dvojice hr&aacute;čů se před hrou domluv&iacute; mezi sebou, jako
dvojici hradů si vyberou. Pokud se nebudou moci domluvit, tak
nap&iacute;&scaron;ou organiz&aacute;torovi, nebo Lordu.Alexovi. Byl t&iacute;mto pověřen.<br />

&nbsp;Děkuji za pochopen&iacute; a douf&aacute;m, že každ&yacute; z ligy je schopen se dohodnout.<br />



&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small></big><big><small><span style="font-weight: bold;"><br />

</span></small></big><big><small><span style="font-weight: bold;"></span>
</small></big><span style="font-weight: bold;">Turnajov&eacute; hry</span>

    
    
<p>

    Nastaven&iacute; hry je n&aacute;sleduj&iacute;c&iacute;:
    </p>
<ul>
  <li>mapa: n&aacute;hodn&aacute; L bez podzem&iacute;, množstv&iacute;
vody: ž&aacute;dn&eacute;, s&iacute;la bojovn&iacute;ků: vysok&aacute;</li>
<li>časov&yacute; limit: 4 minuty</li>

  <li>obt&iacute;žnost: expert (160%)</li>


  <li>oba hr&aacute;či zač&iacute;naj&iacute; s přidělen&yacute;m městem a n&aacute;hodn&yacute;m hrdinou</li>

</ul>

Hraje se bez diplomacie, bez gr&aacute;lu a monsters na artefakty, pandořiny skř&iacute;òky, budovy a svitky.<br />

Odehran&eacute; duely se ukl&aacute;daj&iacute; do z&aacute;znamů, jako
každ&aacute; jin&aacute; hra. Na zač&aacute;tek koment&aacute;ře k
turnajov&eacute; hře napi&scaron;te *TURNAJ - DZ*. Odehran&eacute; duely je
nutn&eacute; nahl&aacute;sit organiz&aacute;torovi, aby mohli
b&yacute;t zaps&aacute;ny do pavouka.<br />

<span style="font-weight: bold;"><br />

Reklamace mapy</span><br />

<br />

<p>Hr&aacute;č m&aacute; n&aacute;rok na reklamaci v př&iacute;padě:
    </p>

<ul>


  <li>m&aacute; před městem siln&eacute; jednotky, t&iacute;m je mapa nehrateln&aacute;</li>

  <li>když hr&aacute;či zač&iacute;naj&iacute; s dvěma či v&iacute;ce městy, nebo jsou vesnice(chyb&iacute; tvrz)</li>


  <li>když jsou města na &scaron;patn&eacute;m povrchu</li>

  <li>pokud u hradu chyb&iacute; cesta</li>

</ul>

V př&iacute;padě sporu rozhodne o hratelnosti/nehratelnosti mapy
nestrann&aacute; porota složen&aacute; z organiz&aacute;tora nebo
j&iacute;mž pověřen&yacute;ch asistentů. S jak&yacute;mkoliv dotazem
pi&scaron;tě mě, nebo Lordu.Alexovi.<br />

<br />

<h4>Kvalifikace do TURNAJE</h4>

Pokud bude v&iacute;ce hr&aacute;čů jak 16 v každ&eacute; skupince, tak
se budou hr&aacute;t kvalifikace. Pořad&iacute; soupeřů bude
z&aacute;ležet na pořad&iacute; ve skupink&aacute;ch dobro vs. zlo.
Posledn&iacute; s prvn&iacute;m atd. Pokud nebude vyrovnan&yacute; stav
na obou stran&aacute;ch mus&iacute;m cht&iacute;t necht&iacute;t
naposledy přihl&aacute;&scaron;en&eacute; hr&aacute;če z
kvalifikac&iacute; přesunout na druhou stranu.<br />


<br />

<h4>Kontumace neaktivn&iacute;ch hr&aacute;čů</h4>

<p>
    
    Pokud se do konce term&iacute;nu dan&eacute;ho kola neozve ani jeden z hr&aacute;čů, 
kteř&iacute; maj&iacute; hr&aacute;t turnajovou hru, dostanou oba kontumačn&iacute; prohru.
    
    V př&iacute;padě že je jeden hr&aacute;č aktivn&iacute; a jeho soupeř nen&iacute; k sehn&aacute;n&iacute;, 
dostane kontumaci jen soupeř. V tom př&iacute;padě mus&iacute; b&yacute;t vidět
    snaha aktivn&iacute;ho hr&aacute;če o sehn&aacute;n&iacute; soupeře. Aktivn&iacute; hr&aacute;č by proto měl 
několik dn&iacute; před term&iacute;nem upozornit organiz&aacute;tora nebo veřejnost v 
diskuzi.
    
    </p>

<h4>Nasazen&iacute; n&aacute;hradn&iacute;ků do turnaje</h4>

<p> Kdyby do&scaron;lo k nez&aacute;jmu někter&yacute;ch
přihl&aacute;&scaron;en&yacute;ch hr&aacute;čů a z&aacute;jmu
nepřihl&aacute;&scaron;en&yacute;ch hr&aacute;čů bude přid&aacute;no
toto speci&aacute;ln&iacute; pravidlo:<br />

Posledn&iacute; den prvn&iacute;ho kola turnaje může
nepřihl&aacute;&scaron;en&yacute; hr&aacute;č nahradit v turnaji
neaktivn&iacute;ho přihl&aacute;&scaron;en&eacute;ho hr&aacute;če.<br />

            Toto je možn&eacute; prov&eacute;st za předpokladu, že:
      </p>
<ul>
  <li>hr&aacute;č, kter&yacute; m&aacute; b&yacute;t nahrazen
prokazatelně nen&iacute; schopen odehr&aacute;t svůj z&aacute;pas ve
stanoven&eacute;m term&iacute;nu</li>

  <li>voln&yacute; soupeř neaktivn&iacute;ho hr&aacute;če a
př&iacute;choz&iacute; n&aacute;hradn&iacute;k se dok&aacute;žou
dohodnout na z&aacute;pase do konce term&iacute;nu prvn&iacute;ho kola
(po dohodě s organiz&aacute;torem je možn&eacute; udělat v&yacute;jimku
a odehr&aacute;t z&aacute;pas o den později)</li>
</ul>
V tom př&iacute;padě bude n&aacute;hradn&iacute;k zaps&aacute;n do
turnaje jako plnohodnotn&yacute; &uacute;častn&iacute;k na m&iacute;sto
vyřazen&eacute;ho neaktivn&iacute;ho hr&aacute;če.<br />
  
      Pokud je n&aacute;hradn&iacute;ků v&iacute;ce než 1, tak nasazov&aacute;ni bude chronologick&eacute; 
dle přihl&aacute;&scaron;ek do turnaje, samozřejmě roli bude hr&aacute;t aktivnost a 
dosavadn&iacute; ELO.
 
<h1>Přihlášky</h1>

<p>
      Přihl&aacute;&scaron;ky pi&scaron;te Nivenovi na ICQ: 232-123-291.<br />

Konec přihl&aacute;&scaron;ek je v p&aacute;tek 15. ř&iacute;jna 2010,
pot&eacute; bude sestaven pavouk a od soboty 16.10. se začne
hr&aacute;t.<br />
      Pro přihl&aacute;&scaron;en&iacute; do turnaje mus&iacute;te b&yacute;t registrovan&iacute; v lize.</p>  
<table style="width: 654px; height: 336px;">
  <tbody>
    <tr>
      <td valign="top">
      <h3>Přihl&aacute;&scaron;en&iacute; hr&aacute;či(Zlo)</h3>
      <p>
        01: Niven<br />
        02: Unreal<br />
        03: Davinho<br />
        04: Jakbe<br />
        05: KekelZpekel<br />
        06: LordMike<br />
        07: Phoenixisnc<br />
        08: Lord.Alex<br />
        09: mrchicken<br />
        10: Maras<br />
        11: Spectral<br />
        12: Pagi<br />
        13: PetoPalo<br />
        14: ChuckNorris<br />
        15: Pagy<br />
        16: Nightwright<br />
        17: Demon<br />
        18: hujerOZ<br />
        19: MiraQ<br />
        20: Aersen
      </p>
      </td>
      <td valign="top">
      <h3>Přihl&aacute;&scaron;en&iacute; hr&aacute;či(Dobro)</h3>
     01: lyk<br />
      02: Fuckinek<br />
      03: Vitkov<br />
      04: John T.<br />
      05: LadinaS<br />
      06: Macwar<br />
      07: Corwin<br />
      08: F&iacute;fa<br />
      09: Diablo4911<br />
      10: Nethos<br />
      11: marekrocky<br />
      12: Skalda<br />
      13: Imposieble<br />
      14: Magi<br />
      15: CD lavino<br />
      16: CD Cido<br />
      17: Kisane<br />
      18: Peterson<br />
      19: acid<br />
      20: Paolos<br />
      </td>
      <td valign="top">
      <h5><big>Seřazeno podle ELO(Zlo)</big><br />
      </h5>
      01: KekelZpekel(2200)<br />
02: Nightwright(2071) &nbsp;<br />
      03: Pagi(1940)<br />
      04: ChuckNorris(1903)<br />
      05: Maras(1863)<br />
      06: Jakbe(1860)<br />
      07: Aersen(1789)<br />
      08: Unreal(1777)<br />
      09: Lord.Alex(1753)<br />
10: hujerOZ(1727)<br />
      11: PetoPalo(1699)<br />
      12: Niven(1698)<br />
      13: LordMike(1692)<br />
      14: Pagy(1652)<br />
      15: MiraQ(1609)<br />
      16: mrchicken(1468)<br />
      17: Demon(1438)<br />
      18: Davinho(1422)<br />
      19: Spectral(1368)<br />
      20:&nbsp;Phoenixisnc(1315)         
      </td>
      <td valign="top">
      <h5><big>Seřazeno podle ELO(Dobro)</big></h5>
01: Nethos(1963)<br />
02: lyk(1908)<br />
      03: Imposieble(1854)<br />
      04: CD lavino(1835)<br />
      05: acid(1834)<br />
      06: Peterson(1826)<br />
      07: CD Cido(1822)<br />
      08: Vitkov(1748)<br />
      09: F&iacute;fa(1663)<br />
10: John T.(1628)<br />
      11: Kisane(1620)<br />
      12: Corwin(1555)<br />
      13: Macwar(1537)<br />
      14: LadinaS(1495)<br />
      15: Magi(1468)<br />
      16: Paolos(1455)<br />
      17: Fuckinek(1380)<br />
      18: marekrocky(1328)<br />
      19: Diablo4911(0)<br />
      20:&nbsp;Skalda(0)
      <big>            
      </big></td>
      </tr>
  </tbody>
</table>
<br />

N&aacute;hradn&iacute;ci: Torn&aacute;do, rAstor, Aresz, Hellfirr, chucklorn<br />
Přesunut&iacute; hr&aacute;či: 0<br />
Vyřazen&iacute; hr&aacute;či (podvody): Skalda, Diablo4911<br />
<br />
<br />

<p>Kvalifikace: čtvrtek 23.9. - čtvrtek 14.10.<br />
1. kolo: sobota 16.10. - p&aacute;tek 29.10.2010<br />
2. kolo: sobota 30.10. - p&aacute;tek 12.11.2010<br />
3. kolo(čtvrtfin&aacute;le): sobota 13.11. - p&aacute;tek 19.11.2010<br />
4. kolo(semifin&aacute;le): sobota 20.11. - p&aacute;tek 26.11.2010<br />
5. kolo(boj o v&iacute;těze turnaje a o třet&iacute; m&iacute;sto):
sobota 27.11. - p&aacute;tek 3.12.2010<br />
vyhl&aacute;&scaron;en&iacute;: sobota 4.12.2010</p>
Zde se můžete pod&iacute;vat na syst&eacute;m, kter&yacute; byl použit
pro tento turnaj: <a href="../pavouk.php">pavouk</a><br />
<br />
<span style="font-weight: bold;">O 3.t&iacute; m&iacute;sto</span><br />
<span style="font-weight: bold;"><br />
</span>
<table border="1">
  <tbody>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Pagi<br />
(zlo)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Pagi<br />
(zlo)<br />
      <span style="color: rgb(255, 102, 0);">&nbsp;</span><a
 href="http://www.heroes3.cz/hry/?detail=5831&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(5831)</span></a><br />
      <span style="color: rgb(255, 102, 0);"> <img
 style="width: 100px; height: 100px;" alt="" src="files/tetmsto.gif" /></span></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Or1<br />
(dobro)</td>
    </tr>
  </tbody>
</table>
<br />
<span style="font-weight: bold;">Absolutn&iacute; v&iacute;těz TURNAJE</span><br />
<br />
<table border="1">
  <tbody>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Nightwright<br />
(zlo)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Nightwright<br />
(zlo)<br />
      <a href="http://www.heroes3.cz/hry/?detail=5694&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(5694)</span></a><br />
      <img style="width: 100px; height: 100px;" alt=""
 src="files/prvnmsto.gif" /></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Imposieble<br />
(dobro)</td>
    </tr>
  </tbody>
</table>
<br />
<span style="font-weight: bold;">Hlavn&iacute; pavouk</span><br />
<br />
<?php require_once("dvz_pavouk.php");?><br />
</div>
</div>

<?php

ramecek_end(1024);



html_end();

?>
