<?php


require('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

ramecek_start(1024, 'Dobro Versus Zlo 2');

echo "<div style=\"margin-left: 10px;\">";

echo "<img style=\"width: 500px; height: 300px;\" alt=\"\" src=\"logo_dvz.jpg\" />";
?>
  
<div><h1>Pravidla</h1></div>


<big style="font-weight: bold;"><br />
Pravidla Turnaje Dobro versus Zlo 2<br />
<br />
</big><big><small>Ve hře dobro versus zlo jsou hrady rozděleny
následovně:<br />
&nbsp;<span style="font-weight: bold;">Dobro</span>: Hrad,
Bašta, Věž, Tvrz<br />
&nbsp;<span style="font-weight: bold;">Zlo</span>: Kobka, Peklo,
Necropolis, Pevnost<br />
<br />
Soutok byl vypuštěn, jelikož hradů musí být
sudý počet, a soutok je hodně nevyrovnaný oproti
ostatním.<br />
<br />
Systém je klasický vyřazovací pavouk, kde se hraje
se na 2 vítězné, takže hráči spolu svedou 2-3 hry
než jeden z nich vypadne. Díky tomu je větší
šance, že hráč nevypadne hned kvůli faktoru, že dostane
zlou mapu. Jakmile jeden hráč získá 2 výhry
nad soupeřem tak postupuje do dalšího kola. 2
hráči -
vítězové skupin se pak utkají ve finále o
celkové vítězství. Každý hráč se
dobrovolně a libovolně přihlásí do skupiny, za kterou by
chtěl hrát. Není možné v průběhu turnaje
přejít na druhou stranu. Hráč co si vybere dobro nebo
zlo, tak hraje za hrady určené k dané straně. Pokud bude
v jedné skupině
větší počet hráčů, jak ve druhé, tak se
automaticky přidělí naposledy přihlášení
hráči do skupiny druhé, aby byl stejný počet
zúčastněných hráčů na každé straně.<span
 style="font-weight: bold;"><br />
</span></small></big><big><small><span style="font-weight: bold;"><br />
Systém přidělování měst<br />
<br />
</span></small></big><big><small>Proti sobě budou vždycky stát
města v této podobě: <br />
&nbsp;1)Bašta vs. Kobka<br />
&nbsp;2)Věž vs. Peklo<br />
&nbsp;3)Hrad vs. Necropolis<br />
&nbsp;4)Tvrz vs. Pevnost<br />
<br />
Pokud se do turnaje dostanou strany dobro versus dobro, či zlo versus
zlo, tak dvojice hradů, budou vypadat následovně:<br />
&nbsp;1)Hrad vs. Bašta<br />
&nbsp;2)Věž vs. Tvrz<br />
&nbsp;3)Pevnost vs. Peklo<br />
&nbsp;4)Kobka vs. Necropolis<br />
<br />
Dvojice hráčů se před hrou domluví mezi sebou, jako
dvojici hradů si vyberou. Pokud se nebudou moci domluvit, tak
napíšou organizátorovi, nebo Lordu.Alexovi. Byl
tímto pověřen.<br />
&nbsp;Děkuji za pochopení a doufám, že každý z
ligy je schopen se dohodnout.<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small></big><big><small><span
 style="font-weight: bold;"><br />
</span></small></big><big><small><span style="font-weight: bold;">
</span></small></big><span style="font-weight: bold;">Turnajové
hry</span>
<p> Nastavení hry je následující: </p>
<ul>
  <li>mapa: náhodná L bez podzemí, množství
vody: žádné, síla bojovníků: vysoká</li>
  <li>časový limit: 4 minuty</li>
  <li>obtížnost: expert (160%)</li>
  <li>oba hráči začínají s přiděleným
městem a náhodným hrdinou</li>
</ul>
Hraje se bez diplomacie, bez grálu a monsters na artefakty,
pandořiny skříòky, budovy a svitky.<br />
Odehrané duely se ukládají do záznamů, jako
každá jiná hra. Na začátek komentáře k
turnajové hře napište *TURNAJ - DZ*. Odehrané
duely je
nutné nahlásit organizátorovi, aby mohli
být zapsány do pavouka.<br />
<span style="font-weight: bold;"><br />
Reklamace mapy</span><br />
<br />
<p>Hráč má nárok na reklamaci v případě: </p>
<ul>
  <li>má před městem silné jednotky, tím je mapa
nehratelná</li>
  <li>když hráči začínají s dvěma či více
městy, nebo jsou vesnice(chybí tvrz)</li>
  <li>když jsou města na špatném povrchu</li>
  <li>pokud u hradu chybí cesta</li>
</ul>
V případě sporu rozhodne o hratelnosti/nehratelnosti mapy
nestranná porota složená z organizátora nebo
jímž pověřených asistentů. S jakýmkoliv dotazem
piště mě, nebo Lordu.Alexovi.<br />
<br />
<h4>Kvalifikace do TURNAJE</h4>
Pokud bude více hráčů jak 16 v každé skupince, tak
se budou hrát kvalifikace. Pořadí soupeřů bude
záležet na pořadí ve skupinkách dobro vs. zlo.
Poslední s prvním atd. Pokud nebude vyrovnaný stav
na obou stranách musím chtít nechtít
naposledy přihlášené hráče z
kvalifikací přesunout na druhou stranu.<br />
<br />
<h4>Kontumace neaktivních hráčů</h4>
<p> Pokud se do konce termínu daného kola neozve ani
jeden z hráčů, kteří mají hrát turnajovou
hru, dostanou oba kontumační prohru. V případě že je
jeden hráč aktivní a jeho soupeř není k
sehnání, dostane kontumaci jen soupeř. V tom
případě musí být vidět snaha aktivního
hráče o sehnání soupeře. Aktivní
hráč by proto měl několik dní před termínem
upozornit organizátora nebo veřejnost v diskuzi. </p>
<h4>Nasazení náhradníků do turnaje</h4>
<p> Kdyby došlo k nezájmu některých
přihlášených hráčů a zájmu
nepřihlášených hráčů bude přidáno
toto speciální pravidlo:<br />
Poslední den prvního kola turnaje může
nepřihlášený hráč nahradit v turnaji
neaktivního přihlášeného hráče.<br />
Toto je možné provést za předpokladu, že: </p>
<ul>
  <li>hráč, který má být nahrazen
prokazatelně není schopen odehrát svůj zápas ve
stanoveném termínu</li>
  <li>volný soupeř neaktivního hráče a
příchozí náhradník se dokážou
dohodnout na zápase do konce termínu prvního kola
(po dohodě s organizátorem je možné udělat výjimku
a odehrát zápas o den později)</li>
</ul>
V tom případě bude náhradník zapsán do
turnaje jako plnohodnotný ůčastník na místo
vyřazeného neaktivního hráče.<br />
Pokud je náhradníků více než 1, tak
nasazováni bude chronologické dle přihlášek
do turnaje, samozřejmě roli bude hrát aktivnost a
dosavadní ELO.

<div><h1>Přihlášky</h1></div>


<img style="width: 400px; height: 286px;" alt=""
 src="files/logo_dvz2.jpg" />
<p> Přihlášky pište Nivenovi na ICQ: 232-123-291.<br />
Konec přihlášek je ???,
poté bude sestaven pavouk a od ???. se začne
hrát.<br />
Pro přihlášení do turnaje musíte být
registrovaní v lize a musíte mít odehráno
minimálně 15 ligových her.</p>
<table style="width: 654px; height: 336px;">
  <tbody>
    <tr>
      <td valign="top">
      <h3>Přihlášení hráči(Zlo)</h3>
      01: Niven<br />
      02: Siam<br />
      03: Chuck Norris<br />
      04: Nightwright<br /> 
      05: Amroth_CZ<br /> 
      06: Vítek<br /> 
      07: PetoPalo<br />
      08: Imposieble<br />
      09: Maras<br />
      10: Borosenior<br />
      11: Unreal<br />
      12: robi82<br />
      13: Lord.Alex<br />
      14: Pagy<br />
      15: Jelen<br />
      16: jukufa<br />
      </td>
      <td valign="top">
      <h3>Přihlášení hráči(Dobro)</h3>
      01: red joc<br />
      02: Gaborn<br />
      03: Antony<br />
      04: chichtofnuk<br />
      05: Macwar<br />
      06: Fuckinek<br />
      07: Pagi<br />
      08: Paolos<br />
      09: Spectral<br />
      10: Nethos<br />
      11: SD<br />
      12: Demon<br />
      13: Kotletka<br />
      14: Gigiman<br />
      15: Kacer<br />
      16: MiraQ<br />
      </td>
   <td valign="top">
      <h3>Seřazeno podle ELO(Zlo)</h3>
      01: Imposieble(2275)<br />
      02: Maras(2211)<br />
      03: Vítek(2125)<br /> 
      04: Nightwright(2068)<br />
      05: Chuck Norris(2029)<br />
      06: PetoPalo(2002)<br />
      07: Niven(1992)<br />
      08: Lord.Alex(1992)<br />
      09: Unreal(1952)<br />
      10: Amroth_CZ(1885)<br /> 
      11: Siam(1691)<br />
      12: robi82(1688)<br />
      13: Pagy(1652)<br />
      14: Borosenior(1601)<br />
      15: Jelen(1565)<br />
      16: jukufa(1377)<br />
      </td>
  <td valign="top">
      <h3>Seřazeno podle ELO(Dobro)</h3>
      01: chichtofnuk(2038)<br />
      02: Pagi(2029)<br />
      03: Gaborn(2024)<br />
      04: Macwar(1983)<br />
      05: Gigiman(1928)<br />
      06: SD(1916)<br />
      07: Nethos(1908)<br />
      08: Kacer(1848)<br />
      09: Antony(1719)<br />
      10: Demon(1603)<br />
      11: red joc(1601)<br />
      12: Paolos(1581)<br />
      13: Kotletka(1507)<br />
      14: Spectral(1504)<br />
      15: MiraQ(1482)<br />
      15: Fuckinek(1385)<br />
      </td>
    </tr>
  </tbody>
</table>
<br />
Náhradníci: 0<br />
Přesunutí hráči: 0<br />
Vyřazení hráči: 0<br />


<div><h1>Průběh</h1></div>

Zde se můžete podívat na systém, který byl použit
pro tento turnaj: <a href="http://heroes3.cz/turnaje/pavouk.php">pavouk</a><br />
<br />
<span style="font-weight: bold;">O 3.tí místo</span><br />
<span style="font-weight: bold;"><br />
</span>
<table border="1">
  <tbody>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Imposieble
(zlo)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Imposieble
(zlo)<br />
      <span style="font-style: italic; color: rgb(255, 102, 0);">kontumační
výhra<br />
      <img title="třetí místo" style="width: 40px; height: 44px;"
 alt="třetí místo" src="3rd_tourney.jpg" /></span><span
 style="color: rgb(255, 102, 0);"> </span></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Gaborn
(dobro) </td>
    </tr>
  </tbody>
</table>
<br />
<span style="font-weight: bold;">Absolutní vítěz TURNAJE</span><br />
<br />
<table border="1">
  <tbody>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Niven (zlo) </td>
      <td rowspan="2" style="width: 100pt;" align="center">Niven (zlo)<br />
2:1<br />
      <img title="první místo" style="width: 40px; height: 44px;"
 alt="první místo" src="1st_tourney.jpg" /><br />
      <a href="http://www.heroes3.cz/hry/?detail=8712&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8712)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8773&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8773)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8791&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8791)</span></a><br />
      </td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Unreal (zlo)</td>
    </tr>
  </tbody>
</table>
<br />
<span style="font-weight: bold;">Hlavní pavouk</span><br />
<br />


<br />
<span style="font-weight: bold;">1. strana</span><br />
<br />
<table border="1">
  <tbody>
    <tr>
      <th style="color: rgb(255, 112, 0);">1. kolo</th>
      <th style="color: rgb(255, 112, 0);">2. kolo</th>
      <th style="color: rgb(255, 112, 0);">čvtrtfinále</th>
      <th style="color: rgb(255, 112, 0);">Semifinále</th>
      <th style="color: rgb(255, 112, 0);">Finále - první
vítěz</th>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">&nbsp;Imposieble
(zlo)</td>
      <td rowspan="2" style="width: 100pt;" align="center">&nbsp;Imposieble
(zlo)<br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8133&amp;page=2"><span
 style="color: rgb(255, 102, 0);">(8133)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8253&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8253)</span></a></td>
      <td rowspan="4" style="width: 100pt;" align="center">Imposieble
(zlo)<br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8280&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8280)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8282&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8282)</span></a></td>
      <td rowspan="8" style="width: 100pt;" align="center">Imposieble
(zlo)<br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8394&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8394)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8447&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8447)</span></a></td>
      <td rowspan="16" style="width: 100pt;" align="center">Niven (zlo)<br />
2:1<br />
      <a href="http://www.heroes3.cz/hry/?detail=8502&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8502)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8503&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8503)</span></a><br />
      <span style="font-style: italic; color: rgb(255, 102, 0);">kontumační
výhra</span></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">&nbsp;Fuckinek
(dobro)</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Lord.Alex
(zlo)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Antony
(dobro)<br />
      <span style="font-style: italic; color: rgb(255, 102, 0);">kontumační
výhra</span></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Antony
(dobro)</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Nightwright
(zlo)<br />
      </td>
      <td rowspan="2" style="width: 100pt;" align="center">Kotletka
(dobro)<br />
      <span style="font-style: italic; color: rgb(255, 102, 0);">kontumační
výhra</span></td>
      <td rowspan="4" style="width: 100pt;" align="center">Chuck Norris
(zlo)<br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8230&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8230)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8259&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8259)</span></a></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Kotletka
(dobro)</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Chuck Norris
(zlo)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Chuck Norris
(zlo)<br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8176&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8176)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8180&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8180)</span></a></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Paolos
(dobro)</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">PetoPalo
(zlo)</td>
      <td rowspan="2" style="width: 100pt;" align="center">red joc
(dobro)<br />
2:1<br />
      <a href="http://www.heroes3.cz/hry/?detail=8177&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8177)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8191&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8191)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8223&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8223)</span></a></td>
      <td rowspan="4" style="width: 100pt;" align="center">Vítek
(zlo)<br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8250&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8250)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8261&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8261)</span></a></td>
      <td rowspan="8" style="width: 100pt;" align="center">Niven (zlo)<br />
2:1<br />
      <a href="http://www.heroes3.cz/hry/?detail=8290&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8290)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8366&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8366)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8444&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8444)</span></a></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">red joc
(dobro)</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Vítek
(zlo)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Vítek
(zlo)<br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8157&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8157)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8158&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8158)</span></a><br />
      </td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Spectral
(dobro)</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Niven (zlo)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Niven (zlo)<br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8202&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8202)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8203&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8203)</span></a></td>
      <td rowspan="4" style="width: 100pt;" align="center">Niven (zlo)<br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8235&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8235)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8266&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8266)</span></a></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Demon (dobro)</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">&nbsp;Maras
(zlo)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Maras
(zlo)<br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8153&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8153)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8166&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8166)</span></a></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">MiraQ (dobro)</td>
    </tr>
  </tbody>
</table>
<br />
<span style="font-weight: bold;">2. strana</span><br />
<br />
<table border="1">
  <tbody>
    <tr>
      <th style="color: rgb(255, 112, 0);">1. kolo</th>
      <th style="color: rgb(255, 112, 0);">2. kolo</th>
      <th style="color: rgb(255, 112, 0);">čvtrtfinále</th>
      <th style="color: rgb(255, 112, 0);">Semifinále</th>
      <th style="color: rgb(255, 112, 0);">Finále - druhý
vítěz</th>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">chichtofnuk
(dobro)</td>
      <td rowspan="2" style="width: 100pt;" align="center">chichtofnuk
(dobro)<br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8167&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8167)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8170&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8170)</span></a></td>
      <td rowspan="4" style="width: 100pt;" align="center">Unreal (zlo)<br />
2:1<br />
      <a href="http://www.heroes3.cz/hry/?detail=8279&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8279)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8317&amp;page=2"><span
 style="color: rgb(255, 102, 0);">(8317)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8325&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8325)</span></a></td>
      <td rowspan="8" style="width: 100pt;" align="center">Unreal (zlo)<br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8457&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8457)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8555&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8555)</span></a></td>
      <td rowspan="16" style="width: 100pt;" align="center">Unreal (zlo)<br />
2:0<br />
      <img title="druhé místo" style="width: 40px; height: 44px;"
 alt="druhé místo" src="2nd_tourney.jpg" /><br />
      <a href="http://www.heroes3.cz/hry/?detail=8570&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8570)</span></a><br />
      <span style="font-style: italic; color: rgb(255, 102, 0);">kontumační
výhra</span></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">&nbsp;jukufa
(zlo)</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Kacer (dobro)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Unreal (zlo)<br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8151&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8151)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8254&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8254)</span></a></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Unreal (zlo)</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Macwar
(dobro)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Macwar
(dobro) <br />
&nbsp;2:1<br />
      <a href="http://www.heroes3.cz/hry/?detail=8175&amp;page=5"><span
 style="color: rgb(255, 102, 0);">(8175)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8184&amp;page=2"><span
 style="color: rgb(255, 102, 0);">(8184)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8185&amp;page=2"><span
 style="color: rgb(255, 102, 0);">(8185)</span></a></td>
      <td rowspan="4" style="width: 100pt;" align="center">Macwar
(dobro) <br />
&nbsp;2:1<br />
      <a href="http://www.heroes3.cz/hry/?detail=8291&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8291)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8293&amp;page=3"><span
 style="color: rgb(255, 102, 0);">(8293)</span></a><br />
      <span style="font-style: italic; color: rgb(255, 102, 0);">kontumační
výhra</span></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Pagy (zlo)</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Gigiman
(dobro)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Gigiman
(dobro)<br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8214&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8214)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8269&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8269)</span></a></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">robi82 (zlo)</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">SD (dobro)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Siam (zlo) <br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8122&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8122)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8127&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8127)</span></a></td>
      <td rowspan="4" style="width: 100pt;" align="center">Gaborn
(dobro) <br />
2:1<br />
      <a href="http://www.heroes3.cz/hry/?detail=8211&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8211)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8234&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8234)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8237&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8237)</span></a></td>
      <td rowspan="8" style="width: 100pt;" align="center">Gaborn
(dobro) <br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8242&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8242)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8251&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8251)</span></a></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Siam (zlo)</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Gaborn
(dobro)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Gaborn
(dobro) <br />
2:0<br />
      <a href="http://www.heroes3.cz/hry/?detail=8188&amp;page=2"><span
 style="color: rgb(255, 102, 0);">(8188)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8195&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8195)</span></a></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Borosenior
(zlo)</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Nethos
(dobro)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Amroth_CZ
(zlo)<br />
&nbsp;2:1<br />
      <a href="http://www.heroes3.cz/hry/?detail=8121&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8121)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8150&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8150)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8159&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8159)</span></a></td>
      <td rowspan="4" style="width: 100pt;" align="center">Amroth_CZ
(zlo)<br />
&nbsp;2:1<br />
      <a href="http://www.heroes3.cz/hry/?detail=8181&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8181)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8187&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8187)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8189&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8189)</span></a></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Amroth_CZ
(zlo)</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">&nbsp;Pagi
(dobro)</td>
      <td rowspan="2" style="width: 100pt;" align="center">Jelen (zlo) <br />
2:1<br />
      <a href="http://www.heroes3.cz/hry/?detail=8139&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8139)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8142&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8142)</span></a><br />
      <a href="http://www.heroes3.cz/hry/?detail=8144&amp;page=1"><span
 style="color: rgb(255, 102, 0);">(8144)</span></a></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Jelen (zlo)</td>
    </tr>
  </tbody>
</table>
<br />


</div>





<?php

ramecek_end(1024);



html_end();

?>
