<?php


require('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

ramecek_start(1024, 'First Vítek Tournament');
echo "<div>&nbsp;</div>";
echo "<div style=\"margin-left: 10px; margin-right: 10px;\">";

echo "
				<img border=\"0\" src=\"vitek_pic_2.gif\" width=\"580\" height=\"381\">

	<h1>Pravidla</h1>

     <h4><span style=\"font-weight: 400\">Snažil jsem se nastavit férová pravidla, 
		někdy můžeš hrát jako třeba Maretti, to myslím s velkou nadsázkou :-) 
		ale pokud někdo najde svitek létání a vletí ti přes horu s armou do 
		prázdného hradu druhý týden tak jsi pak ze hry znechucen.&nbsp; Pokud 
		najde někdo armagedon a dálá ti pořád Hit and run , do té doby dokud hru 
		dobrovolně nevzdáš, tak tě hra taky moc netěší a podobně ....</span></h4>
		<h4>Přihlasování do turnaje:</h4>
      <p>
         V případě zájmu účastnit se turnaje mě stačí kontaktovat na icq: 311-438-390, Registrace je možná do 
			30.11.2013.
      </p>
		<p>
         V případě brzkého velkého zájmu o turnaj bude termín registrace zkrácen.</p>
		<p>
         Každý hráč musí mít ve svém profilu kontakt, na kterém bude v období turnaje dostupný, nejlépe ICQ, GR nebo Skype.</p> 
      
      
      <h4>Začátek turnaje </h4>
      <p>   
          Začátek kvalifikace: datum bude doplněn.</p>
		<p>   
          Začátek turnaje:&nbsp; datum bude doplněn.</p>  
      
                                
      <h4>Systém turnaje:</h4>        
      <p style=\"margin-bottom: -0px\">
        Prvních 8 přihlášených hráčů seřazených podle ELO automaticky postupuje 
		do Hlavního pavouku.Ostatní hráči budou bojovat o umístění v hlavním 
		pavouku v kvalifikaci. </p>
		<p style=\"margin-bottom: -0px\">
        Kvalifikační duely budou sestaveny podle přihlášených hráčů.</p>
		<p style=\"margin-bottom: -0px\">
        Systém turnaje je pavouk s druhou šancí. Hráč, který prohraje v hlavním 
		pavouku sestupuje do záchranného pavouka - dostává tedy v turnaji druhou 
		šanci.</br>
        Prohra v záchranném pavouku znamená konec hráče v turnaji.<br />
       
      </p>
               
    
    <h4>Turnajové hry</h4>
    
    <p>

    <b>Nastavení hry je následující: </b>
    <ul>
     <li>mapa: random L bez podzemí, voda žádná, neutrálové strong</li>        
     <li>časový limit: 4 minuty, pokud se hráči nedohodnou jinak(tzn. v případě, že se oba hráči zhodnou na jinem limitu napr. 2  minuty, můžou svou hru odehrat s tímto limitem. V případě neshody obou stran na jiném limitu platí 4 minutový limit.)</li>
     <li>obtížnost: expert (160%)</li>
     <li>Startovací město si volí každý hráč sám. Pokud hráč zvolí jeden typ 
		startovacího města například &quot;Bašta&quot;, nesmí v následujících dvou 
		turnajových hrách opakovaně stejné město zvolit. To znamená, že může 
		volit baštu až následnou 3 turnajovou hru. Hrdinové i bonusy jsou nastaveny jako náhodné. Města Necropolis a Conflux jsou 
		v turnaji zakázány. V případě, že je získáte ve hře, můžete je využívat bez omezení.</li>
     <li>slabší hráč podle ELO hodnocení má taky právo zvolit si barvu.</li>
		<li>v případě, že budou chtít oba hráči použít stejné starovací město, 
		má slabší hráč podle ELO právo vyzvat soupeře, aby volil jiné startovací 
		město. V takovém případě může hráč volit jakékoliv město kromě Nekra a 
		Confluxu.</li>
    </ul><b>Omezení hry</b> :&nbsp;
      
    	<ul>
			<li>pravidlo monsters, které platí pouze pro objekty (artefakty, budovy, 
			svitky...) Monsters neplatí na cesty.&nbsp; </li>
			<li>diplomacie a grál jsou zakázány ! </li>
			<li>když soupeř najde druhý stejný hrad, tak v něm nesmí kupovat 
			budovy na jednotky 3 level a výše. (pouze jednotky po druhý level)</li>
			<li>zákaz hit and run !!!&nbsp;Pokud útočící hráč použije na soupeře 
			útočné kouzlo 4 a vyššího levelu, nesmí po dobu dvou odehraných kol 
			z boje utéct, nebo se vzdát. </li>
			<li>Kouzla <b>DD a fly</b> <b>ze svitku </b>použít nesmíte. Pokud 
			získáte kouzla z věží kouzel, nebo pandor, nebo díky artefaktům co 
			jste poctivě získaly, tak je můžete používat bez omezení. </li>
			<li>Kouzla <b>imploze a armagedon</b> jsou <b>ze svitku zakázány.</b> 
			Smíte je použít pouze na neutráli. Pokud získáte kouzla z věží 
			kouzel, nebo pandor, nebo díky artefaktům co jste poctivě získaly, 
			tak je rovněž můžete používat bez omezení. </li>
		</ul>
		<p>Odehrané duely se ukládají do záznamů, jako každá jiná hra. Na začátek komentáře k turnajové hře napište *TURNAJ-VITEK*.
		<br/>Odehrané duely je nutné nahlásit organizátorovi pomocí&nbsp; ICQ (Vitek), abych je zapsal do pavouka.<br/>
		</br>Pokud to bude možné, snažte se odehrát hru nepřetržite, tzn. bez save-ů.</br>
</br>

      </p>

      <h4>Reklamace mapy</h4>
    <p>
      Hráč má nárok na novou hru pouze v případě, že je mapa naprosto nehratelná, (tzn. například cestu k hradu blokuje 
		tlupa Draků :-))
      určitě nestačí na reklamaci nedostatek dolů.
      <br/>
      <br/>
      Dále je možné restartovat hru v případě, že v městech chybí tvrz, hráčí mají více měst, chybí cesta, města jsou na špatném povrchu apod.
      <br/>
      
      V případě sporu rozhodne o hratelnosti/nehratelnosti mapy nestranná porota 
		z ligy na které se oba hráči dohodnou. Hráč, který spor rozhodne by měl 
		mít odehráno více než 50 her na lize.
      </br> 
      Pro porovnávání hráčů v sestavení pavouka je rozhodující ELO, s jakým jste se do turnaje zapsali.
    </p>
    
      <h4>Kontumace neaktivních hráčů</h4>
      
    <p>
    
    Pokud se do konce termínu daného kola neozve ani jeden z hráčů, kteří mají hrát turnajovou hru, dostanou oba kontumační prohru.
    
    V případě že je jeden hráč aktivní a jeho soupeř není k sehnání, dostane kontumaci jen soupeř. V tom případě musí být vidět
    snaha aktivního hráče o sehnání soupeře. Aktivní hráč by proto měl několik dní před termínem upozornit organizátora 
	i veřejnost v diskuzi.
    
    </p>  
    
    <h4>Nasazení náhradníků do turnaje</h4>
    <p>
    
    
    <p>
     V případě, že 2 dny před koncem daného kola se daná hrací dvojice stále nedohodla na termínu odehrání hry z důvodu nedosažitelnosti jednoho z hračů,</br>
     bude do turnaje dosazen jeden z náhradníků a to v pořadí, ve kterém se do turnaje přihlásili. 
		Nasazení náhradníků je možné jen v prvních dvou kolech&nbsp;hlavního 
		pavouka a max. do 3 kola záchranného pavouka.</br>
          
    </p>
		<p>
     <b>Všem přeji hodně štěstí v novém turnaji a snažte se hrát fér, hrajete 
		přece pro zábavu :-)</b></p>




	<h1>Přihlášky</h1>

 

		<p>
      V průběhu několika posledních dní se do turnaje nikdo nepřihlásil, tak 
		nebudu začátek turnaje zbytečně protahovat :-)</p>
		<p style=\"margin-top: 2px\">Nyní můžete začít hrát jednotlivé zápasy dle rozpisu v pavouku.</p>
      
      <table style=\"border-spacing: 20pt;\"><tr>
      <td valign=\"top\" width=\"187\">
      <h3>Přihlášení hráči</h3>
      
      <p style=\"margin-top: 0px\">
        1.&nbsp;&nbsp; Vítek (2178)<p style=\"margin-top: 0px\">
        2.&nbsp;&nbsp; Siam (2105)<p style=\"margin-top: 0px\">
        3.&nbsp;&nbsp; Kisane (1963)<p style=\"margin-top: 0px\">
        4.&nbsp;&nbsp; Dobyvatel (1731)<p style=\"margin-top: 0px\">
        5.&nbsp;&nbsp; Lord Sookar (1786)<p style=\"margin-top: 0px\">
        6.&nbsp;&nbsp; Kotletka (1453)<p style=\"margin-top: 0px\">
        7.&nbsp;&nbsp; Macwar (1874)<p style=\"margin-top: 0px\">
        8.&nbsp;&nbsp; Houhou (1830)<p style=\"margin-top: 0px\">
        9.&nbsp;&nbsp; Chichtofnuk (2141)<p style=\"margin-top: 0px\">
        10. DavidHakl (1992&nbsp;)<p style=\"margin-top: 0px\">
        11. Pagi (2154)<p style=\"margin-top: 0px\">
        12. Red Joc (1651)<p style=\"margin-top: 0px\">
        
        
        13. Petopalo(2061)<p style=\"margin-top: 0px\">
        14. Peterson (1784)<p style=\"margin-top: 0px\">
        15. Twister (1863)<p style=\"margin-top: 0px\">
        16. Pagy (1702)</td>         
        
        
          <td valign=\"top\">
      <h3>Seřazeno podle ELO</h3>
      
      <p style=\"margin-top: 0px\">1. Vítek (2178)<p style=\"margin-top: 0px\">2. Pagi (2154)<p style=\"margin-top: 0px\">3. Chichtofnuk (2141)<p style=\"margin-top: 0px\">
		4. Siam (2105)<p style=\"margin-top: 0px\">5. Petopalo(2061)<p style=\"margin-top: 0px\">
		6. DavidHakl&nbsp; (1992&nbsp;)<p style=\"margin-top: 0px\">7. Kisane (1963)<p style=\"margin-top: 0px\">
		8. Macwar (1874)<p style=\"margin-top: 0px\">9. Twister (1863)<p style=\"margin-top: 0px\">
		10. Houhou (1830)<p style=\"margin-top: 0px\">11. Lord Sookar (1786)<p style=\"margin-top: 0px\">
		12. Peterson (1784)<p style=\"margin-top: 0px\">13. Dobyvatel (1731)<p style=\"margin-top: 0px\">
		14. Pagy (1702)<p style=\"margin-top: 0px\">15. Red Joc (1651)<p style=\"margin-top: 0px\">
		16. Kotletka (1453)</td>        
      
      
        
        
        
        
        
        
      </tr></table>

    
      <p style=\"margin-top: 0px\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Náhradníci :
		</p>
		<p style=\"margin-top: -15px\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 17. Michal 
		Hruša</p>
		<p style=\"margin-top: -15px\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 18. Karcma







	<h1>Kvalifikace</h1>

<h3>
        </br>
        <span style=\"font-weight: 400\">Kvalifikace je boj o umístnení v hlavním pavoukovi.</span></h3>
		<h3><span style=\"font-weight: 400\">Kvalifikační pavouk bude sestaven dle 
		počtu přihlášených hráčů.</span></br>
        <br>
		</h3>
<table style=\"text-align: left; width: 507px; height: 256px;\" border=\"1\"
 cellpadding=\"2\" cellspacing=\"2\">
  <tbody>
    <tr>
      <td
 style=\"font-weight: bold; color: rgb(255, 102, 0); text-align: center;\">#</td>
      <td
 style=\"font-weight: bold; color: rgb(255, 102, 0); text-align: center;\">ELO-silnější</td>
      <td
 style=\"font-weight: bold; color: rgb(255, 102, 0); text-align: center;\">ELO-slabší</td>
      <td
 style=\"font-weight: bold; color: rgb(255, 102, 0); text-align: center;\">Win(W)/Def(D)</td>
    </tr>
    <tr>
      <td
 style=\"color: rgb(255, 102, 0); font-weight: bold; text-align: center;\">1.</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
    </tr>
    <tr>
      <td
 style=\"color: rgb(255, 102, 0); font-weight: bold; text-align: center;\">2.</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
    </tr>
    <tr>
      <td
 style=\"color: rgb(255, 102, 0); font-weight: bold; text-align: center;\">3.</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
    </tr>
    <tr>
      <td
 style=\"color: rgb(255, 102, 0); font-weight: bold; text-align: center;\">4.</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
    </tr>
    <tr>
      <td
 style=\"color: rgb(255, 102, 0); font-weight: bold; text-align: center;\">5.</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
    </tr>
    <tr>
      <td
 style=\"color: rgb(255, 102, 0); font-weight: bold; text-align: center;\">6.</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
    </tr>         
    <tr>
      <td
 style=\"color: rgb(255, 102, 0); font-weight: bold; text-align: center;\">7.</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
    </tr>
    <tr>
          <td
 style=\"color: rgb(255, 102, 0); font-weight: bold; text-align: center;\">8.</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
      <td style=\"text-align: center;\">&nbsp;</td>
    </tr>
  </tbody>
</table>
<br>
<span style=\"font-weight: bold;\">
    
    
     <h3>Náhradníci v případě odstoupení/kontumace:</h3>
     </span>


	<h1>Průběh</h1>

<img border=\"0\" src=\"turnaj.jpg\" width=\"803\" height=\"500\">
<br/><br/>
Finále obou pavouků
<br/><br/>
<table border=\"1\">
  <tbody>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Siam</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">
		<a href=\"http://www.heroes3.cz/hry/?detail=12731&page=3\">Siam</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Chichtofnuk</td>
    </tr>
  </tbody>
</table>
<br/><br/>
HLAVNÍ PAVOUK - Poražený sestupuje do záchranného pavouka s druhou šancí.
<br/><br/>
<table border=\"1\">
  <tbody>
    <tr>
      <th style=\"color: rgb(255, 112, 0);\">1. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">2. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">čvtrtfin&aacute;le</th>
      <th style=\"color: rgb(255, 112, 0);\">Semifin&aacute;le</th>
      <th style=\"color: rgb(255, 112, 0);\">Fin&aacute;le - prvn&iacute;
v&iacute;těz</th>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Vítek</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Vítek<br>
		Kotletka&nbsp; vzdala<br>
      </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Vítek<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12317&page=1\">12317</a><br>
      </td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\">Siam<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12374&page=1\">12374</a><br>
<br>
      </td>
      <td rowspan=\"16\" style=\"width: 100pt;\" align=\"center\">Siam<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12419&page=1\">12419</a><br>
<br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Kotletka </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Dobyvatel </td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Dobyvatel <br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12313&page=1\">12313</a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Macwar</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Petopalo</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Petopalo<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12284&page=1\">12284</a><br>
  </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Siam<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12329&page=1\">12329</a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Houhou</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Peterson </td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Siam<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12280&page=1\">12280</a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Siam</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Chichtofnuk </td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Chichtofnuk <br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12290&page=1\">12290</a><br>
      </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Kisane<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12292&page=1\">12292</a><br>
      </td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\">Kisane<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12345&page=1\">12345</a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Pagy</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Twister</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Kisane<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12256&page=1\">12256</a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Kisane</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">DavidHakl </td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">DavidHakl 
<br>
      <a href=\"http://www.heroes3.cz/hry/?detail=12296&page=1\">12296</a> 
<br>
      &nbsp;</td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">DavidHakl<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12318&page=1\">12318</a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Lord Sookar</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Red Joc </td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Pagi<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12299&page=1\">12299</a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Pagi</td>
    </tr>
  </tbody>
</table>
<br/><br/>
 Záchranný pavouk - Poražený vypadává z turnaje


<br/><br/>
<table border=\"1\">
  <tbody>
    <tr>
      <th style=\"color: rgb(255, 112, 0);\">1. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">2. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">čvtrtfinále(1)</th>
      <th style=\"color: rgb(255, 112, 0);\">čvtrtfinále(2)</th>
      <th style=\"color: rgb(255, 112, 0);\">Semifinále(1) </th>
      <th style=\"color: rgb(255, 112, 0);\">Semifinále(2) </th>
      <th style=\"color: rgb(255, 112, 0);\">Fin&aacute;le - druhý </br>vítěz</th>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Karcma </td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Karcma <br>
		Macwar&nbsp; vzdal</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Karcma <br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12401&page=1\">12401</a> </td>
      
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Vítek</td>
      
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">
		Vítek <br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12572&page=1\">12572</a></td>
       <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Chichtofnuk<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12699&page=1\">12699</a></td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\">Chichtofnuk<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12711&page=3\">12711</a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Macwar</td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Dobyvatel </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Houhou</td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Peterson <br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12336&page=1\">12336</a> </td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Petopalo <br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12365&page=2\">12365</a> </td>
  <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Petopalo <br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12415&page=1\">12415</a></tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Peterson </td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Petopalo</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Pagy</td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Twister<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12383&page=1\">12383</a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Chichtofnuk <br>
		Kontumační&nbsp; prohra</td>
    <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Chichtofnuk<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12611&page=1\">12611</a></td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Chichtofnuk<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12655&page=1\">12655</a></td>
<td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Kisane</td>
      
      <span
 style=\"color: rgb(255, 102, 0);\"></span></td>
    </tr>
    
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Twister</td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Chichtofnuk </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Lord Sookar</td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Red Joc<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12353&page=1\">12353</a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Pagi<br>
		<a href=\"http://www.heroes3.cz/hry/?detail=12378&page=1\">12378</a></td>
        <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">DavidHakl</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Red Joc</td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Pagi</td>
    </tr>
  </tbody>
</table>



";

  

echo "</div>";
echo "<div>&nbsp;</div>";
ramecek_end(1024);



html_end();

?>
