<?php


require('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

ramecek_start(1024, 'Imp\'s Random Tournament');

echo "<div style=\"margin-left: 10px;\">";
             
echo "

&nbsp;
<img style=\"width: 800px; height: 572px;\" alt=\"\" src=\"imp_tournament.jpg\" />
     <h4>Přihlasování do turnaje</h4>
      <p>
         V případě zájmu účastnit se turnaje mně stačí kontaktovat na icq: 402-045-016, nebo napsat žádost v diskuzi. Registrace je možná do 28.2.2013.
      </p>
      </br>
      *Každý hráč musí mít v profilu kontakt, na kterém bude v období turnaje dostupný, nejlépe ICQ, GR nebo Skype. Před začátkem turnaje to bude zkontrolováno. 
      
      
      <h4>Začátek turnaje</h4>
      <p>   
          1.3.-7.3. Proběhne kvalifikace do turnaje.</br>
          8.3. Turnaj oficiálně začíná.</br>
          </br>
          </br>
          * Seznam účastníků pro kvalifikaci bude zveřejněn k 1.3.2013.
          
          
      </p>  
      
                                
      <h4>Systém</h4>        
      <p>
        Systém turnaje je pavouk s druhou šancí. Pavouk byl upraven dle počtu přihlášených hráčů.
        </br>
        Zde se můžete podívat na systém, který byl použit
pro tento turnaj: <a href=\"pavouk.php\">pavouk</a><br />
       
      </p>
               
    
    <h4>Turnajové hry</h4>
    
    <p>

    Nastavení hry je následující:
    <ul>
     <li>mapa: random L bez podzemí, voda žádná, neutrálové strong</li>        
     <li>časový limit: 4 minuty, pokud se hráči nedohodnou jinak(tzn. v případě, že se oba hráči zhodnou na jinem limitu napr. 2  minuty, můžou svou hru odehrat s tímto limitem. V případě nezhody obou stran na jiném limitu platí 4 minutový limit.)</li>
     <li>obtížnost: expert (180%)</li>
     <li>města, hrdinové i bonusy jsou nastaveny jako náhodné, slabší hráč podle ELO hodnocení má však právo zvolit si startovní město dle svého výběru. Města Necropolis a Conflux jsou jako přimární města zakázány. V případě, že je získáte ve hře, můžete je využívat bez omezení</li>
     <li>slabší hráč podle ELO hodnocení má taky právo zvolit si farbu</li>
    </ul>

    Jediné omezení je pravidlo monsters, které platí pouze pro objekty(artefakty, budovy,...), diplomacie a grál jsou povoleny.
    <br/>
    Odehrané duely se ukládají do záznamů, jako každá jiná hra. Na začátek komentáře k turnajové hře napište *TURNAJ-IMP*.
    <br/>
    Odehrané duely je nutné nahlásit organizátorovi(Imposieble), abych je zapsal do pavouka.<br/>
    </br>
      Pokud to bude možné, snažte se odehrát hru nepřetržite, tzn. bez save-ů.</br>
      
    </p>
    
    <h4> Finále</h4>
    
 
    Finalista z hlavního pavouka vlastně postoupí bez jediný prohry, čili jako protiváha bude, že v úplnym finále si \"slabší\" souper nevolí město. 
Dále má v celkovém finále nárok na jeden restart v 1ním dnu.
</br>

      <h4>Reklamace mapy</h4>
    <p>
      Hráč má nárok na novou hru v pouze případě, že je mapa naprosto nehratelná, (tzn. například cestu k hradu blokuje parta titánů)
      určitě nestačí absence kamenolomu.
      <br/>
      <br/>
      Kromě toho je možné restartovat hru v případě, že v městech chybí tvrz, hráčí mají více měst, chybí cesta, města jsou na špatném povrchu apod.
      <br/>
      
      V případě sporu rozhodne o hratelnosti/nehratelnosti mapy nestranná porota složená z organizátora(Imp) a Kekela. Můžete taky oslovit libovolnou osobu, na které se zhodnete.
      </br> 
      Pro porovnávání hráčů při výběre města je rozhodující ELO, s jakým jste se do turnaje zapsali
    </p>
    
      <h4>Kontumace neaktivních hráčů</h4>
      
    <p>
    
    Pokud se do konce termínu daného kola neozve ani jeden z hráčů, kteří mají hrát turnajovou hru, dostanou oba kontumační prohru.
    
    V případě že je jeden hráč aktivní a jeho soupeř není k sehnání, dostane kontumaci jen soupeř. V tom případě musí být vidět
    snaha aktivního hráče o sehnání soupeře. Aktivní hráč by proto měl několik dní před termínem upozornit organizátora nebo veřejnost v diskuzi.
    
    </p>  
    
    <h4>Nasazení náhradníků do turnaje</h4>
    <p>
    
    
    <p>
     V případě, že 2 dny před koncem daného kola se daná hrací dvojice stále nedohodla na termínu odehrání hry z důvodu nedosažitelnosti jednoho z hračů,</br>
     bude do turnaje dosazen jeden z náhradníků a to v pořadí, ve kterém se do turnaje přihlásili.
     </br>
     Nasazení náhradníků je dále možné jen v prvních dvou kolech(vrátaně kvalifikačního), v dalších kolech pak už bude udělena kontumační prohra.
          
    </p>


     <p>
      Registrace hráčů je možná do 28.2.2013.
      </p>";
?>

     <table style=\"border-spacing: 20pt;\"><tr>
      <td valign=\"top\">
      <h3>Přihlášení hráči</h3>
      
      <p>
        1. Imposieble(2384)</br>
        2. Siam(1939)</br>
        3. Niven(1972)</br>
        4. ChuckNorris(2079)</br>
        5. Pagi(2194)</br>
        6. Owl(2012)</br>
        7. Macwar(1863)</br>
        8. <s>Nightwright</s>(2342)<a href="http://heroes3.cz/hry/?detail=10977">(10977)</a>     </br>
        9. Demon(1802)</br>
        10. Petopalo(2090)</br>
        11. Kisane(1763)</br>
        12. Pagy(1664)</br>
        13. Vitek(2155)</br>
        14. Karcma(1730)</br>
        15. Chichtofnuk(2123)</br>
        16. DavidHakl(1907)</br>
        17. MiraQ(1899)</br>
        18. Gigiman(1898)</br>
        19. Hroso(0)</br>
        20. Marge(0)</br>
        21. SD(1976)</br>
        22. Paolos(1703)</br>
        23. Kotletka(1459)</br>
        24. 103 pes(1768)</br>
        25. Liso1(0)</br>
        26. jjauda(0)</br>
        27. Antony(2081)</br>
        
        
        </td>         
        
        
          <td valign=\"top\">
      <h3>Seřazeno podle ELO</h3>
      
      1: Imposieble(2384)</br>
      2: Pagi(2194)</br>
      3: Vitek(2155)</br>
      4: Chichtofnuk(2123)</br>
      5: Petopalo(2090)</br>
      6: Antony(2081)</br>
      7: ChuckNorris(2079)</br>
      8: Owl(2012)</br>
      <hr>
      9: SD(1976)</br>
      10: Niven(1972)</br>
      11: Siam(1939)</br>
      12: DavidHakl(1907)</br>
      13: MiraQ(1899)</br>
      14: Gigiman(1898)</br>
      15: Macwar(1863)</br>
      16: Demon(1802)</br>
      <hr>
      17: 103 pes(1768)</br>
      18: Kisane(1763)</br>
      19: Karcma(1730)</br>
      20: Paolos(1703)</br>
      21: Pagy(1664)</br>
      22: Kotletka(1459)</br>
      23: Hroso(0)</br>
      24: Marge(0)</br>
      <hr>
      25: Liso1(0)</br> 
      26: jjauda(0)</br>
      </td>        
      
      
        
        
        
        
        
        
        <br/>
      
      </p>
      
      
      </td>

   
   
      <td valign=\"top\">
      <h5></h5>
                 
      </td>   
      
      <td valign=\"top\">
      <h5></h5>
      
                    
      </td>       
      
      
      
      </tr></table>



     <span style="font-weight: bold;">ABSOLUTNÍ VÍTěZ TURNAJE</span><br />
<br />
<table border="1">
  <tbody>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Imposieble </td>
      <td rowspan="2" style="width: 100pt;" align="center">Imposieble
<br />
    
      <a href="http://www.heroes3.cz/hry/?detail=11333&page=1"><span
 style="color: rgb(255, 102, 0);">(11333)</span></a><br />
     
      </td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">DavidHakl</td>
    </tr>
  </tbody>
</table>
</BR>
</BR>


       <span style="font-weight: bold;"> HLAVNÍ PAVOUK - Poražený sestupuje do pavouvka s druhou šancí</span><br>
</br>

<table border="1">
  <tbody>
    <tr>
      <th style="color: rgb(255, 112, 0);">1. kolo</th>
      <th style="color: rgb(255, 112, 0);">2. kolo</th>
      <th style="color: rgb(255, 112, 0);">čvtrtfin&aacute;le</th>
      <th style="color: rgb(255, 112, 0);">Semifin&aacute;le</th>
      <th style="color: rgb(255, 112, 0);">Fin&aacute;le - první
vítěz</th>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">&nbsp;Imposieble</td>
      <td rowspan="2" style="width: 100pt;" align="center">Imposieble<br>
           <a href="http://www.heroes3.cz/hry/?detail=11132&page=1"><span
 style="color: rgb(255, 102, 0);">(11132)</span></a>
      </td>
      <td rowspan="4" style="width: 100pt;" align="center">Imposieble<br>
           <a href="http://www.heroes3.cz/hry/?detail=11171&page=1"><span
 style="color: rgb(255, 102, 0);">(11171)</span></a>
      </td>
      <td rowspan="8" style="width: 100pt;" align="center">Imposieble<br>
           <a href="http://www.heroes3.cz/hry/?detail=11252&page=1"><span
 style="color: rgb(255, 102, 0);">(11252)</span></a>
      </td>
      </td>
      <td rowspan="16" style="width: 100pt;" align="center">Imposieble<br>
           <a href="http://www.heroes3.cz/hry/?detail=11282&page=1"><span
 style="color: rgb(255, 102, 0);">(11282)</span></a>
      </td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">&nbsp;Demon</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Owl</td>
      <td rowspan="2" style="width: 100pt;" align="center">SD<br>
           <a href="http://www.heroes3.cz/hry/?detail=11124&page=1"><span
 style="color: rgb(255, 102, 0);">(11124)</span></a>
      <span
 style="color: rgb(255, 102, 0);"></span></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">SD</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Chichtofnuk</td>
      <td rowspan="2" style="width: 100pt;" align="center">Chichtofnuk<br>
           <a href="http://www.heroes3.cz/hry/?detail=11107&page=1"><span
 style="color: rgb(255, 102, 0);">(11107)</span></a>
<br>
  </td>
      <td rowspan="4" style="width: 100pt;" align="center">PetoPalo<br>
           <a href="http://www.heroes3.cz/hry/?detail=11180&page=1"><span
 style="color: rgb(255, 102, 0);">(11180)</span></a>
      <span
 style="color: rgb(255, 102, 0);"></span></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">MiraQ</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Petopalo</td>
      <td rowspan="2" style="width: 100pt;" align="center">PetoPalo<br>
           <a href="http://www.heroes3.cz/hry/?detail=11145&page=2"><span
 style="color: rgb(255, 102, 0);">(11145)</span></a>
      </td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Siam</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Antony</td>
      <td rowspan="2" style="width: 100pt;" align="center">DavidHakl<br>
           <a href="http://www.heroes3.cz/hry/?detail=11141&page=1"><span
 style="color: rgb(255, 102, 0);">(11107)</span></a>
      </td>
      <td rowspan="4" style="width: 100pt;" align="center">DavidHakl<br>
           <a href="http://www.heroes3.cz/hry/?detail=11188&page=1"><span
 style="color: rgb(255, 102, 0);">(11188)</span></a>
      </td>
      <td rowspan="8" style="width: 100pt;" align="center">Pagi<br>
           <a href="http://www.heroes3.cz/hry/?detail=11207&page=1"><span
 style="color: rgb(255, 102, 0);">(11207)</span>
      </td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">DavidHakl</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Vítek</td>
      <td rowspan="2" style="width: 100pt;" align="center">Vítek<br>
      (Kontumační výhra)
<br>
      </td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Gigiman</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">ChuckNorris</td>
      <td rowspan="2" style="width: 100pt;" align="center">ChuckNorris<br>
           <a href="http://www.heroes3.cz/hry/?detail=11125&page=1"><span
 style="color: rgb(255, 102, 0);">(11125)</span></a>
      </td>
      <td rowspan="4" style="width: 100pt;" align="center">Pagi<br>
           <a href="http://www.heroes3.cz/hry/?detail=11134&page=1"><span
 style="color: rgb(255, 102, 0);">(11134)</span></a>
      </td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Niven</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Pagi</td>
      <td rowspan="2" style="width: 100pt;" align="center">Pagi<br>
           <a href="http://www.heroes3.cz/hry/?detail=11078&page=1"><span
 style="color: rgb(255, 102, 0);">(11078)</span></a><br>
      </td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Kisane</td>
    </tr>
  </tbody>
</table>
<br>
</br>


<span style="font-weight: bold;"> VEDLEJ&#352Í PAVOUK - Poražený vypadává z turnaje</span><br>
</br>
<table border="1">
  <tbody>
    <tr>
      <th style="color: rgb(255, 112, 0);">1. kolo</th>
      <th style="color: rgb(255, 112, 0);">2. kolo VP/VP</th>
      <th style="color: rgb(255, 112, 0);">3. kolo VP/HP</th>
      <th style="color: rgb(255, 112, 0);">čvtrtfin&aacute;le(1) VP/VP</th>
      <th style="color: rgb(255, 112, 0);">čvtrtfin&aacute;le(2) VP/HP</th>
      <th style="color: rgb(255, 112, 0);">Semifin&aacute;le(1) VP/VP</th>
      <th style="color: rgb(255, 112, 0);">Semifin&aacute;le(2) porazený má bronz:)</th>
      <th style="color: rgb(255, 112, 0);">Fin&aacute;le - druhý </br>vítěz</th>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Marge</td>
      <td rowspan="2" style="width: 100pt;" align="center">Demon </br>
           <a href="http://www.heroes3.cz/hry/?detail=11193&page=1"><span
 style="color: rgb(255, 102, 0);">(11193)</span></a>
      </td>
      
      <td rowspan="2" style="width: 100pt;" align="center">Demon </br>
           <a href="http://www.heroes3.cz/hry/?detail=11231&page=1"><span
 style="color: rgb(255, 102, 0);">(11231)</span></a>
      </td>
      
      <td rowspan="4" style="width: 100pt;" align="center">SD</br>
           <a href="http://www.heroes3.cz/hry/?detail=11219&page=1"><span
 style="color: rgb(255, 102, 0);">(11219)</span></a>
      </td>
       <td rowspan="4" style="width: 100pt;" align="center">Chichtofnuk<br>
           <a href="http://www.heroes3.cz/hry/?detail=11273&page=1"><span
 style="color: rgb(255, 102, 0);">(11273)</span></a>
      </td>
      <td rowspan="8" style="width: 100pt;" align="center">DavidHakl<br>
           <a href="http://www.heroes3.cz/hry/?detail=11293&page=2"><span
 style="color: rgb(255, 102, 0);">(11293)</span></a>
      </td>
       <td rowspan="8" style="width: 100pt;" align="center">DavidHakl<br>
           <a href="http://www.heroes3.cz/hry/?detail=11304&page=1"><span
 style="color: rgb(255, 102, 0);">(11304)</span></a>
      </td>
      <td rowspan="16" style="width: 100pt;" align="center">DavidHakl<br>
           <a href="http://www.heroes3.cz/hry/?detail=11328&page=1"><span
 style="color: rgb(255, 102, 0);">(11328)</span></a>
      </td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">&nbsp;Demon</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">jjauda</td>
      <td rowspan="2" style="width: 100pt;" align="center">Kisane<br>
           <a href="http://http://www.heroes3.cz/hry/?detail=11203&page=1"><span
 style="color: rgb(255, 102, 0);">(11203)</span></a></td>
  <td rowspan="2" style="width: 100pt;" align="center">SD
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Kisane</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Pagy</td>
      <td rowspan="2" style="width: 100pt;" align="center">Pagy<br>
           <a href="http://www.heroes3.cz/hry/?detail=11178&page=1"><span
 style="color: rgb(255, 102, 0);">(11178)</span></a>
  </td>
    <td rowspan="2" style="width: 100pt;" align="center">Pagy<br>
           <a href="http://www.heroes3.cz/hry/?detail=11211&page=1"><span
 style="color: rgb(255, 102, 0);">(11211)</span></a></td>
      <td rowspan="4" style="width: 100pt;" align="center">Chichtofnuk<br>
           <a href="http://www.heroes3.cz/hry/?detail=11213&page=1"><span
 style="color: rgb(255, 102, 0);">(11213)</span></a></td>
<td rowspan="4" style="width: 100pt;" align="center">DavidHakl

      </td>
      
      <span
 style="color: rgb(255, 102, 0);"></span></td>
    </tr>
    
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Niven</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Paolos</td>
      <td rowspan="2" style="width: 100pt;" align="center">Paolos
      </td>
        <td rowspan="2" style="width: 100pt;" align="center">Chichtofnuk</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Antony</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Karcma</td>
      <td rowspan="2" style="width: 100pt;" align="center">Gigiman<br>
           <a href="http://www.heroes3.cz/hry/?detail=11187&page=1"><span
 style="color: rgb(255, 102, 0);">(11187)</span></a>
      </td>
      <td rowspan="2" style="width: 100pt;" align="center">Gigiman<br>
<br>
      </td>
      <td rowspan="4" style="width: 100pt;" align="center">Vítek<br>
           <a href="http://www.heroes3.cz/hry/?detail=11245&page=1"><span
 style="color: rgb(255, 102, 0);">(11245)</span></a> </td>
<td rowspan="4" style="width: 100pt;" align="center">Siam<br>
           <a href="http://www.heroes3.cz/hry/?detail=11276&page=1"><span
 style="color: rgb(255, 102, 0);">(11276)</span></a>
<td rowspan="8" style="width: 100pt;" align="center">Siam<br>
           <a href="http://www.heroes3.cz/hry/?detail=11302&page=1"><span
 style="color: rgb(255, 102, 0);">(11302)</span></a>
<td rowspan="8" style="width: 100pt;" align="center">Pagi
      </td>
      </td>
      </td>
    </tr>
    
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Gigiman</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Kotletka</td>
      <td rowspan="2" style="width: 100pt;" align="center">MiraQ<br>
           <a href="http://www.heroes3.cz/hry/?detail=11179&page=1"><span
 style="color: rgb(255, 102, 0);">(11179)</span></a>
      </td>
        <td rowspan="2" style="width: 100pt;" align="center">Vítek</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">MiraQ</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Macwar</td>
      <td rowspan="2" style="width: 100pt;" align="center">Siam<br>
           <a href="http://www.heroes3.cz/hry/?detail=11195&page=11"><span
 style="color: rgb(255, 102, 0);">(11195)</span></a>
      </td>
        <td rowspan="2" style="width: 100pt;" align="center">Siam<br>
           <a href="http://www.heroes3.cz/hry/?detail=11205&page=2"><span
 style="color: rgb(255, 102, 0);">(11205)</span></a></td>
      <td rowspan="4" style="width: 100pt;" align="center">Siam<br>
           <a href="http://www.heroes3.cz/hry/?detail=11226&page=2"><span
 style="color: rgb(255, 102, 0);">(11226)</span></a>
      </td>
      <td rowspan="4" style="width: 100pt;" align="center">PetoPalo
      
    </tr>
      
     <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Siam</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Hroso</td>
      <td rowspan="2" style="width: 100pt;" align="center">Hroso<br>
           <a href="http://www.heroes3.cz/hry/?detail=11163&page=1"><span
 style="color: rgb(255, 102, 0);">(11163)</span></a>
      </td>
        <td rowspan="2" style="width: 100pt;" align="center">ChuckNorris</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Owl</td>
    </tr>
  </tbody>
</table>
  

<?php


echo "</div>";




ramecek_end(1024);

html_end();

?>
