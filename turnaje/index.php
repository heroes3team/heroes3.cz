<?php
/*
* Domovska stranka ligy
*/

include('../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

sloupec_start();

# login / uzivatel.panel
include($INCLUDE_DIR.'/login_ramecek.php');
	
sloupec_end();

sloupec_start();


	ramecek_start(1024, 'Single player challenges');
	echo "<div style=\"margin-left: 30px;\">";

		table_start('tab_top');
			table_row(array("NÁZEV VÝZVY", "START", 'ORGANIZÁTOR', 'POZNÁMKA'),0,0,0,0,1);
			table_row(array(html_href("challange-lotr", "Jeden prsten vládne všem"), '05/2018', 'Macwar', ''));
		table_end();

	echo "</div><br>";
	ramecek_end(1024);
	
	ramecek_start(1024, 'Turnaje');
	echo "<div style=\"margin-left: 30px;\">";

		table_start('tab_top');
			table_row(array("NÁZEV TURNAJE", "TERMÍN", 'ORGANIZÁTOR', 'POZNÁMKA'),0,0,0,0,1);
			table_row(array(html_href("prvni_cup", "První Cup"), 'březen - září 2009', 'Gaborn', 'zrušeno'));
			table_row(array(html_href("brt", "Brutal Random Tournament"), '7. 11. 2009', 'Tomis', '&nbsp;'));
			table_row(array(html_href("brt2", "Brutal Random Tournament 2"), '4. 8. 2010', 'Tornádo', '&nbsp;'));
			table_row(array(html_href("dobro_vs_zlo", "Dobro versus Zlo"), '15. 10. 2010', 'Niven', '&nbsp;'));
			table_row(array(html_href("vodni_valky", "Vodní Války"), '1.2.2011', 'Niven', '&nbsp;'));
			table_row(array(html_href("dobro_vs_zlo2", "Dobro versus Zlo 2"), '30.10.2011', 'Niven', '&nbsp;'));
			table_row(array(html_href("world_cup_2012", "World Cup 2012"), '1. 3. 2012', 'Sticky Fingaz (RUS)', 'mezinárodní turnaj, info u Tomise'));
			table_row(array(html_href("imps_rt", "Imp's Random Tournament"), '1. 3. 2013', 'Imposieble', '&nbsp;'));
			table_row(array(html_href("first_vitek_tournament", "First Vitek Tournament"), '3. 11. 2013', 'Vítek', '&nbsp;'));
			table_row(array(html_href("vt2", "Second Vitek Tournament"), '15. 9. 2015', 'Vítek', '&nbsp;'));
			table_row(array(html_href("scooby1hota", "Scoobyho HOTA turnaj vol.1"), '9. 6. 2017', 'Scooby, H34D', 'Horn of the Abyss'));
			table_row(array(html_href("world_cup_2018", "World Cup 2018"), '5. 3. 2018 (?)', 'Armageddets', 'účast zrušena pro nízký zájem'));
			table_row(array(html_href("speedygonzales", "Speedy Gonzales Tour"), '1. 3. 2018', 'H34D', 'minutovka'));
			table_row(array(html_href("zrcadlo", "Zrcadlo, zrcadlo, řekni mi..."), '1. 8. 2018', 'H34D', 'zrcadlový template mapy'));
			table_row(array(html_href("scooby_world_rules", "Scooby´s WORLD RULES tournament Vol. 1"), '1. 7. 2019', 'Scooby', 'Pravidla světové ligy'));
		table_end();

	echo "</div><br>";
	

	ramecek_end(1024);
	
sloupec_end();


html_end();

?>
