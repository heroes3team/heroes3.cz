<?php


require('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

ramecek_start(1024, 'SCOOBYHO HOTA TURNAJ VOL. 1');

echo "<div style=\"margin-left: 10px;\">";
echo "<center><img style=\"width: 860px; height: 625px;\" alt=\"\" src=\"scoobyhota1.jpg\" /></center>";
echo "

	<h1>Pravidla</h1>


      <h4>Systém hlavního turnaje</h4> 

       
      <div>
        Pavouk double elimination - winners bracket + finále loosers bracket na 2 vítězné zápasy, loosers bracket kromě finále na 1 vítězný zápas.<br/>
		Grandfinále se bude hrát na 3 vítězné zápasy s tím, že postupující z winners bracketu vede 1:0.<br/>
        Prvních 8 přihlášených hráčů s nejvyšším ELO ligy bude do pavouka nasazeno rovnoměrně, ostatní budou náhodně rozlosováni.<br/>
		Na odehrání každého kola bude určen maximálně 1 měsíc.
      </div>
             
			 
      <h4>Systém doprovodného turnaje nováčků</h4> 

       
      <div>
        Turnaje nováčků se mohou účastnit hráči, kteří mají v lize odehráno méně než 20 her, anebo mají nižší winrate než 40%.<br/>
        Hráči budou nejprve v rámci jedné skupiny hrát každý s každým na 1 vítězný zápas, kdy první 4 hráči postoupí do pavuka.<br/>
		Pavouk se bude hrát na 2 vítězné zápasy. <br/>
		Na odehrání každého kola bude určen maximálně 1 měsíc.
      </div>			 
    
    <h4>Turnajové hry</h4>
	
    <div>

    Nastavení hry:
    <ul>
     <li>Hraje se datadisk Horn of the Abyss ve své aktuální verzi s HD+ modem pro aktivaci simultánních tahů.</li>
     <li>Mapa: random L bez pozdemí, voda žádná, neutrálové strong, template random mapy je 2sm4d(3), pokud se hráči nedohodnou jinak.</li>
     <li>Na mapách L bez podzemí a menších musí být vždy povoleny všechny cesty. Na větších mapách je nejpomalejší cesta (nalevo) zakázána.</li>
     <li>Časový limit: 6 minut se zapnutými simultánními tahy, pokud se hráči nedohodnou jinak.</li>
	 <li>V případech, kdy vlivem simultánních tahů dostane jeden z hráčů do jakékoli výhody, či nevýhody, která by bez užití simultánních tahů nemohla nastat,<br/>
	 hráči načtou dřívější save bez simultánních tahů. Výhodou či nevýhodou se rozumí např. nemožnost modrého hráče reagovat na tah červeného hráče v rámci určitého herního dne.</li>
     <li>V případě, že modrý hráč přeruší simultánní tahy, je po restartu dne povinen svůj tah co nejpřesněji zopakovat.</li>
     <li>Obtížnost: expert (160%)</li>
     <li>Před spuštěním hry si mohou hráči nastavit heslo, aby nedošlo k možnému zneužití savů před ukončením hry.<br/>
	 Po hře je hráč povinen na požádání své heslo poskytnout rozhodčímu turnaje z důvodu případné kontroly fair play.</li>
     <li>Hráči nemohou hrát v jednom turnajovém kole dvakrát stejné město.</li>
     <li>Hráč s nižším aktuálním ELO vybírá první barvu, přičemž červený hráč vybírá první město, hrdinu a bonus. Následující hru se barvy střídají.</li>
     <li>Odehrané hry se ukládají neprodleně po jejich ukončení s vyznačením textu *TURNAJ-SCOOBY* do komentáře. Soupeř je povinen neprodleně po uložení záznamu hru potvrdit.</li>
	 </ul>

    Přerušení hry:
    <ul>
     <li>V případě, že je nutné hru přerušit, hráč je povinen kompletně dokončit svůj tah. V případě simultánních tahů, hráči opouští hru po ukončení tahu modrého, je-li to možné.</li>
     <li>Hráči načítají již dokončený tah (např. autosave), v kterém není dovoleno cokoli měnit.</li>        
     <li>V případě náhlého odchodu hráče ze hry, např. vlivem technické chyby, či pádu internetu, je tento hráč na požádání soupeře povinen ihned zaslat savy rozhodčímu, </br>včetně aktuálního save „battle.gm2.“
	 Za tímto účelem je třeba mít v HD_Launcheru zaškrtnuto „save before each battle“ a „save all days (for tournaments)“ </li> 
	 </ul>
	 
    Omezení hry:
    <ul>
     <li>Pravidlo monsters na předměty i cesty.</li>
     <li>Zákaz užívání diplomacie a vykopání grálu.</li>        
     <li>Zákaz užívání artefaktu andělská křídla
	 <li>Kouzla Dimenzní brána a Let je možno užít, pouze v případě, že se hráči objeví v postavené věži kouzel 5. stupně, pokud se hráči nedohodnou jinak.</li>
     <li>Ve stejném městě jako je startovní město hráče nesmí hráč nakoupit bojovníky vyššího než 3. stupně. To neplatí, přijde-li hráč o své startovní město.</li>
	 </ul>
      
    </div>

      <h4>Reklamace mapy</h4>
    <div>
      Hráč má nárok na novou hru v případě, že je mapa nehratelná, tedy např. pokud hráč v prvních 3 dnech hry zjistí, že má určitý důležitý průchod<br/>
	  blokován silným monstrem, které zároveň hlídá nějakou budovu, či artefakt.
      <br/>
      V případě hry na jiném než turnajovém templatu 2sm4d(3) je možné restartovat hru, pokud v městech chybí tvrz, 
	  hráčí začínají s více městy,<br/>města jsou na špatném povrchu, z města nevede žádná cesta, anebo pokud se v okolí startovního města nacházejí 
	  pouze monstra ze startovního města.
      <br/>
      V případě sporu o (ne)hratelnosti mapy rozhodne rozhodčí turnaje.
    </div>
    
      <h4>Kontumace neaktivních hráčů</h4>
      
    <div>
    
    Pokud se do konce termínu daného kola neozve ani jeden z hráčů, kteří mají hrát turnajovou hru, dostanou oba kontumační prohru.
      <br/>
    V případě že je jeden hráč aktivní a jeho soupeř není k sehnání, dostane kontumaci jen soupeř. V tom případě musí být vidět
    snaha aktivního hráče.
      <br/>
    Po první kontumaci v horní části pavouka (winners bracket) se hráč přesouvá do spodní části pavouka (loosers bracket), odkud může pokračovat.
      <br/>
    Po dohodě s organizátorem turnaje lze ve výjimečných případech zápas odložit.
      <br/>
    Při porušení pravidel, jakož i při porušení fair play, může být hráč organizátorem diskvalifikován, včetně banu na další turnaje.
	
    </div>  

	<h1>Ceny turnaje</h1>

      <div>
      <h4>Hlavní turnaj</h4>
	  
      1. místo - 1000,- Kč<br/> 
      2. místo - 650,- Kč<br/>
      3. místo - 350,- Kč<br/>
	  <br/>
	  
      <h4>Doprovodný turnaj</h4>
	  
      1. místo - 300,- Kč<br/> 
      </div> 
	
	
	<h1>Přihlášky</h1>

      <p>
      Přihlášky pište do diskuse, sekce turnaje na webu ligy, anebo Scoobymu na jeden z kontaktů v jeho profilu.<br/>
      Konec přihlášek je v pátek 9. června 2017.<br/>
      Pro přihlášení do turnaje musíte být registrovaní v lize.
      </p>
      
      <table style=\"border-spacing: 15pt;\"><tr>
      <td valign=\"top\">
      <h3>Přihlášení hráči hlavního turnaje</h3>
      
      <p>
        1: <a href=http://heroes3.cz/hraci/detail.php?id=3113><font color=#ff6633>Scooby</font></a><br/>
        2: <a href=http://heroes3.cz/hraci/detail.php?id=46><font color=#ff6633>Twister</font></a><br/>
        3: <a href=http://heroes3.cz/hraci/detail.php?id=5149><font color=#ff6633>Liut</font></a><br/>
        4: <a href=http://heroes3.cz/hraci/detail.php?id=5271><font color=#ff6633>Bonca</font></a><br/>
        5: <a href=http://heroes3.cz/hraci/detail.php?id=5224><font color=#ff6633>Madafaka</font></a><br/>
        6: <a href=http://heroes3.cz/hraci/detail.php?id=105><font color=#ff6633>Jirkahron</font></a><br/>
        7: <a href=http://heroes3.cz/hraci/detail.php?id=222><font color=#ff6633>H34D</font></a><br/>
        8: <a href=http://heroes3.cz/hraci/detail.php?id=706><font color=#ff6633>Acid</font></a><br/>
        9: <a href=http://heroes3.cz/hraci/detail.php?id=1927><font color=#ff6633>Jelen</font></a><br/>
        10: <a href=http://heroes3.cz/hraci/detail.php?id=2762><font color=#ff6633>maGe</font></a><br/>
        11: <a href=http://heroes3.cz/hraci/detail.php?id=29><font color=#ff6633>Gaborn</font></a><br/>
        12: <a href=http://heroes3.cz/hraci/detail.php?id=2><font color=#ff6633>Lord.Alex</font></a><br/>
        13: <a href=http://heroes3.cz/hraci/detail.php?id=519><font color=#ff6633>Pagi</font></a><br/>
        14: <a href=http://heroes3.cz/hraci/detail.php?id=823><font color=#ff6633>Karcma</font></a><br/>
        15: <a href=http://heroes3.cz/hraci/detail.php?id=3><font color=#ff6633>Kacer</font></a><br/>
        16: <a href=http://heroes3.cz/hraci/detail.php?id=5317><font color=#ff6633>Stamz</font></a><br/>
        17: <a href=http://heroes3.cz/hraci/detail.php?id=3202><font color=#ff6633>Marge</font></a><br/>
        18: <a href=http://heroes3.cz/hraci/detail.php?id=5315><font color=#ff6633>Nabodávač</font></a><br/>
        19: <a href=http://heroes3.cz/hraci/detail.php?id=5307><font color=#ff6633>Naoblaku</font></a><br/>
        20: <a href=http://heroes3.cz/hraci/detail.php?id=613><font color=#ff6633>Kisane</font></a><br/>
        21: <a href=http://heroes3.cz/hraci/detail.php?id=623><font color=#ff6633>Siam</font></a><br/>
        22: <a href=http://heroes3.cz/hraci/detail.php?id=5258><font color=#ff6633>Klára</font></a><br/>
        23: <a href=http://heroes3.cz/hraci/detail.php?id=2898><font color=#ff6633>Lord Sookar</font></a><br/>
        24: <a href=http://heroes3.cz/hraci/detail.php?id=5316><font color=#ff6633>ssgsd</font></a><br/>
        25: <a href=http://heroes3.cz/hraci/detail.php?id=5262><font color=#ff6633>Gorky</font></a><br/>
        26: <a href=http://heroes3.cz/hraci/detail.php?id=5319><font color=#ff6633>Mixxxer</font></a><br/>
        27: <a href=http://heroes3.cz/hraci/detail.php?id=5301><font color=#ff6633>Mr.Hyde</font></a><br/>
        28: <a href=http://heroes3.cz/hraci/detail.php?id=5323><font color=#ff6633>owencz</font></a><br/>
		29: <a href=http://heroes3.cz/hraci/detail.php?id=5321><font color=#ff6633>Lord Slayer</font></a><br/>
		30: <a href=http://heroes3.cz/hraci/detail.php?id=6><font color=#ff6633>Niven</font></a><br/>
		31: <a href=http://heroes3.cz/hraci/detail.php?id=3486><font color=#ff6633>Houhou</font></a><br/>
		32: <a href=http://heroes3.cz/hraci/detail.php?id=836><font color=#ff6633>Imposieble</font></a><br/>
      </p>
      
      </td>
      
      <td valign=\"top\">
      <h3>Nasazení do pavouka (top8 dle ELO)</h3>
      
        1: Houhou (2351)<br/>
        2: Imposieble (2342)<br/>
        3: H34D (2222)<br/>
        4: Karcma (2161)<br/>
        5: Acid (2139)<br/>
        6: Bonca (2073)<br/>
        7: Pagi (2007)<br/>
        8: Kisane (1973)<br/>
        9: Jirkahron<br/>
        10: Twister<br/>
        11: Lord.Alex<br/>
        12: Naoblaku<br/>
        13: Marge<br/>
        14: Gorky<br/>
        15: owencz<br/>
        16: Lord Sookar<br/>
        17: Gaborn<br/>
        18: Madafaka<br/>
        19: Siam<br/>
        20: Jelen<br/>
        21: maGe<br/>
        22: Kacer<br/>
        23: Nabodávač<br/>
        24: Scooby<br/>
        25: Mr.Hyde<br/>
        26: Niven<br/>
        27: Liut<br/>
        28: Klára<br/>
		29: Lord Slayer<br/>
		30: Stamz<br/>
		31: Mixxxer<br/>
		32: ssgsd<br/>
      </td>
   
      <td valign=\"top\">
      <h3>Přihlášení hráči doprovodného turnaje</h3>
	  
      1: owencz<br/>
      2: Jirkahron<br/>
      3: Liut<br/>
      4: Stamz<br/>
      5: Nabodávač<br/>
      6: Pepix<br/>
      7: Klára<br/>
      8: ssgsd<br/>
      9: Gorky<br/>
     10: Mixxxer<br/>
	 11: Siska96<br/>
	 12: Gabo<br/>
	 
      </td>   
      
      <td valign=\"top\">
      <h5></h5>
              
      </td>       
      
      </tr></table>

	<h1>Průběh</h1>

      <div>
      1. kolo winners bracket: 09.06.2017 - 09.07.2017<br/>
      2. kolo winners bracket + 1. kolo loosers bracket: 09.07.2017 - 09.08.2017<br/> 
      3. kolo winners bracket + 2. a 3. kolo loosers bracket: 09.08.2017 - 09.09.2017<br/>
      4. kolo winners bracket + 4. a 5. kolo loosers bracket: 09.09.2017 - 09.10.2017<br/>
      5. kolo winners bracket + 6. a 7. kolo loosers bracket: 09.10.2017 - 09.11.2017<br/>
      finále winners bracket + finále loosers bracket: 09.11.2017 - 09.12.2017<br/>
      velké finále : 09.12.2017 - 24.12.2017<br/>
      vyhlášení a rozdání cen: 24.12.2017
      </div>
";

echo "

</br>Odkaz na pavouka živě: <a href=http://challonge.com/y6aetpo9>http://challonge.com/y6aetpo9</a>

<center><font size=3><h3>Velké finále</h3>
<table border=\"1\">
  <tbody>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">H34D</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">
		H34D</br>1:0, <a href=\"".$ROOT_URL."/hry/?detail=15991\"><font color=#ff6633>2:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16012\"><font color=#ff6633>2:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16014\"><font color=#ff6633>3:1</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">maGe</td>
    </tr>
  </tbody>
</table></font></center>
";

echo "<h3>Pavouk hlavního turnaje - winners bracket</h3>";

      echo pavouk(32,  array(
        array("Houhou", "Houhou<br/><a href=\"".$ROOT_URL."/hry/?detail=14997\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15069\"><font color=#ff6633>2:0</font></a>",
          "Houhou<br/><a href=\"".$ROOT_URL."/hry/?detail=15407\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15408\"><font color=#ff6633>2:0</font></a>",
          "Scooby<br/><a href=\"".$ROOT_URL."/hry/?detail=15566\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15567\"><font color=#ff6633>0:2</font></a>",
          "maGe<br/><a href=\"".$ROOT_URL."/hry/?detail=15770\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15788\"><font color=#ff6633>0:2</font></a>",
          "H34D<br/><a href=\"".$ROOT_URL."/hry/?detail=15836\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15823\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15837\"><font color=#ff6633>1:2</font></a>"),
        array("ssgsd"),
        array("Lord Sookar", "Lord Sookar<br/><font color=#ff6633>kontumace</font>"),
        array("Gaborn"),
        array("Kisane", "Kisane<br/><font color=#ff6633>kontumace</font>",
          "Scooby<br/><a href=\"".$ROOT_URL."/hry/?detail=15374\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15552\"><font color=#ff6633>0:2</font></a>"),
        array("Mr.Hyde"),
        array("Jirkahron", "Scooby<br/><a href=\"".$ROOT_URL."/hry/?detail=15123\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15128\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15135\"><font color=#ff6633>1:2</font></a>"),
        array("Scooby"),
        array("Karcma", "Karcma<br/><a href=\"".$ROOT_URL."/hry/?detail=15141\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15140\"><font color=#ff6633>2:0</font></a>",
          "Karcma<br/><a href=\"".$ROOT_URL."/hry/?detail=15369\"><font color=#ff6633>1:0</font></a>, <font color=#ff6633>kontumace</font>",
          "maGe<br/><a href=\"".$ROOT_URL."/hry/?detail=15662\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15698\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15701\"><font color=#ff6633>1:2</font></a>"),
        array("Lord Slayer"),
        array("Marge", "Jelen<br/><font color=#ff6633>kontumace</font>"),
        array("Jelen"),
        array("Acid", "Acid<br/><a href=\"".$ROOT_URL."/hry/?detail=15120\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15138\"><font color=#ff6633>2:0</font></a>",
          "maGe<br/><a href=\"".$ROOT_URL."/hry/?detail=15333\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15344\"><font color=#ff6633>0:2</font></a>"),
        array("Klara"),
        array("Naoblaku", "maGe<br/><a href=\"".$ROOT_URL."/hry/?detail=14970\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15098\"><font color=#ff6633>0:2</font></a>"),
        array("maGe"),
        array("Imposieble", "Imposieble<br/><a href=\"".$ROOT_URL."/hry/?detail=14991\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=14996\"><font color=#ff6633>2:0</font></a>",
          "owencz<br/><a href=\"".$ROOT_URL."/hry/?detail=15432\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15436\"><font color=#ff6633>0:2</font></a>",
          "Twister<br/><a href=\"".$ROOT_URL."/hry/?detail=15663\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15681\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15728\"><font color=#ff6633>1:2</font></a>",
          "H34D<br/><a href=\"".$ROOT_URL."/hry/?detail=15759\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15767\"><font color=#ff6633>0:2</font></a>"),
        array("Mixxxer"),
        array("owencz", "owencz<br/><a href=\"".$ROOT_URL."/hry/?detail=15030\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15035\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15170\"><font color=#ff6633>2:1</font></a>"),
        array("Madafaka"),
        array("Pagi", "Pagi<br/><a href=\"".$ROOT_URL."/hry/?detail=15171\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15178\"><font color=#ff6633>2:0</font></a>",
          "Twister<br/><a href=\"".$ROOT_URL."/hry/?detail=15264\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15275\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15281\"><font color=#ff6633>1:2</font></a>"),
        array("Niven"),
        array("Twister", "Twister<br/><a href=\"".$ROOT_URL."/hry/?detail=15102\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15184\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15185\"><font color=#ff6633>2:1</font></a>"),
        array("Nabodávač"),
        array("H34D", "H34D<br/><a href=\"".$ROOT_URL."/hry/?detail=15114\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15115\"><font color=#ff6633>2:0</font></a>",
          "H34D<br/><a href=\"".$ROOT_URL."/hry/?detail=15313\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15334\"><font color=#ff6633>2:0</font></a>",
          "H34D<br/><a href=\"".$ROOT_URL."/hry/?detail=15373\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15375\"><font color=#ff6633>1:1</font>, <a href=\"".$ROOT_URL."/hry/?detail=15404\"><font color=#ff6633>2:1</font></a></a>"),
        array("Stamz"),
        array("Gorky", "Siam<br/><font color=#ff6633>kontumace</font>"),
        array("Siam"),
        array("Bonca", "Bonca<br/><a href=\"".$ROOT_URL."/hry/?detail=15074\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15081\"><font color=#ff6633>2:0</font></a>",
          "Kacer<br/><a href=\"".$ROOT_URL."/hry/?detail=15129\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15130\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15131\"><font color=#ff6633>1:2</font></a>"),
        array("Liut"),
        array("Lord.Alex", "Kacer<br/><a href=\"".$ROOT_URL."/hry/?detail=14983\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=14984\"><font color=#ff6633>2:0</font></a>"),
        array("Kacer")          
      ), 1); // "style=\"width: 95%\""

echo"

<br/>

 <h3>Pavouk hlavního turnaje - loosers bracket</h3>
 
<br/><br/>
<table border=\"1\">
  <tbody>
    <tr>
      <th style=\"color: rgb(255, 112, 0);\">1. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">2. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">3. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">4. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">5. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">6. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">7. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">finále LB</th>
      <th style=\"color: rgb(255, 112, 0);\">vítěz LB</th>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">ssgsd</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Bonca<br>
		</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Bonca<br/><a href=\"".$ROOT_URL."/hry/?detail=15564\"><font color=#ff6633>1:0</font></a><br>
		</td>
      
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Karcma</td>
      
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">
		Karcma<br><a href=\"".$ROOT_URL."/hry/?detail=15793\"><font color=#ff6633>1:0</font></a><br>
		</td>
       <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Twister<br>
		</td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\">Twister<br/><a href=\"".$ROOT_URL."/hry/?detail=15906\"><font color=#ff6633>1:0</font></a><br>
		</td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\">maGe<br>
		</td>
      <td rowspan=\"16\" style=\"width: 70pt;\" align=\"center\">maGe<br/><a href=\"".$ROOT_URL."/hry/?detail=15960\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15961\"><font color=#ff6633>2:0</font></a><br>
		</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Gaborn</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=#ff6633>(k)</font> ssgsd</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Mr.Hyde</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Siam<br>
		</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Jirkahron</br><a href=\"".$ROOT_URL."/hry/?detail=15671\"><font color=#ff6633>0:1</font></a><br>
		</td>
  <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Jirkahron</br><a href=\"".$ROOT_URL."/hry/?detail=15709\"><font color=#ff6633>0:1</font></a><br>
		</tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Jirkahron</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=#ff6633>(k)</font> Jirkahron</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Lord Slayer</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Pagi<br>
		</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Pagi</br><a href=\"".$ROOT_URL."/hry/?detail=15609\"><font color=#ff6633>1:0</font></a><br>
		</td>
    <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Houhou<br>
		</td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Houhou</br><a href=\"".$ROOT_URL."/turnaje/scooby1hota/houhou_vs_pagi.jpg\"><font color=#ff6633>1:0</font></a><br>
		</td>
<td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Karcma</br><a href=\"".$ROOT_URL."/hry/?detail=15893\"><font color=#ff6633>1:0</font></a></td>
      <span
 style=\"color: rgb(255, 102, 0);\"></span></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Marge</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><a href=\"".$ROOT_URL."/hry/?detail=15597\"><font color=#ff6633>1:0</font></a> Marge</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Klára</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Imposieble<br>
		</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Imposieble</br><a href=\"".$ROOT_URL."/hry/?detail=15700\"><font color=#ff6633>1:0</font></a><br>
		</td>
        <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Pagi</br><font color=#ff6633>kontumace</font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Naoblaku</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=#ff6633>(k)</font> Naoblaku</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Mixxxer</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Acid<br>
		</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Acid</br><a href=\"".$ROOT_URL."/hry/?detail=15640\"><font color=#ff6633>1:0</font></a><br>
		</td>
      
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Kacer</td>
      
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Acid</br><font color=#ff6633>kontumace</font><br>
		</td>
       <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Scooby<br>
		</td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\">Acid</br><a href=\"".$ROOT_URL."/hry/?detail=15950\"><font color=#ff6633>0:1</font></a><br>
		</td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\">Twister</br><a href=\"".$ROOT_URL."/hry/?detail=15953\"><font color=#ff6633>1:0</font></a><br>
		</td>		
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Madafaka</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><a href=\"".$ROOT_URL."/hry/?detail=15207\"><font color=#ff6633>1:0</font></a> Madafaka</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Niven</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Jelen<br>
		</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Nabodávač</br><font color=#ff6633>kontumace</font><br>
		</td>
        <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Acid</br><a href=\"".$ROOT_URL."/hry/?detail=15753\"><font color=#ff6633>1:0</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Nabodávač</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><a href=\"".$ROOT_URL."/hry/?detail=15451\"><font color=#ff6633>1:0</font></a> Nabodávač</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Stamz</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Kisane<br>
		</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Stamz</br><font color=#ff6633>kontumace</font><br>
		</td>
    <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">owencz<br>
		</td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">owencz</br><a href=\"".$ROOT_URL."/hry/?detail=15877\"><font color=#ff6633>1:0</font></a><br>
		</td>
<td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Acid</br><a href=\"".$ROOT_URL."/hry/?detail=15921\"><font color=#ff6633>1:0</font></a></td>
      <span
 style=\"color: rgb(255, 102, 0);\"></span></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Gorky</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=#ff6633>(k)</font> Stamz</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Liut</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Lord Sookar<br>
		</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Liut</br><font color=#ff6633>kontumace</font><br>
		</td>
        <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Liut</br><a href=\"".$ROOT_URL."/hry/?detail=15779\"><font color=#ff6633>0:1</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Lord.Alex</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=#ff6633>(k)</font> Liut<br>
		</td>
    </tr>
	
  </tbody>
</table>

<br>

<h3>Doprovodný turnaj nováčků - skupina každý s každým</h3>

<table border=1>
<tbody>
<tr>
<td>&nbsp;</td>
<td width=7.75%>owencz</td>
<td width=7.75%>Jirkahron</td>
<td width=7.75%>Liut</td>
<td width=7.75%>Stamz</td>
<td width=7.75%>Nabodávač</td>
<td width=7.75%>Pepix</td>
<td width=7.75%>Klára</td>
<td width=7.75%>ssgsd</td>
<td width=7.75%>Gorky</td>
<td width=7.75%>Mixxxer</td>
<td width=7.75%>Siska96</td>
<td width=7.75%>Gabo</td>
</tr>
<tr>
<td>owencz</td>
<td><center>X</center></td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15467\"><font color=#ff6633>1:0</font></center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15828\"><font color=#ff6633>0:1</font></center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15747\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15748\"><font color=#ff6633>0:1</font></center></td>
</tr>
<tr>
<td>Jirkahron</td>
<td>&nbsp;</td>
<td><center>X</center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15389\"><font color=#ff6633>0:1</font></center></td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15008\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15278\"><font color=#ff6633>1:0</font></center></td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15562\"><font color=#ff6633>1:0</font></center></td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15012\"><font color=#ff6633>1:0</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15771\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15372\"><font color=#ff6633>1:0</font></center></td>
</tr>
<tr>
<td>Liut</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15467\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15389\"><font color=#ff6633>1:0</font></center></td>
<td><center>X</center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15202\"><font color=#ff6633>0:1</font></center></td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15503\"><font color=#ff6633>0:1</font></center></td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15144\"><font color=#ff6633>1:0</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15732\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15741\"><font color=#ff6633>1:0</font></center></td>
</tr>
<tr>
<td>Stamz</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center>X</center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15101\"><font color=#ff6633>1:0</font></center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Nabodávač</td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15008\"><font color=#ff6633>1:0</font></center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center>X</center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Pepix</td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15278\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15202\"><font color=#ff6633>1:0</font></center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center>X</center></td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15819\"><font color=#ff6633>0:1</font></center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15811\"><font color=#ff6633>1:0</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15336\"><font color=#ff6633>1:0</font></center></td>
</tr>
<tr>
<td>Klára</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center>X</center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>ssgsd</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15828\"><font color=#ff6633>1:0</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15562\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15503\"><font color=#ff6633>1:0</font></center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15819\"><font color=#ff6633>1:0</font></center></td>
<td>&nbsp;</td>
<td><center>X</center></td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15220\"><font color=#ff6633>1:0</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15812\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15801\"><font color=#ff6633>0:1</font></center></td>
</tr>
<tr>
<td>Gorky</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center>X</center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Mixxxer</td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15012\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15144\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15101\"><font color=#ff6633>0:1</font></center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15220\"><font color=#ff6633>0:1</font></center></td>
<td>&nbsp;</td>
<td><center>X</center></td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15341\"><font color=#ff6633>0:1</font></center></td>
</tr>
<tr>
<td>Siska96</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15747\"><font color=#ff6633>1:0</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15771\"><font color=#ff6633>1:0</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15732\"><font color=#ff6633>1:0</font></center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15811\"><font color=#ff6633>0:1</font></center></td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15812\"><font color=#ff6633>1:0</font></center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center>X</center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15068\"><font color=#ff6633>1:0</font></center></td>
</tr>
<tr>
<td>Gabo</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15748\"><font color=#ff6633>1:0</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15372\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15741\"><font color=#ff6633>0:1</font></center></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15336\"><font color=#ff6633>0:1</font></center></td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15801\"><font color=#ff6633>1:0</font></center></td>
<td>&nbsp;</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15341\"><font color=#ff6633>1:0</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=15068\"><font color=#ff6633>0:1</font></center></td>
<td><center>X</center></td>
</tr>
</tbody>
</table>

<h3>Doprovodný turnaj nováčků - pavouk pro top4</h3>
<table border=\"1\">
  <tbody>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Siska96</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">
		Pepix</br><center><a href=\"".$ROOT_URL."/hry/?detail=15996\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16017\"><font color=#ff6633>0:2</font></a></td>
	 <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">
		ssgsd</br><center><a href=\"".$ROOT_URL."/hry/?detail=16024\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16031\"><font color=#ff6633>0:2</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Pepix</td>
    </tr>
	<tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">ssgsd</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">
		ssgsd</br><center><a href=\"".$ROOT_URL."/hry/?detail=15932\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15942\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=15941\"><font color=#ff6633>2:1</font></a></center></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Jirkahron</td>
    </tr>
  </tbody>
</table>
<br>

";	  
	  
echo "</div>";

ramecek_end(1024);



html_end();

?>
