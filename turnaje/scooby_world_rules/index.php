<?php


require('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

ramecek_start(1024, 'Scooby´s WORLD RULES tournament Vol. 1');

echo "<div style=\"margin-left: 10px;\">";

echo "

&nbsp;
	<h1>Pravidla</h1>

      <h4>Systém turnaje</h4> 

      <div>
    <ul>
     <li>Prvních 8 hráčů dle aktuálního ligového ELO k 1.7.2019 budou nasazeni do pavouka.</li>
     <li>Zbývající hráči budou rozlosování do pavouka v systému challonge v přímém přenosu na streamu.</li>	 
     <li>Pavouk ve formátu double elimination se hraje ve winners bracket na 2 vítězné zápasy, pokud se hráči v daném kole nedomluví jinak.</li>
     <li>Losers bracket se hraje na 1 vítězný zápas. Finále losers bracket na 2 vítězné zápasy.</li> 
     <li>GRAND finále se hraje na 3 vítězné zápasy. V GRAND finále postupující z winners bracketu vede 1:0</li>
     <li>Turnaj nováčků je určen pro hráče, kteří mají odehráno méně než 30 her. Turnaj nováčků se hraje na skupinu každý s každým na 1 vítězný zápas a poté pavouk pro top 4. Pokud se hráči nedohodnou jinak, hraje se template h3dm1 dle pravidel hlavního turnaje, avšak s NEJRYCHLEJŠÍ cestou.</li>
    </ul>
	 </div>
    
    <h4>Povinná pravidla - turnajové templaty a jejich výběr</h4>

    <div>	
    <ul>
     <li>Hraje se datadisk Horn of the Abyss ve své aktuální verzi s HD+ modem s aktivovanými simultánními tahy.</li>
     <li>Hrají se tyto následující templaty: 1. h3dm1 (zrcadlo), 2. MT_Wrzosy (zrcadlo se silnější stráží u portálu vedoucímu k protihráči), 3. Jebus Cross XL (či po dohodě jeho zrcadlová varinta MT_Jebus L s podzemím, pokud oba hráči souhlasí) </li>
     <li>Templaty h3dm1, Jebus Cross a MT_Jebus jsou součástí aktuální verze hry. Template MT_Wrzosy je možno stáhnout v sekci odkazy - templaty náhodných map z Polské Heroes 3 ligy a poté dole „Pobierz“.</li>    
     <li>Následující příklad výběru templatu je platný pro všechny hry kromě GRAND finále. Elo je bráno k 1.7.2019, viz tabulka přihlášených hráčů.</li>
     <li>Příklad výběru templatu: Známe protihráče - hráč s vyšším elem zabanuje jeden z trojice turnajových templatů, například Jebuss Cross, hráč s nižsím elem vybere ze 2 zbývajích - h3dm1 a MT_Wrzosy, která mapa se bude hrát</li>  
     <li>Následující hru v sérii se situace otáčí. Ten kdo banoval hrad bude v další hře vybírat ze 2 nezabanovaných templatů.</li>

     <li> GRAND finálové templaty: 1. zápas Výhra hráče winner bracketu, 2. h3dm1, 3. Jebus Cross, 4. MT_Wrzosy, 5. h3dm1</li>

    </ul>

Přerušení hry:
    <ul>
     <li>V případě, že je nutné hru přerušit, hráč pokud možno kompletně dokončí svůj tah.</li>
     <li>Za splnění výše uvedené podmínky hráči načítají již dokončený tah (např. autosave), v kterém není dovoleno cokoli měnit.</li>        
     <li>V případě náhlého odchodu hráče ze hry, např. vlivem technické chyby, či přerušení připojení k internetu, je tento hráč na požádání soupeře povinen ihned zaslat savy rozhodčímu, včetně aktuálního save „battle.gm2.“</li>
	 Za tímto účelem je třeba mít v HD_Launcheru zaškrtnuto „save before each battle“ a „save all days (for tournaments)“</li>          
     <li>Hráči mohou požádat o přerušení simultánních tahů, ne však dříve než v 123, tedy v 1. měsíci, 2. týdnu a 3. dnu hry. V tom případě hráči dokončí aktuální tah, v kterém po načtení hry není dovoleno cokoli měnit. Výjimkou je uplatnění následujícího pravidla.</li>
	 <li>V případě, že modrý hráč neměl v simultánním tahu možnost reagovat na tah červeného, může požádat o restart svého dne.
	 Při restartu dne může modrý hráč změnit jen tu část svého tahu, která bezprostředně souvisí s jeho reakcí na tah červeného hráče.</li>
     <li>Před spuštěním hry si mohou hráči nastavit heslo, aby nedošlo k možnému zneužití savů před ukončením hry. Po hře je hráč povinen na požádání své heslo poskytnout protihráči.</li>
     <li>Odehrané hry se ukládají bez zbytečného odkladu po jejich ukončení s vyznačením textu. Soupeř je povinen hru potvrdit bez zbytečného odkladu.</li>
    </ul>

    Omezení hry:
    <ul>
     <li>Hráč nesmí pomocí diplomacie přijmout žádné neutrální jednotky do své armády.</li>        
     <li>Tournament rules zapnuto (omezuje používání kouzla Dimenzion Door 1x za kola za 1 hrdinu; dále omezuje hit and run, kdy útočník nemůž v prvním kole bitvy zakouzlit útočné kouzlo a zároveň utéct z boje.</li>  
     </ul>
	
    Template h3dm1 - nastavení hry + výběr měst a hrdinů a restarty:
    <ul>     
     <li>Časový limit: Custom limit 14 7 2, nebo souhlasí-li oba hráči, tak classic timer 6 minut, se simultánními tahy minimálně do 123.</li>
     <li>Mapa: random, L s podzemím, voda žádná, neutrálové strong, povolena POUZE PROSTŘEDNÍ cesta.</li>
     <li>Obtížnost: expert (160%)</li>
     <li>Tournament rules - zapnuto.</li>    
     <li>Výběr města - Potřeba zapnout v záložce výběru města vpravo PVP OPTIONS.</li> 
     <li>Hostující hráč klikne na RANDOM TOWN - systém vybere například Necropolis. POkud oba hráči se hraním necropolis souhlasí, oba napíší „+“.</li>
     <li>Pokud jeden z nich nesouhlasí, může požádat o zabanování 1 města a reroll (znovuzvolení) města za 500 svého startovního zlata. Takto se pokračuje dokud se oba hráči neshodnou. Limit rerollů není omezen.</li>
     <li>Výběr červené barvy: Trade systémem (obchodování) </li>
     <li>Příklad tradování o barvu: Hrač 1 - obětuje 1000 startovního zlata, Hráč 2 - 1500, Hráč 1 - 1800, Hráč 2 - napíše „+“, což znamená, že přijímá nabídku druhého hráče, smíří se s modrou barvou, avšak obdrží 1800 startovního zlata druhého hráče.</li>
     <li>Výběr hrdinů: Hráč který výhral tradování červené barvy může banovat 0-3 hrdinů. Modrý hráč vybírá ze zbytku hrdinu jako první.</li>
     <li>Hostující hráč upraví poměr startovního zlata pro oba hráče - započítá výběr barvy a rerolly.</li>
     <li>Výběr bonusu - vybírá červený hráč.</li> 
     <li>Start hry</li> 
     <li>Oba hráči mají právo na jeden restart 111, tedy 1. den hry. O případný restart žádá první červený hráč.</li>
     </ul>
	
    Template MT_Wrzosy nastavení hry + výběr měst a hrdinů a restarty:
    <ul>     
     <li>Časový limit: Custom limit 14 8 2, či vyšší dle dohody, se simultánními tahy minimálně do 127.</li>
     <li>Mapa: random, L s podzemím, voda žádná, neutrálové strong, povolena POUZE NEJRYCHLEJŠÍ cesta.</li>
     <li>Obtížnost: expert (160%)</li>
     <li>Tournament rules - zapnuto.</li>     
     <li>Výběr města - Potřeba zapnout v záložce výběru města vpravo PVP OPTIONS.</li> 
     <li>Hostující hráč klikne na RANDOM TOWN - systém vybere například Necropolis. POkud oba hráči se hraním necropolis souhlasí, oba napíší „+“. </li>
     <li>Pokud jeden z nich nesouhlasí, může požádat o zabanování 1 města a reroll (znovuzvolení) města za 500 svého startovního zlata. Takto se pokračuje dokud se oba hráči neshodnou. Limit rerollů není omezen.</li>
     <li>Výběr červené barvy: Trade systémem (obchodování) </li>
     <li>Příklad tradování o barvu: Hrač 1 - obětuje 1000 startovního zlata, Hráč 2 - 1500, Hráč 1 - 1800, Hráč 2 - napíše „+“, což znamená, že přijímá nabídku druhého hráče, smíří se s modrou barvou, avšak obdrží 1800 startovního zlata druhého hráče.</li>
     <li>Výběr hrdinů: bez banu, templat některé hrdiny banuje automaticky. Modrý hráč vybírá hrdinu jako první.</li>
     <li>Hostující hráč upraví poměr startovního zlata pro oba hráče - započítá výběr barvy + rerolly.</li>
     <li>Výběr bonusu - vybírá červený hráč.</li> 
     <li>Start hry</li> 
     <li>Oba hráči mají právo na jeden restart 111, tedy 1. den hry. O případný restart žádá první červený hráč.</li> 
     </ul>

    Template Jebuss Cross nastavení hry + výběr měst a hrdinů a restarty:
    <ul>     
     <li>Časový limit: Custom limit 18 8 2, či vyšší dle dohody, se simultánními tahy do 121.</li>
     <li>Mapa: random, XL bez podzemí, voda žádná, neutrálové strong, povolena POUZE NEJRYCHLEJŠÍ cesta.</li>
     <li>Obtížnost: expert (160%)</li>
     <li>Tournament rules - zapnuto.</li>
     <li>Výběr města - Trade systém (obchodování) - Potřeba zapnout v záložce výběru města vpravo PVP OPTIONS.</li> 
     <li>Hostující hráč klikne na RANDOM VS RANDOM - systém vybere například NECROPOLIS vs CASTLE. Pokud oba hráči souhlasí, oba napíší „+“.</li>
     <li>Pokud jeden z nich nesouhlasí, může požádat o zabanování 1 města a reroll (znovuzvolení) měst. První reroll je zdarma, každý další pak stojí daného hráče 500 startovního zlata. Takto se pokračuje dokud se oba hráči neshodnou na dvojici měst. Limit rerollů není omezen.</li>
     <li>Následuje trade měst, tedy zvolení, který hráč bude hrát které město - v následujícím příkladu uveden trade CASTLE vs DUNGEON.</li>
     <li>Příklad tradu měst: Hráči budou platit za to, aby si mohli zvolit město. Hrač 1 - obětuje 1000 startovního zlata, Hráč 2 - 1500, Hráč 1 - 2000, Hráč 2 - 2100, Hráč 1 napíše „+“, což znamená, že přijímá nabídku druhého hráče, smíří se s tím, že hrad nezvolí, avšak obdrží 2100 startovního zlata druhého hráče. </li>
     <li>Výběr červené barvy: Trade systémem (obchodování).</li>
     <li>Příklad tradování o barvu: Hrač 1 - obětuje 500 startovního zlata, Hráč 2 - 700, Hráč 1 - 800, Hráč 2 - napíše „+“, což znamená, že přijímá nabídku druhého hráče, smíří se s modrou barvou, avšak obdrží 800 startovního zlata druhého hráče.</li>
     <li>Výběr hrdinů: bez banu.</li>
     <li>Hostující hráč upraví poměr startovního zlata pro oba hráče - započítá výběr hradů + barvy + rerolly.</li>
     <li>Výběr bonusu - vybírá červený hráč.</li> 
     <li>Start hry</li> 
     <li>Oba hráči mají právo buď na 2x restart 111, tedy 1. den hry, anebo 1x restart 112, tedy druhý den hry. O případný restart žádá první červený hráč.</li>
     <li>Hráč nesmí vstoupit do treasure zóny na středu mapy (např. užitím Andělských křídel, nebo kouzel Dimenzní brána a Let), pokud neporazil alespoň jednu stráž, která přechod do této zóny hlídá.</li>	 
     <li>Hráč nesmí vstoupit do vedlejší startovní zóně na mapě (např. užitím kouzla Dimenzní brána nebo Let), pokud neporazil alespoň jednu stráž, která přechod do této zóny hlídá, anebo nemá důkaz, že tuto stráž porazil soupeř.</li>
     </ul>

    Template MT_jebus nastavení hry + výběr měst a hrdinů a restarty:
    <ul>   
     <li>Časový limit: custom limit 18 8 2, či vyšší dle dohody, se simultánními tahy do 121.</li>
     <li>Mapa: random, L s podzemím, voda žádná, neutrálové strong, povolena POUZE NEJRYCHLEJŠÍ cesta.</li>
     <li>Obtížnost: expert (160%)</li>
     <li>Tournament rules - zapnuto.</li>     
     <li>Výběr města - Potřeba zapnout v záložce výběru města vpravo PVP OPTIONS.</li> 
     <li>Hostující hráč klikne na RANDOM TOWN - systém vybere například Necropolis. POkud oba hráči se hraním necropolis souhlasí, oba napíší „+“.</li>
     <li>Pokud jeden z nich nesouhlasí, může požádat o zabanování 1 města a reroll města za 500 svého startovního zlata. Takto se pokračuje dokud se oba hráči neshodnou. Limit rerollů není omezen. </li>
     <li>Výběr červené barvy: Trade systémem (obchodování).</li>
     <li>Příklad tradování o barvu: Hrač 1 - obětuje 1000 startovního zlata, Hráč 2 - 1500, Hráč 1 - 1800, Hráč 2 - napíše „+“, což znamená, že přijímá nabídku druhého hráče, smíří se s modrou barvou, avšak obdrží 1800 startovního zlata druhého hráče.</li>
     <li>Výběr hrdinů: Hráč který výhral tradování červené barvy může banovat 0-3 hrdinů. Modrý hráč vybírá hrdinu jako první.</li>
     <li>Hostující hráč upraví poměr startovního zlata pro oba hráče - započítá výběr barvy a rerolly.</li>
     <li>Výběr bonusu - vybírá červený hráč.</li> 
     <li>Start hry</li> 
     <li>Oba hráči mají právo na jeden restart 111, tedy 1. den hry. O případný restart žádá první červený hráč.</li>
     <li>Hráč nesmí vstoupit do treasure zóny na středu mapy (např. užitím kouzla Dimenzní brána nebo Let), pokud neporazil alespoň jednu stráž, která přechod do této zóny hlídá.</li>     
     </ul>
    </div>
      <h4>Kontumace neaktivních hráčů</h4>
      
    <div>
    <ul>
	<li>Každý zápas je třeba odehrát ve lhůtě 1 měsíce ode dne, kdy bylo hráči známo, kdo je jeho soupeř. Pro první kolo je tedy pro všechny hráče závazné odehrát do 1.8.2019 </li>
    <li>V případě že je jeden hráč aktivní a jeho soupeř není k sehnání, udělí se kontumační prohra neaktivnímu hráči.</li>
    <li>Po první kontumaci v horní části pavouka (winners bracket) se hráč přesouvá do spodní části pavouka (losers bracket), odkud může pokračovat.</li>
    <li>Kontumací zápasu vzniká vítězi právo na zápis výhry nad daným hráčem do ligy. Hráč, který se nechal vykontumovat je povinen svoji prohru potvrdit.</li>
    </ul>
	 </div>  

	<h1>Ceny turnaje</h1>

      <div>
      <h4>Hlavní turnaj</h4>
	  
      1. místo - 1250,- Kč<br/> 
      2. místo - 800,- Kč<br/>
      3. místo - 450,- Kč<br/>
	  <br/>
	  
      <h4>Doprovodný turnaj</h4>
	  
      1. místo - 200,- Kč<br/> 
      </div> 
	<h1>Přihlášky</h1>

    <ul>
     <li>Přihlášky pište do diskuse sekce turnaje na webu ligy, anebo organizátorovi turnaje (Scooby) na e-mail, Skype nebo Discord.</li>
     <li>Pro přihlášení do turnaje musíte být registrovaní v lize.</li>
     <li>Konec přihlášek je stanoven na 30.06.2019 23:59.</li>
    </ul> 
      <table style=\"border-spacing: 15pt;\">
      <td valign=\"top\">
      <h3><font color=white>Přihlášení hráči hlavního turnaje</h3>
      
    <p>
        1: <a href=http://heroes3.cz/hraci/detail.php?id=3113><font color=#ff6633>Scooby</font></a> - ELO k 1.7.2019 - 2033<br/>
		2: <a href=http://heroes3.cz/hraci/detail.php?id=2762><font color=#ff6633>maGe</font></a> - ELO k 1.7.2019 - 1902<br/>
		3: <a href=http://heroes3.cz/hraci/detail.php?id=5499><font color=#ff6633>Zetor</font></a> - ELO k 1.7.2019 - 1545<br/>
		4: <a href=http://heroes3.cz/hraci/detail.php?id=5481><font color=#ff6633>Ondřej Dušek</font></a> - ELO k 1.7.2019 - 1509<br/>
		5: <a href=http://heroes3.cz/hraci/detail.php?id=222><font color=#ff6633>H34D</font></a> - ELO k 1.7.2019 - 2267<br/>
		6: <a href=http://heroes3.cz/hraci/detail.php?id=5307><font color=#ff6633>naoblaku</font></a> - ELO k 1.7.2019 - 1742<br/>
		7: <a href=http://heroes3.cz/hraci/detail.php?id=5321><font color=#ff6633>Lord Slayer</font></a> - ELO k 1.7.2019 - 1808<br/>
		8: <a href=http://heroes3.cz/hraci/detail.php?id=3315><font color=#ff6633>BigLams</font></a> - ELO k 1.7.2019 - 1448<br/>
		9: <a href=http://heroes3.cz/hraci/detail.php?id=1122><font color=#ff6633>Vítek</font></a> - ELO k 1.7.2019 - 2062<br/>
		10: <a href=http://heroes3.cz/hraci/detail.php?id=5360><font color=#ff6633>Helmut</font></a> - ELO k 1.7.2019 - 1623<br/>
		11: <a href=http://heroes3.cz/hraci/detail.php?id=46><font color=#ff6633>Twister</font></a> - ELO k 1.7.2019 - 1794<br/>
		12: <a href=http://heroes3.cz/hraci/detail.php?id=5396><font color=#ff6633>Lukyn</font></a> - ELO k 1.7.2019 - 1657<br/>
		13: <a href=http://heroes3.cz/hraci/detail.php?id=5446><font color=#ff6633>Older Hero</font></a> - ELO k 1.7.2019 - 1601<br/>
		14: <a href=http://heroes3.cz/hraci/detail.php?id=823><font color=#ff6633>Karcma</font></a> - ELO k 1.7.2019 - 1791<br/>
		15: <a href=http://heroes3.cz/hraci/detail.php?id=343><font color=#ff6633>DT.Azgard</font></a> - ELO k 1.7.2019 - 2048<br/>
		16: <a href=http://heroes3.cz/hraci/detail.php?id=5515><font color=#ff6633>Mal1k</font></a> - ELO k 1.7.2019 - 1522 - diskvalifikován<br/>
		17: <a href=http://heroes3.cz/hraci/detail.php?id=5513><font color=#ff6633>Kingboost</font></a> - ELO k 1.7.2019 - 1522<br/>
		18. <a href=http://heroes3.cz/hraci/detail.php?id=5320><font color=#ff6633>Siska96</font></a> - ELO k 1.7.2019 - 1892<br/>
		19. <a href=http://heroes3.cz/hraci/detail.php?id=5322><font color=#ff6633>gabo</font></a> - ELO k 1.7.2019 - 1892<br/>
		20. <a href=http://heroes3.cz/hraci/detail.php?id=5149><font color=#ff6633>Liut</font></a> - ELO k 1.7.2019 - 1484<br/>
		21. <a href=http://heroes3.cz/hraci/detail.php?id=5361><font color=#ff6633>BeerWarrior</font></a> - ELO k 1.7.2019 - 2018<br/>
		22. <a href=http://heroes3.cz/hraci/detail.php?id=3486><font color=#ff6633>Houhou</font></a> - ELO k 1.7.2019 - 1759<br/>
		23. <a href=http://heroes3.cz/hraci/detail.php?id=200><font color=#ff6633>Empair</font></a> - ELO k 1.7.2019 - 1648<br/>
		24. <a href=http://heroes3.cz/hraci/detail.php?id=5271><font color=#ff6633>Bonca</font></a> - ELO k 1.7.2019 - 2105<br/>
		25. <a href=http://heroes3.cz/hraci/detail.php?id=3775><font color=#ff6633>Ester</font></a> - ELO k 1.7.2019 - 1894<br/>
		26. <a href=http://heroes3.cz/hraci/detail.php?id=2><font color=#ff6633>Lord.Alex</font></a> - ELO k 1.7.2019 - 2026<br/>
		27. <a href=http://heroes3.cz/hraci/detail.php?id=836><font color=#ff6633>Imposieble</font></a> - ELO k 1.7.2019 - 1992 (registrovaný po uzávěrce)<br/>
	</p>     
      </td></font>

      <td valign=\"top\">
      <h3><font color=white>Přihlášení hráči do turnaje nováčků</h3>
		1: <a href=http://heroes3.cz/hraci/detail.php?id=5499><font color=#ff6633>Zetor</font></a> - ELO k 1.7.2019 - 1545<br/>
		2: <a href=http://heroes3.cz/hraci/detail.php?id=5481><font color=#ff6633>Ondřej Dušek</font></a> - ELO k 1.7.2019 - 1509<br/>
		3: <a href=http://heroes3.cz/hraci/detail.php?id=5509><font color=#ff6633>Bjolda</font></a> - ELO k 1.7.2019 - 1522<br/>
		4: <a href=http://heroes3.cz/hraci/detail.php?id=5511><font color=#ff6633>ManikCZ</font></a> - ELO k 1.7.2019 - 1522<br/>
		5: <a href=http://heroes3.cz/hraci/detail.php?id=3315><font color=#ff6633>BigLams</font></a> - ELO k 1.7.2019 - 1448<br/>
		6: <a href=http://heroes3.cz/hraci/detail.php?id=5396><font color=#ff6633>Lukyn</font></a> - ELO k 1.7.2019 - 1657<br/>
		7: <a href=http://heroes3.cz/hraci/detail.php?id=5446><font color=#ff6633>Older Hero</font></a> - ELO k 1.7.2019 - 1601<br/>
		8. <a href=http://heroes3.cz/hraci/detail.php?id=200><font color=#ff6633>Empair</font></a> - ELO k 1.7.2019 - 1648<br/>
		9. <a href=http://heroes3.cz/hraci/detail.php?id=5358><font color=#ff6633>TomikuS</font></a> - ELO k 1.7.2019 - 1510<br/>
		10. <a href=http://heroes3.cz/hraci/detail.php?id=3758><font color=#ff6633>uniqfazer</font></a> - ELO k 1.7.2019 - 1757<br/>
		11. <a href=http://heroes3.cz/hraci/detail.php?id=2755><font color=#ff6633>crossguard</font></a> - ELO k 1.7.2019 - 1511<br/>
		12. <a href=http://heroes3.cz/hraci/detail.php?id=4912><font color=#ff6633>Harpner</font></a> - ELO k 1.7.2019 - 1521<br/>
      </td>
      </tr></table></font>
";

echo "
<ul>Odkaz na pavouka v systému challonge: <a href=https://challonge.com/ScoobyWorldRulesTournamentHeroes3><font color=#ff6633>https://challonge.com/ScoobyWorldRulesTournamentHeroes3</font></a>
</ul>
	
<center><font size=3><h3>Velké finále</h3>
<table border=\"1\">
  <tbody>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><font color=white>H34D</font></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>1:0, ?</font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><font color=white>Scooby</font></td>
    </tr>
  </tbody>
</table></font></center>

<br>Před velkým finále Scooby odmítl nastoupit na zápas dle stanovených pravidel, jednostranně si z pozice organizátora změnil pravidla simultánních tahů na mapě h3dm1 ve svůj prospěch, načež H34D odmítl na takovou změnu přistoupit. Scooby později vyplatil hráči H34D cenu za 1. místo.

<center><h4>Winners bracket</h4></center>
<table border=\"1\">
  <tbody>
    <tr>
      <th style=\"color: rgb(255, 112, 0);\">1. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">2. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">3. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">4. kolo</th>
	  <th style=\"color: rgb(255, 112, 0);\">5. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">Vítěz WB</th>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=222><font color=#ff6633>H34D</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>H34D</font><br>
      </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\"><font color=white>H34D</font><br><a href=\"".$ROOT_URL."/hry/?detail=17213\"><font color=#ff6633>1:0</font></a><br>
      </td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\"><font color=white>H34D<br><i>kontumace</i></font><br><br>
      </td>
      <td rowspan=\"16\" style=\"width: 100pt;\" align=\"center\"><font color=white>H34D</font><br><a href=\"".$ROOT_URL."/hry/?detail=17573\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=17669\"><font color=#ff6633>2:0</font></a><br>
      </td>
	  <td rowspan=\"32\" style=\"width: 100pt;\" align=\"center\"><font color=white>H34D<br>2:0</font>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><font color=white><i>- free win -</i></font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=823><font color=#ff6633>Karcma</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>Karcma</font><br><a href=\"".$ROOT_URL."/hry/?detail=17175\"><font color=#ff6633>1:0</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=5396><font color=#ff6633>Lukyn</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=2762><font color=#ff6633>maGe</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>maGe<br><a href=\"".$ROOT_URL."/hry/?detail=17207\"><font color=#ff6633><i>kontumace</i></font></a>
  </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\"><font color=white>Ester</font><br><a href=\"".$ROOT_URL."/hry/?detail=17233\"><font color=#ff6633>0:1</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=5320><font color=#ff6633>Siska96</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=3775><font color=#ff6633>Ester</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>Ester<br><i>kontumace</i></font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=3315><font color=#ff6633>BigLams</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=343><font color=#ff6633>DT.Azgard</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>DT.Azgard</font><br></td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\"><font color=white>DT.Azgard<br><a href=\"".$ROOT_URL."/hry/?detail=17463\"><font color=#ff6633>1:0</font></a></font><br></td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\"><font color=white>Scooby<br><i>kontumace</i></font><br></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><font color=white><i>- free win -</i></font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=5322><font color=#ff6633>gabo</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>Lord Slayer<br><i>kontumace</i></font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=5321><font color=#ff6633>Lord Slayer</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=3113><font color=#ff6633>Scooby</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>Scooby</font><br><a href=\"".$ROOT_URL."/hry/?detail=17129\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=17131\"><font color=#ff6633>2:0</font></a><br></td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\"><font color=white>Scooby</font><br><a href=\"".$ROOT_URL."/hry/?detail=17185\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=17191\"><font color=#ff6633>2:0</font></a><br></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=836><font color=#ff6633>Imposieble</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=5515><font color=#ff6633>Mal1k</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>Helmut<br><i>diskvalifikace</i></font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=5360><font color=#ff6633>Helmut</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=5271><font color=#ff6633>Bonca</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>Bonca</font><br>
      </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\"><font color=white>Kingboost<br><i>kontumace</i></font></td></td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\"><font color=white>Kingboost<br><i>kontumace</i></font><br></td>
      <td rowspan=\"16\" style=\"width: 100pt;\" align=\"center\"><font color=white>Kingboost<br>1:0</font><br><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><font color=white><i>- free win -</i></font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=200><font color=#ff6633>Empair</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>Kingboost<br><i>kontumace</i></font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=5513><font color=#ff6633>Kingboost</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=5361><font color=#ff6633>BeerWarrior</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>BeerWarrior<br><i>kontumace</i></font><br>
  </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\"><font color=white>BeerWarrior<br><i>kontumace</i></font><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=5481><font color=#ff6633>Ondřej Dušek</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=5307><font color=#ff6633>naoblaku</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>Zetor<br><i>kontumace</i></font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=5499><font color=#ff6633>Zetor</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=1122><font color=#ff6633>Vítek</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>Vítek</font><br></td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\"><font color=white>Houhou<br><i>kontumace</i></font></td><br></td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\"><font color=white>Older Hero<br><i>kontumace</i></font><br></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><font color=white><i>- free win -</i></font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=3486><font color=#ff6633>Houhou</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>Houhou<br><i>kontumace</i></font><br></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=5149><font color=#ff6633>Liut</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=2><font color=#ff6633>Lord.Alex</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>Lord.Alex</font><br></td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\"><font color=white>Older Hero</font><br><a href=\"".$ROOT_URL."/hry/?detail=17169\"><font color=#ff6633>0:1</font></a><br></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><font color=white><i>- free win -</i></font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=5446><font color=#ff6633>Older Hero</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\"><font color=white>Older Hero</font><br><a href=\"".$ROOT_URL."/hry/?detail=17111\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=17151\"><font color=#ff6633>2:0</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><a href=http://heroes3.cz/hraci/detail.php?id=46><font color=#ff6633>Twister</font></a></td>
    </tr>	
  </tbody>
</table>
<br/><br/>

<center><h4>Losers Bracket</h4></center>
<table border=\"1\">
  <tbody>
    <tr>
      <th style=\"color: rgb(255, 112, 0);\">1. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">2. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">3. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">4. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">5. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">6. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">7. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">finále LB</th>
      <th style=\"color: rgb(255, 112, 0);\">vítěz LB</th>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white><i>- free win -</i></font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Lord Alex</font></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>Lord Alex <i>kontumace</i></font></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>DT.Azgard</font></td>   
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\"><font color=white>Lord Alex</font></br><a href=\"".$ROOT_URL."/hry/?detail=17725\"><font color=#ff6633>0:1</font></a></td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\"><font color=white>Older Hero</font></td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\"><font color=white>Imposieble</font><br><a href=\"".$ROOT_URL."/hry/?detail=17777\"><font color=#ff6633>0:1</font></a></td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\"><font color=white>Kingboost</font></td>
      <td rowspan=\"16\" style=\"width: 70pt;\" align=\"center\"><font color=white>Scooby<br>1:2</font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Lukyn</font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Lukyn</font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>siska96</font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Vítek</font></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>siska96</br><i>kontumace</i></font><</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>Lord Alex <i>kontumace</i></font></td>
		</tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>BigLams</font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>siska96 <i>kontumace</i></font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white><i>- free win -</i></font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Zetor</font></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>Zetor <i>kontumace</i></font></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>Ester</font></td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\"><font color=white>Imposieble<br><i>kontumace</i></font><br></td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\"><font color=white>Imposieble</font><br><a href=\"".$ROOT_URL."/hry/?detail=17763\"><font color=#ff6633>0:1</font></a></td>
      <span
 style=\"color: rgb(255, 102, 0);\"></span></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Gabo</font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Gabo</font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Imposieble</font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Bonca</font></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>Imposieble <i>kontumace</i></font></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>Imposieble <i>kontumace</i></font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Mal1k</font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Imposieble <i>kontumace</i></font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white><i>- free win -</i></font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Helmut</font><br></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>Helmut <i>kontumace</i></font></td>    
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>Houhou</font></td>     
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\"><font color=white>Lord Slayer</font></br><a href=\"".$ROOT_URL."/hry/?detail=17637\"><font color=#ff6633>0:1</font></a></td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\"><font color=white>Scooby</font></td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\"><font color=white>Scooby</font></br><a href=\"".$ROOT_URL."/hry/?detail=17835\"><font color=#ff6633>1:0</font></a></td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\"><font color=white>Scooby</font></br><a href=\"".$ROOT_URL."/hry/?detail=17887\"><font color=#ff6633>0:1</font></a></td>		
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Empair</font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Empair</font></tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Ondřej Dušek</font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Lord Slayer</font></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>Lord Slayer <i>kontumace</i></font></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>Lord Slayer</font></br><a href=\"".$ROOT_URL."/hry/?detail=17521\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=17525\"><font color=#ff6633>0:2</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Naoblaku</font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Naoblaku <i>kontumace</i></font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white><i>- free win -</i></font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>maGe</font></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>maGe<br><i>kontumace</i></font></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>BeerWarrior</i></font></td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\"><font color=white>maGe<br><i>kontumace</i></font></td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\"><font color=white>Lord Slayer<br><i>kontumace</i></font></td>
      <span
 style=\"color: rgb(255, 102, 0);\"></span></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Liut</font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Liut</font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white><i>- free win -</i></font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Karcma</font></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>Karcma</font></br><a href=\"".$ROOT_URL."/hry/?detail=17261\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=17341\"><font color=#ff6633>1:1</font></a>, <font color=white><i>(k)</i></font></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\"><font color=white>maGe</font></br><a href=\"".$ROOT_URL."/hry/?detail=17419\"><font color=#ff6633>1:0</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Twister</font></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><font color=white>Twister</font></td>
    </tr>
	
  </tbody>
</table>

<h3>Doprovodný turnaj nováčků - skupina každý s každým</h3>

<table border=1>
<tbody>
<tr>
<td>&nbsp;</td>
<td width=7.75%><a href=http://heroes3.cz/hraci/detail.php?id=5499><font color=#ff6633>Zetor</font></a></td>
<td width=7.75%><a href=http://heroes3.cz/hraci/detail.php?id=5481><font color=#ff6633>Ondřej Dušek</font></a></td>
<td width=7.75%><a href=http://heroes3.cz/hraci/detail.php?id=5509><font color=#ff6633>Bjolda</font></a></td>
<td width=7.75%><a href=http://heroes3.cz/hraci/detail.php?id=5511><font color=#ff6633>ManikCZ</font></a></td>
<td width=7.75%><a href=http://heroes3.cz/hraci/detail.php?id=3315><font color=#ff6633>BigLams</font></a></td>
<td width=7.75%><a href=http://heroes3.cz/hraci/detail.php?id=5396><font color=#ff6633>Lukyn</font></a></td>
<td width=7.75%><a href=http://heroes3.cz/hraci/detail.php?id=5446><font color=#ff6633>Older Hero</font></a></td>
<td width=7.75%><a href=http://heroes3.cz/hraci/detail.php?id=200><font color=#ff6633>Empair</font></a></td>
<td width=7.75%><a href=http://heroes3.cz/hraci/detail.php?id=5358><font color=#ff6633>TomikuS</font></a></td>
<td width=7.75%><a href=http://heroes3.cz/hraci/detail.php?id=3758><font color=#ff6633>uniqfazer</font></a></td>
<td width=7.75%><a href=http://heroes3.cz/hraci/detail.php?id=2755><font color=#ff6633>crossguard</font></a></td>
<td width=7.75%><a href=http://heroes3.cz/hraci/detail.php?id=4912><font color=#ff6633>Harpner</font></a></td>
</tr>
<tr>
<td><a href=http://heroes3.cz/hraci/detail.php?id=5499><font color=#ff6633>Zetor</font></a></td>
<td><center>X</center></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17155\"><font color=#ff6633>1:0</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17153\"><font color=#ff6633>0:1</font></center></td>
<td>?</td>
</tr>
<tr>
<td><a href=http://heroes3.cz/hraci/detail.php?id=5481><font color=#ff6633>Ondřej Dušek</font></a></td>
<td>?</td>
<td><center>X</center></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17305\"><font color=#ff6633>0:1</font></center></td>
<td>?</td>
<td>?</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17179\"><font color=#ff6633>0:1</font></center></td>
<td>?</td>
<td>?</td>
</tr>
<tr>
<td><a href=http://heroes3.cz/hraci/detail.php?id=5509><font color=#ff6633>Bjolda</font></a></td>
<td>?</td>
<td>?</td>
<td><center>X</center></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17183\"><font color=#ff6633>1:0</font></center></td>
<td>?</td>
<td>?</td>
</tr>
<tr>
<td><a href=http://heroes3.cz/hraci/detail.php?id=5511><font color=#ff6633>ManikCZ</font></a></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td><center>X</center></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17181\"><font color=#ff6633>0:1</font></center></td>
<td>?</td>
<td>?</td>
</tr>
<tr>
<td><a href=http://heroes3.cz/hraci/detail.php?id=3315><font color=#ff6633>BigLams</font></a></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td><center>X</center></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail==17147\"><font color=#ff6633>0:1</font></center></td>
<td>?</td>
<td>?</td>
</tr>
<tr>
<td><a href=http://heroes3.cz/hraci/detail.php?id=5396><font color=#ff6633>Lukyn</font></a></td>
<td>&nbsp;</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td><center>X</center></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
</tr>
<tr>
<td><a href=http://heroes3.cz/hraci/detail.php?id=5446><font color=#ff6633>Older Hero</font></a></td>
<td>?</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17305\"><font color=#ff6633>1:0</font></center></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td><center>X</center></td>
<td>?</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17321\"><font color=#ff6633>1:0</font></center></td>
<td>?</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17227\"><font color=#ff6633>1:0</font></center></td>
<td>?</td>
</tr>
<tr>
<td><a href=http://heroes3.cz/hraci/detail.php?id=200><font color=#ff6633>Empair</font></a></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td><center>X</center></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
</tr>
<tr>
<td><a href=http://heroes3.cz/hraci/detail.php?id=5358><font color=#ff6633>TomikuS</font></a></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17321\"><font color=#ff6633>0:1</font></center></td>
<td>?</td>
<td><center>X</center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17127\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17177\"><font color=#ff6633>0:1</font></center></td>
<td>?</td>
</tr>
<tr>
<td><a href=http://heroes3.cz/hraci/detail.php?id=3758><font color=#ff6633>uniqfazer</font></a></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17155\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17179\"><font color=#ff6633>1:0</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17183\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17181\"><font color=#ff6633>1:0</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail==17147\"><font color=#ff6633>1:0</font></center></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail==17127\"><font color=#ff6633>1:0</font></center></td>
<td><center>X</center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17123\"><font color=#ff6633>1:0</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17167\"><font color=#ff6633>1:0</font></center></td>
</tr>
<tr>
<td><a href=http://heroes3.cz/hraci/detail.php?id=2755><font color=#ff6633>crossguard</font></a></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17153\"><font color=#ff6633>1:0</font></center></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17227\"><font color=#ff6633>0:1</font></center></td>
<td>?</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17177\"><font color=#ff6633>1:0</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17123\"><font color=#ff6633>0:1</font></center></td>
<td><center>X</center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17221\"><font color=#ff6633>0:1</font></center></td>
</tr>
<tr>
<td><a href=http://heroes3.cz/hraci/detail.php?id=4912><font color=#ff6633>Harpner</font></a></td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td>?</td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17167\"><font color=#ff6633>0:1</font></center></td>
<td><center><a href=\"".$ROOT_URL."/hry/?detail=17221\"><font color=#ff6633>1:0</font></center></td>
<td><center>X</center></td>
</tr>
</tbody>
</table>

<h3>Doprovodný turnaj nováčků - pavouk pro top4</h3>
<table border=\"1\">
  <tbody>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">?</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">?</td>
	  <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">?</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">?</td>
    </tr>
	<tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">?</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">?</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">?</td>
    </tr>
  </tbody>
</table>
<br>

"; 
	  
echo "</div>";

ramecek_end(1024);



html_end();

?>
