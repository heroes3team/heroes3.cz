<?php


require('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

ramecek_start(1024, 'Speedy Gonzales Tour');

echo "<div style=\"margin-left: 10px;\">";
echo "<center><img style=\"width: 860px; height: 625px;\" alt=\"\" src=\"speedygonzales.jpg\" /></center>";
echo "

	<h1>Pravidla</h1>

      <h4>Systém turnaje</h4> 

      <div>
    <ul>
	  <li>Přihlášení hráči budou rozlosování do pavouka v systému challonge dle svého počtu ELO bodů v době začátku turnaje (01.03.2018).</li>
     <li>Pavouk ve formátu double elimination - po hlasování hráčů pouze na 1 vítěznou mapu, pokud se hráči v daném kole nedomluví jinak.</li>
     <li>Finále losers Bracket a velké finále se bude hrát na 2 vítězné zápasy.</li>
	 <li>Ve velkém finále má postupující z winners bracketu v první mapě výhodu spočívající v tom, že vybírá první barvu a následně může soupeři zakázat 3 města a 3 hrdiny.</li>
    </ul>
	 </div>
    
    <h4>Povinná pravidla</h4>

    <div>	
    <ul>
     <li>Časový limit: 1 minuta.</li>
     <li>Odehrané hry se ukládají bez zbytečného odkladu po jejich ukončení s vyznačením textu „Speedy Gonzales“ do komentáře. Soupeř je povinen hru potvrdit bez zbytečného odkladu.</li>
    </ul>
	 </div>

    <h4>Nepovinná pravidla platící, pokud se hráči výslovně nedohodnou jinak</h4>
    <div>

    Nastavení hry:
    <ul>
     <li>Hraje se datadisk Horn of the Abyss ve své aktuální verzi s HD+ modem s aktivovanými simultánními tahy.</li>
     <li>Mapa: random M s pozdemím, template random mapy 4sm3i, voda žádná, neutrálové strong, povoleny všechny cesty.</li>
     <li>Obtížnost: expert (160%)</li>
     <li>Tournament rules - vypnuto.</li>
	 <li>Hráči mohou kdykoliv požádat o přerušení simultánních tahů. V tom případě hráči dokončí aktuální tah, v kterém po načtení hry není dovoleno cokoli měnit. Výjimkou je uplatnění následujícího pravidla.</li>
	 <li>V případě, že modrý hráč neměl v simultánním tahu možnost reagovat na tah červeného, může požádat o restart svého dne.
	 Při restartu dne může modrý hráč změnit jen tu část svého tahu, která bezprostředně souvisí s jeho reakcí na tah červeného hráče.</li>
     <li>Před spuštěním hry si mohou hráči nastavit heslo, aby nedošlo k možnému zneužití savů před ukončením hry. Po hře je hráč povinen na požádání své heslo poskytnout rozhodčímu turnaje z důvodu případné kontroly fair play.</li>
	 </ul>

    Výběr měst a hrdinů:
    <ul>	 
     <li>Hráč s nižším aktuálním ELO vybírá první barvu. Následující hru se barvy střídají.</li>
     <li>Červený hráč vybírá město jako první. Modrý hráč červenému nejprve zakáže výběr jednoho startovního města. Po výběru startovního města, modrý hráč červenému zakáže používání jednoho hrdiny. Tento zakázaný hrdina se
	 v průběhu hry nesmí účastnit plánované bitvy se soupeřem, při které by měl při sobě více než cca 50% celkové armády hráče.</li>
     <li>Volba startovního města a hrdiny modrého hráče probíhá stejným způsobem.
     <li><i>Příklad: Modrý hráč zakazuje červenému Rampart, červený hráč volí jako startovní město Castle, modrý hráč zakazuje červenému používání Solmyra. Výsledek: červený hráč nemůže využít Solmyra jako hlavního hrdinu
	 ani v případě, že by jej v průběhu hry nakoupil v Taverně.
     <li>Příklad 2: Modrý hráč zakazuje červenému Dungeon, červený hráč volí jako startovní město Conflux, modrý hráč zakazuje červenému používání Grindana. Výsledek: červený hráč stále může využít Grindana jako startovního hrdinu, 
	 avšak nemůže se s ním účastnit plánované bitvy. To nemění nic na tom, že Grindan může být s jakýmkoli množstvím armády napaden soupeřem.</i>
    </ul>
	 
    Přerušení hry:
    <ul>
     <li>V případě, že je nutné hru přerušit, hráč pokud možno kompletně dokončí svůj tah.</li>
     <li>Za splnění výše uvedené podmínky hráči načítají již dokončený tah (např. autosave), v kterém není dovoleno cokoli měnit.</li>        
     <li>V případě náhlého odchodu hráče ze hry, např. vlivem technické chyby, či přerušení připojení k internetu, je tento hráč na požádání soupeře povinen ihned zaslat savy rozhodčímu, včetně aktuálního save „battle.gm2.“ 
	 Za tímto účelem je třeba mít v HD_Launcheru zaškrtnuto „save before each battle“ a „save all days (for tournaments)“</li> 
    </ul>
	 
    Omezení hry:
    <ul>
     <li>Pravidlo monsters (Fight Misplaced Guards) na předměty i cesty.</li>
     <li>Hráč nesmí pomocí diplomacie přijmout žádné neutrální jednotky do své armády.</li>        
     <li>Je zakázáno postavit svatostánek grálu.</li>
	 <li>Kouzlo Dimenzní brána je možno užít pouze 1x za den.</li>
     <li>Ve stejném městě jako je startovní město hráče nesmí hráč nakoupit bojovníky vyššího než 3. stupně. To neplatí, přijde-li hráč o své startovní město.</li>
	 </ul>
      
    </div>

      <h4>Reklamace mapy</h4>
    <div>
    <ul>
     <li>Hráč má nárok na jeden restart mapy v průběhu prvních dvou herních dnů bez udání důvodu.</li>
     <li>V případě, že hráč již vyčerpal svůj restart mapy a nová mapa je pro jeho herní pozici nehratelná, přičemž soupeř nesouhlasí s dalším restartem mapy, hráči bez zbytečného odkladu zašlou mapu k posouzení rozhodčím turnaje (H34D, maGe).</li>
	</ul>
	 </div>
    
      <h4>Kontumace neaktivních hráčů</h4>
      
    <div>
    <ul>
	<li>Pokud určitý zápas není odehrán v přiměřeném termínu a zjevně prodlužuje trvání celého turnaje, je udělena hráčům kontumační prohra.</li>
     <li>V případě že je jeden hráč aktivní a jeho soupeř není k sehnání, udělí se kontumační prohra jen neaktivnímu hráči.</li>
     <li>Po první kontumaci v horní části pavouka (winners bracket) se hráč přesouvá do spodní části pavouka (losers bracket), odkud může pokračovat.</li>	
    </ul>
	 </div>  

	<h1>Ceny turnaje</h1>

      <div>
    <ul>
      1. místo - 500,- Kč<br/> 
    </ul>
      </div> 
	
	<h1>Přihlášky</h1>

    <ul>
     <li>Přihlášky pište do diskuse, sekce turnaje na webu ligy, anebo organizátorovi turnaje (H34D) na Skype, či e-mail.<br/>
     <li>Pro přihlášení do turnaje musíte být registrovaní v lize.</li>
     <li>Konec přihlášek a začátek turnaje je plánovan na 01.03.2018.</li>
    </ul>
	 
      <table style=\"border-spacing: 15pt;\"><tr>
      <td valign=\"top\">
      <h3>Přihlášení hráči hlavního turnaje</h3>
      
      <p>
        1. <a href=http://heroes3.cz/hraci/detail.php?id=222><font color=#ff6633>H34D</font></a><br/>
		<s>2. <a href=http://heroes3.cz/hraci/detail.php?id=3486><font color=#ff6633>Houhou</font></a></s> (odhlášen)<br/>
		2. <a href=http://heroes3.cz/hraci/detail.php?id=5149><font color=#ff6633>Liut</font></a><br/>
		3. <a href=http://heroes3.cz/hraci/detail.php?id=7><font color=#ff6633>Tornádo</font></a><br/>
        4. <a href=http://heroes3.cz/hraci/detail.php?id=5307><font color=#ff6633>Naoblaku</font></a><br/>
		5. <a href=http://heroes3.cz/hraci/detail.php?id=5321><font color=#ff6633>Lord Slayer</font></a><br/>
        6. <a href=http://heroes3.cz/hraci/detail.php?id=5316><font color=#ff6633>ssgsd</font></a><br/>
        7. <a href=http://heroes3.cz/hraci/detail.php?id=3473><font color=#ff6633>Skalimse</font></a><br/>
		8. <a href=http://heroes3.cz/hraci/detail.php?id=105><font color=#ff6633>Jirkahron</font></a><br/>
		9. <a href=http://heroes3.cz/hraci/detail.php?id=519><font color=#ff6633>Pagi</font></a><br/>
		10. <a href=http://heroes3.cz/hraci/detail.php?id=5131><font color=#ff6633>Pepix</font></a><br/>
		11. <a href=http://heroes3.cz/hraci/detail.php?id=2762><font color=#ff6633>maGe</font></a><br/>
		12. <a href=http://heroes3.cz/hraci/detail.php?id=5358><font color=#ff6633>TomikuS</font></a><br/>
		13. <a href=http://heroes3.cz/hraci/detail.php?id=5271><font color=#ff6633>Bonca</font></a><br/>
		14. <a href=http://heroes3.cz/hraci/detail.php?id=2><font color=#ff6633>Lord.Alex</font></a><br/>
		15. <a href=http://heroes3.cz/hraci/detail.php?id=46><font color=#ff6633>Twister</font></a><br/>
		</p>
      
      </td>
      
      <td valign=\"top\">
      <h3>Nasazení do pavouka dle ELO k 01.03.2018</h3>
        1. <a href=http://heroes3.cz/hraci/detail.php?id=222><font color=#ff6633>H34D</font> (2247)</a><br/>
		2. <a href=http://heroes3.cz/hraci/detail.php?id=7><font color=#ff6633>Tornádo</font> (2184)</a><br/>
		3. <a href=http://heroes3.cz/hraci/detail.php?id=2762><font color=#ff6633>maGe</font> (2104)</a><br/>
		4. <a href=http://heroes3.cz/hraci/detail.php?id=5271><font color=#ff6633>Bonca</font>(2069)</a><br/>
		5. <a href=http://heroes3.cz/hraci/detail.php?id=519><font color=#ff6633>Pagi</font> (1937)</a><br/>
		6. <a href=http://heroes3.cz/hraci/detail.php?id=2><font color=#ff6633>Lord.Alex</font> (1892)</a><br/>
		7. <a href=http://heroes3.cz/hraci/detail.php?id=46><font color=#ff6633>Twister</font> (1829)</a><br/>
		8. <a href=http://heroes3.cz/hraci/detail.php?id=5131><font color=#ff6633>Pepix</font> (1821)</a><br/>
        9. <a href=http://heroes3.cz/hraci/detail.php?id=5316><font color=#ff6633>ssgsd</font> (1736)</a><br/>
        10. <a href=http://heroes3.cz/hraci/detail.php?id=3473><font color=#ff6633>Skalimse</font> (1720)</a><br/>
        11. <a href=http://heroes3.cz/hraci/detail.php?id=5307><font color=#ff6633>Naoblaku</font> (1701)</a><br/>
		12. <a href=http://heroes3.cz/hraci/detail.php?id=105><font color=#ff6633>Jirkahron</font> (1685)</a><br/>
		13. <a href=http://heroes3.cz/hraci/detail.php?id=5321><font color=#ff6633>Lord Slayer</font> (1659)</a><br/>
		14. <a href=http://heroes3.cz/hraci/detail.php?id=5358><font color=#ff6633>TomikuS</font> (1471)</a><br/>
		15. <a href=http://heroes3.cz/hraci/detail.php?id=5149><font color=#ff6633>Liut</font> (1437)</a><br/>
      </td> 
      
      <td valign=\"top\">
      <h5></h5>
              
      </td>       
      
      </tr></table>

";

echo "

</br>Odkaz na pavouka živě: <a href=http://challonge.com/8an7lw05>http://challonge.com/8an7lw05</a>

<center><font size=3><h3>Velké finále</h3>
<table border=\"1\">
  <tbody>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">maGe</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">maGe</br><a href=\"".$ROOT_URL."/hry/?detail=16482\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16483\"><font color=#ff6633>2:0</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Pepix</td>
    </tr>
  </tbody>
</table></font></center>

<center><h4>Winners bracket</h4></center>
<table border=\"1\">
  <tbody>
    <tr>
      <th style=\"color: rgb(255, 112, 0);\">1. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">2. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">3. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">4. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">Vítěz WB</th>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">H34D</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">H34D<br>
      </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">H34D</br><a href=\"".$ROOT_URL."/hry/?detail=16189\"><font color=#ff6633>1:0</font></a><br>
      </td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\">Bonca</br><a href=\"".$ROOT_URL."/hry/?detail=16192\"><font color=#ff6633>0:1</font></a><br>
      </td>
      <td rowspan=\"16\" style=\"width: 100pt;\" align=\"center\">maGe</br><a href=\"".$ROOT_URL."/hry/?detail=16376\"><font color=#ff6633>0:1</font></a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Pepix</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Pepix</br><a href=\"".$ROOT_URL."/hry/?detail=16144\"><font color=#ff6633>1:0</font></a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">ssgsd</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Bonca</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Bonca</br><a href=\"".$ROOT_URL."/hry/?detail=16151\"><font color=#ff6633>1:0</font></a><br>
  </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Bonca</br><a href=\"".$ROOT_URL."/hry/?detail=16169\"><font color=#ff6633>1:0</font></a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Lord Slayer</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Pagi</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Pagi<br><font color=#ff6633>kontumace</font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Jirkahron</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Tornádo</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Tornádo</br><a href=\"".$ROOT_URL."/hry/?detail=16143\"><font color=#ff6633>1:0</font></a><br>
      </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Twister</br><a href=\"".$ROOT_URL."/hry/?detail=16163\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16173\"><font color=#ff6633>0:2</font></a><br>
      </td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\">maGe</br><a href=\"".$ROOT_URL."/hry/?detail=16256\"><font color=#ff6633>0:1</font></a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Liut</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Twister</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Twister</br><a href=\"".$ROOT_URL."/hry/?detail=16148\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16149\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16150\"><font color=#ff6633>2:1</font></a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Skalimse</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">maGe</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">maGe</br><a href=\"".$ROOT_URL."/hry/?detail=16146\"><font color=#ff6633>1:0</font></a><br>
      </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">maGe</br><a href=\"".$ROOT_URL."/hry/?detail=16154\"><font color=#ff6633>1:0</font></a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">TomikuS</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Lord.Alex</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Naoblaku</br><a href=\"".$ROOT_URL."/hry/?detail=16141\"><font color=#ff6633>0:1</font></a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Naoblaku</td>
    </tr>
  </tbody>
</table>
<br/><br/>
<center><h4>Losers Bracket</h4></center>
<table border=\"1\">
  <tbody>
    <tr>
      <th style=\"color: rgb(255, 112, 0);\">1. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">2. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">3. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">4. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">5. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">6. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">Vítěz LB</th>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i>- free win -</i></td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Naoblaku<br></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">ssgsd<br><a href=\"".$ROOT_URL."/hry/?detail=16228\"><font color=#ff6633>0:1</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">H34D</td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">H34D<br><a href=\"".$ROOT_URL."/hry/?detail=16313\"><font color=#ff6633>1:0</font></a><br></td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Bonca<br></td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\">Pepix<br><a href=\"".$ROOT_URL."/hry/?detail=16403\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16404\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16445\"><font color=#ff6633>1:2</font></a><br><br></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">ssgsd</td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">ssgsd</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Lord Slayer</td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Tornádo</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Tornádo<br><a href=\"".$ROOT_URL."/hry/?detail=16264\"><font color=#ff6633>1:0</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Tornádo<br><a href=\"".$ROOT_URL."/hry/?detail=16270\"><font color=#ff6633>0:1</font></a></tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Jirkahron</td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Lord Slayer <a href=\"".$ROOT_URL."/hry/?detail=16255\"><font color=#ff6633>0:1</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Liut</td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Pagi</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Skalimse<br><a href=\"".$ROOT_URL."/hry/?detail=16209\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16247\"><font color=#ff6633>0:2</font></a><br></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Twister<br></td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Pepix<br><a href=\"".$ROOT_URL."/hry/?detail=16298\"><font color=#ff6633>0:1</font></a><br></td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Pepix<br><a href=\"".$ROOT_URL."/hry/?detail=16334\"><font color=#ff6633>0:1</font></a></td>
      
      <span
 style=\"color: rgb(255, 102, 0);\"></span></td>
    </tr>
    
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Skalimse</td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Skalimse <a href=\"".$ROOT_URL."/hry/?detail=16165\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16175\"><font color=#ff6633>0:2</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">TomikuS</td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Pepix<br></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Pepix<br><a href=\"".$ROOT_URL."/hry/?detail=16252\"><font color=#ff6633>1:0</font></a></td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Pepix<br><a href=\"".$ROOT_URL."/hry/?detail=16276\"><font color=#ff6633>0:1</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Lord.Alex</td>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">TomikuS <a href=\"".$ROOT_URL."/hry/?detail=16164\"><font color=#ff6633>1:0</font></a></td>
    </tr>
  </tbody>
</table>

"; 
	  
echo "</div>";

ramecek_end(1024);



html_end();

?>
