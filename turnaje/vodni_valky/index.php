<?php


require('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

ramecek_start(1024, 'Vodní Války');

echo "<div style=\"margin-left: 10px;\">";
echo "<img style=\"width: 500px; height: 300px;\" alt=\"\" src=\"vodni_valky_logo.jpg\" />";
?>
  
<div><h1>Pravidla</h1></div>


<big style="font-weight: bold;"><br />
Pravidla Turnaje Vodní války<br />
<br />
</big>Systém turnaje je klasický vyřazovací
pavouk. Počet kol se určí podle počtu
přihlášených. Počáteční
rozmístění hráčů v pavouku se určí tak, aby
byly obě poloviny každého (pod)pavouka vyrovnané.<br />
Na 1 a 2 kolo bude limit 2 týdny, na
následující kola 1 týden. Kdo nestihne
odehrát, má kontumační prohru. Jak už jste
poznali, můžeme se i domluvit že eventuelně můžete dohrát i 2 -
3 dny potom, ale nechci aby to bylo pravidlem.<big><small><span
 style="font-weight: bold;" /></small></big><big><small><br />
<br />
Přihlásit se mohou jen hráči co mají
odehráno více jak 10 her v lize. Takže ne, že se někdo
sotva zaregistruje a hned se přihlásí. Nemějte mi to za
zlé, ale už nechci problémy viz. Skalda, Dexter a
podobní výrostci.<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </small></big><big><small><span
 style="font-weight: bold;"><br />
</span></small></big><big><small><span style="font-weight: bold;">
</span></small></big><span style="font-weight: bold;">Turnajové
hry</span>
<p> Nastavení hry je následující: </p>
<ul>
  <li>mapa: náhodná L bez podzemí, množství
vody: ostrovy, síla bojovníků: vysoká</li>
  <li>časový limit: 4 minuty</li>
  <li>obtížnost: expert (160%)</li>
  <li>oba hráči si vyberou město dle svého
uvážení, hrdinu a bonus nechávají na
náhodným<br />
  </li>
</ul>
Hraje se bez diplomacie, bez grálu. Monsters na artefakty,
pandořiny skříňky, budovy a svitky.<br />
Odehrané duely se ukládají do záznamů, jako
každá jiná hra. Na začátek komentáře k
turnajové hře napište *TURNAJ - VV*. Odehrané
duely je
nutné nahlásit organizátorovi, aby mohli
být zapsány do pavouka.<br />
<span style="font-weight: bold;"><br />
Reklamace mapy</span><br />
<br />
<p>Hráč má nárok na reklamaci v případě: </p>
<ul>
  <li>má před městem silné jednotky, tím je mapa
nehratelná</li>
  <li>když hráči začínají s dvěma či více
městy, nebo jsou vesnice(chybí tvrz)</li>
  <li>když jsou města na špatném povrchu</li>
  <li>pokud u hradu chybí cesta</li>
  <li>pokud je mapa tzv. hradovka, je to pouze na hráčích
zda vymění nebo ne</li>
</ul>
V případě sporu rozhodne o hratelnosti/nehratelnosti mapy
nestranná porota složená z organizátora nebo
jímž pověřených asistentů. S jakýmkoliv dotazem
piště mě, nebo Lordu.Alexovi.<br />
<br />
<h4>Kontumace neaktivních hráčů</h4>
<p> Pokud se do konce termínu daného kola neozve ani
jeden z hráčů, kteří mají hrát turnajovou
hru, dostanou oba kontumační prohru. V případě že je
jeden hráč aktivní a jeho soupeř není k
sehnání, dostane kontumaci jen soupeř. V tom
případě musí být vidět snaha aktivního
hráče o sehnání soupeře. Aktivní
hráč by proto měl několik dní před termínem
upozornit organizátora nebo veřejnost v diskuzi. </p>
<h4>Nasazení náhradníků do turnaje</h4>
<p> Kdyby došlo k nezájmu některých
přihlášených hráčů a zájmu
nepřihlášených hráčů bude přidáno
toto speciální pravidlo:<br />
Poslední den prvního kola turnaje může
nepřihlášený hráč nahradit v turnaji
neaktivního přihlášeného hráče.<br />
Toto je možné provést za předpokladu, že: </p>
<ul>
  <li>hráč, který má být nahrazen
prokazatelně není schopen odehrát svůj zápas ve
stanoveném termínu</li>
  <li>volný soupeř neaktivního hráče a
příchozí náhradník se dokážou
dohodnout na zápase do konce termínu prvního kola
(po dohodě s organizátorem je možné udělat výjimku
a odehrát zápas o den později)</li>
</ul>
V tom případě bude náhradník zapsán do
turnaje jako plnohodnotný &uacute;častník na místo
vyřazeného neaktivního hráče.<br />
Pokud je náhradníků více než 1, tak
nasazováni bude chronologické dle přihlášek
do turnaje, samozřejmě roli bude hrát aktivnost a
dosavadní ELO.


<div><h1>Přihlášky</h1></div>


<p> Přihlášky pište Nivenovi na ICQ: 232-123-291.<br />
Konec přihlášek je v 31.1. 2011,
poté bude sestaven pavouk a od 1.2. se začne
hrát.<br />
Pro přihlášení do turnaje musíte být
registrovaní v lize a musíte mít odehráno
minimálně 10 ligových her.<br />
</p>
<table style="width: 425px; height: 330px;">
  <tbody>
    <tr>
      <td valign="top">
      <h3>Přihlášení hráči</h3>
01: Niven<br />
02: Lord.Alex<br />
03: Imposieble<br />
04: Vítek<br />
05: LadinaS<br />
06: Spectral<br />
07: PetoPalo<br />
08: borosenior<br />
09: Magi<br />
10: Gaborn<br />
11: Siam<br />
12: Paolos<br />
13: acid<br />
14: hujerOZ<br />
15: Pagi<br />
16: Peterson<br />
17: red joc<br />
18: AresAlien<br />
19: ChuckNorris<br />
20: Pagy<br />
21: Kisane<br />
22: Phoenixisnc<br />
23: Nightwright<br />
24: Maras<br />
25: Macwar<br />
26: marekrocky<br />
27: CD lavino<br />
28: CD Cido<br />
29: Fuckinek<br />
30: SD<br />
31: DavidHakl<br />
32: Maniak </td>
      <td valign="top">
      <h5><big>Seřazeno podle ELO</big></h5>
01: Nightwright(2079)<br />
02: Pagi(2052)<br />
03: Vítek(2009)<br />
04: Imposieble(1994)<br />
05: Gaborn(1961)<br />
06: DavidHakl(1922)<br />
07: Maras(1899)<br />
08: Niven(1898)<br />
09: SD(1880)<br />
10: acid(1879)<br />
11: CD Cido(1876)<br />
12: Lord.Alex(1859)<br />
13: Maniak(1832)<br />
14: ChuckNorris(1831)<br />
15: CD lavino(1799)<br />
16: Peterson(1798)<br />
17: PetoPalo(1794)<br />
18: hujerOZ(1762)<br />
19: Siam(1737)<br />
20: borosenior(1648)<br />
21: Macwar(1664)<br />
22: Pagy(1643)<br />
23: red joc(1614)<br />
24: Kisane(1591)<br />
25: Magi(1552)<br />
26: LadinaS(1539)<br />
27: Paolos(1470)<br />
28: AresAlien(1453)<br />
29: marekrocky(1354)<br />
30: Spectral(1347)<br />
31: Fuckinek(1331)<br />
32: Phoenixisnc(1296) </td>
    </tr>
  </tbody>
</table>
<br />
Náhradníci: Gigiman, Afroman<br />

<div><h1>Průběh</h1></div>

1. kolo: &uacute;terý 1.2. - pátek 11.2.2011<br />




2. kolo: sobota 12.2. - pátek 25.2.2011<br />




3. kolo(čtvrtfinále): sobota 26.2. - pátek 4.3.2011<br />




4. kolo(semifinále): sobota 5.3. - pátek 11.3.2011<br />




5. kolo(boj o vítěze turnaje a o třetí místo):
sobota 12.3. - pátek 18.3.2011<br />




vyhlášení: sobota 19.3.2011<br />




<br />




Zde se můžete podívat na systém, který byl použit
pro tento turnaj: <a href="pavouk.php">pavouk</a><br />




<br />




<span style="font-weight: bold;">O 3.tí místo</span><br />




<span style="font-weight: bold;"><br />




</span>
<table border="1">




  <tbody>




    <tr>




      <td rowspan="1" style="width: 100pt;" align="center">Niven</td>




      <td rowspan="2" style="width: 100pt;" align="center">&nbsp;Niven<br />




      <a href="http://www.heroes3.cz/hry/?detail=6450&amp;page=1"><span style="color: rgb(255, 102, 0);">(6450)</span></a><br />
      <img style="width: 35px; height: 48px;" alt="" src="http://www.heroes3.cz/img/medals/t2_3.jpg" /><span style="color: rgb(255, 102, 0);"></span></td>




    </tr>




    <tr>




      <td rowspan="1" style="width: 100pt;" align="center">CD lavino</td>




    </tr>




  
  
  
  
  </tbody>
</table>




<br />




<span style="font-weight: bold;">Hlavní pavouk</span><br />




<br />




<?php require_once("vv_pavouk.php");?><br />




</div>




<br />

</div>

<?php
ramecek_end(1024);



html_end();

?>
