<?php


require('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

ramecek_start(1024, 'Second Vitek Tournament');

echo "<div style=\"margin-left: 10px;\">";

echo "

&nbsp;
	
  <center>
  
  <!--
  <object data=\"flash.swf\" type=\"application/x-shockwave-flash\" id=\"myflash\" width=\"1000\" height=\"253\">
<param name=\"movie\" value=\"flash.swf\" />
<param name=\"bgcolor\" value=\"#ffffff\" />
<param name=\"height\" value=\"1000\" />
<param name=\"width\" value=\"253\" />
<param name=\"quality\" value=\"high\" />
<param name=\"menu\" value=\"false\" />
<param name=\"allowscriptaccess\" value=\"samedomain\" />
<p>Adobe <a href=\"http://get.adobe.com/flashplayer/\">Flash Player</a> is required to view this content.</p>
</object>
-->

  <img alt='' src='logo8.gif' width='500' height='330' />
  <br />
  </center>
    <h1>Přihlášení do turnaje</h1>
    
    V případě zájmu účastnit se turnaje mě stačí kontaktovat na icq: 311-438-390, na mailu, popřípadě v diskuzi turnaje.
    Kadý hráč musí mít ve svém profilu kontakt, na kterém bude v období turnaje dostupný, nejlépe ICQ nebo GR.
    
    <h4>Začátek turnaje <h4> 
    
    Začátek kvalifikace: <br/>
    
    Začátek turnaje: 25.10.2015<br/>
    
    <h1>Pravidla<h1>
    
       <h4>Systém turnaje</h4>        
      <p>
        Prvních 8 přihlášených hráčů seřazených podle ELO automaticky postupuje do Hlavního pavouku.Ostatní hráči budou 
        bojovat o umístění v hlavním pavouku v kvalifikaci.Kvalifikační duely budou sestaveny podle přihlášených hráčů.
        Systém turnaje je pavouk s druhou šancí. Hráč, který prohraje v hlavním pavouku sestupuje do záchranného pavouka - dostává
        tedy v turnaji druhou šanci. Prohra v záchranném pavouku znamená konec hráče v turnaji. </li>
      </p>
               
        
    <h4>Turnajové hry</h4>
    
    <p>

    <h4>Nastavení hry je následující<h4>
    <ul>
     <li>mapa: random L bez podzemí, voda žádná, neutrálové strong </li>        
     <li>časový limit: 4 minuty pokud se hráči nedohodnou jinak(tzn. v případě,
     že se oba hráči zhodnou na jinem limitu napr. 2 minuty, můžou svou hru odehrat 
     s tímto limitem. V případě neshody obou stran na jiném limitu platí 4 minutový limit.) </li>
     <li>obtížnost: expert (160%)</li>
     <li>Startovací město si volí každý hráč sám. Pokud hráč zvolí jeden typ startovacího města například Bašta,
     nesmí v následujících dvou turnajových hrách opakovaně stejné město zvolit. To znamená, že může volit baštu 
     až následnou 3 turnajovou hru. Hrdinové i bonusy jsou nastaveny jako náhodné. Města Necropolis a Conflux jsou 
     v turnaji zakázány. V případě, že je získáte ve hře, můžete je využívat bez omezení. </li>
     <li>slabší hráč podle ELO hodnocení má taky právo zvolit si barvu.</li>
      </p>
    </ul>
    
    <h4>Omezení hry<h4>
    <ul>
    <li>pravidlo monsters, které platí pouze pro objekty (artefakty, budovy, svitky...) Monsters neplatí na cesty. </li>
    <li>diplomacie a grál jsou zakázány !  </li>
    <li>když soupeř najde druhý stejný hrad, tak v něm nesmí kupovat budovy na jednotky 3 level a výše. (pouze jednotky po druhý level) </li>
    <li>zákaz hit and run !!! Pokud útočící hráč použije na soupeře útočné kouzlo 4 a vyššího levelu, nesmí po dobu dvou odehraných kol z boje utéct, nebo se vzdát. </li>
    <li>Kouzla DD a fly ze svitku použít nesmíte. Pokud získáte kouzla z věží kouzel, nebo pandor, nebo díky artefaktům co jste poctivě získali, tak je můžete používat bez omezení. </li> 
    </ul>
    
    </p>
    <h4>Zápis hry<h4>
    </p>
    Odehrané duely se ukládají do záznamů, jako každá jiná hra. Na začátek komentáře k turnajové hře napište *TURNAJ-VITEK*. 
    <br/>
    Odehrané duely je nutné nahlásit organizátorovi pomocí  ICQ (Vitek), nebo mailem,  popřípadě v diskuzi turnaje, abych je zapsal do pavouka.
    <br/>
    Pokud to bude možné, snažte se odehrát hru nepřetržite, tzn. bez save-ů  
    </p>

      <h4>Reklamace mapy</h4>
    <p>
      Hráč má nárok na novou hru pouze v případě, že je mapa naprosto nehratelná, (tzn. například cestu k hradu blokuje tlupa Draků :-)) určitě nestačí na reklamaci nedostatek dolů.
      <br/>
      Dále je možné restartovat hru v případě, že v městech chybí tvrz, hráčí mají více měst, chybí cesta, města jsou na špatném povrchu apod. V případě sporu rozhodne o hratelnosti /nehratelnosti mapy nestranná porota z ligy na které se oba hráči dohodnou. Hráč, který spor rozhodne by měl mít odehráno více než 50 her na lize. </li> 
      <br/>
      </p>
      Pro porovnávání hráčů v sestavení pavouka je rozhodující ELO, s jakým jste se do turnaje zapsali. 
      <br/>
     
    
      <h4>Kontumace neaktivních hráčů.</h4>
      
    <p>
    
    Pokud se do konce termínu daného kola neozve ani jeden z hráčů, kteří mají hrát turnajovou hru, dostanou oba kontumační prohru. V případě že je jeden hráč aktivní a jeho soupeř není k sehnání,
     dostane kontumaci jen soupeř. V tom případě musí být vidět snaha aktivního hráče o sehnání soupeře. Aktivní hráč by proto měl několik dní před termínem upozornit organizátora i veřejnost v diskuzi. 
     
    
    </p>  
    
    <h4>Nasazení náhradníků do turnaje</h4>
    
    <p>
    
     V případě, že 2 dny před koncem daného kola se daná hrací dvojice stále nedohodla na termínu odehrání hry z důvodu nedosažitelnosti jednoho z hračů,
     bude do turnaje dosazen jeden z náhradníků a to v pořadí, ve kterém se do turnaje přihlásili. Nasazení náhradníků je možné jen v prvních dvou kolech 
     hlavního pavouka a max. do 3. kola záchranného pavouka.
     <br/>
    
    </p>
    <br/>
    
	<h1>Přihlášky</h1>   
      <table style=\"border-spacing: 15pt;\"><tr>
      <td valign=\"top\">
      <h3>Přihlášení hráči</h3>
      
    <p>
        1: Vitek (2217)<br/>
        2: Siam (1999) <br/>
        3: Pagi (2123) <br/>
        4: Twister (2044) <br/>
        5: Kisane (1928) <br/>
        6: Tomis (2176) <br/>
        7: Houhou (2410) - odstupuje <br/> 
        8: Imposieble (2416)<br/> 
        9: Macwar (1871)<br/>
       10: Mikeš (1758)<br/>
       11: KekelZpekel (2332) <br/> 
       12: Kotletka (1454) <br/>
       13: Bůh (1947) <br/>
       14: Nicitel Prdeli (1634)<br/>
       15: Ester (2294) <br/> 
       16: karcma (2249) <br/>
       17: Tygr Dřevák (1559) <br/>

        <br/>
<p>

       <h2> Náhradníci<h2>
<p>  <h5> <h5>
      1: Chachar <br/>
      2:<br/>
      3:<br/>
      </p>
      
      
      </td>

      
      <td valign=\"top\">
      <h3>Seřazeno podle ELO</h3>
      
      1: Imposieble (2416)<br/>
      2: Houhou (2410) - odstupuje <br/>
      3: KekelZpekel (2332) <br/>
      4: Ester (2294) <br/>
      5: karcma (2249) <br/>
      6: Vitek (2217) <br/>
      7: Tomis (2176) <br/>
      8: Pagi (2123) <br/>
      9: Twister (2044) <br/>
     10: Siam (1999) <br/>
     11: Bůh (1947) <br/>
     12: Kisane (1928) <br/>
     13: Macwar (1871) <br/>
     14: Mikeš (1758) <br/>
     15: Nicitel Prdeli (1634) <br/>
     16: Tygr Dřevák (1559) <br/>
     17: Kotletka (1454) <br/>

      </td>
   
      <td valign=\"top\">
      <h5></h5>
                 
      </td>   
      
      <td valign=\"top\">
      <h5></h5>
      
                    
      </td>       
      
      
      
      </tr></table>


	<h1>Průběh</h1>

   <p>
      Kvalifikace<br/>
      1. kolo:<br/>
      2. kolo:<br/>
      3. kolo:<br/>
      4. kolo:<br/>
      
      </p> 
      

      <h3>o třetí místo</h3>";
  
       echo pavouk(2, array(
        array("", "<br/><a href=\"".$ROOT_URL."/hry/?detail=5044\""),
        array("")
        ), 0);
      
      ?>
      
   <h2>Hlavní pavouk</h2>
      
     <table border="1">
  <tbody>
    <tr>
      <th style="color: rgb(255, 112, 0);">1. kolo</th>
      <th style="color: rgb(255, 112, 0);">2. kolo</th>
      <th style="color: rgb(255, 112, 0);">Čtvrtfinále</th>
      <th style="color: rgb(255, 112, 0);">Semifinále</th>
      <th style="color: rgb(255, 112, 0);">Finále - první vítěz </th>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Imposieble</td>
      <td rowspan="2" style="width: 100pt;" align="center">Imposieble<p>
		<a href="http://heroes3.cz/hry/?detail=14165&page=1">14165</a></td>
      <td rowspan="4" style="width: 100pt;" align="center">&nbsp;</td>
      <td rowspan="8" style="width: 100pt;" align="center">&nbsp;</td>
      <td rowspan="16" style="width: 100pt;" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Kotletka</td>
    </tr>
    <tr>
        <td rowspan="1" style="width: 100pt;" align="center">Kisane</td>
      <td rowspan="2" style="width: 100pt;" align="center">Kisane<p>
		<a href="http://heroes3.cz/hry/?detail=14176&page=1">14176</a></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Tomis</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Karcma </td>
      <td rowspan="2" style="width: 100pt;" align="center">Karcma<p>
		<a href="http://heroes3.cz/hry/?detail=14174&page=1">14174</a></td>
      <td rowspan="4" style="width: 100pt;" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Siam </td>
      <td rowspan="2" style="width: 100pt;" align="center"><p>
		&nbsp;</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Mikeš</td>
      <td rowspan="2" style="width: 100pt;" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">KekelZpekel</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Ester</td>
      <td rowspan="2" style="width: 100pt;" align="center">Ester<p>
		<a href="http://heroes3.cz/hry/?detail=14166&page=1">14166</a></td>
      <td rowspan="4" style="width: 100pt;" align="center">&nbsp;</td>
      <td rowspan="8" style="width: 100pt;" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Macwar</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Bůh </td>
      <td rowspan="2" style="width: 100pt;" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Vitek </td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Pagi </td>
      <td rowspan="2" style="width: 100pt;" align="center">&nbsp;</td>
      <td rowspan="4" style="width: 100pt;" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Twister</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Nicitel Prdeli</td>
      <td rowspan="2" style="width: 100pt;" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 100pt;" align="center">Tygr Dřevák</td>
    </tr>
  </tbody>
</table>
<br>
</br>

     
     <h2>Záchranný pavouk - Poražený vypadává z turnaje.</h2>
     </p>
</br>
<table border="1">
  <tbody>
    <tr>
       <th style="color: rgb(255, 112, 0);" align="center"></td>1. kolo </th> 
      <th style="color: rgb(255, 112, 0);" align="center"></td>2. kolo</th>
      <th style="color: rgb(255, 112, 0);" align="center"></td>Čtvrtfinále(1)</th>
      <th style="color: rgb(255, 112, 0);" align="center"></td>Čtvrtfinále(2)</th>
      <th style="color: rgb(255, 112, 0);" align="center"></td>Semifinále(1) </th>
      <th style="color: rgb(255, 112, 0);" align="center"></td>Semifinále(2) </th>
      <th style="color: rgb(255, 112, 0);" align="center"></td>Finále </th>

    </tr>
  

   </tr>
    <tr>
      <td rowspan="1" style="width: 90pt;" align="center">Kotletka</td>
      <td rowspan="1" style="width: 90pt;" align="center">&nbsp;</td>
      <td rowspan="2" style="width: 90pt;" align="center">&nbsp;</td>
      
      <td rowspan="2" style="width: 90pt;" align="center"></td>
      
      <td rowspan="4" style="width: 90pt;" align="center">
		 &nbsp;</td>
       <td rowspan="4" style="width: 90pt;" align="center">&nbsp;</td>
      <td rowspan="8" style="width: 90pt;" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 90pt;" align="center">Tomis</td>
      <td rowspan="1" style="width: 90pt;" align="center"></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 90pt;" align="center">Siam</td>
      <td rowspan="1" style="width: 90pt;" align="center">&nbsp;</td>
      <td rowspan="2" style="width: 90pt;" align="center">&nbsp;</td>
  <td rowspan="2" style="width: 90pt;" align="center">&nbsp;</tr>
    <tr>
      <td rowspan="1" style="width: 90pt;" align="center"></td>
      <td rowspan="1" style="width: 90pt;" align="center"></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 90pt;" align="center">Macwar<td rowspan="1" style="width: 90pt;" align="center">&nbsp;</td>
      <td rowspan="2" style="width: 90pt;" align="center"> &nbsp;</td>
    <td rowspan="2" style="width: 90pt;" align="center">&nbsp;</td>
      <td rowspan="4" style="width: 90pt;" align="center">&nbsp;</td>
<td rowspan="4" style="width: 90pt;" align="center"></td>
      
      <span
 style="color: rgb(255, 102, 0);"></span></td>
    </tr>
    
    <tr>
      <td rowspan="1" style="width: 90pt;" align="center"></td>
      <td rowspan="1" style="width: 90pt;" align="center"></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 90pt;" align="center"></td>
      <td rowspan="1" style="width: 90pt;" align="center">&nbsp;</td>
      <td rowspan="2" style="width: 90pt;" align="center">&nbsp;</td>
        <td rowspan="2" style="width: 90pt;" align="center"></td>
    </tr>
    <tr>
      <td rowspan="1" style="width: 90pt;" align="center"></td>
      <td rowspan="1" style="width: 90pt;" align="center"></td>
    </tr>
  </tbody>
</table>
<br>





     
      
   
    </div>    
  </body>
</html>

     
     
     
     
     
     
     

      <div>
	<br/>
      Kvalifikace:<br/>
      1.
      2. 
      3.
      4.  
      5. 
    

<?php
echo "</div>";

ramecek_end(1024);



html_end();

?>
