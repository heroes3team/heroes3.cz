<?php


require('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

ramecek_start(1024, 'World Cup 2012');

echo "<div style=\"margin-left: 10px; margin-right: 15px;\">";

echo"
<div> &nbsp;</div>
<h1>Pravidla</h1>
<h2>Obecně</h2>

<p>
Jde o mezinárodní turnaj pořádaný ruským organizátorem s přezdívkou StickyFingaz (icq: 465786677)
Turanje ze zúčastní 32 hráčů z různých zemí, prý tam mají být minilálně Rusové, Poláci, číňani, češi, Bulhaři, možná Korejci,
přičemž my jako česká Liga můžeme do turnaje vyslat 3 hráče. Takže se přihlašujte u mě na icq nebo mailu a pokud bude hodně zájemců,
tak se uspořádá kvalifikační turnaj. Zájemci by měli umět anglicky, aby se dorozuměli s organizátory a se soupeři.
Navíc upozorňuji, že např. Poláci mají ve zvyku hrát s limitem 8 minut na kolo, tak s tím raději předem počítejte, ať se pak
zbytečně neodhlašujete. Turnaj začne na začátku března, do té doby musíme vybrat účastníky. Systém turnaje bude prý pavouk
s druhou šancí (každý může 1x prohrát než vypadne). Celé by to pak mělo být přehledně zveřejněno na nějaké stránce, která zatím není k dizpozici.
Mapy budou zřejmě randomy se šablonou, viz pravidla níže.

<br/><br/>Tomis
</p>

<h2>Pravidla</h2>

<p>
<h4>FMG - Fight Misplaced Guards</h4>
\"Misplaced guard\" is a guard (monster) that is guarding object or zone border and placed in a way that you can pick up object or travel between zones without fighting - this is error of random map generator. On Random Maps, ALL misplaced guards MUST be fought (this means you may not walk around a creature in order to pick up a Pandora's Box, a Treasure, Artifact, Utopia or other object as well as you can't walk past the guards for moving between zones). In case there are multiple misplaced guards at zone border, you must fight \"1\" of the stacks (any). Also, you may not enter another zone if there is definitely misplaced mountain or other object that allows you to pass without fighting any guard. Notes: Guards near resource mines are considered to guard only mine itself - you may pick up resources near mine without fighting guard. On Balance template there are few unguarded zone borders, so you are allowed to pass there. If zone has several separated entries (each is guarded) and if player killed 1 guard (cleared entry #1), he may not enter another entry through misplaced guard. In case there are multiple misplaced guards AND 1 player has already killed 1 guard AND second player have seen this breaking (hero movement) - then second player may enter this zone without fighting rest of guards. Else, must fight 1 guard. In case if resource or treasure chest is near misplaced guard and if it's not clear that it should be guarded - then players are allowed to pick it
</p>

<p>
<h4>No First Day (Red) Rush</h4>
 In the first day of a week, red player (or any with a turn before another player) is not allowed to use population from the current week against any units or flagged locations (non-guarded included) of an opponent who has not had a chance to move this turn. Notice: This rule also applies to blocking any passages with the new population which would stop the opponent from delivering his or her army; In case a player fought his or her way inside the opponent's zone using the new population, he or she is not allowed to take any flagged locations even with the old army; In case a player used his or her old army only to gain entrance to an opponent's zone, he or she is free to attack anything; New heroes are not considered a new army as long as they have only a single level 1 unit with them.
</p>

<p>
<h4>No Hit & Run</h4>
 The attacking hero has no right to retreat/surrender in the first round of the fight if he casted any magic. The defending hero is not affected by this restriction.
</p>

<p>
<h4>No diplomacy, no joiners</h4>
 Diplomacy may only be used to gain earlier access to Library of Enlightenment and to reduce the price of surrendering; It is forbidden to accept any neutral units (wandering monsters) into the army (does not include units gained as Pandora's Box and Quest Hut reward). You can use Refugee Camp.
</p>

<p>
<h4>Forbidden Heroes</h4>
 Sir Mullich, Logistics specialists (Kyrre, Gunnar, Dessa),  4lv spells specialists (Aislinn, Alamar, Jeddite, Solmyr, Deemer, Loynis) should be dismissed immediately when they reach 5 level (Coronius and Xyron are excluded); Isra and Vidomina can take expert Necromancy on level-ups only as the last skill (when there is no other choice).
</p>

<p>
<h4>No grail</h4>
 It is forbidden to build Grail structures in any town on the map.
</p>

<p>
<h4>DD, Fly, TP</h4>
 Players are not allowed to use the Fly, Dimension Door, Town Portal on Jebus Cross, Balance and 2sm4d3 templates. On 8mm6a and 8xm8 templates you can use DD twice a day on expert air magic and only once otherwise.
</p>

<p>
<h4>No Hill Fort, Cartographer or Trade Center</h4>
 It is forbidden to use Hill Forts, Cartographers or Trade Centers on the map.
</p>

<p>
<h4>Forbidden Artefacts</h4>
 You can't use Angel's Wing on Jebus Cross, Balance and 2sm4d3 templates, Boots of Levitation and Armageddon spell wearing Armageddon's Blade. You can't pick Cloak of the Undead King.
</p>

<p>
<h4>No month creatures</h4>
 In case any week/month grants (by astrologists' proclamations) additional units to any player's starting town, the players have to either load last save of a previous week or strike a deal they will not be using those additional units.
</p>

<p>
<h4>Doublebuild</h4>
Allow.
</p>

<p>
<h4>7 level Dwellings</h4>
 You can use more than 1 dwelling.
</p>

<p>
<h4>Bank Upgrades</h4>
 Allow. You can upgrade Angles and Wywerns from conservatories and dragon fly hives.
</p>

<p>
<h4>No 4-5lvl Scrolls</h4>
 The players are not allowed to use 4-5 level spell scrolls regardless of the way they were obtained.
</p>

<p>
<h4>Necro and Flux restrictions</h4>
 Starting hero for these towns must be set to random. You can't build Necromancy amplifier and upgrade level 4 creatures in Necropolis; you can't upgrade level 1, 6 and 7 creatures in Conflux. Note: if computer builds any forbidden structure, you can't upgrade your units of forbidden type or buy new upgraded units of this type; you are free to use additional skeletons from necromancy bonus for amplifier.
</p>

<p>
<h4>Game Bugs</h4>
 The players are not allowed to exploit game bugs, i.e. reducing neutral parties (it is allowed to retreat/surrender/lose at important, unupgraded guardians only once - including neutral parties guarding zone entrances, Utopias, Pandora's Boxes with an upgraded stack within); It is forbidden to exploit Artifact Merchants bug, which enables a player to recruit units for the opponent's resources; Ghost hero is forbidden (a hero with no units).
</p>

<p>
<h4>Draw Conditions:</h4>
 If battle ends in a draw - then attacking player must flee (even if attacking player has better army). Example: attacking player has horde of Golems and can't kill 1 Ghost dragon which flies away). In \"shackles present during draw battle\" condition - players must attack each other each turn.
</p>

<p>
<h4>Town Choosing</h4>
 Players remove town by turns. When 3 towns remain, the first player chooses his town and the second player chooses his color. Necro and Conflux are allowed, but you can remove them in your turn if you don't like them. For example: StickyFingaz - Castle, Uzza - Rampart, S - Necropolis, U - Conflux, S - Stronghold, U - Fortress, so there are 3 towns left now - Inferno, Dungeon and Tower, S: I choose Dungeon, U: I choose Tower and red color.
</p>

<p>
<h4>Template Choosing</h4>
 Players remove templates by turns. There are 5 templates for tournament: 2sm4d(3), 8mm6a, Balance, Jebus Cross and 8xm8. Last template will be played. For example: StickyFingaz - 2sm4d(3), Uzza - 8mm6, S - 8xm8, U - Balance. So the last template - Jebus Cross will be played. Players first choose template and after it is chosen they choose castles.
</p>

<p>
<h4>Restarts</h4>
</p>

<p>
<h4>Obligatory restarts:</h4>
- if starting hero is not level 1;
- if player has no starting castle;
- if there are no roads leading from starting castle;
</p>

<p>
<h4>Subjective restarts </h4>
Players can take either 2*111 or 1*112 restart.
Restart after the first meeting until 115-116-117 inclusively for cobblestone-gravel-dirt roads correspondingly. If both players agree to continue then the game goes on.
Restart by the strong block, if it is impossible to go round this block and it divides the castle from the main part of the starting zone, leaving no opportunity for development. It is taken until 112 inclusively.
The strength of the block is evaluated in the first instance by the player declaring about the block, then by his opponent, then, in the case of an argument between players, by the judge.
If player has subjective restarts remaining, he spends subjective restart of his choice.
If all subjective restarts are spent the player has a right to demand an additional restart.
</p>

<p>
It is forbidden to use cheats, map-hacking and watching saves before the end of the game.
 Game version is Shadow of Death 3.2 or Complete version 4.0. HD mod is allowed. It is allowed to play other Heroes 3 versions if both players agree.
</p>

<p>
<h4>Templates features:</h4>
2sm4d(3) - 2 players, L+U size, 160%
Balance - 2 players, L-U size, 160%
Jebus Cross - 2 players, XL-U size, 160%. Restart if player have the only road to the desert zone (if player wish). Restart if player hasn't road to the desert zone (if player wish). In case if map has bugged pass between zones, it is allowed to use it only if guards between these zones and desert zone were eliminated.
8mm6a and 8xm8 - 2 players + 6 computers, XL+U size, 160%. Restart if roads are dirt. It is allowed not to fight misplaced zone's guards.
</p>
<h1>Přihlášky</h1>
<p>
Přihlašovat se můžete u Tomise na icq 258-431-157 nebo na mailu liga@heroes3.cz (mail je spolehlivější).
<br/>Před přihlášením si přečtěte pravidla!
</p>
<h2>Přihlášení hráči</h2>
Imposieble<br/>
KekelZpekel<br/>
Tomis<br/>
<p>
<h1>Průběh</h1>
odkaz na stránku turnaje: <a href=\"http://h3cup.net\">http://h3cup.net/ </a> <br/>
pavouk: <a href=\"http://h3cup.net/worldcup2012.htm\">http://h3cup.net/worldcup2012.htm </a>
</p>
<img style=\"\" alt=\"\" src=\"3musketyri.jpg\" />";
echo "</div>";

ramecek_end(1024);



html_end();

?>
