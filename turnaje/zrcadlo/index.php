<?php


require('../../_common_start.php');

$TITLE = 'Turnaje';
$MODUL = 'turnaje';

html_start();

ramecek_start(1024, 'Zrcadlo, zrcadlo, řekni mi...');

echo "<div style=\"margin-left: 10px;\">";

echo "

&nbsp;
	<h1>Pravidla</h1>

      <h4>Systém turnaje</h4> 

      <div>
    <ul>
     <li>Přihlášení hráči budou rozlosování do pavouka v systému challonge dle pevně daného nasazení.</li>
     <li>Přihlášení hráči před začátkem turnaje seřadí všechny účastníky včetně sebe podle toho, jak se domnívají, že jsou hráči dobří.
	 Seznam hráčů odešlou neveřejně organizátorovi.<br>
	 Po zprůměrování všech seznamů (obodování hráčů) se určí finální nasazení do turnaje a následně se z důvodu transparentnosti jednotlivá hlasování hráčů zveřejní.
     <li>Pavouk ve formátu double elimination se hraje na 1 vítěznou mapu, pokud se hráči v daném kole nedomluví jinak.</li>
     <li>Finále losers bracket a velké finále se hraje na 2 vítězné zápasy.</li>
	 <li>Ve velkém finále postupující z winners bracketu vede 1:0. Postupující z losers bracket vždy při výběru měst a hrdinů volí, zda chce být hráčem A nebo B. Hráči se mohou domluvit na hře na 3 vítězné mapy.</li>
    </ul>
	 </div>
    
    <h4>Povinná pravidla</h4>

    <div>	
    <ul>
     <li>Hraje se zrcadlový template nahodné mapy mt_Dom. Ke stažení v sekci odkazy, v balíčku „Templaty náhodných map pro WorldCup 2018“.</li>
     <li>Odehrané hry se ukládají bez zbytečného odkladu po jejich ukončení s vyznačením textu „Zrcadlo, zrcadlo, řekni mi...“ do komentáře. Soupeř je povinen hru potvrdit bez zbytečného odkladu.</li>
	 </ul>
	 </div>

    <h4>Nepovinná pravidla platící, pokud se hráči výslovně nedohodnou jinak</h4>
    <div>

    Nastavení hry:
    <ul>
     <li>Hraje se datadisk Horn of the Abyss ve své aktuální verzi s HD+ modem s aktivovanými simultánními tahy.</li>
     <li>Časový limit: 6 minut.</li>
     <li>Mapa: random, M s podzemím, voda žádná, neutrálové strong, povoleny všechny cesty.</li>
     <li>Obtížnost: expert (160%)</li>
     <li>Tournament rules - vypnuto.</li>
	 <li>Hráči mohou kdykoliv požádat o přerušení simultánních tahů. V tom případě hráči dokončí aktuální tah, v kterém po načtení hry není dovoleno cokoli měnit. Výjimkou je uplatnění následujícího pravidla.</li>
	 <li>V případě, že modrý hráč neměl v simultánním tahu možnost reagovat na tah červeného, může požádat o restart svého dne.
	 Při restartu dne může modrý hráč změnit jen tu část svého tahu, která bezprostředně souvisí s jeho reakcí na tah červeného hráče.</li>
     <li>Před spuštěním hry si mohou hráči nastavit heslo, aby nedošlo k možnému zneužití savů před ukončením hry. Po hře je hráč povinen na požádání své heslo poskytnout rozhodčímu turnaje z důvodu případné kontroly fair play.</li>
	 </ul>

    Výběr měst a hrdinů:
    <ul>	 
     <li>Níže nasazený hráč zvolí, zda chce být při výběru měst a hrdinů hráčem A nebo hráčem B.</li>
     <li>Hráč B vybere barvu a rozhodne, zda jako hlavní hrdina může být použit tzv. „specialista“. Specialistou se rozumí hrdina se specializací na schopnosti Logistics, Offense, Armorer nebo útočné kouzlo 4. stupně kromě Inferna. Hlavním hrdinou se rozumí hrdina, který se účastní většiny bitev, má po většinu hry nejvyšší level a při útoku na soupeře využívá většinu hráčovy armády.
	 V případě sporu rozhodne rozhodčí a neplatí zásada <i>in dubio pro reo</i>, tak se nesnažte toto pravidlo obejít, protože všichni víme, co je to hlavní hrdina. :)</li>
     <li>Hráč A zvolí město, které budou hrát oba hráči, vybere startovní bonus a vybere dvojici startovních hrdinů. V případě zákazu specialistů je hráč A nevolí ani jako startovní hrdiny.<br></li>
     <li>Hráč B z vybrané dvojice hrdinů rozhodne, koho z nich si který hráč vezme.</li>
     <li><i>Příklad: Níže nasazený hráč si zvolí, že chce být hráčem A. Výše nasazený hráč volí červenou barvu a zakazuje specialisty. Níže nasazený hráč vybírá Castle a dvojici hrdinů Tyris a Loynis. Výše nasazený hráč volí Loynise jako svého hrdinu.</i></li>
    </ul>
	 
    Přerušení hry:
    <ul>
     <li>V případě, že je nutné hru přerušit, hráč pokud možno kompletně dokončí svůj tah.</li>
     <li>Za splnění výše uvedené podmínky hráči načítají již dokončený tah (např. autosave), v kterém není dovoleno cokoli měnit.</li>        
     <li>V případě náhlého odchodu hráče ze hry, např. vlivem technické chyby, či přerušení připojení k internetu, je tento hráč na požádání soupeře povinen ihned zaslat savy rozhodčímu, včetně aktuálního save „battle.gm2.“ 
	 Za tímto účelem je třeba mít v HD_Launcheru zaškrtnuto „save before each battle“ a „save all days (for tournaments)“</li> 
    </ul>
	 
    Omezení hry:
    <ul>
     <li>Pravidlo monsters (Fight Misplaced Guards) na předměty i cesty.</li>
     <li>Hráč nesmí pomocí diplomacie přijmout žádné neutrální jednotky do své armády.</li>        
     <li>Je zakázáno postavit svatostánek grálu.</li>
	 <li>Kouzlo Dimension Door je možno užít pouze 1x za den.</li>
     <li>Je zakázáno provádět hit and run s útočnými kouzly 4. a 5. stupně vyjma kouzla Inferno. Hit and runem se rozumí situace, kdy hráč zaútočí na soupeře, v prvním kole bitvy užije útočné kouzlo a v témže kole uteče, anebo se vykoupí z boje.</li>
	 </ul>
      
    </div>

      <h4>Reklamace mapy</h4>
    <div>
    <ul>
     <li>Restart mapy je možný pouze, pokud s tím oba hráči výslovně souhlasí.</li>
	 </div>
    
      <h4>Kontumace neaktivních hráčů</h4>
      
    <div>
    <ul>
	<li>Každý zápas je třeba odehrát ve lhůtě 1 měsíce ode dne, kdy bylo hráči známo, kdo je jeho soupeř.</li>
     <li>V případě že je jeden hráč aktivní a jeho soupeř není k sehnání, udělí se kontumační prohra neaktivnímu hráči.</li>
     <li>Po první kontumaci v horní části pavouka (winners bracket) se hráč přesouvá do spodní části pavouka (losers bracket), odkud může pokračovat.</li>	
    </ul>
	 </div>  

	<h1>Ceny turnaje</h1>

      <div>
    <ul>
	<li>Ceny budou takové, jaké si je uděláme, kdokoli může přispět na prizepool.</li>
	<li>Donátoři:<br>
	Liut - 200 Kč (uhrazeno)</br>
	Acid - 200 Kč</br>
	Pagi - 200 Kč (uhrazeno)</br>
	Skalimse - 200 Kč (uhrazeno)</br>
	Imposieble - 200 Kč (uhrazeno 8 EUR, odečteno -100 Kč za zahraniční platbu, dorovnáno +100 Kč H34D)</br>
	Gabo+Siska96 - 1000 Kč (uhrazeno)</li>
    </ul>
      </div> 
	
	<h1>Přihlášky</h1>

    <ul>
     <li>Přihlášky pište do diskuse, sekce turnaje na webu ligy, anebo organizátorovi turnaje (H34D) na Skype, či e-mail.<br/>
     <li>Pro přihlášení do turnaje musíte být registrovaní v lize.</li>
     <Brázdimská 1939 li>Konec přihlášek je stanoven na 15.07.2018 23:59.</li>
    </ul>
	 
      <table style=\"border-spacing: 15pt;\"><tr>
      <td valign=\"top\">
      <h3>Přihlášení hráči hlavního turnaje</h3>
      
      <p>
        1. <a href=http://heroes3.cz/hraci/detail.php?id=222><font color=#ff6633>H34D</font></a><br/>
		2. <a href=http://heroes3.cz/hraci/detail.php?id=5149><font color=#ff6633>Liut</font></a><br/>
		3. <a href=http://heroes3.cz/hraci/detail.php?id=5316><font color=#ff6633>Ssgsd</font></a><br/>
 		4. <a href=http://heroes3.cz/hraci/detail.php?id=343><font color=#ff6633>DT.Azgard</font></a><br/>
 		5. <a href=http://heroes3.cz/hraci/detail.php?id=5307><font color=#ff6633>Naoblaku</font></a><br/>
		6. <a href=http://heroes3.cz/hraci/detail.php?id=3486><font color=#ff6633>Houhou</font></a><br/>
		7. <a href=http://heroes3.cz/hraci/detail.php?id=3473><font color=#ff6633>Skalimse</font></a><br/>
		8. <a href=http://heroes3.cz/hraci/detail.php?id=5320><font color=#ff6633>Siska96</font></a><br/>
		9. <a href=http://heroes3.cz/hraci/detail.php?id=5322><font color=#ff6633>gabo</font></a><br/>
		10. <a href=http://heroes3.cz/hraci/detail.php?id=5321><font color=#ff6633>Lord Slayer</font></a><br/>
		11. <a href=http://heroes3.cz/hraci/detail.php?id=706><font color=#ff6633>Acid</font></a><br/>
		12. <a href=http://heroes3.cz/hraci/detail.php?id=823><font color=#ff6633>Karcma</font></a><br/>
		13. <a href=http://heroes3.cz/hraci/detail.php?id=519><font color=#ff6633>Pagi</font></a><br/>
		14. <a href=http://heroes3.cz/hraci/detail.php?id=5394><font color=#ff6633>Weedman</font></a><br/>
		15. <a href=http://heroes3.cz/hraci/detail.php?id=836><font color=#ff6633>Imposieble</font></a><br/>
		16. <a href=http://heroes3.cz/hraci/detail.php?id=46><font color=#ff6633>Twister</font></a><br/>
		17. <a href=http://heroes3.cz/hraci/detail.php?id=75><font color=#ff6633>seimour</font></a><br/>
		18. <a href=http://heroes3.cz/hraci/detail.php?id=5224><font color=#ff6633>Madafaka</font></a><br/>
		19. <a href=http://heroes3.cz/hraci/detail.php?id=105><font color=#ff6633>Jirkahron</font></a><br/>
		20. <a href=http://heroes3.cz/hraci/detail.php?id=3775><font color=#ff6633>Ester</font></a><br/>
		21. <a href=http://heroes3.cz/hraci/detail.php?id=2762><font color=#ff6633>maGe</font></a><br/>
		22. <a href=http://heroes3.cz/hraci/detail.php?id=5361><font color=#ff6633>BeerWarrior</font></a><br/>
		23. <a href=http://heroes3.cz/hraci/detail.php?id=2><font color=#ff6633>Lord.Alex</font></a><br/>
		24. <a href=http://heroes3.cz/hraci/detail.php?id=5131><font color=#ff6633>Pepix</font></a><br/>
		
		</p>
      
      </td>
      
      <td valign=\"top\">
      <h3>Nasazení do pavouka dle hlasování hráčů:</h3>
        1. <a href=http://heroes3.cz/hraci/detail.php?id=222><font color=#ff6633>H34D</font></a> (515)<br/> 
		2. <a href=http://heroes3.cz/hraci/detail.php?id=2762><font color=#ff6633>maGe</font></a> (415)<br/> 
		3. <a href=http://heroes3.cz/hraci/detail.php?id=706><font color=#ff6633>Acid</font></a> (396)<br/>
		4. <a href=http://heroes3.cz/hraci/detail.php?id=836><font color=#ff6633>Imposieble</font></a> (352)<br/>
		5. <a href=http://heroes3.cz/hraci/detail.php?id=823><font color=#ff6633>Karcma</font></a> (338)<br/>
		6. <a href=http://heroes3.cz/hraci/detail.php?id=3486><font color=#ff6633>Houhou</font></a> (322)<br/>
		7. <a href=http://heroes3.cz/hraci/detail.php?id=5131><font color=#ff6633>Pepix</font></a> (319)<br/>
		8. <a href=http://heroes3.cz/hraci/detail.php?id=3775><font color=#ff6633>Ester</font></a> (310)<br/>
		9. <a href=http://heroes3.cz/hraci/detail.php?id=343><font color=#ff6633>DT.Azgard</font></a> (282)<br/>
		10. <a href=http://heroes3.cz/hraci/detail.php?id=46><font color=#ff6633>Twister</font></a> (281)<br/>
		11. <a href=http://heroes3.cz/hraci/detail.php?id=519><font color=#ff6633>Pagi</font></a> (262)<br/>
		12. <a href=http://heroes3.cz/hraci/detail.php?id=5361><font color=#ff6633>BeerWarrior</font></a> (260)<br/>
		13. <a href=http://heroes3.cz/hraci/detail.php?id=2><font color=#ff6633>Lord.Alex</font></a> (253)<br/>
		14. <a href=http://heroes3.cz/hraci/detail.php?id=5316><font color=#ff6633>Ssgsd</font></a> (224)<br/>
		15. <a href=http://heroes3.cz/hraci/detail.php?id=5224><font color=#ff6633>Madafaka</font></a> (218)<br/>
 		16. <a href=http://heroes3.cz/hraci/detail.php?id=5307><font color=#ff6633>Naoblaku</font></a> (191)<br/>
		17. <a href=http://heroes3.cz/hraci/detail.php?id=5320><font color=#ff6633>Siska96</font></a> (176)<br/>
		18. <a href=http://heroes3.cz/hraci/detail.php?id=3473><font color=#ff6633>Skalimse</font></a> (162)<br/>
		19. <a href=http://heroes3.cz/hraci/detail.php?id=5322><font color=#ff6633>gabo</font></a> (159)<br/>
		20. <a href=http://heroes3.cz/hraci/detail.php?id=5321><font color=#ff6633>Lord Slayer</font></a> (128)<br/>
		21. <a href=http://heroes3.cz/hraci/detail.php?id=5149><font color=#ff6633>Liut</font></a> (127)<br/>
		22. <a href=http://heroes3.cz/hraci/detail.php?id=105><font color=#ff6633>Jirkahron</font></a> (117)<br/>
		23. <a href=http://heroes3.cz/hraci/detail.php?id=75><font color=#ff6633>seimour</font></a> (112)<br/>
		24. <a href=http://heroes3.cz/hraci/detail.php?id=5394><font color=#ff6633>Weedman</font></a> (71)<br/>
      </td> 
      
      <td valign=\"top\">
      <h5></h5>
              
      </td>       
      
      </tr></table>

";

echo "

Odkaz na pavouka v systému challonge: <a href=https://challonge.com/nr9s3no0>https://challonge.com/nr9s3no0</a><br/>
Odkaz na tabulku hlasování hráčů: <a href=".$ROOT_URL."/turnaje/zrcadlo/nasazeni.xls><font color=#ff6633>Tabulka nasazení</font></a>

<center><font size=3><h3>Velké finále</h3>
<table border=\"1\">
  <tbody>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">H34D</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">H34D<br>1:0, <a href=\"".$ROOT_URL."/hry/?detail=16891\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16893\"><font color=#ff6633>2:1</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">BeerWarrior</td>
    </tr>
  </tbody>
</table></font></center>

<center><h4>Winners bracket</h4></center>
<table border=\"1\">
  <tbody>
    <tr>
      <th style=\"color: rgb(255, 112, 0);\">1. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">2. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">3. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">4. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">5. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">Vítěz WB</th>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">H34D</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">H34D<br>
      </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">H34D<br><a href=\"".$ROOT_URL."/hry/?detail=16697\"><font color=#ff6633>1:0</font></a><br>
      </td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\">H34D<br><a href=\"".$ROOT_URL."/hry/?detail=16746\"><font color=#ff6633>1:0</font></a><br>
      </td>
      <td rowspan=\"16\" style=\"width: 100pt;\" align=\"center\">H34D<br><a href=\"".$ROOT_URL."/hry/?detail=16781\"><font color=#ff6633>1:0</font></a><br>
      </td>
      <td rowspan=\"32\" style=\"width: 100pt;\" align=\"center\">H34D<br><a href=\"".$ROOT_URL."/hry/?detail=16838\"><font color=#ff6633>1:0</font></a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i> - free win - </i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Naoblaku</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Siska96<br><a href=\"".$ROOT_URL."/hry/?detail=16574\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16576\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16581\"><font color=#ff6633>1:2</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Siska96</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Ester</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Ester<br>
  </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Ester<br><font color=#ff6633>kontumace</font><br>
      </td>
	  </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i> - free win - </i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">DT.Azgard</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">DT.Azgard<br><font color=#ff6633>kontumace</font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Weedman</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Imposieble</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Imposieble<br></td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Imposieble<br><a href=\"".$ROOT_URL."/hry/?detail=16621\"><font color=#ff6633>1:0</font></a><br></td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\">Imposieble<br><a href=\"".$ROOT_URL."/hry/?detail=16668\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16765\"><font color=#ff6633>2:0</font></a><br></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i> - free win - </i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Lord.Alex</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Lord.Alex<br><a href=\"".$ROOT_URL."/hry/?detail=16539\"><font color=#ff6633>1:0</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Lord Slayer</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Karcma</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Karcma<br></td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Karcma<br><a href=\"".$ROOT_URL."/hry/?detail=16618\"><font color=#ff6633>1:0</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i> - free win - </i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">BeerWarrior</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">BeerWarrior<br><a href=\"".$ROOT_URL."/hry/?detail=16538\"><font color=#ff6633>1:0</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Liut</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">maGe</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">maGe<br>
      </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Madafaka<br><a href=\"".$ROOT_URL."/hry/?detail=16565\"><font color=#ff6633>0:1</font></a><br>
      </td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\">Madafaka<br><a href=\"".$ROOT_URL."/hry/?detail=16709\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16747\"><font color=#ff6633>2:0</font></a>
      </td>
      <td rowspan=\"16\" style=\"width: 100pt;\" align=\"center\">Acid<br><a href=\"".$ROOT_URL."/hry/?detail=16822\"><font color=#ff6633>0:1</font></a><br>
      </td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i> - free win - </i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Madafaka</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Madafaka<br><a href=\"".$ROOT_URL."/hry/?detail=16544\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16556\"><font color=#ff6633>2:0</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Skalimse</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Pepix</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Pepix<br>
  </td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Twister<br><font color=#ff6633>kontumace</font><br>
      </td>
	  </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i> - free win - </i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Twister</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Twister<br><a href=\"".$ROOT_URL."/hry/?detail=16567\"><font color=#ff6633>1:0</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Seimour</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Acid</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Acid<br></td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Acid<br><a href=\"".$ROOT_URL."/hry/?detail=16622\"><font color=#ff6633>1:0</font></a><br></td>
      <td rowspan=\"8\" style=\"width: 100pt;\" align=\"center\">Acid<br><a href=\"".$ROOT_URL."/hry/?detail=16686\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16713\"><font color=#ff6633>2:0</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i> - free win - </i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Ssgsd</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Gabo<br><a href=\"".$ROOT_URL."/hry/?detail=16575\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16577\"><font color=#ff6633>0:2</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Gabo</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Houhou</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Houhou<br></td>
      <td rowspan=\"4\" style=\"width: 100pt;\" align=\"center\">Houhou<br><a href=\"".$ROOT_URL."/hry/?detail=16588\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16591\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16593\"><font color=#ff6633>2:1</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\"><i> - free win - </i></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Pagi</td>
      <td rowspan=\"2\" style=\"width: 100pt;\" align=\"center\">Jirkahron<br><a href=\"".$ROOT_URL."/hry/?detail=16585\"><font color=#ff6633>0:1</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 100pt;\" align=\"center\">Jirkahron</td>
    </tr>
  </tbody>
</table>
<br/><br/>
<center><h4>Losers Bracket</h4></center>
<table border=\"1\">
  <tbody>
    <tr>
      <th style=\"color: rgb(255, 112, 0);\">1. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">2. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">3. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">4. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">5. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">6. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">7. kolo</th>
      <th style=\"color: rgb(255, 112, 0);\">finále LB</th>
      <th style=\"color: rgb(255, 112, 0);\">vítěz LB</th>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i> - free win - </i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Jirkahron<br>
		</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Jirkahron<br><a href=\"".$ROOT_URL."/hry/?detail=16730\"><font color=#ff6633>1:0</font></a></td>
      
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Karcma</td>
      
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Gabo<br><a href=\"".$ROOT_URL."/hry/?detail=16802\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16806\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16837\"><font color=#ff6633>1:2</font></a></a></td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Madafaka</td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\">Gabo<br><a href=\"".$ROOT_URL."/hry/?detail=16860\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16863\"><font color=#ff6633>0:2</font></a></td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\">Acid</td>
      <td rowspan=\"16\" style=\"width: 70pt;\" align=\"center\">BeerWarrior</br><font color=#ff6633>kontumace</font></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Naoblaku</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Naoblaku</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i> - free win - </i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Gabo<br>
		</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Gabo<br><font color=#ff6633>kontumace</font></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Gabo<br><a href=\"".$ROOT_URL."/hry/?detail=16757\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16793\"><font color=#ff6633>0:2</font></a></tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Weedman</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Weedman</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i> - free win - </i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Pepix</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Lord Slayer<br><font color=#ff6633>kontumace</font></td>
    <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Ester<br>
		</td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Ester<br><a href=\"".$ROOT_URL."/hry/?detail=16821\"><font color=#ff6633>1:0</font></a></td>
<td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Gabo<br><a href=\"".$ROOT_URL."/hry/?detail=16844\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16846\"><font color=#ff6633>2:0</font></a></td>
      <span
 style=\"color: rgb(255, 102, 0);\"></span></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Lord Slayer</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Lord Slayer</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i> - free win - </i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">maGe</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">maGe<br><a href=\"".$ROOT_URL."/hry/?detail=16605\"><font color=#ff6633>1:0</font></a></td>
        <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">maGe<br><a href=\"".$ROOT_URL."/hry/?detail=16638\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16650\"><font color=#ff6633>0:2</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Liut</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Liut</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i> - free win - </i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">BeerWarrior<br>
		</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">BeerWarrior<br><a href=\"".$ROOT_URL."/hry/?detail=16643\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16683\"><font color=#ff6633>2:0</font></a></td>
      
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Houhou</td>
      
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">BeerWarrior<br><a href=\"".$ROOT_URL."/hry/?detail=16773\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16771\"><font color=#ff6633>2:0</font></a></td>
       <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Imposieble</td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\">BeerWarrior<br><font color=#ff6633>kontumace</font></td>
      <td rowspan=\"8\" style=\"width: 70pt;\" align=\"center\">BeerWarrior<br><a href=\"".$ROOT_URL."/hry/?detail=16872\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16875\"><font color=#ff6633>0:2</font></a></td>		
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Skalimse</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Skalimse</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i> - free win - </i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Lord.Alex<br>
		</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Lord.Alex<br><font color=#ff6633>kontumace</font></td>
        <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">BeerWarrior<br><a href=\"".$ROOT_URL."/hry/?detail=16694\"><font color=#ff6633>1:0</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Seimour</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Seimour</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i> - free win - </i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">DT.Azgard<br>
		</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">DT.Azgard<br><a href=\"".$ROOT_URL."/hry/?detail=16774\"><font color=#ff6633>1:0</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16775\"><font color=#ff6633>2:0</font></a></td>
    <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Twister<br>
		</td>
      <td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">Siska96<br><a href=\"".$ROOT_URL."/hry/?detail=16795\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16784\"><font color=#ff6633>0:2</font></a><br>
		</td>
<td rowspan=\"4\" style=\"width: 70pt;\" align=\"center\">BeerWarrior<br><a href=\"".$ROOT_URL."/hry/?detail=16812\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16833\"><font color=#ff6633>1:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16843\"><font color=#ff6633>2:1</font></a></td>
      <span
 style=\"color: rgb(255, 102, 0);\"></span></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Ssgsd</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Ssgsd</td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\"><i> - free win - </i></td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Siska96</td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Siska96<br><a href=\"".$ROOT_URL."/hry/?detail=16724\"><font color=#ff6633>1:0</font></a></td>
      <td rowspan=\"2\" style=\"width: 70pt;\" align=\"center\">Siska96<br><a href=\"".$ROOT_URL."/hry/?detail=16783\"><font color=#ff6633>0:1</font></a>, <a href=\"".$ROOT_URL."/hry/?detail=16805\"><font color=#ff6633>0:2</font></a></td>
    </tr>
    <tr>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Pagi</td>
      <td rowspan=\"1\" style=\"width: 70pt;\" align=\"center\">Pagi</td>
    </tr>
	
  </tbody>
</table>

"; 
	  
echo "</div>";

ramecek_end(1024);



html_end();

?>
