<?php

include('_common_start.php');

$TITLE = 'Zapomenuté heslo';

html_start();

ramecek_start(1024, 'Zapomenuté heslo');

$errors = "";
$deadline = Date("Y-m-d h:i:s", time()-86400);

$h = sql_one_row(sprintf("SELECT * FROM hraci WHERE id=%d",$_GET['hrac']));

echo "<div style=\"margin-left: 50px;\">";

table_start('', $_SERVER['PHP_SELF'].'?hrac='.$_GET['hrac']);
# kontrola, ze uzivatel ma email
if(empty($h['email'])) {
	echo "Nemáte v profilu uložený email, takže není možné vám obnovit heslo. Kontaktujte administrátora.";
} else {
	# Kontrola, ze uzivatel jiz nema reset token
	if(!empty($h['reset_token']) && strtotime($h['posledni_prihlaseni']) > strtotime($deadline)) {
		echo "Email pro obnovu hesla již byl odeslán. Pokračujte podle instrukcí v emailu.";
	} else {
		if($_SERVER['REQUEST_METHOD']=='POST' && isset($_POST['submit'])) {
			$token = hash('sha256', mt_rand(1, 1000000000000) * microtime(true));
			$sql = sql_query(sprintf("UPDATE hraci SET reset_token='%s', posledni_prihlaseni=DEFAULT WHERE id=%d", $token, $_GET['hrac']));
			if($sql) {
      				$predmet = "Liga Heroes 3: obnovení hesla";
      				$telo = "Dobrý den,\n\npro obnovení vašeho hesla klikněte na odkaz uvedený níže.\nBudete přihlášení pod svůj uživatelský účet.\nNásledně kliknutím na svoje jméno otevřete svůj profil, klikněte na upravit profil a změňte si heslo.\n\nPlatnost odkazu je 24 hodin a lze jej použít pouze jednou.\n\n".$ROOT_URL."/obnoveni_hesla.php?hrac=".$_GET['hrac'].'&reset_token='.$token."\n\nO obnovu hesla jste požádali z I.P. adresy ".$_SERVER['HTTP_X_FORWARDED_FOR']." / ".get_cient_ip()."\n\nS pozdravem,\nČeská Liga Heroes of Might and Magic III\n";
				if(send_email($h['email'], $predmet, $telo)) {	
					echo "Email byl úspěšně odeslán.";
				} else {
					echo "Nastala chyba při odesílání emailu.";
				}
			}

		} else {
			echo "Přejete si vygenerovat odkaz pro obnovení hesla a odeslat ho na váš email ".$h['email']."?<br/><br/><br/>";
			echo input_submit(' Ano, chci obnovit heslo', 'submit');
		}


	}
}

table_end();

echo "</div>";



ramecek_end(1024);


html_end();

?>
